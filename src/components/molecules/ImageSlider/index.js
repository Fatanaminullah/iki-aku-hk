import React, {Component} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import {Touchable, Gap, SkeletonLoading, SvgWrapper} from '../../atoms';
import Modal from 'react-native-modal';
import {WebView} from 'react-native-webview';
import I18n from 'react-native-redux-i18n';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {colors, fonts, hp, wp} from '../../../utilities';
import ViewPager from '@react-native-community/viewpager';
import {LeftArrowIcon} from '../../../assets';

export class ImageSlider extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalShow: true,
      newsTimer: 0,
      activePage_news: 0,
      news: [],
    };
  }

  componentDidMount = () => {
    this.setState({
      news: this.props.data,
    });
  };

  componentDidUpdate = () => {
    if (this.state.news.length === 0) {
    } else {
      if (this.state.newsTimer === 0) {
        this._newsTimer();
      }
    }
  };

  componentWillUnmount() {
    clearInterval(this._newsInterval);
  }

  _newsTimer = () => {
    this.setState({
      newsTimer: 1,
    });
    this._newsInterval = setInterval(() => {
      this.setState({
        activePage_news: this.state.activePage_news + 1,
      });
      this.viewPager.setPage(this.state.activePage_news);
      if (this.state.activePage_news === this.state.news.length) {
        this._resetNews();
      }
    }, 4000);
  };

  _resetNews = () => {
    clearInterval(this._newsInterval);
    this.setState({
      activePage_news: 0,
    });
    this.viewPager.setPage(0);
    this._newsTimer();
  };

  _pauseNews = () => {
    clearInterval(this._newsInterval);
  };

  render() {
    const disableZoomMetaInject = `const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `;

    return (
      <View>
        <View
          style={{
            borderBottomColor: 'rgba(201, 201, 201, .5)',
            borderBottomWidth: 3,
          }}
        />
        {this.state.news.length === 0 ? (
          <View>
            <SkeletonLoading width={wp(100)} height={hp(22)} type="rectangle" />
            <Gap height={hp(1.9)} />
          </View>
        ) : (
          <ViewPager
            ref={(viewPager) => {
              this.viewPager = viewPager;
            }}
            style={styles.viewPager}
            initialPage={0}
            showPageIndicator={true}
            onPageSelected={(e) =>
              this.setState({activePage_news: e.nativeEvent.position})
            }>
            {this.state.news.map((item, i) => {
              return (
                <View
                  key={i}
                  onTouchStart={() => this._pauseNews()}
                  onTouchCancel={() => this._newsTimer()}
                  style={styles.newsItemContainer}>
                  <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {
                      this._pauseNews();
                      this.setState({
                        modalShow: i,
                      });
                    }}
                    onLongPress={() => null}
                    delayLongPress={500}>
                    <Image source={{uri: item.img}} style={styles.image} />
                  </TouchableOpacity>
                  <Modal
                    animationType={'slide'}
                    transparent={false}
                    visible={this.state.modalShow === i}
                    style={styles.modal}
                    onBackButtonPress={() => this.setState({modalShow: -1})}>
                    <View style={styles.modalHeader}>
                      <Touchable
                        onPress={() => {
                          this._newsTimer();
                          this.setState({modalShow: -1});
                        }}>
                        <SvgWrapper
                          component={<LeftArrowIcon />}
                          width={wp(5)}
                          height={wp(5)}
                        />
                      </Touchable>
                      <Gap width={16} />
                      <Text style={styles.modalTitle}>
                        {I18n.t('homeScreen.news')}
                      </Text>
                    </View>
                    <View style={styles.modalContentContainer}>
                      <View style={styles.modalContentHeader}>
                        <Image
                          style={styles.modalImage}
                          source={{uri: item.img}}
                        />
                        <Text style={styles.modalNewsTitle}>{item.title}</Text>
                      </View>
                      <View style={styles.modalNewsDescription}>
                        <WebView
                          source={{html: item.description}}
                          textZoom={100}
                          containerStyle={{}}
                          originWhitelist={['file://']}
                          injectedJavaScript={disableZoomMetaInject}
                        />
                      </View>
                    </View>
                  </Modal>
                </View>
              );
            })}
          </ViewPager>
        )}
        <View style={styles.newsCounterContainer}>
          {this.state.news.length > 0 ? (
            this.state.news.map((item, i) => {
              return (
                <View
                  key={i}
                  style={[
                    styles.newsCounter,
                    {
                      borderColor:
                        this.state.activePage_news === i
                          ? 'rgba(0, 54, 82, 0.7)'
                          : 'rgba(122, 211, 255, 0.8)',
                    },
                  ]}
                />
              );
            })
          ) : (
            <View></View>
          )}
        </View>
        <View
          style={{
            borderBottomColor: 'rgba(201, 201, 201, .5)',
            borderBottomWidth: 3,
            marginTop: hp(2),
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewPager: {
    backgroundColor: colors.white,
    height: hp(22),
  },
  newsCounterContainer: {
    marginTop: hp(-3),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  newsCounter: {
    width: 7,
    height: 7,
    marginHorizontal: 7,
    backgroundColor: colors.white,
    borderWidth: 2,
    borderRadius: 30,
  },
  image: {
    resizeMode: 'stretch',
    height: hp(22),
    width: wp(100),
    borderWidth: 0.3,
    borderColor: colors.grey2,
  },
  newsItemContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentLoader: {
    flex: 1,
    height: hp(22),
    width: wp(100),
    marginBottom: hp(1),
    alignSelf: 'center',
  },
  modal: {
    alignItems: undefined,
    justifyContent: undefined,
  },
  modalHeader: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  modalTitle: {
    fontFamily: fonts.primary.bold,
    color: colors.black,
    fontSize: fonts.size[20],
  },
  modalContentContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: 10,
  },
  modalContentHeader: {
    marginTop: 10,
    justifyContent: 'space-between',
    marginBottom: 14,
  },
  modalImage: {
    resizeMode: 'stretch',
    marginTop: 11,
    marginBottom: 15,
    alignSelf: 'center',
    height: hp(22),
    width: wp(100),
    borderWidth: 0.5,
    borderColor: colors.grey2,
  },
  modalNewsTitle: {
    color: colors.black,
    fontSize: 16,
    fontWeight: 'bold',
  },
  modalNewsDescription: {
    flex: 1,
    marginHorizontal: -wp(2),
  },
});

export default ImageSlider;
