import OTPInputView from '@twotalltotems/react-native-otp-input';
import React from 'react';
import {StyleSheet, View} from 'react-native';
import {colors, fonts, wp} from '../../../utilities';

const OTPInput = ({
  height,
  onCodeFilled,
  code,
  onCodeChanged,
  secureTextEntry,
  pinCount = 6,
  clearInputs,
}) => {
  return (
    <View>
      <OTPInputView
        style={styles.container(height)}
        pinCount={pinCount}
        code={code}
        onCodeChanged={onCodeChanged}
        autoFocusOnLoad={false}
        codeInputFieldStyle={styles.underlineStyleBase}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
        onCodeFilled={onCodeFilled}
        secureTextEntry={secureTextEntry}
        clearInputs={clearInputs}
      />
    </View>
  );
};

export default OTPInput;

const styles = StyleSheet.create({
  container: (height) => ({
    width: wp(80),
    height: height ? height : 49,
    alignSelf: 'center',
  }),
  underlineStyleBase: {
    width: 50,
    height: 50,
    borderWidth: 3,
    fontSize: fonts.size[26],
    color: colors.black,
  },

  underlineStyleHighLighted: {
    borderColor: colors.red2,
  },
});
