import Moment from 'moment';
import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {PinBackground} from '../../assets';
import {
  Button,
  Container,
  Content,
  Gap,
  Header,
  PinDialog,
} from '../../components';
import {showAlertError} from '../../redux/store/actions/alert';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {
  createTransaction,
  setDetailTransaction,
} from '../../redux/store/actions/transaction';
import {colors, fonts} from '../../utilities';

const TransferConfirmationDataScreen = ({navigation, route}) => {
  const {transferData} = useSelector((state) => state.transaction);
  const dispatch = useDispatch();
  const [visiblePin, setVisiblePin] = useState(false);
  const onFinished = (value) => {
    const streamData = {
      senderD: transferData?.sender,
      purposeD: transferData?.purpose,
      sourceD: transferData?.sourceOfFunds,
      receiverD: transferData?.recipient?.fullName,
      payoutD: transferData?.recipient.accountNumber,
      amountD: transferData?.transferAmount,
      receiveD: transferData?.transferredEstimate,
      adminD: transferData?.adminFee,
      totalD: transferData?.totalEstimate,
      discountD: transferData?.discount,
      totalpayD: transferData?.totalPayment,
      kurs: transferData?.kurs,
    };
    const request = {
      dtime: Moment().format('DDMMYYYY'),
      stream: JSON.stringify(streamData),
      reff_type: 'h1iu2h3iu12h31iu2h3iu1as1',
      pay_method: 'CVS',
    };
    dispatch(createTransaction(request, showLoading, dismissLoading))
      .then(async (res) => {
        if (res?.paymentNo && res?.expiredDate) {
          dispatch(
            setDetailTransaction({...res, statusCode: 1}, 'TRANSFER'),
          ).then(() => {
            navigation.navigate('TransactionDetailsScreen');
          });
        } else {
          dispatch(
            showAlertError(
              '',
              I18n.t('transactionHistoryScreen.trxFailedMessage'),
            ),
          );
        }
      })
      .catch((err) => {
        dispatch(
          showAlertError('', err?.response?.data?.message || err.message),
        );
      });
  };
  return (
    <Container>
      <PinDialog
        screen={route?.name}
        visibility={visiblePin}
        setVisibility={setVisiblePin}
        onFinished={onFinished}
        background={PinBackground}
        labelForgot={I18n.t('forgotPinScreen.forgotPin')}
      />
      <Header title={I18n.t('transferConfirmationDataScreen.title')} />
      <Content>
        <View style={styles.flex1}>
          <Text>Ini nanti webview</Text>
        </View>
      </Content>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          style={styles.buttonCancel}
          onPress={() => navigation.goBack()}>
          <Text style={styles.textCancel}>
            {I18n.t('transferConfirmationDataScreen.cancel')}
          </Text>
        </TouchableOpacity>
        <Gap width={20} />
        <View style={styles.flex1}>
          <Button
            onPress={() => setVisiblePin(true)}
            title={I18n.t('transferConfirmationDataScreen.verify')}
            rounded
          />
        </View>
      </View>
    </Container>
  );
};

export default TransferConfirmationDataScreen;

const styles = StyleSheet.create({
  buttonCancel: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  textCancel: {
    color: colors.red1,
    fontFamily: fonts.primary.bold,
  },
  buttonContainer: {
    padding: 20,
    flexDirection: 'row',
  },
  flex1: {
    flex: 1,
  },
});
