import React, {useEffect, useState} from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import Contacts from 'react-native-contacts';
import {useDispatch, useSelector} from 'react-redux';
import {ContactImage} from '../../assets';
import {
  Container,
  ContactItem,
  Gap,
  Header,
  ImageInfo,
  Input,
} from '../../components';
import {alertActions, loadingActions} from '../../redux/store/actions';
import {
  colors,
  compare,
  fonts,
  hp,
  phoneNumberNormalizer,
  requestContactPermission,
  useForm,
} from '../../utilities';
import I18n from '../../utilities/i18n';

const {showLoading, dismissLoading} = loadingActions;
const {showAlertError} = alertActions;

const ListContactScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {contacts} = useSelector((state) => state.profile);
  const [form, setForm] = useForm({input: ''});
  const [listContacts, setListContacts] = useState(contacts);
  const [listContactsTemp, setListContactsTemp] = useState([]);

  useEffect(() => {
    if (contacts.length === 0) {
      getContact();
    }
  }, []);

  const getContact = () => {
    dispatch(showLoading());
    requestContactPermission(dispatch, showAlertError)
      .then((res) => {
        if (res.status === 'granted') {
          Contacts.getAll((err, contacts) => {
            if (err) {
              dispatch(dismissLoading());
              dispatch(
                showAlertError(
                  '',
                  `${I18n.t('listContactScreen.contactNotFound')}`,
                ),
              );
              navigation.goBack();
            } else if (!contacts.length) {
              navigation.goBack();
              dispatch(dismissLoading());
              dispatch(
                showAlertError(
                  '',
                  `${I18n.t('listContactScreen.contactNotFound')}`,
                ),
              );
            } else {
              const tempContacts = contacts
                .filter((item) => {
                  return (
                    item.phoneNumbers.length &&
                    !item?.displayName.includes('+') &&
                    !item?.displayName.includes('.')
                  );
                })
                .sort((a, b) => compare(a, b, 'displayName'));
              const finalContacts = [];
              tempContacts.forEach((item) => {
                if (item.phoneNumbers.length === 1) {
                  finalContacts.push({
                    displayName: item?.displayName,
                    phoneNumber: item.phoneNumbers[0].number,
                    label: item.phoneNumbers[0].number,
                  });
                } else {
                  item.phoneNumbers.forEach((items) => {
                    finalContacts.push({
                      displayName: item?.displayName,
                      phoneNumber: items.number,
                      label: items.number,
                    });
                  });
                }
              });
              setListContacts(finalContacts);
              setListContactsTemp(finalContacts);
              dispatch(dismissLoading());
            }
          });
        } else {
          navigation.goBack();
          dispatch(dismissLoading());
        }
      })
      .catch((err) => {
        navigation.goBack();
        dispatch(dismissLoading());
      });
  };

  const filterContact = (value) => {
    if (value) {
      const arr = contacts.filter((item) => {
        return (
          phoneNumberNormalizer(item.phoneNumber).includes(value) ||
          item?.displayName.toLowerCase().includes(value.toLowerCase())
        );
      });
      setListContacts(arr);
    } else {
      setListContacts(contacts);
    }
  };

  const renderContactItem = ({item, index}) => {
    return (
      <ContactItem
        key={`${item?.displayName}-${index}`}
        item={item}
        text={item?.displayName}
        iconColor={colors[item?.displayName[0].toLowerCase()]}
        phoneNumber={item.phoneNumber}
        onPress={() =>
          navigation.navigate(route.params.screen, {
            setterFunc: route.params.setter ? route.params.setter : null,
            clearErrorsFunc: route.params.clearer ? route.params.clearer : null,
            phoneNumber: phoneNumberNormalizer(item.phoneNumber),
          })
        }
      />
    );
  };

  const onChange = (value) => {
    setForm('input', value);
    filterContact(value);
  };

  return (
    <Container>
      <Header title={I18n.t('listContactScreen.headerTitle')} />
      <View style={styles.content}>
        <Input
          rounded
          placeholder={I18n.t('listContactScreen.inputPlaceHolder')}
          value={form.input}
          onChangeText={(value) => onChange(value)}
        />
        {listContacts.length > 0 && (
          <Text style={styles.listLabel}>
            {I18n.t('listContactScreen.listLabel')}
          </Text>
        )}
        <Gap height={10} />
        <FlatList
          data={listContacts}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
          ListEmptyComponent={
            <ImageInfo
              text={I18n.t('listContactScreen.contactNotFound')}
              source={ContactImage}
              hpSizeImage={22}
              hpSizeText={1.8}
              textStyle={{marginTop: hp(-3)}}
            />
          }
          renderItem={renderContactItem}
          keyExtractor={(item) => item.id}
        />
      </View>
    </Container>
  );
};

export default ListContactScreen;

const styles = StyleSheet.create({
  content: {
    flex: 1,
    paddingHorizontal: 30,
    paddingTop: 15,
  },
  inputContainer: {
    borderWidth: 1,
    borderColor: colors.grey2,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  innerInputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  listLabel: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[12],
    color: colors.grey2,
    paddingTop: 15,
  },
  itemListContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 10,
  },
  itemListIcon: {
    backgroundColor: colors.black,
    width: 45,
    height: 45,
    borderRadius: 100,
  },
  itemListTextContainer: {
    flexDirection: 'column',
    paddingLeft: 10,
    width: '100%',
  },
  itemListLabel: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    color: colors.black,
    paddingBottom: 5,
  },
  itemListContent: {
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.normal,
    color: colors.black,
    paddingBottom: 5,
  },
  horizontalLine: {
    height: 1,
    backgroundColor: colors.grey2,
    width: '100%',
  },
  contactContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});
