import React from 'react';
import I18n from 'react-native-redux-i18n';
import {Image, StyleSheet, Text, View} from 'react-native';
import {
  PPOBPDataIcon,
  PPOBPLNPrepIcon,
  PPOBPulsaIcon,
  ReferralIcon,
  RewardIcon,
  TransferPNGIcon,
} from '../../../assets';
import {colors, fonts, hp, wp} from '../../../utilities';
import {Touchable} from '../../atoms';

const menu = [
  {
    screen: 'PlnPrepaidScreen',
    icon: PPOBPLNPrepIcon,
    label: 'homeScreen.menuPLNPrepaid',
  },
  {
    screen: 'ReferralScreen',
    icon: ReferralIcon,
    label: 'homeScreen.menuReferral',
  },
  {
    screen: 'Transfer',
    icon: TransferPNGIcon,
    label: 'homeScreen.menuTransfer',
  },
  {
    screen: 'MobileTopUpScreen',
    icon: PPOBPulsaIcon,
    label: 'homeScreen.menuPulsa',
  },
  {
    screen: 'DataPackageScreen',
    icon: PPOBPDataIcon,
    label: 'homeScreen.menuPData',
  },
  {
    screen: 'RewardScreen',
    icon: RewardIcon,
    label: 'homeScreen.menuReward',
  },
];

const HomeTrxMenu = ({navigation}) => {
  const renderMenu = () => {
    return menu.map((item, i) => (
      <Touchable
        key={i}
        style={styles.buttonContainer}
        onPress={() => navigation.navigate(item.screen)}>
        <Image style={styles.menuIcon} source={item.icon} />
        <Text style={styles.menuLabel}>{I18n.t(item.label)}</Text>
      </Touchable>
    ));
  };
  return <View style={styles.menuContainer}>{renderMenu()}</View>;
};

export default HomeTrxMenu;

const styles = StyleSheet.create({
  menuContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    marginTop: hp(-2),
  },
  buttonContainer: {
    height: hp(10),
    alignItems: 'center',
    flexBasis: '20%',
    marginVertical: wp(1),
  },
  menuIcon: {
    width: hp(6),
    height: hp(6),
  },
  menuLabel: {
    textAlign: 'center',
    color: colors.black,
    marginTop: hp(1),
    fontFamily: fonts.primary[600],
    fontSize: fonts.size[12],
  },
});
