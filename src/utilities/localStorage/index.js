import RNSecureStorage, {ACCESSIBLE} from 'rn-secure-storage';

export const storeData = (key, value) => {
  return new Promise((resolve, reject) => {
    RNSecureStorage.set(key, JSON.stringify(value), {
      accessible: ACCESSIBLE.WHEN_UNLOCKED,
    })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        resolve(err);
      });
  });
};

export const getData = (key) => {
  return new Promise((resolve, reject) => {
    RNSecureStorage.get(key)
      .then((res) => {
        resolve(JSON.parse(res));
      })
      .catch((err) => {
        resolve(err);
      });
  });
};

export const removeData = (key) => {
  return new Promise((resolve, reject) => {
    RNSecureStorage.remove(key)
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        resolve(err);
      });
  });
};
