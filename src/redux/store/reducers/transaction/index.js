import {transactionTypes} from '../../action-types';
import I18n from 'react-native-redux-i18n';
import Moment from 'moment';
import {
  coupon100Percent,
  coupon20Percent,
  coupon50Percent,
} from './mockCouponImg';

const {
  SET_TRX_SUMMARY,
  SET_TRX_DETAILS,
  SET_TRX_HISTORY_LIST,
  SET_COUPON,
  SET_TRANSFER_DATA,
  SET_RATE_DATA,
  SET_SOURCE_OF_FUNDS,
} = transactionTypes;

const date = Moment(new Date()).add(1, 'h').format('YYYY-MM-DD HH:mm:ss');

const dummyTrxHistoryList = [
  {
    type: 0,
    cardno: 'AD30579531',
    nama: 'WULANDARI',
    b_acc: '018381',
    b_name: 'TEST TRANSFER-003',
    d_amount: '10500',
    d_admin: '149',
    d_discount: '0',
    d_totalpay: '10649',
    d_receive: '5701500',
    d_purpose: 'Uang Belanja',
    d_source: 'Salary',
    d_payout: '018381  -  BRI',
    tglorder: '2020-06-19 14:54:19.411226+07',
    barcode1: null,
    barcode2: null,
    barcode3: null,
    payment_no: 'LLL20171938800',
    trade_no: '494c4d7368e17bc94e05',
    expire: date,
    expire_in_minutes: '-166747',
    status: 0,
  },
  {
    type: 0,
    cardno: 'AD30579531',
    nama: 'WULANDARI',
    b_acc: '018381',
    b_name: 'TEST TRANSFER-003',
    d_amount: '10500',
    d_admin: '149',
    d_discount: '0',
    d_totalpay: '10649',
    d_receive: '5701500',
    d_purpose: 'Uang Belanja',
    d_source: 'Salary',
    d_payout: '018381  -  BRI',
    tglorder: '2020-06-19 14:54:19.411226+07',
    barcode1: null,
    barcode2: null,
    barcode3: null,
    payment_no: 'LLL20171938800',
    trade_no: '494c4d7368e17bc94e05',
    expire: '2020-06-20 15:54:21',
    expire_in_minutes: '-166747',
    status: 1,
  },
  {
    type: 0,
    cardno: 'AD30579531',
    nama: 'WULANDARI',
    b_acc: '018381',
    b_name: 'TEST TRANSFER-003',
    d_amount: '10500',
    d_admin: '149',
    d_discount: '0',
    d_totalpay: '10649',
    d_receive: '5701500',
    d_purpose: 'Uang Belanja',
    d_source: 'Salary',
    d_payout: '018381  -  BRI',
    tglorder: '2020-06-19 14:54:19.411226+07',
    barcode1: null,
    barcode2: null,
    barcode3: null,
    payment_no: 'LLL20171938800',
    trade_no: '494c4d7368e17bc94e05',
    expire: date,
    expire_in_minutes: '-166747',
    status: 3,
  },
  {
    type: 0,
    cardno: 'AD30579531',
    nama: 'WULANDARI',
    b_acc: '018381',
    b_name: 'TEST TRANSFER-003',
    d_amount: '10500',
    d_admin: '149',
    d_discount: '0',
    d_totalpay: '10649',
    d_receive: '5701500',
    d_purpose: 'Uang Belanja',
    d_source: 'Salary',
    d_payout: '018381  -  BRI',
    tglorder: '2020-06-19 14:54:19.411226+07',
    barcode1: null,
    barcode2: null,
    barcode3: null,
    payment_no: 'LLL20171938800',
    trade_no: '494c4d7368e17bc94e05',
    expire: '2020-06-20 15:54:21',
    expire_in_minutes: '-166747',
    status: 99,
  },
  {
    type: 0,
    cardno: 'AD30579531',
    nama: 'WULANDARI',
    b_acc: '018381',
    b_name: 'TEST TRANSFER-003',
    d_amount: '10500',
    d_admin: '149',
    d_discount: '0',
    d_totalpay: '10649',
    d_receive: '5701500',
    d_purpose: 'Uang Belanja',
    d_source: 'Salary',
    d_payout: '018381  -  BRI',
    tglorder: '2020-06-19 14:54:19.411226+07',
    barcode1: null,
    barcode2: null,
    barcode3: null,
    payment_no: 'LLL20171938800',
    trade_no: '494c4d7368e17bc94e05',
    expire: date,
    expire_in_minutes: '-166747',
    status: 1,
  },
];

const initialState = {
  trxSummary: {},
  trxDetails: {},
  trxHistoryList: dummyTrxHistoryList,
  trxHistoryDetail: {},
  rateData: {},
  sourceOfFunds: [],
  purpose: [
    {label: I18n.t('transferScreen.shoppingMoney'), value: 'shoppingMoney'},
    {label: I18n.t('transferScreen.buySnack'), value: 'buySnack'},
  ],
  listCoupons: [
    {
      id: 'coupon1',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 20%',
      amount: 20,
      min: 100,
      max: 200,
      expired: '10-24-2020 23:59:59',
      image: coupon20Percent,
    },
    {
      id: 'coupon2',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 20%',
      amount: 20,
      min: 100,
      max: 200,
      expired: '10-23-2020 23:59:59',
      image: coupon20Percent,
    },
    {
      id: 'coupon3',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 50%',
      amount: 50,
      min: 100,
      max: 200,
      expired: '10-22-2020 23:59:59',
      image: coupon50Percent,
    },
    {
      id: 'coupon4',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 100%',
      amount: 100,
      min: 100,
      max: 200,
      expired: '10-21-2020 23:59:59',
      image: coupon100Percent,
    },
  ],
  selectedCoupon: {},
  transferData: {},
};

export default (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SET_TRX_SUMMARY:
      return {...state, trxSummary: payload};
    case SET_TRX_DETAILS:
      return {...state, trxDetails: payload};
    case SET_TRX_HISTORY_LIST:
      return {...state, trxHistoryList: payload};
    case SET_COUPON:
      return {...state, selectedCoupon: payload};
    case SET_TRANSFER_DATA:
      return {...state, transferData: payload};
    case SET_RATE_DATA:
      return {...state, rateData: payload};
    case SET_SOURCE_OF_FUNDS:
      return {...state, sourceOfFunds: payload};
    default:
      return state;
  }
};
