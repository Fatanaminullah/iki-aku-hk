import React, {useEffect} from 'react';
import I18n from 'react-native-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {IkiAkuIcon1} from '../../assets';
import {
  Button,
  Container,
  Content,
  Field,
  Form,
  Gap,
  Header,
  Input,
  SvgWrapper,
} from '../../components';
import {
  showAlertError,
  showAlertSuccess,
} from '../../redux/store/actions/alert';
import {changePassword} from '../../redux/store/actions/authentication';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {getUserInfo} from '../../redux/store/actions/profile';
import {hp, useForm, validation, wp} from '../../utilities';

const {required, confirmPassword} = validation;

const ChangePasswordScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {arcNumber} = useSelector((state) => state.profile.userDetails);
  useEffect(() => {
    dispatch(getUserInfo());
  }, []);
  const [form, setForm] = useForm({
    currentPassword: '',
    newPassword: '',
    confirmNewPassword: '',
  });
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        type="danger"
        title={I18n.t('changePasswordScreen.submit')}
        onPress={handleSubmit}
        rounded
      />
    );
  };

  const onSubmit = (data, setValue, setError) => {
    const request = {
      username: arcNumber,
      currentPassword: data.currentPassword,
      password: data.newPassword,
    };
    dispatch(
      changePassword(
        request,
        showLoading,
        dismissLoading,
        showAlertSuccess,
        showAlertError,
      ),
    )
      .then((res) => {
        navigation.navigate('MainApp', {screen: 'Others'});
      })
      .catch((err) => {
        if (err?.response?.status === 404) {
          setError('currentPassword', {
            type: 'manual',
            message: err.response?.data?.message,
          });
        }
      });
  };

  return (
    <Container>
      <Header title={I18n.t('changePasswordScreen.title')} separator={false} />
      <Content>
        <SvgWrapper
          component={<IkiAkuIcon1 />}
          width={wp(25)}
          height={wp(25)}
        />
        <Gap height={hp(4)} />
        <Form onSubmit={onSubmit} submitComponent={renderSubmitComponent}>
          <Field name="currentPassword" validation={{required}}>
            <Input
              rounded
              label={I18n.t('changePasswordScreen.oldPassword')}
              onChangeText={(val) => setForm('currentPassword', val)}
              placeholder={I18n.t('changePasswordScreen.oldPassword')}
              secureTextEntry
            />
          </Field>
          <Gap height={hp(3)} />
          <Field name="newPassword" validation={{required}}>
            <Input
              rounded
              label={I18n.t('changePasswordScreen.newPassword')}
              onChangeText={(val) => setForm('newPassword', val)}
              placeholder={I18n.t('changePasswordScreen.newPassword')}
              secureTextEntry
            />
          </Field>
          <Gap height={hp(3)} />
          <Field
            name="confirmNewPassword"
            validation={{
              required,
              validate: (val) => confirmPassword(val, form.newPassword),
            }}>
            <Input
              rounded
              label={I18n.t('changePasswordScreen.confirmNewPassword')}
              onChangeText={(val) => setForm('confirmNewPassword', val)}
              placeholder={I18n.t('changePasswordScreen.confirmNewPassword')}
              secureTextEntry
            />
          </Field>
          <Gap height={hp(4)} />
        </Form>
      </Content>
    </Container>
  );
};

export default ChangePasswordScreen;
