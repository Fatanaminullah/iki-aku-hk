import React, { Component } from 'react';
import { View, Text, Image, ActivityIndicator, Dimensions } from 'react-native';
import { colors, hp, wp } from '../../../utilities'
import Carousel, { Pagination } from 'react-native-snap-carousel';
import styles from './SliderBox.styles';
import { Touchable, NewsModal } from '../../';

const width = Dimensions.get('window').width;

export default class SliderBox extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentImage: 0,
      loading: [],
      interval: 3000
    };
    this.onCurrentImagePressedHandler = this.onCurrentImagePressedHandler.bind(
      this,
    );
    this.onSnap = this.onSnap.bind(this);
  }
  componentDidMount() {
    let a = [...Array(this.props.images.length).keys()].map((i) => false);
  }
  onCurrentImagePressedHandler() {
    if (this.props.onCurrentImagePressed) {
      this.props.onCurrentImagePressed(this.state.currentImage);
    }
  }

  onSnap(index) {
    const { currentImageEmitter } = this.props;
    this.setState({ currentImage: index }, () => {
      if (currentImageEmitter) currentImageEmitter(this.state.currentImage);
    });
  }

  _renderItem = ({ item, index }) => {

    const {
      ImageComponent,
      ImageComponentStyle = {},
      sliderBoxHeight,
      resizeMethod,
      resizeMode,
      imageLoadingColor = colors.red2,
    } = this.props;

    return (
      <View>
        <Touchable
          onPress={() => {
            this.setState({
              modalShow: index,
              interval: undefined
            })

          }}
        >
          <ImageComponent
            style={[
              // eslint-disable-next-line react-native/no-inline-styles
              {
                height: sliderBoxHeight || 200,
                alignSelf: 'center',
              },
              ImageComponentStyle,
            ]}
            source={typeof item.img === 'string' ? { uri: item.img } : item.img}
            resizeMethod={resizeMethod || 'resize'}
            resizeMode={resizeMode || 'cover'}
            onLoad={() => { }}
            onLoadStart={() => { }}
            onLoadEnd={() => {
              let t = this.state.loading;
              t[index] = true;
              this.setState({ loading: t });
            }}
            {...this.props}
          />
          {!this.state.loading[index] && (
            <ActivityIndicator
              size="large"
              color={imageLoadingColor}
              style={styles.activityIndicatorStyle}
            />
          )}
        </Touchable>
        <NewsModal
          trigger={true}
          img={item.img}
          title={item.title}
          description={item.description}
        />
      </View>
    );
  }

  get pagination() {
    const { currentImage } = this.state;
    const {
      images,
      dotStyle,
      dotColor = colors.grey2,
      dotElement,
      inactiveDotColor = colors.white,
      paginationBoxStyle,
    } = this.props;

    return (
      <Pagination
        borderRadius={2}
        dotsLength={images.length}
        activeDotIndex={currentImage}
        dotStyle={dotStyle || styles.dotStyle}
        dotColor={dotColor}
        dotElement={dotElement || null}
        inactiveDotColor={inactiveDotColor}
        inactiveDotScale={0.8}
        carouselRef={this._ref}
        inactiveDotOpacity={0.8}
        tappableDots={!!this._ref}
        containerStyle={[
          styles.paginationBoxStyle,
          paginationBoxStyle && paginationBoxStyle,
        ]}
        {...this.props}
      />
    );
  }

  render() {
    const {
      images,
      circleLoop,
      autoplay,
      autoPlayInterval,
      parentWidth,
      loopClonesPerSide,
    } = this.props;
    return (
      <View>
        <Carousel
          layout={'default'}
          data={images}
          ref={(c) => (this._ref = c)}
          loop={circleLoop || true}
          enableSnap={true}
          autoplay={autoplay || false}
          autoplayInterval={autoPlayInterval || this.state.interval}
          itemWidth={parentWidth || width}
          sliderWidth={parentWidth || width}
          loopClonesPerSide={loopClonesPerSide || 5}
          renderItem={(item) => this._renderItem(item)}
          onSnapToItem={(index) => this.onSnap(index)}
          {...this.props}
        />
        {images.length > 1 && this.pagination}
      </View>
    );
  }
}

SliderBox.defaultProps = {
  ImageComponent: Image,
};
