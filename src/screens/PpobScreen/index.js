import React from 'react';
import I18n from 'react-native-redux-i18n';
import {StyleSheet, View, TouchableOpacity, Text} from 'react-native';
import {Container, Content, Header} from '../../components';
import {
  MobileTopUpIcon,
  DataPackageIcon,
  PlnIcon,
  PdamIcon,
  BpjsIcon,
} from '../../assets';

const listPpob = [
  {
    title: I18n.t('ppobScreen.mobileTopUp'),
    icon: <MobileTopUpIcon />,
    screen: 'MobileTopUpScreen',
  },
  {
    title: I18n.t('ppobScreen.dataPackage'),
    icon: <DataPackageIcon />,
    screen: 'DataPackageScreen',
  },
  {title: I18n.t('ppobScreen.pln'), icon: <PlnIcon />, screen: 'PlnScreen'},
  {title: I18n.t('ppobScreen.pdam'), icon: <PdamIcon />, screen: 'PdamScreen'},
  {title: I18n.t('ppobScreen.bpjs'), icon: <BpjsIcon />, screen: 'BpjsScreen'},
];

const PpobScreen = ({navigation}) => {
  const renderButton = ({title, icon, screen}) => {
    return (
      <TouchableOpacity
        key={title}
        style={styles.itemContainer}
        onPress={() => navigation.navigate(screen)}>
        {icon}
        <Text>{title}</Text>
      </TouchableOpacity>
    );
  };
  return (
    <Container>
      <Header title={I18n.t('ppobScreen.title')} />
      <Content>
        <View style={styles.container}>
          {listPpob.map((item) => {
            return renderButton(item);
          })}
        </View>
      </Content>
    </Container>
  );
};

export default PpobScreen;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  itemContainer: {
    flexBasis: `${100 / 3}%`,
    marginVertical: 19,
    height: 80,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
