import React from 'react';
import {Item, Picker} from 'native-base';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utilities';

const SelectBox = ({
  dataSource,
  onValueChange,
  onChange,
  value,
  placeholder,
  error,
  label,
  rounded,
  containerStyle,
}) => {
  if (placeholder) {
    dataSource = [{label: placeholder, value: ''}, ...dataSource];
  }
  return (
    <View>
      {label && <Text style={styles.label}>{label}</Text>}
      <Item
        style={[styles.inputContainer(error), containerStyle]}
        rounded={rounded}>
        <View style={styles.picker}>
          <Picker
            placeholder={placeholder}
            selectedValue={value}
            mode="dropdown"
            placeholderStyle={styles.placeholder}
            onValueChange={(val) => {
              if (onChange && onValueChange) {
                onChange(val);
                onValueChange(val);
                return;
              }
              if (onChange) {
                onChange(val);
              }
              if (onValueChange) {
                onValueChange(val);
              }
            }}>
            {dataSource.map((item, i) => {
              if (item.value) {
                return (
                  <Picker.Item key={i} label={item.label} value={item.value} />
                );
              }
              return (
                <Picker.Item
                  key={i}
                  label={item.label}
                  value={item.value}
                  color={colors.grey2}
                />
              );
            })}
          </Picker>
        </View>
      </Item>
    </View>
  );
};

export default SelectBox;

const styles = StyleSheet.create({
  inputContainer: (error) => ({
    borderWidth: error ? 1 : 0,
    borderColor: error ? colors.red1 : colors.grey2,
    paddingHorizontal: 15,
  }),
  picker: {
    flex: 1,
  },
  label: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
  placeholder: {
    color: colors.grey2,
    fontSize: fonts.size[14],
  },
});
