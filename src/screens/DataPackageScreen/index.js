import React, {useEffect, useState} from 'react';
import I18n from '../../utilities/i18n';
import {useDispatch} from 'react-redux';
import {Keyboard, StyleSheet, View, Text} from 'react-native';
import {ContactIcon, IkiAkuIcon5} from '../../assets';
import {
  Container,
  Header,
  Input,
  Content,
  Gap,
  CardList,
  SvgWrapper,
} from '../../components';
import {
  useForm,
  LIST_PROVIDER_PREFIX,
  LIST_DATA_PACKAGE_PRODUCT,
  fonts,
  hp,
  PPOB,
  wp,
  colors,
} from '../../utilities';
import {transactionActions} from '../../redux/store/actions';

const {checkBill} = transactionActions;

const DataPackageScreen = ({navigation, route}) => {
  const [form, setForm] = useForm({phoneNumber: ''});
  const [isPackageShow, setIsPackageShow] = useState(false);
  const [providerIcon, setProviderIcon] = useState(null);
  const dispatch = useDispatch();

  useEffect(() => {
    if (route.params && route.params.phoneNumber) {
      const phone = route.params.phoneNumber;
      setForm('phoneNumber', phone);
      if (LIST_PROVIDER_PREFIX.get(phone.slice(0, 4))) {
        const {icon, provider} = LIST_PROVIDER_PREFIX.get(phone.slice(0, 4));
        if (icon) {
          setProviderIcon(icon);
          setIsPackageShow(true);
        }
      }
      navigation.setParams({phoneNumber: ''});
    }
  }, [route.params && route.params.phoneNumber]);

  const onChangeText = (value) => {
    setForm('phoneNumber', value);
    if (value.length === 4 || value.length > 4) {
      let inputValue = value.length > 4 ? value.substring(0, 4) : value;
      if (LIST_PROVIDER_PREFIX.get(inputValue)) {
        const {icon, provider} = LIST_PROVIDER_PREFIX.get(inputValue);
        if (icon) {
          setProviderIcon(icon);
          setIsPackageShow(true);
        }
      }
    } else if (value.length < 4) {
      setProviderIcon(null);
      setIsPackageShow(false);
    }
  };

  const onSelect = (data) => {
    Keyboard.dismiss();
    dispatch(checkBill(PPOB.DATA_PACKAGE)).then((res) => {
      navigation.navigate('TransactionSummaryScreen', {
        title: I18n.t('dataPackageScreen.title'),
        productName: PPOB.DATA_PACKAGE,
      });
    });
  };
  return (
    <Container>
      <Header title={I18n.t('dataPackageScreen.title')} />
      <Content noPadding>
        <View style={styles.logoContainer}>
          <SvgWrapper
            component={<IkiAkuIcon5 />}
            width={wp(20)}
            height={wp(20)}
          />
        </View>
        <View style={styles.topContainer}>
          <Input
            rounded
            placeholder={I18n.t('dataPackageScreen.inputDestinationNumber')}
            value={form.phoneNumber}
            keyboardType="numeric"
            onChangeText={onChangeText}
            icon={
              <SvgWrapper
                component={<ContactIcon />}
                height={hp(6)}
                width={wp(6)}
              />
            }
            icon2={
              <SvgWrapper
                component={providerIcon}
                height={hp(6)}
                width={wp(6)}
              />
            }
            onPressIcon={() =>
              navigation.navigate('ListContactScreen', {
                screen: route.name,
              })
            }
          />
          <Gap height={hp(2)} />
          {isPackageShow && (
            <Text style={styles.chooseNominalText}>
              {I18n.t('dataPackageScreen.chooseDataPackageInstruction')}
            </Text>
          )}
        </View>
        {isPackageShow && (
          <CardList
            list={LIST_DATA_PACKAGE_PRODUCT}
            onPress={(item) => onSelect(item)}
            itemAlign="left"
            containerStyle={{backgroundColor: colors.grey3}}
          />
        )}
      </Content>
    </Container>
  );
};

export default DataPackageScreen;

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
  topContainer: {
    padding: 20,
  },
  chooseNominalText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
  },
  logoContainer: {
    marginLeft: 20,
  },
});
