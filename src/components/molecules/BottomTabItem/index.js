import React from 'react';
import I18n from 'react-native-redux-i18n';
import {StyleSheet, Text, View, Pressable} from 'react-native';
import {
  HomeIcon,
  HomeActiveIcon,
  BeneficiaryIcon,
  BeneficiaryActiveIcon,
  TransferIcon,
  HelpIcon,
  HelpActiveIcon,
  OtherIcon,
  OtherActiveIcon,
} from '../../../assets';
import {Gap, SvgWrapper} from '../../atoms';
import {colors, fonts, wp, hp} from '../../../utilities';

const BottomTabItem = ({title, active, onPress, onLongPress}) => {
  const Icon = () => {
    if (title === 'Home') {
      return active ? (
        <SvgWrapper
          component={<HomeActiveIcon />}
          width={hp(4)}
          height={hp(4)}
        />
      ) : (
        <SvgWrapper component={<HomeIcon />} width={hp(4)} height={hp(4)} />
      );
    }
    if (title === 'Beneficiary') {
      return active ? (
        <SvgWrapper
          component={<BeneficiaryActiveIcon />}
          width={hp(4)}
          height={hp(4)}
        />
      ) : (
        <SvgWrapper
          component={<BeneficiaryIcon />}
          width={hp(4)}
          height={hp(4)}
        />
      );
    }
    if (title === 'Transfer') {
      return (
        <SvgWrapper component={<TransferIcon />} width={hp(4)} height={hp(4)} />
      );
    }
    if (title === 'Help') {
      return active ? (
        <SvgWrapper
          component={<HelpActiveIcon />}
          width={hp(4)}
          height={hp(4)}
        />
      ) : (
        <SvgWrapper component={<HelpIcon />} width={hp(4)} height={hp(4)} />
      );
    }
    if (title === 'Other') {
      return active ? (
        <SvgWrapper
          component={<OtherActiveIcon />}
          width={hp(4)}
          height={hp(4)}
        />
      ) : (
        <SvgWrapper component={<OtherIcon />} width={hp(4)} height={hp(4)} />
      );
    }
    return <SvgWrapper component={<HomeIcon />} width={hp(4)} height={hp(4)} />;
  };

  const renderTitle = (title) => {
    if (title === 'Home') {
      return I18n.t('homeScreen.title');
    }
    if (title === 'Beneficiary') {
      return I18n.t('beneficiaryScreen.title');
    }
    if (title === 'Transfer') {
      return I18n.t('transferScreen.title');
    }
    if (title === 'Help') {
      return I18n.t('helpScreen.title');
    }
    if (title === 'Other') {
      return I18n.t('otherScreen.title');
    }
    return I18n.t('homeScreen.title');
  };

  if (title === 'Transfer') {
    return (
      <Pressable
        onPress={onPress}
        hitSlop={{top: 30, left: 30, bottom: 30, right: 30}}
        onLongPress={onLongPress}
        style={styles.transferButtonContainer}>
        <View style={styles.iconOuterContainer}>
          <View style={styles.iconContainer}>
            <Icon />
          </View>
        </View>
        <Gap height={32} />
        <Text style={styles.text(active)}>{renderTitle(title)}</Text>
      </Pressable>
    );
  }
  return (
    <Pressable
      style={styles.container}
      onPress={onPress}
      hitSlop={{top: 30, left: 30, bottom: 30, right: 30}}
      onLongPress={onLongPress}>
      <Icon />
      <Text style={styles.text(active)}>{renderTitle(title)}</Text>
    </Pressable>
  );
};

export default BottomTabItem;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    justifyContent: 'space-between',
    height: hp(5),
    marginHorizontal: hp(-2),
  },
  text: (active) => ({
    fontSize: fonts.size[10],
    fontFamily: fonts.primary.bold,
    color: active ? colors.blue2 : colors.grey4,
    marginHorizontal: hp(-2),
  }),
  transferButtonContainer: {
    justifyContent: 'space-between',
    alignItems: 'center',
    zIndex: 10,
    marginHorizontal: hp(1),
  },
  iconOuterContainer: {
    height: wp(10),
    width: wp(10),
    borderRadius: 60 / 2,
    backgroundColor: colors.blue2,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: hp(-2.5),
    flex: 1,
  },
  iconContainer: {
    height: hp(7),
    width: hp(7),
    borderRadius: 60 / 2,
    backgroundColor: colors.blue2,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
