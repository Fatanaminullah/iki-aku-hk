import {transferActionTypes} from '../../action-types';

const {SET_COUPON, SET_TRANSFER_DATA} = transferActionTypes;

const setCoupon = (item) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({
      type: SET_COUPON,
      payload: item,
    });
    resolve(item);
  });
};

const setTransferData = (item) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({
      type: SET_TRANSFER_DATA,
      payload: item,
    });
    resolve(item);
  });
};

export {setCoupon, setTransferData};
