import React from 'react';
import {Image, StyleSheet, Text} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {
  PPOBPDataIcon,
  RewardIcon,
  PPOBPLNPrepIcon,
  PPOBPulsaIcon,
  PPOBVGameIcon,
  ReferralIcon,
  TransferPNGIcon,
} from '../../../assets';
import {fonts, hp, wp} from '../../../utilities';
import {CustomScrollbar, Touchable} from '../../atoms';

const HomeSliderMenu = ({navigation}) => {
  return (
    <CustomScrollbar viewBoxStyle={styles.menuSliderContainer}>
      <Touchable
        style={styles.menuSliderButtonContainer}
        onPress={() => navigation.navigate('Transfer')}>
        <Image style={styles.menuSliderImage} source={TransferPNGIcon} />
        <Text style={styles.menuSliderLabel}>
          {I18n.t('homeScreen.menuTransfer')}
        </Text>
      </Touchable>
      <Touchable
        style={styles.menuSliderButtonContainer}
        onPress={() => navigation.navigate('ReferralScreen')}>
        <Image style={styles.menuSliderImage} source={ReferralIcon} />
        <Text style={styles.menuSliderLabel}>
          {I18n.t('homeScreen.menuReferral')}
        </Text>
      </Touchable>
      <Touchable
        style={styles.menuSliderButtonContainer}
        onPress={() => navigation.navigate('MobileTopUpScreen')}>
        <Image style={styles.menuSliderImage} source={PPOBPulsaIcon} />
        <Text style={styles.menuSliderLabel}>
          {I18n.t('homeScreen.menuPulsa')}
        </Text>
      </Touchable>
      <Touchable
        style={styles.menuSliderButtonContainer}
        onPress={() => navigation.navigate('DataPackageScreen')}>
        <Image style={styles.menuSliderImage} source={PPOBPDataIcon} />
        <Text style={styles.menuSliderLabel}>
          {I18n.t('homeScreen.menuPData')}
        </Text>
      </Touchable>
      <Touchable
        style={styles.menuSliderButtonContainer}
        onPress={() => navigation.navigate('PlnPrepaidScreen')}>
        <Image style={styles.menuSliderImage} source={PPOBPLNPrepIcon} />
        <Text style={styles.menuSliderLabel}>
          {I18n.t('homeScreen.menuPLNPrepaid')}
        </Text>
      </Touchable>
      <Touchable
        style={styles.menuSliderButtonContainer}
        onPress={() => navigation.navigate('RewardScreen')}>
        <Image style={styles.menuSliderImage} source={RewardIcon} />
        <Text style={styles.menuSliderLabel}>
          {I18n.t('homeScreen.menuReward')}
        </Text>
      </Touchable>
      <Touchable
        style={styles.menuSliderButtonContainer}
        onPress={() => navigation.navigate('VoucherGamesScreen')}>
        <Image style={styles.menuSliderImage} source={PPOBVGameIcon} />
        <Text style={styles.menuSliderLabel}>
          {I18n.t('homeScreen.menuVoucherGame')}
        </Text>
      </Touchable>
    </CustomScrollbar>
  );
};

export default HomeSliderMenu;

const styles = StyleSheet.create({
  indicatorContainer: {
    paddingVertical: 15,
  },
  menuSliderContainer: {
    maxHeight: hp(14.5),
    marginBottom: hp(4),
    marginTop: hp(-2),
  },
  menuSliderButtonContainer: {
    marginTop: hp(0.7),
    marginHorizontal: wp(2),
    width: hp(7),
    height: hp(12.8),
    marginBottom: 1.5,
    alignItems: 'center',
  },
  menuSliderImage: {
    width: hp(6),
    height: hp(6),
  },
  menuSliderLabel: {
    textAlign: 'center',
    color: 'black',
    marginTop: hp(1),
    fontFamily: fonts.primary[600],
    fontSize: fonts.size[12],
  },
});
