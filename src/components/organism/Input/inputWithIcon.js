import {Item, Label} from 'native-base';
import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import {colors, wp} from '../../../utilities';
import {Gap, Touchable} from '../../atoms';

const InputWithIcon = ({
  rounded,
  label,
  labelStyle,
  containerStyle,
  icon,
  icon2,
  iconPosition = 'right',
  onChangeText,
  keyboardType,
  disable,
  placeholder,
  onChange,
  value,
  error,
  clearErrors,
  maxLength,
  autoCapitalize,
  ref,
  onPressIcon,
  setValue,
}) => {
  return (
    <View>
      {label && <Label style={[styles.label, labelStyle]}>{label}</Label>}
      <Item
        style={[styles.inputContainer(error), containerStyle]}
        rounded={rounded}>
        {iconPosition === 'left' &&
          (onPressIcon ? (
            <Touchable
              style={styles.buttonContainer}
              onPress={() => onPressIcon(setValue, clearErrors)}>
              {icon}
            </Touchable>
          ) : (
            <View>{icon}</View>
          ))}
        {iconPosition === 'left' && <Gap width={wp(2)} />}
        <TextInput
          value={value}
          style={styles.input}
          placeholder={placeholder}
          placeholderTextColor={colors.grey2}
          keyboardType={keyboardType}
          onChangeText={(val) => {
            if (onChange && onChangeText) {
              onChange(val);
              onChangeText(val);
            }
            if (onChange) {
              onChange(val);
            }
            if (onChangeText) {
              onChangeText(val);
            }
          }}
          editable={!disable}
          selectTextOnFocus={!disable}
          maxLength={maxLength}
          autoCapitalize={autoCapitalize}
          ref={ref}
        />
        {icon2}
        {iconPosition === 'right' && <Gap width={15} />}
        {iconPosition === 'right' &&
          (onPressIcon ? (
            <Touchable
              style={styles.buttonContainer}
              onPress={() => onPressIcon(setValue, clearErrors)}>
              {icon}
            </Touchable>
          ) : (
            <View>{icon}</View>
          ))}
      </Item>
    </View>
  );
};

export default InputWithIcon;

const styles = StyleSheet.create({
  inputContainer: (error) => ({
    borderWidth: error ? 1 : 0,
    borderColor: error ? colors.red1 : colors.grey2,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 15,
  }),
  buttonContainer: {
    borderLeftWidth: 1,
    paddingLeft: 10,
    borderLeftColor: colors.grey2,
  },
  input: {
    flex: 1,
  },
});
