import Moment from 'moment';
import {notificationTypes} from '../../action-types';

const {SET_NOTIFICATION_LIST} = notificationTypes;

const dummyNotification = [
  {
    id: 0,
    title: 'Transaksi Dibatalkan',
    body: 'Transaksi dengan kode 23137EF berhasil dibatalkan',
    createTime: Moment([2020, 0, 29]).fromNow(),
    isRead: false,
  },
  {
    id: 1,
    title: 'Transaksi Tertunda',
    body:
      'Anda memiliki transaksi tertunda. Mohon segera menunjukan QR Code anda ke merchant terdekat sebelum waktu berlaku QR Code habis. ',
    createTime: Moment([2020, 0, 29]).fromNow(),
    isRead: true,
  },
];

const initialState = {
  notificationList: dummyNotification,
};

export default (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SET_NOTIFICATION_LIST:
      return {...state, notificationList: payload};
    default:
      return state;
  }
};
