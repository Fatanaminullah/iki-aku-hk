import React from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import {Item, Label} from 'native-base';
import {colors, fonts, hp} from '../../../utilities';
import InputPassword from './InputPassword';
import InputWithIcon from './inputWithIcon';
import InputSignature from './InputSignature';
import InputMoney from './InputMoney';

const Input = ({
  rounded,
  label,
  labelStyle,
  containerStyle,
  onChangeText,
  secureTextEntry,
  keyboardType,
  disable,
  placeholder,
  placeholderTextColor = colors.grey2,
  onChange,
  value,
  error,
  setValue,
  setError,
  clearErrors,
  maxLength,
  ref,
  autoCapitalize,
  icon,
  icon2,
  iconPosition,
  onPressIcon,
  inputSignature,
  name,
  leftIcon,
  rightIcon,
  defaultValue,
  inputStyle,
  textAlign,
  type,
  prefix,
  showPrefix,
  prefixStyle,
}) => {
  if (type === 'money') {
    return (
      <InputMoney
        value={value}
        placeholder={placeholder}
        onChangeText={(val) => {
          if (onChange && onChangeText) {
            onChange(val);
            onChangeText(val);
          }
          if (onChange) {
            onChange(val);
          }
          if (onChangeText) {
            onChangeText(val);
          }
        }}
        secureTextEntry={secureTextEntry}
        editable={!disable}
        selectTextOnFocus={!disable}
        icon={icon}
        onPressIcon={onPressIcon}
        error={error}
        textAlign={textAlign}
        maxLength={maxLength}
        ref={ref}
        prefix={prefix}
        showPrefix={showPrefix}
        labelStyle={labelStyle}
        label={label}
        prefixStyle={prefixStyle}
        inputStyle={inputStyle}
      />
    );
  } else if (secureTextEntry) {
    return (
      <InputPassword
        label={label}
        labelStyle={labelStyle}
        containerStyle={containerStyle}
        rounded={rounded}
        value={value}
        error={error}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        keyboardType={keyboardType}
        onChange={onChange}
        onChangeText={onChangeText}
        editable={!disable}
        selectTextOnFocus={!disable}
        maxLength={maxLength}
        secureTextEntry={secureTextEntry}
        autoCapitalize={autoCapitalize}
        ref={ref}
      />
    );
  } else if (inputSignature) {
    return (
      <InputSignature
        label={label}
        onChange={onChange}
        setValue={setValue}
        name={name}
        labelStyle={labelStyle}
      />
    );
  } else if (icon || icon2) {
    return (
      <InputWithIcon
        label={label}
        onPressIcon={onPressIcon}
        icon={icon}
        icon2={icon2}
        iconPosition={iconPosition}
        labelStyle={labelStyle}
        containerStyle={containerStyle}
        rounded={rounded}
        value={value}
        error={error}
        placeholder={placeholder}
        placeholderTextColor={placeholderTextColor}
        keyboardType={keyboardType}
        onChange={onChange}
        onChangeText={onChangeText}
        editable={!disable}
        selectTextOnFocus={!disable}
        maxLength={maxLength}
        secureTextEntry={secureTextEntry}
        autoCapitalize={autoCapitalize}
        ref={ref}
        setValue={setValue}
        clearErrors={clearErrors}
      />
    );
  } else {
    return (
      <View>
        {label && <Label style={[styles.label, labelStyle]}>{label}</Label>}
        <Item
          style={[styles.inputContainer(error), containerStyle]}
          rounded={rounded}>
          {leftIcon && leftIcon}
          <TextInput
            value={value}
            defaultValue={defaultValue}
            style={[styles.input, inputStyle]}
            placeholder={placeholder}
            placeholderTextColor={placeholderTextColor}
            keyboardType={keyboardType}
            onChangeText={(val) => {
              if (type === 'phoneNumber') {
                const text =
                  val.substr(0, 1) === '0' ? '' + val.substr(1) : val;
                if (onChange && onChangeText) {
                  onChange(text);
                  onChangeText(text);
                }
                if (onChange) {
                  onChange(text);
                }
                if (onChangeText) {
                  onChangeText(text);
                }
              } else {
                if (onChange && onChangeText) {
                  onChange(val);
                  onChangeText(val);
                }
                if (onChange) {
                  onChange(val);
                }
                if (onChangeText) {
                  onChangeText(val);
                }
              }
            }}
            editable={!disable}
            selectTextOnFocus={!disable}
            maxLength={maxLength}
            autoCapitalize={autoCapitalize}
            ref={ref}
          />
          {rightIcon && rightIcon}
        </Item>
      </View>
    );
  }
};
export default Input;

const styles = StyleSheet.create({
  inputContainer: (error) => ({
    borderWidth: error ? 1 : 0,
    borderColor: error ? colors.red1 : colors.grey2,
    paddingHorizontal: 15,
  }),
  label: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
  input: {
    flex: 1,
    height: hp(6),
    paddingVertical: hp(1),
  },
});
