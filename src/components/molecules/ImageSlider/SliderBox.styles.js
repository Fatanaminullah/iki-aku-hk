export default {
  paginationBoxStyle: {
    position: 'absolute',
    bottom: 0,
    padding: 0,
    alignSelf: 'center',
  },
  dotStyle: {
    width: 10,
    height: 10,
    borderRadius: 5,
    marginHorizontal: 0,
    padding: 0,
    margin: 0,
    backgroundColor: 'rgba(128, 128, 128, 0.92)',
  },
  activityIndicatorStyle: {
    position: 'absolute',
    alignSelf: 'center',
    bottom: 50,
  },
};
