import I18n from 'react-native-redux-i18n';
import {localeTypes} from '../../action-types';

const {SET_LOCALE} = localeTypes;

const initState = {
  language: I18n.defaultLocale,
};

export default (state = initState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SET_LOCALE:
      return {
        ...state,
        language: payload,
      };
    default:
      return state;
  }
};
