import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {colors, fonts} from '../../../utilities';

const Link = ({
  type = 'button',
  style,
  title,
  size = fonts.size[16],
  align,
  onPress,
}) => {
  if (type === 'text') {
    return (
      <Text onPress={onPress} style={styles.text(size, align, style)}>
        {title}
      </Text>
    );
  }
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styles.text(size, align, style)}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Link;

const styles = StyleSheet.create({
  text: (size, align, style) => ({
    fontSize: size,
    fontFamily: fonts.primary.bold,
    color: colors.red2,
    textAlign: align,
    ...style,
  }),
});
