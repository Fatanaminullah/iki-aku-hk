import CloseIcon from './close-icon.svg';
import IkiAkuIcon1 from './ikiaku-icon-1.svg';
import IkiAkuIcon2 from './ikiaku-icon-2.png';
import IkiAkuIcon3 from './ikiaku-icon-3.svg';
import IkiAkuIcon4 from './ikiaku-icon-2.svg';
import IkiAkuIcon5 from './ikiaku-icon-5.svg';
import MoreIcon from './more-icon.svg';
import LeftArrowIcon from './left-arrow-icon.svg';
import FlagIndoRectanglePng from './flag-indo-rectangle.png';
import FlagIndoRectangle from './flag-indo-rectangle.svg';
import FlagIndoRectangle3 from './flag-indo-rectangle-2.svg';
import FlagUkRectangle from './flag_uk_rectangle.png';
import FlagHkgRound from './flag_hkg_round.svg';
import FlagTaiwanRound from './flag_taiwan_round.svg';
import CalendarIcon from './calendar-icon.svg';
import CameraIcon from './camera-icon.svg';
import HomeIcon from './home-icon.svg';
import HomeActiveIcon from './home-active-icon.svg';
import BeneficiaryIcon from './beneficiary-icon.svg';
import BeneficiaryActiveIcon from './beneficiary-active-icon.svg';
import TransferIcon from './transfer-icon.svg';
import HelpIcon from './help-icon.svg';
import HelpActiveIcon from './help-active-icon.svg';
import OtherIcon from './other-icon.svg';
import OtherActiveIcon from './other-active-icon.svg';
import OnNotifIcon from './on-notif-icon.svg';
import NotifIcon from './notif-icon.svg';
import DummyProfileIcon from './dummy-profile-icon.svg';
import LineIcon from './line-icon.svg';
import ContactIcon from './contact-icon.svg';
import MobileTopUpIcon from './mobile-top-up-icon.svg';
import DataPackageIcon from './data-package-icon.svg';
import PlnIcon from './pln-icon.svg';
import BpjsIcon from './bpjs-icon.svg';
import PdamIcon from './pdam-icon.svg';
import TelkomselIcon from './telkomsel-icon.svg';
import IndosatIcon from './indosat-icon.svg';
import XlIcon from './xl-icon.svg';
import ThreeIcon from './tri-icon.svg';
import AxisIcon from './axis-icon.svg';
import SmartfrenIcon from './smartfren-icon.svg';
import SearchIcon from './search-icon.svg';
import SearchIcon2 from './search-icon-2.svg';
import NoProfilePictIcon from './no-profile-pict-icon.svg';
import EditIcon from './edit-icon.svg';
import RightArrowNoTailIcon from './right-arrow-no-tail-icon.svg';
import CheckListIcon from './checklist-icon.svg';
import CameraIcon2 from './camera-icon-2.svg';
import FlashOffIcon from './flash-off-icon.svg';
import ChangeCameraIcon from './change-camera-icon.svg';
import BackIcon from './back-icon.svg';
import CameraIcon3 from './camera-icon-3.svg';
import PayFammartIcon from './pay-fammart-icon.png';
import PayHilifeIcon from './pay-hilife-icon.png';
import PayOkmartIcon from './pay-okmart-icon.png';
import PaySevelIcon from './pay-sevel-icon.png';
import PPOBPulsaIcon from './ppob-pulsa-icon.png';
import PPOBPDataIcon from './ppob-paketdata-icon.png';
import PPOBPLNPrepIcon from './ppob-pln-prepaid-icon.png';
import PPOBPLNPostpIcon from './ppob-pln-postpaid-icon.png';
import PPOBVGameIcon from './ppob-voucher-game-icon.png';
import ReferralIcon from './referral-icon.png';
import RewardIcon from './reward-icon.png';
import RewardPoinIcon from './reward-poin-icon.svg';
import RecipientsIcon from './recipients-icon.png';
import TransferPNGIcon from './transfer-icon.png';
import AddUserIcon from './add-user-icon.svg';
import TrashIcon from './trash-icon.svg';
import FlagIndoRectangle2 from './flag-indo-rectangle.svg';
import FlagTaiwanRectangle from './flag-taiwan-retangle.svg';
import UnusedCouponIcon from './unused-coupon-icon.svg';
import DeleteIcon from './delete-icon.svg';
import SendIcon from './send-icon.svg';
import CouponMoneyIcon from './coupon-money-icon.svg';
import CouponTimeIcon from './coupon-time-icon.svg';
import UsedCouponIcon from './used-coupon-icon.svg';
import PencilIcon from './pencil-icon.svg';
import CopyIcon from './copy-icon.svg';
import FilterIcon from './filter-icon.svg';
import AovIcon from './aov-icon.svg';
import BleachIcon from './bleach-icon.svg';
import DragonNestIcon from './dragon-nest-icon.svg';
import EternalCityGemIcon from './eternal-city-gem-icon.svg';
import FreeFireIcon from './free-fire-icon.svg';
import GarenaIcon from './garena-icon.png';
import LaplaceMSpiralIcon from './laplace-m-spiral-icon.svg';
import LoveNikkiIcon from './love-nikki-icon.svg';
import MobileLegendIcon from './mobile-legend-icon.svg';
import RagnarokIcon from './ragnarok-icon.svg';
import SpeedDriftersIcon from './speed-drifters-icon.svg';
import AeriaIcon from './aeria-icon.png';
import FacebookGameCardIcon from './facebook-game-card-icon.png';
import GemscoolIcon from './gemscool-icon.png';
import LytoIcon from './lyto-icon.png';
import MegaxusIcon from './megaxus-icon.png';
import PlaystationNetworkIcon from './ps-icon.png';
import PointBlankIcon from './pb-icon.png';
import PubgIcon from './pubg-icon.png';
import SteamWalletIcon from './steam-icon.png';
import ClockIcon from './clock-icon.svg';
import DeletePinIcon from './delete-pin-icon.svg';
import FlashOnIcon from './flash-on-icon.svg';
import MerchantIcon from './merchant-icon.svg';
import FacebookIcon from './facebook.svg';
import WhatsAppIcon from './whatsapp.svg';

export {
  CloseIcon,
  IkiAkuIcon1,
  IkiAkuIcon2,
  IkiAkuIcon3,
  IkiAkuIcon4,
  IkiAkuIcon5,
  MoreIcon,
  FlagIndoRectangle,
  FlagUkRectangle,
  LeftArrowIcon,
  FlagHkgRound,
  FlagTaiwanRound,
  CalendarIcon,
  CameraIcon,
  HomeIcon,
  HomeActiveIcon,
  BeneficiaryIcon,
  BeneficiaryActiveIcon,
  TransferIcon,
  HelpIcon,
  HelpActiveIcon,
  OtherIcon,
  OtherActiveIcon,
  OnNotifIcon,
  NotifIcon,
  DummyProfileIcon,
  LineIcon,
  ContactIcon,
  MobileTopUpIcon,
  DataPackageIcon,
  PlnIcon,
  BpjsIcon,
  PdamIcon,
  TelkomselIcon,
  IndosatIcon,
  XlIcon,
  ThreeIcon,
  AxisIcon,
  SmartfrenIcon,
  SearchIcon,
  SearchIcon2,
  NoProfilePictIcon,
  EditIcon,
  RightArrowNoTailIcon,
  CheckListIcon,
  CameraIcon2,
  FlashOffIcon,
  ChangeCameraIcon,
  BackIcon,
  CameraIcon3,
  PayHilifeIcon,
  PayFammartIcon,
  PaySevelIcon,
  PayOkmartIcon,
  PPOBPulsaIcon,
  PPOBPDataIcon,
  PPOBPLNPrepIcon,
  PPOBPLNPostpIcon,
  PPOBVGameIcon,
  ReferralIcon,
  RewardIcon,
  RewardPoinIcon,
  RecipientsIcon,
  TransferPNGIcon,
  AddUserIcon,
  TrashIcon,
  FlagIndoRectangle2,
  FlagTaiwanRectangle,
  UnusedCouponIcon,
  FlagIndoRectangle3,
  DeleteIcon,
  SendIcon,
  CouponMoneyIcon,
  CouponTimeIcon,
  UsedCouponIcon,
  PencilIcon,
  CopyIcon,
  FilterIcon,
  AovIcon,
  BleachIcon,
  DragonNestIcon,
  EternalCityGemIcon,
  FreeFireIcon,
  GarenaIcon,
  LaplaceMSpiralIcon,
  LoveNikkiIcon,
  MobileLegendIcon,
  RagnarokIcon,
  SpeedDriftersIcon,
  AeriaIcon,
  FacebookGameCardIcon,
  GemscoolIcon,
  LytoIcon,
  MegaxusIcon,
  PlaystationNetworkIcon,
  PointBlankIcon,
  PubgIcon,
  SteamWalletIcon,
  ClockIcon,
  FlagIndoRectanglePng,
  DeletePinIcon,
  FlashOnIcon,
  MerchantIcon,
  FacebookIcon,
  WhatsAppIcon,
};
