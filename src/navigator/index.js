import React, {useRef} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator, TransitionPresets} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {BottomNavigator} from '../components';
import analytics from '@react-native-firebase/analytics';
import {
  SplashScreen,
  SignInScreen,
  ForgotPasswordScreen,
  ForgotPasswordOtpScreen,
  CreateNewPasswordScreen,
  ForgotPinScreen,
  ForgotPinOtpScreen,
  CreateNewPinScreen,
  WalkthroughScreen,
  AgreementScreen,
  SignUpScreen,
  HomeScreen,
  BeneficiaryScreen,
  TransferScreen,
  HelpScreen,
  OtherScreen,
  PpobScreen,
  ListContactScreen,
  MobileTopUpScreen,
  DataPackageScreen,
  PlnScreen,
  PlnPrepaidScreen,
  PlnPostpaidScreen,
  BpjsScreen,
  BpjsKesehatanScreen,
  PdamScreen,
  TransactionHistoryScreen,
  TransactionSummaryScreen,
  TransactionDetailsScreen,
  BeneficiaryFormScreen,
  DetailBeneficiaryScreen,
  TransferConfirmationScreen,
  CouponScreen,
  DetailCouponScreen,
  EditProfileScreen,
  UserDetailsScreen,
  EditProfileScreenV2,
  AboutUsScreen,
  TermsAndConditionsScreen,
  PrivacyPolicyScreen,
  ReferralScreen,
  RewardScreen,
  DetailRewardScreen,
  NotificationScreen,
  TransferConfirmationDataScreen,
  ChangePasswordScreen,
  UpdateProfileOtpScreen,
  ChangePinScreen,
  SignUpOtpScreen,
  VoucherGamesScreen,
  VoucherGamesProductScreen,
} from '../screens';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
  return (
    <Tab.Navigator
      backBehavior="initialRoute"
      tabBar={(props) => <BottomNavigator {...props} />}>
      <Tab.Screen name="Home" component={HomeScreen} />
      <Tab.Screen name="Beneficiary" component={BeneficiaryScreen} />
      <Tab.Screen
        name="Transfer"
        component={TransferScreen}
        options={{tabBarVisible: false}}
      />
      <Tab.Screen name="Help" component={HelpScreen} />
      <Tab.Screen name="Other" component={OtherScreen} />
    </Tab.Navigator>
  );
};

export default function Navigator({navigationRef}) {
  const routeNameRef = useRef();
  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() =>
        (routeNameRef.current = navigationRef.current.getCurrentRoute().name)
      }
      onStateChange={async () => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = navigationRef.current.getCurrentRoute().name;
        if (previousRouteName !== currentRouteName) {
          await analytics().logScreenView({
            screen_name: currentRouteName,
            screen_class: currentRouteName,
          });
        }
        routeNameRef.current = currentRouteName;
      }}>
      <Stack.Navigator
        headerMode="none"
        screenOptions={{
          ...TransitionPresets.SlideFromRightIOS,
        }}>
        <Stack.Screen name="SplashScreen" component={SplashScreen} />
        <Stack.Screen name="WalkthroughScreen" component={WalkthroughScreen} />
        <Stack.Screen name="SignInScreen" component={SignInScreen} />
        <Stack.Screen name="SignUpScreen" component={SignUpScreen} />
        <Stack.Screen name="SignUpOtpScreen" component={SignUpOtpScreen} />
        <Stack.Screen
          name="ForgotPasswordScreen"
          component={ForgotPasswordScreen}
        />
        <Stack.Screen
          name="ForgotPasswordOtpScreen"
          component={ForgotPasswordOtpScreen}
        />
        <Stack.Screen
          name="CreateNewPasswordScreen"
          component={CreateNewPasswordScreen}
        />
        <Stack.Screen name="ForgotPinScreen" component={ForgotPinScreen} />
        <Stack.Screen
          name="ForgotPinOtpScreen"
          component={ForgotPinOtpScreen}
        />
        <Stack.Screen
          name="CreateNewPinScreen"
          component={CreateNewPinScreen}
        />
        <Stack.Screen name="AgreementScreen" component={AgreementScreen} />
        <Stack.Screen name="MainApp" component={MainApp} />
        <Stack.Screen name="PpobScreen" component={PpobScreen} />
        <Stack.Screen name="ListContactScreen" component={ListContactScreen} />
        <Stack.Screen name="MobileTopUpScreen" component={MobileTopUpScreen} />
        <Stack.Screen name="DataPackageScreen" component={DataPackageScreen} />
        <Stack.Screen name="PlnScreen" component={PlnScreen} />
        <Stack.Screen name="PlnPrepaidScreen" component={PlnPrepaidScreen} />
        <Stack.Screen name="PlnPostpaidScreen" component={PlnPostpaidScreen} />
        <Stack.Screen name="BpjsScreen" component={BpjsScreen} />
        <Stack.Screen
          name="BpjsKesehatanScreen"
          component={BpjsKesehatanScreen}
        />
        <Stack.Screen name="PdamScreen" component={PdamScreen} />
        <Stack.Screen
          name="TransactionHistoryScreen"
          component={TransactionHistoryScreen}
        />
        <Stack.Screen
          name="TransactionSummaryScreen"
          component={TransactionSummaryScreen}
        />
        <Stack.Screen
          name="TransactionDetailsScreen"
          component={TransactionDetailsScreen}
        />
        <Stack.Screen name="BeneficiaryScreen" component={BeneficiaryScreen} />
        <Stack.Screen
          name="DetailBeneficiaryScreen"
          component={DetailBeneficiaryScreen}
        />
        <Stack.Screen
          name="BeneficiaryFormScreen"
          component={BeneficiaryFormScreen}
        />
        <Stack.Screen
          name="TransferConfirmationScreen"
          component={TransferConfirmationScreen}
        />
        <Stack.Screen name="CouponScreen" component={CouponScreen} />
        <Stack.Screen
          name="DetailCouponScreen"
          component={DetailCouponScreen}
        />
        <Stack.Screen name="EditProfileScreen" component={EditProfileScreen} />
        <Stack.Screen name="UserDetailsScreen" component={UserDetailsScreen} />
        <Stack.Screen
          name="EditProfileScreenV2"
          component={EditProfileScreenV2}
        />
        <Stack.Screen name="HelpScreen" component={HelpScreen} />
        <Stack.Screen name="AboutUsScreen" component={AboutUsScreen} />
        <Stack.Screen
          name="TermsAndConditionsScreen"
          component={TermsAndConditionsScreen}
        />
        <Stack.Screen
          name="PrivacyPolicyScreen"
          component={PrivacyPolicyScreen}
        />
        <Stack.Screen name="ReferralScreen" component={ReferralScreen} />
        <Stack.Screen name="RewardScreen" component={RewardScreen} />
        <Stack.Screen
          name="DetailRewardScreen"
          component={DetailRewardScreen}
        />
        <Stack.Screen
          name="NotificationScreen"
          component={NotificationScreen}
        />
        <Stack.Screen
          name="TransferConfirmationDataScreen"
          component={TransferConfirmationDataScreen}
        />
        <Stack.Screen
          name="ChangePasswordScreen"
          component={ChangePasswordScreen}
        />
        <Stack.Screen
          name="UpdateProfileOtpScreen"
          component={UpdateProfileOtpScreen}
        />
        <Stack.Screen
          name="VoucherGamesScreen"
          component={VoucherGamesScreen}
        />
        <Stack.Screen
          name="VoucherGamesProductScreen"
          component={VoucherGamesProductScreen}
        />
        <Stack.Screen name="ChangePinScreen" component={ChangePinScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
