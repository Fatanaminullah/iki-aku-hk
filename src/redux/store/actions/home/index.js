import {homeActionTypes} from '../../action-types';

const {SET_MENU_LIST, SET_PROMOTION_BANNERS} = homeActionTypes;

const setListMenu = (payload) => ({
  type: SET_MENU_LIST,
  payload,
});

const setPromotionBanners = (payload) => ({
  type: SET_PROMOTION_BANNERS,
  payload,
});

export {setListMenu, setPromotionBanners};
