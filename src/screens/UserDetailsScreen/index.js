import {Image} from 'native-base';
import React, {useEffect} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {DummyProfileIcon, EditIcon, NoProfilePictIcon} from '../../assets';
import {
  Button,
  Container,
  Content,
  Gap,
  Header,
  HorizontalLine,
  SvgWrapper,
} from '../../components';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {getUserInfo} from '../../redux/store/actions/profile';
import {colors, fonts, hp, wp} from '../../utilities';
import mapperDetail from './mapper';

const UserDetailScreen = ({navigation}) => {
  const {userDetails} = useSelector((state) => state.profile);
  const detail = mapperDetail(userDetails);
  useEffect(() => {
    dispatch(getUserInfo(showLoading, dismissLoading));
  }, []);
  const renderProfileItem = (item) => {
    if (item?.type === 'image-picker') {
      return (
        <View style={[styles.columnContainer, styles.separator()]}>
          <Text style={styles.arcPhotoText}>
            {I18n.t('editProfileScreen.arcPhoto')}
          </Text>
          <Gap height={hp(2)} />
          <View style={styles.arcPhotoContainer}>
            {item.value.map((item) => {
              return (
                <View style={styles.arcPhotoItemContainer}>
                  <View>{item.img}</View>
                  <Gap height={hp(1)} />
                  <Text style={styles.arcPhotoText}>{item.title}</Text>
                </View>
              );
            })}
          </View>
        </View>
      );
    }
    return (
      <View style={[styles.rowContainer, styles.separator()]}>
        <Text style={styles.rowItemText}>{item.title}</Text>
        <View style={styles.rowContainer}>
          {item.isPrivate ? (
            <TextInput
              value={item.value}
              editable={false}
              style={styles.obfuscatedContainer}
              secureTextEntry={true}
            />
          ) : (
            <Text style={styles.rowItemText}>{item.value}</Text>
          )}
        </View>
      </View>
    );
  };

  return (
    <Container>
      <Header title={I18n.t('editProfileScreen.title')} />
      <Content>
        <Button
          title={I18n.t('editProfileScreen.editData')}
          icon={
            <SvgWrapper
              component={<EditIcon />}
              width={hp(2.5)}
              height={hp(2.5)}
            />
          }
          style={styles.editButtonContainer}
          textStyle={styles.editButtonText}
          onPress={() =>
            navigation.navigate('EditProfileScreen', {data: detail})
          }
        />
        <Gap height={hp(3)} />
        <Text style={styles.profilePictureText}>
          {I18n.t('editProfileScreen.profilePicture')}
        </Text>
        <Gap height={hp(2)} />
        {!detail?.profilePicture ? (
          <SvgWrapper
            style={[styles.profileEmpty]}
            component={<NoProfilePictIcon />}
          />
        ) : (
          <Image
            source={{uri: userDetails?.profilePicture}}
            style={styles.profileImg}
          />
        )}
        <Gap height={hp(3)} />
        <HorizontalLine height={0.5} />
        {detail.map((item, index) => {
          return renderProfileItem(item, index);
        })}
        <Gap height={hp(10)} />
      </Content>
    </Container>
  );
};

export default UserDetailScreen;

const styles = StyleSheet.create({
  editButtonContainer: {
    width: wp(40),
    height: hp(6),
    borderRadius: 25,
    backgroundColor: colors.blue2,
    justifyContent: 'center',
    alignSelf: 'flex-end',
  },
  editButtonText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    paddingHorizontal: 0,
  },
  profilePictureText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    alignSelf: 'center',
  },
  profileImg: {
    alignSelf: 'center',
    width: hp(10),
    height: wp(20),
  },
  profileEmpty: {
    alignSelf: 'center',
    width: hp(10),
    height: wp(20),
    borderColor: colors.grey2,
    padding: 5,
    borderWidth: 1,
    borderRadius: 100,
  },
  cameraIcon: {
    position: 'absolute',
    bottom: -5,
    right: 0,
  },
  columnContainer: {
    justifyContent: 'center',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  separator: (color) => ({
    borderBottomColor: color ? color : colors.grey2,
    borderBottomWidth: 1,
    paddingVertical: 8,
  }),
  rowItemText: {
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[12],
  },
  obfuscatedContainer: {
    padding: 0,
    textAlign: 'right',
    color: colors.black,
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.normal,
  },
  arcPhotoText: {
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.normal,
    marginVertical: hp(1),
  },
  arcPhotoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  arcPhotoItemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: hp(1),
  },
});
