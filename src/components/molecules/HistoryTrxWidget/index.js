import React from 'react';
import {FlatList, RefreshControl, StyleSheet, Text, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import HistoryTrxListsItem from './HistoryTrxListsItem';
import HistoryTrxNoTrx from './HistoryTrxNoTrx';
import {SkeletonLoading, Touchable} from '../../atoms';
import {colors, fonts, hp, wp} from '../../../utilities';

const HistoryTrxWidget = ({
  navigation,
  list,
  showRecent,
  loading,
  setDetailTransaction,
  onRefresh,
}) => {
  return (
    <View style={styles.historyTrxContainer}>
      {showRecent && list.length > 0 && (
        <View style={styles.historyTrxHeaderContainer}>
          <Text style={styles.historyTrxTitle}>
            {I18n.t('homeScreen.historyTrxLabel')}
          </Text>
          <Touchable
            onPress={() => {
              navigation.navigate('TransactionHistoryScreen');
            }}>
            <Text style={styles.historyTrxHeaderButtonTitle}>
              {I18n.t('homeScreen.seeAll')}
            </Text>
          </Touchable>
        </View>
      )}
      <View>
        {loading === false ? (
          list.length > 0 ? (
            showRecent === true ? (
              list.slice(0, 3).map((item, i) => {
                return (
                  <HistoryTrxListsItem
                    key={i}
                    index={i}
                    navigation={navigation}
                    trxItem={item}
                    setDetailTransaction={setDetailTransaction}
                  />
                );
              })
            ) : (
              <FlatList
                data={list}
                keyboardShouldPersistTaps="handled"
                keyExtractor={(item) => item.id}
                refreshControl={
                  <RefreshControl onRefresh={onRefresh} refreshing={false} />
                }
                renderItem={({item, index}) => (
                  <HistoryTrxListsItem
                    key={index}
                    setDetailTransaction={setDetailTransaction}
                    index={index}
                    navigation={navigation}
                    trxItem={item}
                  />
                )}
              />
            )
          ) : (
            <HistoryTrxNoTrx />
          )
        ) : (
          <View>
            {showRecent
              ? ['', ''].map((item, i) => (
                  <SkeletonLoading
                    key={i}
                    width={wp(100)}
                    height={hp(10)}
                    type="trxLists"
                  />
                ))
              : ['', '', '', '', '', '', '', ''].map((item, i) => (
                  <SkeletonLoading
                    key={i}
                    width={wp(100)}
                    height={hp(10)}
                    type="trxLists"
                  />
                ))}
          </View>
        )}
      </View>
    </View>
  );
};

export default HistoryTrxWidget;

const styles = StyleSheet.create({
  historyTrxContainer: {
    flex: 1,
    backgroundColor: colors.grey3,
  },
  historyTrxScrollview: {
    paddingHorizontal: wp(1),
  },
  historyTrxHeaderContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  historyTrxTitle: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
  },
  historyTrxHeaderButtonTitle: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    color: colors.blue2,
  },
});
