import React from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {DeleteIcon} from '../../assets';
import {
  Button,
  Container,
  Content,
  Gap,
  Header,
  InitialAvatar,
  ListItem,
  SvgWrapper,
} from '../../components';
import {
  showAlertConfirmation,
  showAlertError,
  showAlertSuccess,
} from '../../redux/store/actions/alert';
import {deleteBeneficiary} from '../../redux/store/actions/beneficiary';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {fonts, wp} from '../../utilities';

const DetailBeneficiaryScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const detail = useSelector((state) => state.beneficiary.detailBeneficiary);
  const listDetail = Object.keys(detail)
    .map((item) => {
      if (
        detail[item] &&
        item !== 'id' &&
        item !== 'bankId' &&
        item !== 'bankCode'
      ) {
        return {
          label: I18n.t(`detailBeneficiaryScreen.${item}`),
          value: detail[item] || '--',
        };
      }
    })
    .filter((item) => item);
  const renderListItem = ({item, index}) => <ListItem item={item} />;
  const onClickDelete = () => {
    dispatch(
      showAlertConfirmation(
        '',
        I18n.t('detailBeneficiaryScreen.deleteConfirmation'),
        () => {
          dispatch(
            deleteBeneficiary(
              detail.id,
              showLoading,
              dismissLoading,
              showAlertError,
              showAlertSuccess,
            ),
          ).then((res) => {
            navigation.goBack();
          });
        },
        '',
      ),
    );
  };
  return (
    <Container>
      <Header title={I18n.t('detailBeneficiaryScreen.title')} />
      <Content>
        <Button
          title={I18n.t('detailBeneficiaryScreen.delete')}
          onPress={() => onClickDelete()}
          type="danger"
          buttonType="outline"
          rounded
          iconRight={
            <SvgWrapper
              component={<DeleteIcon />}
              height={wp(5)}
              width={wp(5)}
            />
          }
          style={styles.buttonDelete}
        />
        <View style={styles.nameContainer}>
          <InitialAvatar
            text={detail?.fullName}
            rounded
            size={65}
            fontSize={fonts.size[22]}
          />
          <Text style={styles.textName}>{detail?.fullName}</Text>
        </View>
        <Gap height={10} />
        <FlatList
          data={listDetail}
          renderItem={renderListItem}
          keyExtractor={(item) => item.id}
          keyboardShouldPersistTaps="handled"
        />
      </Content>
    </Container>
  );
};

export default DetailBeneficiaryScreen;

const styles = StyleSheet.create({
  textName: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[18],
    textAlign: 'center',
  },
  nameContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    padding: 20,
  },
  buttonDelete: {
    alignSelf: 'flex-end',
    paddingHorizontal: 10,
    flexDirection: 'row',
  },
});
