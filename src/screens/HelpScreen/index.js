import React from 'react';
import {StyleSheet, Image, View, Text} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {DummyBanner1, LineIcon, FacebookIcon, WhatsAppIcon} from '../../assets';
import {
  Button,
  Card,
  Container,
  Content,
  Gap,
  Header,
  SvgWrapper,
} from '../../components';
import {colors, fonts, hp, thousandSeparator, wp} from '../../utilities';

const HelpScreen = () => {
  return (
    <Container style={styles.page}>
      <Header title={I18n.t('helpScreen.title')} />
      <Content>
        <Card style={styles.cardContainer}>
          <Image
            source={DummyBanner1}
            style={{width: wp(100), height: hp(20)}}
            resizeMode="contain"
          />
          <View style={styles.caption}>
            <View style={styles.column}>
              <View style={styles.textContainer}></View>
            </View>
          </View>
        </Card>
        <Card style={styles.cardDetailContainer}>
          <Text
            style={{
              fontFamily: fonts.primary.bold,
              color: colors.black,
              fontSize: fonts.size[20],
            }}>
            {I18n.t('helpScreen.title')}
          </Text>
          <Gap height={15} />
          <Text
            style={{
              fontFamily: fonts.primary.normal,
              color: colors.black,
              fontSize: fonts.size[13],
            }}>
            {I18n.t('helpScreen.helpText')}
          </Text>
          <Gap height={hp(5)} />
          <View style={styles.contact}>
            <SvgWrapper
              component={<WhatsAppIcon />}
              width={hp(4)}
              height={hp(4)}
            />
            <Text style={styles.contactText2}>WhatsApp:</Text>
            <Text style={styles.contactText}>+852928699963</Text>
          </View>
          <Gap height={hp(1)} />
          <View style={styles.contact}>
            <SvgWrapper
              component={<FacebookIcon />}
              width={hp(4)}
              height={hp(4)}
            />
            <Text style={styles.contactText2}>Facebook:</Text>
            <Text style={styles.contactText}>IKI Internasional</Text>
          </View>
          <Gap height={hp(1)} />
          <View style={styles.contact}>
            <SvgWrapper component={<LineIcon />} width={hp(4)} height={hp(4)} />
            <Text style={styles.contactText2}>Line:</Text>
            <Text style={styles.contactText}>@ikitaiwan</Text>
          </View>
        </Card>
        <Gap height={15} />
      </Content>
    </Container>
  );
};

export default HelpScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.white,
  },
  cardContainer: {
    marginVertical: 10,
    borderWidth: 0,
    borderRadius: 0,
  },
  cardDetailContainer: {
    flex: 1,
    borderWidth: 0,
    borderRadius: 0,
  },
  caption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(80),
  },
  column: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: wp(35),
  },
  textContainer: {
    flexWrap: 'wrap',
    marginLeft: hp(1),
  },
  contact: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
  },
  contactText: {
    fontFamily: fonts.primary.bold,
    color: colors.red1,
    fontSize: fonts.size[14],
    alignSelf: 'center',
    marginLeft: hp(1),
  },
  contactText2: {
    fontFamily: fonts.primary.bold,
    color: colors.black,
    fontSize: fonts.size[14],
    alignSelf: 'center',
    marginLeft: hp(1),
  },
});
