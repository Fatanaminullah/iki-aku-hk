import {loadingActionTypes} from '../../action-types';
const {SHOW_LOADING, HIDE_LOADING} = loadingActionTypes;

const showLoading = (data) => ({
  type: SHOW_LOADING,
  payload: data,
});

const dismissLoading = (data) => ({
  type: HIDE_LOADING,
  payload: data,
});

export {showLoading, dismissLoading};
