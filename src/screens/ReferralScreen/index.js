import React, {useState} from 'react';
import I18n from 'react-native-redux-i18n';
import {useSelector} from 'react-redux';
import {
  StyleSheet,
  ImageBackground,
  Text,
  View,
  TouchableOpacity,
  Share,
} from 'react-native';
import {
  Container,
  Content,
  Header,
  Button,
  Gap,
  Touchable,
  SvgWrapper,
} from '../../components';
import {ReferralScreenImgBg, CopyIcon} from '../../assets';
import {fonts, colors, hp, wp, copyToClipboard} from '../../utilities';

const ReferralScreen = () => {
  const {userDetails} = useSelector((state) => state.profile);
  const onCopy = () => {
    copyToClipboard(
      userDetails?.arcNumber,
      `${I18n.t('referralScreen.copyReferralCode')}`,
      'SHORT',
      'BOTTOM',
    );
  };
  const onShare = async () => {
    try {
      await Share.share({
        title: I18n.t('referralScreen.shareYourReferralCode'),
        message: userDetails?.arcNumber,
      });
    } catch (error) {
      console.log(error.message);
    }
  };
  return (
    <Container>
      <Header title={I18n.t('referralScreen.title')} />
      <ImageBackground
        source={ReferralScreenImgBg}
        style={styles.backgroundImage}>
        <Content>
          <View style={styles.fullContainer}>
            <Text style={styles.informationHighlightText}>
              {I18n.t('referralScreen.informationHighlight')}
            </Text>
            <Text style={styles.informationComplementerText}>
              {I18n.t('referralScreen.informationComplementer')}
            </Text>
          </View>
          <View style={styles.shareReferralCodeContainer}>
            <Text style={styles.shareReferralCodeText}>
              {I18n.t('referralScreen.shareYourReferralCode')}
            </Text>
            <Gap height={hp(1)} />
            <View style={styles.referralCodeContainer}>
              <Gap width={wp(3)} />
              <Text style={styles.referralCodeText}>
                {userDetails?.arcNumber}
              </Text>
              <Button
                icon={
                  <SvgWrapper
                    component={<CopyIcon />}
                    width={wp(4)}
                    height={hp(2)}
                  />
                }
                onPress={onCopy}
              />
              <Gap width={wp(1.5)} />
              <Touchable style={styles.shareBtnContainer} onPress={onShare}>
                <Text style={styles.shareText}>
                  {I18n.t('referralScreen.share')}
                </Text>
              </Touchable>
            </View>
          </View>
          <Gap height={hp(5)} />
        </Content>
      </ImageBackground>
    </Container>
  );
};

export default ReferralScreen;

const styles = StyleSheet.create({
  backgroundImage: {
    flex: 1,
    resizeMode: 'cover',
  },
  fullContainer: {
    flex: 1,
  },
  informationHighlightText: {
    fontSize: fonts.size[14],
    color: colors.blue2,
    fontFamily: fonts.primary.bold,
    textAlign: 'center',
  },
  informationComplementerText: {
    fontSize: fonts.size[14],
    color: colors.blue2,
    fontFamily: fonts.primary.normal,
    textAlign: 'center',
  },
  shareReferralCodeContainer: {
    backgroundColor: colors.white,
    alignSelf: 'center',
    paddingVertical: 10,
    paddingHorizontal: hp(2),
    borderRadius: 5,
  },
  shareReferralCodeText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    color: colors.blue5,
  },
  referralCodeContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: colors.blue5,
  },
  referralCodeText: {
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.normal,
    flex: 1,
  },
  shareBtnContainer: {
    backgroundColor: colors.blue5,
  },
  shareText: {
    color: colors.white,
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[12],
    paddingVertical: hp(1),
    paddingHorizontal: hp(1),
  },
});
