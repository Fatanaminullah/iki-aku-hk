import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {colors, fonts} from '../../../utilities';
import {Gap} from '../../atoms';

const RoundedButton = ({title, onPress, icon, iconGap, size, color}) => {
  if (title) {
    return (
      <View style={{alignItems: 'center'}}>
        <TouchableOpacity
          style={styles.container(size, color)}
          onPress={onPress}>
          {icon}
        </TouchableOpacity>
        <Gap width={iconGap} />
        <Text style={styles.text}>{title}</Text>
      </View>
    );
  }
  return (
    <TouchableOpacity onPress={onPress} style={styles.container(size, color)}>
      {icon}
    </TouchableOpacity>
  );
};

export default RoundedButton;

const styles = StyleSheet.create({
  container: (size, color) => ({
    alignItems: 'center',
    justifyContent: 'center',
    width: size,
    height: size,
    borderRadius: 100,
    backgroundColor: color,
    marginHorizontal: 15,
  }),
  text: {
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[14],
    color: colors.black,
  },
});
