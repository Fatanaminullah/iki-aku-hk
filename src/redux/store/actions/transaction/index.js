import I18n from 'react-native-redux-i18n';
import Services from '../../../../services';
import {dateToString, thousandSeparator} from '../../../../utilities';
import {transactionTypes} from '../../action-types';
import {
  trxDetailsMapper,
  trxStatusMapper,
  trxSummaryMapper,
  trxTypeMapper,
} from './mapper';

const {
  SET_TRX_SUMMARY,
  SET_TRX_DETAILS,
  SET_COUPON,
  SET_TRANSFER_DATA,
  SET_SOURCE_OF_FUNDS,
  SET_RATE_DATA,
  SET_TRX_HISTORY_LIST,
} = transactionTypes;

const setTrxSummary = (payload) => ({
  type: SET_TRX_SUMMARY,
  payload,
});

const setTrxDetails = (payload) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({
      type: SET_TRX_DETAILS,
      payload,
    });
    resolve(payload);
  });
};

// =========================== PPOB ================================= //
const checkBill = (
  productName,
  request,
  showLoading,
  dismissLoading,
  showAlertError,
) => (dispatch) => {
  return new Promise(async (resolve, reject) => {
    const data = trxSummaryMapper(productName);
    dispatch(setTrxSummary(data));
    resolve(data);
  });
};

const checkout = (
  productName,
  request,
  showLoading,
  dismissLoading,
  showAlertError,
) => (dispatch) => {
  return new Promise(async (resolve, reject) => {
    const data = trxDetailsMapper(productName);
    dispatch(setTrxDetails(data));
    resolve(data);
  });
};

// =========================== REMITTANCE ================================= //

const setCoupon = (item) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({
      type: SET_COUPON,
      payload: item,
    });
    resolve(item);
  });
};

const setRateData = (payload) => ({
  type: SET_RATE_DATA,
  payload,
});

const setSourceOfFunds = (payload) => ({
  type: SET_SOURCE_OF_FUNDS,
  payload,
});

const setTransferData = (item) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch({
      type: SET_TRANSFER_DATA,
      payload: item,
    });
    resolve(item);
  });
};

const setDetailTransaction = (data, type) => (dispatch) => {
  return new Promise((resolve, reject) => {
    const result = trxDetailsMapper(type, data);
    dispatch(setTrxDetails(result));
    resolve(result);
  });
};

const setTrxHistoryList = (payload) => ({
  type: SET_TRX_HISTORY_LIST,
  payload,
});

const getRateData = (request, showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Get('master/rate', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
          dispatch(setRateData(res.data.values));
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const getSourceOfFunds = (showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Get('master/fundingSource')
      .then((res) => {
        if (res?.data?.values) {
          const result = res.data.values.rows.map((item) => ({
            label: I18n.t(
              `transferScreen.${item.nama.toLowerCase().replace(/\s/g, '')}`,
            ),
            value: item.id,
          }));
          result.push({
            label: I18n.t('transferScreen.others'),
            value: 'others',
          });
          dispatch(setSourceOfFunds(result));
          resolve(result);
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const getTransactionHistoryList = (
  request,
  showLoading,
  dismissLoading,
  showAlertError,
) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Post('users/order-history', request)
      .then((res) => {
        if (res?.data?.values && res.data.values.length) {
          const result = res.data.values.map((item) => ({
            ...item,
            type: trxTypeMapper(0).label,
            status: trxStatusMapper(item.statusCode).label,
            trxDate: dateToString(item.paymentDate, 6),
            amount: `HKD${thousandSeparator(item.amount)}`,
          }));
          resolve(result);
          dispatch(setTrxHistoryList(result));
        } else {
          resolve([]);
          dispatch(setTrxHistoryList([]));
        }
      })
      .catch((err) => {
        reject(err);
        dispatch(setTrxHistoryList([]));
        showAlertError &&
          dispatch(
            showAlertError('', err?.response?.data?.message || err.message),
          );
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const createTransaction = (request, showLoading, dismissLoading) => (
  dispatch,
) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Post('ecpay/create_trx', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
        } else {
          reject(res);
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const getTrxByPaymentCode = (request, showLoading, dismissLoading) => (
  dispatch,
) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Get('ecpay/order', request)
      .then((res) => {
        console.log('res order', res.data);
        if (res?.data?.values) {
          resolve(res.data.values);
          dispatch(setDetailTransaction(res.data.values, 'TRANSFER'));
        } else {
          reject(res);
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

// =========================== VOUCHER GAMES ================================= //

const checkPrice = (
  productName,
  request,
  showLoading,
  dismissLoading,
  showAlertError,
) => (dispatch) => {
  return new Promise(async (resolve, reject) => {
    const data = trxSummaryMapper(productName);
    dispatch(setTrxSummary(data));
    resolve(data);
  });
};
// =========================== DUMMY ================================= //

const payTrxDummy = (request, showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Post('ecpay/return_url_dummy', request)
      .then((res) => {
        console.log('res dummy', res.data);
        if (res?.data?.values) {
          resolve(res.data.values);
        } else {
          reject(res);
        }
      })
      .catch((err) => {
        console.log('err dummy', err?.response?.data);
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

export {
  checkBill,
  checkout,
  setCoupon,
  setTransferData,
  setTrxDetails,
  setDetailTransaction,
  getRateData,
  getSourceOfFunds,
  getTransactionHistoryList,
  createTransaction,
  checkPrice,
  getTrxByPaymentCode,
  payTrxDummy,
};
