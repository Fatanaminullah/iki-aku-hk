import React from 'react';
import I18n from '../../utilities/i18n';
import {useDispatch} from 'react-redux';
import {StyleSheet} from 'react-native';
import {
  Header,
  Container,
  Content,
  Form,
  Field,
  Input,
  Button,
  Gap,
} from '../../components';
import {validation, hp, PPOB} from '../../utilities';
import {transactionActions} from '../../redux/store/actions';

const {checkBill} = transactionActions;

const {required} = validation;

const PlnPostpaidScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const onSubmit = (data) => {
    dispatch(checkBill(PPOB.PLN_POSTPAID)).then((res) => {
      navigation.navigate('TransactionSummaryScreen', {
        title: I18n.t('plnPostpaidScreen.title'),
        productName: PPOB.PLN_POSTPAID,
      });
    });
  };
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        title={I18n.t('plnPostpaidScreen.checkInvoice')}
        rounded
        onPress={handleSubmit}
      />
    );
  };
  return (
    <Container>
      <Header title={I18n.t('plnPostpaidScreen.title')} />
      <Content>
        <Form onSubmit={onSubmit} submitComponent={renderSubmitComponent}>
          <Field name="customerId" validation={{required}}>
            <Input
              rounded
              placeholder={I18n.t('plnPostpaidScreen.customerId')}
              keyboardType="numeric"
            />
          </Field>
          <Gap height={hp(2)} />
        </Form>
      </Content>
    </Container>
  );
};

export default PlnPostpaidScreen;

const styles = StyleSheet.create({});
