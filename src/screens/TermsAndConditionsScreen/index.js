import React from 'react';
import I18n from 'react-native-redux-i18n';
import {StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Header, Container, Content, Gap, Button} from '../../components';
import {colors, fonts, hp} from '../../utilities';
import {tnc} from './tncText';

const TermsAndConditionsScreen = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header title={I18n.t('termsAndConditionsScreen.title')} />
      <Content>
        <Text style={styles.title}>
          {I18n.t('termsAndConditionsScreen.title')}
        </Text>
        <Gap height={hp(2)} />
        {tnc.map((item) => (
          <Text style={styles.paragraph}>{item}</Text>
        ))}
      </Content>
      <View style={styles.buttonContainer}>
        <Button
          type="danger"
          title={I18n.t('termsAndConditionsScreen.ok')}
          style={styles.button}
          onPress={() => navigation.goBack()}
        />
      </View>
    </Container>
  );
};

export default TermsAndConditionsScreen;

const styles = StyleSheet.create({
  title: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[20],
    color: colors.red1,
    textAlign: 'center',
  },
  paragraph: {
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[14],
    color: colors.black,
    marginVertical: 5,
  },
  buttonContainer: {
    alignItems: 'center',
    padding: hp(2),
    marginTop: hp(-2),
  },
  button: {
    borderRadius: 50,
    height: hp(5),
    width: hp(43),
  },
});
