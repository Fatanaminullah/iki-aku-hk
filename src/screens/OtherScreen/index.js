import React from 'react';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';
import {
  Container,
  Content,
  Gap,
  Button,
  Touchable,
  SvgWrapper,
  InputPin,
} from '../../components';
import {Separator, List, ListItem, Left, Right, Icon} from 'native-base';
import {
  hp,
  wp,
  colors,
  fonts,
  removeData,
  unregisterUserId,
} from '../../utilities';
import {
  NoProfilePictIcon,
  EditIcon,
  FlagIndoRectanglePng,
  FlagUkRectangle,
  CheckListIcon,
  MoreIcon,
} from '../../assets';

import {
  localeActions,
  loadingActions,
  alertActions,
  authenticationActions,
} from '../../redux/store/actions';
import {showAlertConfirmation} from '../../redux/store/actions/alert';
import SimpleToast from 'react-native-simple-toast';

const {changeLanguage} = localeActions;
const {showLoading, dismissLoading} = loadingActions;
const {showAlertError, showAlertSuccess} = alertActions;
const {logout} = authenticationActions;

const OtherScreen = ({navigation}) => {
  const {userDetails} = useSelector((state) => state.profile);
  const {language} = useSelector((state) => state.locale);
  const dispatch = useDispatch();

  const setLanguage = (locale) => {
    dispatch(changeLanguage(locale)).then(() => {
      SimpleToast.show(
        I18n.t('otherScreen.changeLanguageSuccess'),
        SimpleToast.LONG,
      );
      navigation.replace('MainApp', {screen: 'Home'});
    });
  };

  const listIkiAkuMenu = [
    {title: I18n.t('otherScreen.help'), screen: 'HelpScreen'},
    {title: I18n.t('otherScreen.aboutUs'), screen: 'AboutUsScreen'},
    {
      title: I18n.t('otherScreen.termsAndConditions'),
      screen: 'TermsAndConditionsScreen',
    },
    {title: I18n.t('otherScreen.privacyPolicy'), screen: 'PrivacyPolicyScreen'},
  ];

  const listSecurity = [
    {title: I18n.t('otherScreen.transactionPin'), screen: 'ChangePinScreen'},
    {title: I18n.t('otherScreen.password'), screen: 'ChangePasswordScreen'},
  ];

  const renderChangeLanguageButton = (locale) => {
    if (locale === 'id') {
      return (
        <Touchable
          key={locale}
          style={[styles.changeLanguageButton, styles.activeLocaleId(language)]}
          onPress={() => setLanguage(locale)}>
          <View style={styles.rowContainer}>
            <Image style={styles.flagImg} source={FlagIndoRectanglePng} />
            <Text>{I18n.t(`locale.${locale}`)}</Text>
          </View>
          {language === 'id' && (
            <SvgWrapper
              component={<CheckListIcon />}
              width={wp(8)}
              height={hp(4)}
            />
          )}
        </Touchable>
      );
    }
    return (
      <Touchable
        key={locale}
        style={[styles.changeLanguageButton, styles.activeLocaleEn(language)]}
        onPress={() => setLanguage(locale)}>
        <View style={styles.rowContainer}>
          <Image style={styles.flagImg} source={FlagUkRectangle} />
          <Text>{I18n.t(`locale.${locale}`)}</Text>
        </View>
        {language === 'en' && (
          <SvgWrapper
            component={<CheckListIcon />}
            width={wp(8)}
            height={hp(4)}
          />
        )}
      </Touchable>
    );
  };

  const renderIkiAkuButton = (item, index) => (
    <ListItem
      key={index}
      style={styles.ikiAkuButton}
      onPress={() => navigation.navigate(item.screen)}>
      <Left>
        <Text>{item.title}</Text>
      </Left>
      <Right>
        <SvgWrapper component={<MoreIcon />} width={wp(2)} height={wp(2)} />
      </Right>
    </ListItem>
  );

  const onLogout = async () => {
    dispatch(
      showAlertConfirmation(
        '',
        I18n.t('otherScreen.logoutWarning'),
        async () => {
          await removeData('token');
          unregisterUserId();
          navigation.reset({
            index: 0,
            routes: [{name: 'WalkthroughScreen'}],
          });
        },
        '',
        I18n.t('otherScreen.yes'),
        I18n.t('otherScreen.no'),
      ),
    );
  };
  return (
    <Container>
      <Content noPadding>
        <View style={styles.editPhotoProfileContainer}>
          <View style={styles.rowContainer2}>
            {!userDetails?.arcPhoto?.photo_profile ? (
              <SvgWrapper
                style={[styles.profileIcon]}
                component={<NoProfilePictIcon />}
              />
            ) : (
              <Image
                source={{uri: userDetails?.arcPhoto?.photo_profile}}
                style={styles.profileImg}
              />
            )}
            <Gap width={wp(5)} />
            <Text numberOfLines={1} style={styles.profileNameText}>
              {userDetails?.fullName}
            </Text>
            <Button
              icon={
                <SvgWrapper component={<EditIcon />} style={styles.iconStyle} />
              }
              onPress={() => navigation.navigate('EditProfileScreen')}
            />
          </View>
        </View>
        <View>
          <Separator bordered>
            <Text>{I18n.t('otherScreen.language')}</Text>
          </Separator>
          {renderChangeLanguageButton('id')}
          {renderChangeLanguageButton('en')}
        </View>
        <View>
          <Separator bordered>
            <Text>{I18n.t('otherScreen.security')}</Text>
          </Separator>
          <List>
            {listSecurity.map((item, index) => renderIkiAkuButton(item, index))}
          </List>
        </View>
        <View>
          <Separator bordered>
            <Text>IKI Aku</Text>
          </Separator>
          <List>{listIkiAkuMenu.map((item) => renderIkiAkuButton(item))}</List>
          <Separator bordered />
        </View>
        <TouchableOpacity style={styles.logoutButton} onPress={onLogout}>
          <Text style={styles.logoutText}>{I18n.t('otherScreen.logout')}</Text>
        </TouchableOpacity>
        <Separator />
      </Content>
    </Container>
  );
};

export default OtherScreen;

const styles = StyleSheet.create({
  editPhotoProfileContainer: {
    backgroundColor: colors.blue4,
    height: hp(18),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: hp(2),
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rowContainer2: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: hp(2),
    justifyContent: 'space-around',
  },
  profileNameText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    color: colors.white,
    textTransform: 'uppercase',
    width: hp(29),
  },
  changeLanguageButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: hp(8),
    paddingHorizontal: 20,
    marginHorizontal: 10,
    marginVertical: 5,
  },
  activeLocaleId: (language) => ({
    backgroundColor: language === 'id' ? colors.green2 : null,
    borderRadius: 30,
  }),
  activeLocaleEn: (language) => ({
    backgroundColor: language === 'en' ? colors.green2 : null,
    borderRadius: 30,
  }),
  flagImg: {
    width: hp(5),
    height: hp(3.5),
    marginRight: hp(2),
    borderWidth: 1,
    borderColor: colors.grey2,
  },
  ikiAkuButton: {
    height: hp(8),
    borderBottomColor: colors.grey2,
    borderBottomWidth: 1,
    marginRight: hp(2.1),
  },
  ikiAkuButtonPin: {
    height: hp(8),
    borderBottomColor: colors.grey2,
    borderBottomWidth: 1,
    marginRight: hp(2.1),
    marginLeft: hp(2.1),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: hp(2.1),
  },
  logoutButton: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: hp(3),
  },
  logoutText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    color: colors.red1,
  },
  iconStyle: {
    width: hp(3),
    height: hp(3),
    marginLeft: hp(2),
  },
  profileIcon: {
    width: hp(9),
    height: hp(9),
    marginLeft: hp(1),
  },
  profileImg: {
    width: hp(9),
    height: hp(9),
    marginLeft: hp(1),
    borderRadius: 100,
  },
});
