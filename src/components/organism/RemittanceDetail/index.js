import moment from 'moment';
import React, {useState} from 'react';
import Countdown from 'react-countdown';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch} from 'react-redux';
import {
  showAlertConfirmation,
  showAlertSuccess,
} from '../../../redux/store/actions/alert';
import {
  dismissLoading,
  showLoading,
} from '../../../redux/store/actions/loading';
import {
  getTrxByPaymentCode,
  payTrxDummy,
} from '../../../redux/store/actions/transaction';
import {colors, fonts, hp, TRANSACTION_STATUS, wp} from '../../../utilities';
import {Gap} from '../../atoms';
import {
  Button,
  ProgressStep,
  ProgressSteps,
  TransactionDetailsRemittance,
} from '../../molecules';
import {STAGING} from '../../../environments/env.json';

const RemittanceDetail = ({trxDetails}) => {
  const dispatch = useDispatch();
  const [status, setStatus] = useState(trxDetails.status);
  const [error, setError] = useState(true);
  const a = moment(trxDetails?.expire, 'DD MMM YYYY HH:mm:ss').toISOString();
  const b = moment(new Date());
  const [timer] = useState({
    timer: Date.now() + (Date.parse(a) - Date.parse(b)),
    startTimer: true,
    key: null,
  });
  const abortTrx = () => {
    dispatch(
      showAlertSuccess('', I18n.t('transactionDetailsScreen.abortDone')),
    );
    setStatus(TRANSACTION_STATUS.EXPIRED_ORDER);
  };
  const timerRenderer = ({
    hours,
    minutes,
    seconds,
    completed,
    formatted,
    ...rest
  }) => {
    /** timeout / waktu habis */
    if (completed) {
      setStatus(TRANSACTION_STATUS.EXPIRED_ORDER);
      return (
        <View style={styles.blackBox}>
          <Text style={styles.timerTextExpired}>
            {I18n.t('transactionDetailsScreen.expired')}
          </Text>
        </View>
      );
    } else {
      return (
        <View style={styles.blackBox}>
          <Text style={styles.timerText1}>
            {I18n.t('transactionDetailsScreen.expiryTimer')}
          </Text>
          <Text style={styles.timerText2}>
            {formatted.hours} : {formatted.minutes} : {formatted.seconds}
          </Text>
        </View>
      );
    }
  };
  const stepperStatusMapper = (data) => {
    switch (data) {
      case TRANSACTION_STATUS.WAITING_PAYMENT:
        return 0;
      case TRANSACTION_STATUS.ON_PROCESS:
        return 1;
      case TRANSACTION_STATUS.COMPLETED:
        return 2;
      default:
        return 0;
    }
  };
  let stepperStatus = stepperStatusMapper(status);
  return (
    <View style={styles.flex}>
      <View style={styles.contentStyle}>
        {status !== TRANSACTION_STATUS.EXPIRED_ORDER ? (
          <ProgressSteps
            borderWidth={1.7}
            activeStep={stepperStatus}
            isComplete={status === 4}
            labelColor={colors.grey1}
            activeLabelColor={colors.red1}
            completedLabelColor={colors.red1}
            labelFontFamily={fonts.primary.bold}
            activeLabelFontSize={fonts.size[12]}
            labelFontSize={fonts.size[12]}
            disabledStepIconColor={colors.grey4}
            completedStepIconColor={colors.red1}
            progressBarColor={colors.grey4}
            completedProgressBarColor={colors.red1}
            activeStepNumColor={colors.white}
            activeStepIconColor={colors.red1}
            activeStepIconBorderColor={colors.red1}>
            <ProgressStep
              onNext={() => {
                dispatch(
                  getTrxByPaymentCode(
                    {paymentNo: trxDetails.paymentCode},
                    showLoading,
                    dismissLoading,
                  ),
                ).then((res) => {
                  if (res?.statusCode === TRANSACTION_STATUS.WAITING_PAYMENT) {
                    setError(true);
                  } else {
                    setError(false);
                    setStatus(TRANSACTION_STATUS.ON_PROCESS);
                  }
                });
              }}
              errors={error}
              removeBtnRow
              label={I18n.t('transactionDetailsScreen.awaitingPayment')}>
              <TransactionDetailsRemittance
                containerStyle={styles.detailContainer}
                trxItem={trxDetails?.awaitingPayment}
                trxDetails={trxDetails}
              />
              <Button
                title={I18n.t('transactionDetailsScreen.cancelTrx')}
                style={styles.abortButton}
                textStyle={styles.abortLabel}
                onPress={() => {
                  dispatch(
                    showAlertConfirmation(
                      '',
                      I18n.t('transactionDetailsScreen.abortQuestion'),
                      () => abortTrx(),
                      '',
                      I18n.t('transactionDetailsScreen.yes'),
                      I18n.t('transactionDetailsScreen.no'),
                      null,
                      3000,
                    ),
                  );
                }}
              />
              {STAGING === 'DEVELOPMENT' && (
                <Button
                  title="Pay Dummy"
                  style={styles.abortButton}
                  textStyle={styles.abortLabel}
                  onPress={() => {
                    dispatch(
                      payTrxDummy(
                        {
                          merchantTradeNo: trxDetails.merchantTradeNo,
                        },
                        showLoading,
                        dismissLoading,
                      ),
                    );
                  }}
                />
              )}
            </ProgressStep>
            <ProgressStep
              onNext={() => {
                setError(true);
                dispatch(
                  getTrxByPaymentCode(
                    {paymentNo: trxDetails.paymentCode},
                    showLoading,
                    dismissLoading,
                  ),
                ).then((res) => {
                  if (res?.statusCode === TRANSACTION_STATUS.ON_PROCESS) {
                    setError(true);
                  } else {
                    setError(false);
                    setStatus(TRANSACTION_STATUS.COMPLETED);
                  }
                });
              }}
              errors={error}
              removeBtnRow
              label={I18n.t('transactionDetailsScreen.onProcess')}>
              <TransactionDetailsRemittance
                containerStyle={styles.detailContainer}
                trxItem={trxDetails?.onProcess}
                trxDetails={trxDetails}
              />
            </ProgressStep>
            <ProgressStep
              onNext={false}
              removeBtnRow
              label={I18n.t('transactionDetailsScreen.completed')}>
              <TransactionDetailsRemittance
                containerStyle={styles.detailContainer}
                trxItem={trxDetails?.completed}
                trxDetails={trxDetails}
              />
            </ProgressStep>
          </ProgressSteps>
        ) : (
          <ScrollView showsVerticalScrollIndicator={false}>
            <Gap height={hp(2.5)} />
            <TransactionDetailsRemittance
              containerStyle={styles.detailContainer}
              trxItem={trxDetails?.awaitingPayment}
              trxDetails={trxDetails}
            />
          </ScrollView>
        )}
      </View>
      {(status === TRANSACTION_STATUS.WAITING_PAYMENT ||
        status === TRANSACTION_STATUS.EXPIRED_ORDER) && (
        <>
          <Countdown
            key={timer.key}
            date={timer.timer}
            zeroPadTime={2}
            intervalDelay={1000}
            renderer={timerRenderer}
            autoStart={timer.startTimer}
            daysInHours
          />
          <Gap height={hp(5)} />
        </>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
  contentStyle: {
    flex: 1,
    backgroundColor: colors.grey6,
    marginHorizontal: 14,
    marginTop: 20,
    paddingBottom: 10,
    marginBottom: 10,
    paddingHorizontal: 14,
    borderTopRightRadius: 10,
    borderTopLeftRadius: 10,
  },
  detailContainer: {
    backgroundColor: colors.white,
    paddingHorizontal: wp(2),
    paddingBottom: hp(1),
    borderRadius: 10,
  },
  abortQuestion: {
    textAlign: 'center',
    fontFamily: fonts.primary.normal,
  },
  abortButton: {
    backgroundColor: 'transparent',
    marginBottom: 20,
    borderRadius: 0,
  },
  abortLabel: {
    color: colors.red1,
  },
  blackBox: {
    height: hp(7),
    position: 'absolute',
    bottom: 0,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: wp(5),
    paddingVertical: hp(2),
    backgroundColor: colors.black,
  },
  timerText1: {
    color: colors.grey4,
    fontFamily: fonts.primary[600],
  },
  timerTextExpired: {
    display: 'flex',
    flex: 1,
    textAlign: 'center',
    color: colors.grey4,
    fontFamily: fonts.primary[700],
  },
  timerText2: {
    color: colors.grey2,
    fontSize: fonts.size[16],
    fontFamily: fonts.primary.bold,
  },
});

export default RemittanceDetail;
