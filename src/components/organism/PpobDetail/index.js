import React from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {colors, fonts, hp} from '../../../utilities';
import {Gap} from '../../atoms';
import {KeyValueItem, ProgressStep, ProgressSteps} from '../../molecules';

const PpobDetail = ({trxDetails}) => {
  const renderItem = ({item, index}) => {
    if (item.title === 'Total Bayar') {
      return (
        <KeyValueItem
          index={index}
          title={item.title}
          value={item.value}
          size={fonts.size[12]}
          valueBold
          titleBold
          gap={hp(2)}
        />
      );
    }
    return (
      <KeyValueItem
        index={index}
        title={item.title}
        value={item.value}
        size={fonts.size[12]}
        gap={hp(2)}
      />
    );
  };
  return (
    <View style={styles.flex}>
      <ProgressSteps
        activeLabelColor={colors.red1}
        labelFontFamily={fonts.primary.bold}
        activeLabelFontSize={fonts.size[12]}
        labelFontSize={fonts.size[12]}
        completedStepIconColor={colors.red1}
        completedProgressBarColor={colors.red1}
        activeStepNumColor={colors.white}
        activeStepIconColor={colors.red1}
        activeStepIconBorderColor={colors.red1}>
        <ProgressStep
          removeBtnRow
          label={I18n.t('transactionDetailsScreen.awaitingPayment')}>
          <View>
            <Text style={styles.paymentCodeTitle}>
              {I18n.t('transactionDetailsScreen.paymentCode')}:
            </Text>
            <Text style={styles.paymentCodeText}>{trxDetails.paymentCode}</Text>
            <Gap height={hp(4)} />
            <Text style={styles.transactionDetailsTitle}>
              {I18n.t('transactionDetailsScreen.transactionDetails')}
            </Text>
            <Gap height={hp(2)} />
            <FlatList
              data={trxDetails?.awaitingPayment}
              showsVerticalScrollIndicator={false}
              keyboardShouldPersistTaps="handled"
              renderItem={renderItem}
            />
          </View>
        </ProgressStep>
        <ProgressStep
          removeBtnRow
          label={I18n.t('transactionDetailsScreen.onProcess')}>
          <View>
            <Text style={styles.transactionDetailsTitle}>
              {I18n.t('transactionDetailsScreen.transactionDetails')}
            </Text>
            <Gap height={hp(2)} />
            {trxDetails?.onProcess.map((item, index) => {
              if (item.title === 'Total Bayar') {
                return (
                  <KeyValueItem
                    index={index}
                    title={item.title}
                    value={item.value}
                    size={fonts.size[12]}
                    valueBold
                    titleBold
                    gap={hp(2)}
                  />
                );
              }
              return (
                <KeyValueItem
                  index={index}
                  title={item.title}
                  value={item.value}
                  size={fonts.size[12]}
                  gap={hp(2)}
                />
              );
            })}
          </View>
        </ProgressStep>
        <ProgressStep
          removeBtnRow
          label={I18n.t('transactionDetailsScreen.completed')}>
          <View>
            <Text style={styles.transactionDetailsTitle}>
              {I18n.t('transactionDetailsScreen.transactionDetails')}
            </Text>
            <Gap height={hp(2)} />
            {trxDetails?.completed.map((item, index) => {
              if (item.title === 'Total Bayar') {
                return (
                  <KeyValueItem
                    index={index}
                    title={item.title}
                    value={item.value}
                    size={fonts.size[12]}
                    valueBold
                    titleBold
                    gap={hp(2)}
                  />
                );
              }
              return (
                <KeyValueItem
                  index={index}
                  title={item.title}
                  value={item.value}
                  size={fonts.size[12]}
                  gap={hp(2)}
                />
              );
            })}
          </View>
        </ProgressStep>
      </ProgressSteps>
    </View>
  );
};

export default PpobDetail;

const styles = StyleSheet.create({
  flex: {
    flex: 1,
    padding: 10,
  },
  paymentCodeTitle: {
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[14],
    textAlign: 'center',
  },
  paymentCodeText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    textAlign: 'center',
  },
  transactionDetailsTitle: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
  },
});
