import {Item, Label} from 'native-base';
import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import DatePicker from 'react-native-datepicker';
import {CalendarIcon, CloseIcon} from '../../../assets';
import {Gap, SvgWrapper} from '../../atoms';
import {colors, fonts, hp, wp} from '../../../utilities';

const MyDatePicker = ({
  label,
  disabled,
  placeholder,
  value,
  error,
  onDateChange,
  onChange,
  onClearDate,
  style,
  customStyles,
  containerStyle,
  maxDate,
  minDate,
  setValue,
  name,
  rounded,
  showIcon,
  androidMode,
  format = 'DD MMM YYYY',
}) => {
  return (
    <View>
      {label && <Label style={styles.label}>{label}</Label>}
      <Item
        style={[styles.inputContainer(error), containerStyle]}
        rounded={rounded}>
        <DatePicker
          style={[styles.input, style]}
          iconComponent={
            <View style={styles.icon}>
              {value ? (
                <TouchableOpacity
                  onPress={
                    onClearDate ? onClearDate : () => setValue(name, '')
                  }>
                  <SvgWrapper
                    component={<CloseIcon />}
                    width={wp(3)}
                    height={hp(2)}
                  />
                </TouchableOpacity>
              ) : null}
              <Gap width={10} />
              <SvgWrapper
                component={<CalendarIcon />}
                width={hp(4)}
                height={hp(4)}
              />
            </View>
          }
          showIcon={showIcon ? true : false}
          TouchableComponent={TouchableOpacity}
          date={value}
          mode="date"
          androidMode={androidMode || 'default'}
          placeholder={placeholder}
          format={format}
          confirmBtnText="Konfirmasi"
          cancelBtnText="Batal"
          customStyles={{
            dateInput: {
              borderWidth: 0,
              alignItems: 'flex-start',
            },
            date: {
              backgroundColor: 'red',
            },
            dateText: {
              textAlign: 'left',
            },
          }}
          onDateChange={(val) => {
            if (onChange) {
              onChange(val);
            }
            if (onDateChange) {
              onDateChange(val);
            }
          }}
          disabled={disabled}
          maxDate={maxDate}
          minDate={minDate}
        />
      </Item>
    </View>
  );
};

export default MyDatePicker;

const styles = StyleSheet.create({
  inputContainer: (error) => ({
    borderWidth: error ? 1 : 0,
    borderColor: error ? colors.red1 : colors.grey2,
    paddingHorizontal: 15,
  }),
  input: {
    flex: 1,
    height: hp(6),
    marginLeft: hp(0.5),
  },
  label: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
    color: colors.black,
  },
  icon: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: hp(0.7),
  },
});
