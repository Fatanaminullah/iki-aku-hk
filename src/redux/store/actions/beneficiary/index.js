import I18n from 'react-native-redux-i18n';
import Services from '../../../../services';
import {beneficiaryTypes} from '../../action-types';

const {
  SET_LIST_BENEFICIARY,
  SET_DETAIL_BENEFICIARY,
  SET_LIST_BANK,
  SET_LIST_RECENT_BENEFICIARY,
} = beneficiaryTypes;

const setListBeneficiary = (payload) => ({
  type: SET_LIST_BENEFICIARY,
  payload,
});
const setListRecentBeneficiary = (payload) => ({
  type: SET_LIST_RECENT_BENEFICIARY,
  payload,
});
const setListBank = (payload) => ({
  type: SET_LIST_BANK,
  payload,
});

const getListBeneficiary = (showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Get('beneficiary')
      .then((res) => {
        if (res?.data?.values?.length) {
          const result = res.data?.values?.map((item) => ({
            ...item,
            bankName: `${item?.bankCode} ${
              item?.bankCode && item?.bankName && '-'
            } ${item?.bankName}`,
          }));
          resolve(result);
          dispatch(setListBeneficiary(result));
        } else {
          reject(res);
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};
const getListRecentBeneficiary = (showLoading, dismissLoading) => (
  dispatch,
) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Get('beneficiary/recent')
      .then((res) => {
        if (res?.data?.values.length) {
          const result = res.data?.values?.map((item) => ({
            ...item,
            bankName: `${item?.bankCode} ${
              item?.bankCode && item?.bankName && '-'
            } ${item?.bankName}`,
          }));
          resolve(result);
          dispatch(setListRecentBeneficiary(result));
        } else {
          dispatch(setListRecentBeneficiary([]));
          reject(res?.data?.values);
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const addBeneficiary = (
  request,
  showLoading,
  dismissLoading,
  showAlertError,
  showAlertSuccess,
) => (dispatch) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    Services.Post('beneficiary', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
          dispatch(
            showAlertSuccess('', I18n.t('beneficiaryFormScreen.successAdd')),
          );
        } else {
          reject();
        }
      })
      .catch((err) => {
        dispatch(
          showAlertError('', err?.response?.data?.message || err.message),
        );
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const editBeneficiary = (
  request,
  showLoading,
  dismissLoading,
  showAlertError,
  showAlertSuccess,
) => (dispatch) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    Services.Put(`beneficiary/${request.id}`, request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
          dispatch(
            showAlertSuccess('', I18n.t('beneficiaryFormScreen.successEdit')),
          );
        } else {
          reject();
        }
      })
      .catch((err) => {
        dispatch(showAlertError('', err.message));
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const deleteBeneficiary = (
  id,
  showLoading,
  dismissLoading,
  showAlertError,
  showAlertSuccess,
) => (dispatch) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    Services.Delete(`beneficiary?id=${id}`)
      .then((res) => {
        if (res?.data?.values) {
          dispatch(
            showAlertSuccess(
              '',
              I18n.t('detailBeneficiaryScreen.deleteSuccess'),
            ),
          );
          resolve(res);
        } else {
          reject();
        }
      })
      .catch((err) => {
        dispatch(showAlertError('', err.message));
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const setDetailBeneficiary = (data, list) => (dispatch) => {
  return new Promise((resolve, reject) => {
    const payload = list.filter((item) => data.id === item.id)[0];
    if (payload) {
      dispatch({
        type: SET_DETAIL_BENEFICIARY,
        payload,
      });
      resolve(payload);
    } else {
      reject();
    }
  });
};

const getListBank = (showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Get('master/bank')
      .then((res) => {
        if (res?.data?.values) {
          const result = res.data.values.rows.map((item) => ({
            label: `${item?.bank_code} - ${item?.bank_name}`,
            value: `${item?.bank_id}`,
          }));
          resolve(result);
          dispatch(setListBank(result));
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dispatch(dismissLoading());
      });
  });
};

const checkAccount = (request, showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Post('beneficiary/checkAccount', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

export {
  setDetailBeneficiary,
  getListBeneficiary,
  getListRecentBeneficiary,
  getListBank,
  addBeneficiary,
  deleteBeneficiary,
  editBeneficiary,
  checkAccount,
};
