import {alertActionTypes} from '../../action-types';
const {
  SHOW_ALERT_SUCCESS,
  HIDE_ALERT_SUCCESS,
  SET_TITLE_SUCCESS,
  SET_TEXT_SUCCESS,
  SHOW_ALERT_ERROR,
  HIDE_ALERT_ERROR,
  SET_TITLE_ERROR,
  SET_TEXT_ERROR,
  SHOW_ALERT_CONFIRMATION,
  HIDE_ALERT_CONFIRMATION,
  SET_TITLE_CONFIRMATION,
  SET_TEXT_CONFIRMATION,
  SET_EVENT_CONFIRM_POSITIVE,
  SET_EVENT_CONFIRM_NEGATIVE,
  SET_TEXT_BUTTON_POSITIVE_CONFIRMATION,
  SET_TEXT_BUTTON_NEGATIVE_CONFIRMATION,
  SET_COMPONENT_CONFIRMATION,
  SET_TIMER_CONFIRMATION,
} = alertActionTypes;

const initState = {
  visibleAlertSuccess: false,
  visibleAlertError: false,
  visibleAlertConfirmation: false,
  titleSuccess: '',
  textSuccess: '',
  textButtonSuccess: '',
  titleError: '',
  textError: '',
  textButtonError: '',
  titleConfirmation: '',
  textConfirmation: '',
  textButtonPositiveConfirmation: '',
  textButtonNegativeConfirmation: '',
  timerConfirmation: '',
};

export default (state = initState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SHOW_ALERT_SUCCESS:
      return {...state, visibleAlertSuccess: true};
    case HIDE_ALERT_SUCCESS:
      return {
        ...state,
        visibleAlertSuccess: false,
        titleSuccess: '',
        textSuccess: '',
      };
    case SET_TITLE_SUCCESS:
      return {...state, titleSuccess: payload};
    case SET_TEXT_SUCCESS:
      return {...state, textSuccess: payload};
    case SHOW_ALERT_ERROR:
      return {...state, visibleAlertError: true};
    case HIDE_ALERT_ERROR:
      return {
        ...state,
        visibleAlertError: false,
        titleError: '',
        textError: '',
      };
    case SET_TITLE_ERROR:
      return {...state, titleError: payload};
    case SET_TEXT_ERROR:
      return {...state, textError: payload};
    case SHOW_ALERT_CONFIRMATION:
      return {...state, visibleAlertConfirmation: true};
    case HIDE_ALERT_CONFIRMATION:
      return {
        ...state,
        visibleAlertConfirmation: false,
        titleConfirmation: '',
        textConfirmation: '',
      };
    case SET_TITLE_CONFIRMATION:
      return {...state, titleConfirmation: payload};
    case SET_TEXT_CONFIRMATION:
      return {...state, textConfirmation: payload};
    case SET_EVENT_CONFIRM_POSITIVE:
      return {...state, eventConfirmPositive: payload};
    case SET_EVENT_CONFIRM_NEGATIVE:
      return {...state, eventConfirmNegative: payload};
    case SET_TEXT_BUTTON_POSITIVE_CONFIRMATION:
      return {...state, textButtonPositiveConfirmation: payload};
    case SET_TEXT_BUTTON_NEGATIVE_CONFIRMATION:
      return {...state, textButtonNegativeConfirmation: payload};
    case SET_COMPONENT_CONFIRMATION:
      return {...state, componentConfirmation: payload};
    case SET_TIMER_CONFIRMATION:
      return {...state, timerConfirmation: payload};
    default:
      return state;
  }
};
