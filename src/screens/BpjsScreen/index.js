import React from 'react';
import I18n from 'react-native-redux-i18n';
import {StyleSheet, Text} from 'react-native';
import {Container, Content, Header, Card, Gap} from '../../components';
import {BpjsIcon} from '../../assets';
import {fonts} from '../../utilities';

const listProductPln = [
  {title: I18n.t('bpjsScreen.bpjsKesehatan'), screen: 'BpjsKesehatanScreen'},
];

const BpjsScreen = ({navigation}) => {
  const renderButton = (item) => {
    return (
      <>
        <Card
          onPress={() => navigation.navigate(item.screen)}
          style={styles.cardContainer}>
          <Text style={styles.cardTitle}>{item.title}</Text>
          <BpjsIcon />
        </Card>
        <Gap height={15} />
      </>
    );
  };
  return (
    <Container>
      <Header title={I18n.t('bpjsScreen.title')} />
      <Content>
        {listProductPln.map((item) => {
          return renderButton(item);
        })}
      </Content>
    </Container>
  );
};

export default BpjsScreen;

const styles = StyleSheet.create({
  cardContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
  },
  cardTitle: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    textTransform: 'uppercase',
  },
});
