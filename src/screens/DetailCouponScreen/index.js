import Moment from 'moment';
import React from 'react';
import Countdown from 'react-countdown';
import {Image, StyleSheet, Text, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch} from 'react-redux';
import {CouponMoneyIcon, CouponTimeIcon} from '../../assets';
import {
  Button,
  Card,
  Container,
  Content,
  Gap,
  Header,
  SvgWrapper,
} from '../../components';
import {setCoupon} from '../../redux/store/actions/transaction';
import {colors, fonts, hp, thousandSeparator, wp} from '../../utilities';

const DetailCouponScreen = ({navigation, route}) => {
  const item = route.params.item;
  const dispatch = useDispatch();
  const renderer = ({hours, minutes, seconds, completed, ...rest}) => {
    return (
      <Text
        style={{
          fontFamily: fonts.primary.bold,
          color: colors.red1,
          fontSize: fonts.size[14],
        }}>
        {rest?.formatted?.hours} : {rest?.formatted?.minutes} :{' '}
        {rest?.formatted?.seconds}
      </Text>
    );
  };
  return (
    <Container style={styles.page}>
      <Header title={I18n.t('detailCouponScreen.title')} />
      <Content>
        <Card style={styles.cardContainer}>
          <Image
            source={{uri: `data:image/png;base64,${item?.image}`}}
            style={{width: wp(80), height: hp(20)}}
            resizeMode="contain"
          />
          <View style={styles.caption}>
            <View style={styles.column}>
              <SvgWrapper
                component={<CouponTimeIcon />}
                width={wp(8)}
                height={hp(4)}
              />
              <View style={styles.textContainer}>
                <Text style={styles.textStyle}>
                  {I18n.t('couponScreen.useBefore')}
                </Text>
                <Countdown
                  daysInHours={true}
                  date={Moment(item.expired, 'MM-DD-YYYY HH:mm:ss')}
                  intervalDelay={1000}
                  renderer={renderer}
                  autoStart={true}
                />
              </View>
            </View>
            <Gap width={wp(5)} />
            <View style={styles.column}>
              <SvgWrapper
                component={<CouponMoneyIcon />}
                width={wp(8)}
                height={hp(4)}
              />
              <View style={styles.textContainer}>
                <Text style={styles.textStyle}>
                  {I18n.t('couponScreen.minimumTransaction')}
                </Text>
                <Text
                  style={{
                    fontFamily: fonts.primary.bold,
                    color: colors.red1,
                    fontSize: fonts.size[14],
                  }}>
                  {I18n.t('couponScreen.amount', {
                    amount: thousandSeparator(item.min),
                  })}
                </Text>
              </View>
            </View>
          </View>
        </Card>
        <Card style={styles.cardDetailContainer}>
          <Text
            style={{
              fontFamily: fonts.primary.bold,
              color: colors.red1,
              fontSize: fonts.size[20],
            }}>
            Detail
          </Text>
          <Gap height={15} />
          <Text>
            Reprehenderit adipisicing sint Lorem amet dolor incididunt voluptate
            consequat in. Eu fugiat minim eu elit reprehenderit ut sint dolore
            tempor eiusmod culpa minim. Id aute culpa labore ex ipsum sunt velit
            sit ad laboris non. Ad duis est eu culpa fugiat culpa. Duis magna
            nisi do ullamco laborum ex sunt id nostrud id commodo elit.
          </Text>
        </Card>
        <Gap height={15} />
        {route.params.screen === 'Transfer' ? (
          <Button
            type="danger"
            title={I18n.t('detailCouponScreen.useLater')}
            rounded
            onPress={() =>
              dispatch(setCoupon({})).then((res) => {
                navigation.navigate('MainApp', {screen: 'Transfer'});
              })
            }
          />
        ) : (
          <Button
            title={I18n.t('detailCouponScreen.useCoupon')}
            rounded
            onPress={() =>
              dispatch(setCoupon(item)).then((res) => {
                navigation.navigate('MainApp', {screen: 'Transfer'});
              })
            }
          />
        )}
      </Content>
    </Container>
  );
};

export default DetailCouponScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.grey3,
  },
  cardContainer: {
    marginVertical: 10,
    borderWidth: 0,
    borderRadius: 0,
  },
  cardDetailContainer: {
    flex: 1,
    borderWidth: 0,
    borderRadius: 0,
  },
  caption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(80),
  },
  column: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: wp(35),
  },
  textContainer: {
    flexWrap: 'wrap',
    marginLeft: hp(1.5),
  },
  textStyle: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.primary,
  },
});
