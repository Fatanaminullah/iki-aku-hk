import React, {useState} from 'react';
import I18n from '../../utilities/i18n';
import {useDispatch} from 'react-redux';
import {Keyboard, StyleSheet, Text, View} from 'react-native';
import {
  CardList,
  Container,
  Content,
  Gap,
  Header,
  Input,
} from '../../components';
import {fonts, LIST_PDAM_PRODUCT, useForm, hp, PPOB} from '../../utilities';
import {transactionActions} from '../../redux/store/actions';

const {checkBill} = transactionActions;

const PdamScreen = ({navigation, route}) => {
  const [form, setForm] = useForm({customerId: ''});
  const [inputSearch, setInputSearch] = useState('');
  const [listFilterProduct, setListFilterProduct] = useState(LIST_PDAM_PRODUCT);

  const dispatch = useDispatch();

  const filterProduct = (val) => {
    if (val) {
      const result = LIST_PDAM_PRODUCT.filter((item) => {
        return item.value.toLowerCase().includes(val.toLowerCase());
      });
      setListFilterProduct(result);
    } else {
      setListFilterProduct(LIST_PDAM_PRODUCT);
    }
  };

  const onChangeText = (value) => {
    setForm('customerId', value);
  };

  const onChangeSearch = (val) => {
    setInputSearch(val);
    filterProduct(val);
  };

  const onPressSearchIcon = () => {
    filterProduct(inputSearch);
  };

  const onSelect = (data) => {
    Keyboard.dismiss();
    dispatch(checkBill(PPOB.PDAM)).then((res) => {
      navigation.navigate('TransactionSummaryScreen', {
        title: I18n.t('pdamScreen.title'),
        productName: PPOB.PDAM,
      });
    });
  };
  return (
    <Container>
      <Header title={I18n.t('pdamScreen.title')} />
      <Content noPadding>
        <View style={styles.topContainer}>
          <Input
            rounded
            placeholder={I18n.t('pdamScreen.customerId')}
            value={form.phoneNumber}
            keyboardType="numeric"
            onChangeText={onChangeText}
          />
          <Gap height={hp(2)} />
          <Text style={styles.chooseRegionText}>
            {I18n.t('pdamScreen.chooseRegion')}
          </Text>
        </View>
        <CardList
          isSearch
          list={listFilterProduct}
          itemWidth={`${96 / 2}%`}
          numColumns={2}
          inputSearch={inputSearch}
          onChangeSearch={onChangeSearch}
          onPressSearchIcon={onPressSearchIcon}
          onPress={(item) => onSelect(item)}
        />
      </Content>
    </Container>
  );
};

export default PdamScreen;

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
  topContainer: {
    padding: 20,
  },
  chooseRegionText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
  },
});
