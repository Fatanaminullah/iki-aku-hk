import React from 'react';
import I18n from '../../utilities/i18n';
import {useDispatch} from 'react-redux';
import {StyleSheet} from 'react-native';
import {
  Header,
  Container,
  Content,
  Form,
  Field,
  Input,
  Picker,
  Button,
  Gap,
} from '../../components';
import {validation, hp, PPOB} from '../../utilities';
import {transactionActions} from '../../redux/store/actions';

const {checkBill} = transactionActions;

const {required} = validation;

const BpjsKesehatanScreen = ({navigation, route}) => {
  const dispatch = useDispatch();

  const onSubmit = (data) => {
    dispatch(checkBill(PPOB.BPJS_KESEHATAN)).then((res) => {
      navigation.navigate('TransactionSummaryScreen', {
        title: I18n.t('bpjsKesehatanScreen.title'),
        productName: PPOB.BPJS_KESEHATAN,
      });
    });
  };
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        title={I18n.t('bpjsKesehatanScreen.checkInvoice')}
        rounded
        onPress={handleSubmit}
      />
    );
  };
  return (
    <Container>
      <Header title={I18n.t('bpjsKesehatanScreen.title')} />
      <Content>
        <Form onSubmit={onSubmit} submitComponent={renderSubmitComponent}>
          <Field name="customerId" validation={{required}}>
            <Input
              rounded
              placeholder={I18n.t('bpjsKesehatanScreen.customerId')}
              keyboardType="numeric"
            />
          </Field>
          <Gap height={hp(2)} />
          <Field name="qty" validation={{required}}>
            <Picker
              rounded
              placeholder={I18n.t('bpjsKesehatanScreen.qty')}
              dataSource={[
                {label: '1', value: 1},
                {label: '2', value: 2},
                {label: '3', value: 3},
                {label: '4', value: 4},
              ]}
            />
          </Field>
          <Gap height={hp(2)} />
        </Form>
      </Content>
    </Container>
  );
};

export default BpjsKesehatanScreen;

const styles = StyleSheet.create({});
