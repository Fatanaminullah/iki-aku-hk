import {
  colors,
  dateToString,
  PPOB,
  thousandSeparator,
  TRANSACTION_STATUS,
} from '../../../../utilities';
import I18n from 'react-native-redux-i18n';
import moment from 'moment';

const trxStatusMapper = (code) => {
  switch (code) {
    case TRANSACTION_STATUS.WAITING_PAYMENT:
      return {label: I18n.t('homeScreen.unpaid'), color: colors.yellow1};
    case TRANSACTION_STATUS.EXPIRED_ORDER:
      return {label: I18n.t('homeScreen.expired'), color: colors.red1};
    case TRANSACTION_STATUS.ON_PROCESS:
      return {label: I18n.t('homeScreen.onProcess'), color: colors.blue1};
    case TRANSACTION_STATUS.COMPLETED:
      return {label: I18n.t('homeScreen.success'), color: colors.green1};
    case TRANSACTION_STATUS.FAILED:
      return {label: I18n.t('homeScreen.failed'), color: colors.red1};
    case TRANSACTION_STATUS.ERROR:
      return {label: I18n.t('homeScreen.error'), color: colors.red1};
    case TRANSACTION_STATUS.CANCELLED:
      return {label: I18n.t('homeScreen.cancelled'), color: colors.red1};
    default:
      return {label: '', color: ''};
  }
};

const trxTypeMapper = (code) => {
  switch (code) {
    case 0:
      return {label: 'Remittance', color: colors.blue1};
    case 1:
      return {label: 'PPOB', color: colors.blue1};
    case 2:
      return {label: 'VOUCHER GAMES', color: colors.blue1};
    default:
      return {label: 'Remittance', color: colors.blue1};
  }
};

const trxSummaryMapper = (type, data) => {
  let trxSummary;
  switch (type) {
    case PPOB.MOBILE_TOP_UP:
      trxSummary = {
        transactionDetails: [
          {title: 'Produk', value: 'TELKOMSEL'},
          {title: 'Nomor Pelanggan', value: '081219596800'},
          {title: 'Nominal', value: 'Rp. 5.000'},
        ],
        paymentDetails: [
          {title: 'Harga Produk', value: 'NTD 300'},
          {title: 'Pajak', value: 'NTD 3'},
          {title: '', value: 'line'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.DATA_PACKAGE:
      trxSummary = {
        transactionDetails: [
          {title: 'Produk', value: 'Tsel Paket Malam 1GB (00-07)'},
          {title: 'Nomor Pelanggan', value: '081219596800'},
        ],
        paymentDetails: [
          {title: 'Harga Produk', value: 'NTD 300'},
          {title: 'Pajak', value: 'NTD 3'},
          {title: '', value: 'line'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.PLN_PREPAID:
      trxSummary = {
        transactionDetails: [
          {title: 'Produk', value: 'PLN Prabayar'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif / Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 100.000'},
        ],
        paymentDetails: [
          {title: 'Harga Produk', value: 'NTD 300'},
          {title: 'Pajak', value: 'NTD 3'},
          {title: '', value: 'line'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.PLN_POSTPAID:
      trxSummary = {
        transactionDetails: [
          {title: 'Produk', value: 'PLN Pascabayar'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif / Daya', value: 'R1M / 900 VA'},
          {title: 'Tagihan', value: 'Rp. 150.000'},
          {title: 'Periode', value: 'JAN20'},
        ],
        paymentDetails: [
          {title: 'Harga Produk', value: 'NTD 300'},
          {title: 'Pajak', value: 'NTD 3'},
          {title: '', value: 'line'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.BPJS_KESEHATAN:
      trxSummary = {
        transactionDetails: [
          {title: 'Produk', value: 'BPJS KESEHATAN'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif / Daya', value: 'R1M / 900 VA'},
          {title: 'Tagihan', value: 'Rp. 150.000'},
          {title: 'Periode', value: 'JAN20'},
        ],
        paymentDetails: [
          {title: 'Harga Produk', value: 'NTD 300'},
          {title: 'Pajak', value: 'NTD 3'},
          {title: '', value: 'line'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.PDAM:
      trxSummary = {
        transactionDetails: [
          {title: 'Produk', value: 'PDAM KOTA BOGOR'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif / Daya', value: 'R1M / 900 VA'},
          {title: 'Tagihan', value: 'Rp. 150.000'},
          {title: 'Periode', value: 'JAN20'},
        ],
        paymentDetails: [
          {title: 'Harga Produk', value: 'NTD 300'},
          {title: 'Pajak', value: 'NTD 3'},
          {title: '', value: 'line'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.VOUCHER_GAMES:
      trxSummary = {
        transactionDetails: [{title: 'Produk', value: 'Voucher 30k'}],
        paymentDetails: [
          {title: 'Harga Produk', value: 'NTD 300'},
          {title: 'Pajak', value: 'NTD 3'},
          {title: '', value: 'line'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    default:
      break;
  }
  return trxSummary;
};

const trxDetailsMapper = (type, data) => {
  let trxDetails;
  switch (type) {
    case 'TRANSFER':
      const infoDetails = {
        from: [
          {
            title: I18n.t('transactionDetailsScreen.sender'),
            value: data?.sender,
          },
          {
            title: I18n.t('transactionDetailsScreen.sourceOfFunds'),
            value: data?.fundSource,
          },
          {
            title: I18n.t('transactionDetailsScreen.purpose'),
            value: data?.destination,
          },
        ],
        to: [
          {
            title: I18n.t('transactionDetailsScreen.recipient'),
            value: data?.beneficiary,
          },
          {
            title: I18n.t('transactionDetailsScreen.bank'),
            value: data?.bankName,
          },
          {
            title: I18n.t('transactionDetailsScreen.bankNumber'),
            value: data?.bankNo,
          },
        ],
      };
      const nominalDetails = [
        {
          title: I18n.t('transactionDetailsScreen.sendAmount'),
          currency: 'HKD',
          value: thousandSeparator(data?.amount),
        },
        {
          title: I18n.t('transactionDetailsScreen.receiveAmount'),
          currency: 'IDR',
          value: thousandSeparator(data?.estimatedAmount),
        },
        {
          title: I18n.t('transactionDetailsScreen.fee'),
          currency: 'HKD',
          value: thousandSeparator(data?.admin),
        },
        {
          title: I18n.t('transactionDetailsScreen.total'),
          currency: 'HKD',
          value: thousandSeparator(data?.total),
        },
        {
          title: I18n.t('transactionDetailsScreen.discount'),
          currency: 'HKD',
          value: thousandSeparator(data?.discount),
        },
        {
          title: I18n.t('transactionDetailsScreen.totalFinal'),
          currency: 'HKD',
          value: thousandSeparator(data?.totalAfterDiscount),
        },
      ];
      trxDetails = {
        merchantTradeNo: data.merchantTradeNo,
        paymentCode: data.paymentNo,
        total: data.totalAfterDiscount,
        type: 'TRANSFER',
        expire: dateToString(data.expiredDate, 6),
        status: data.statusCode,
        awaitingPayment: {
          trxDetails: [
            {
              title: I18n.t('transactionDetailsScreen.trxDate'),
              value: dateToString(data?.paymentDate, 5),
            },
            {
              title: I18n.t('transactionDetailsScreen.expiryDate'),
              value: dateToString(data?.expiredDate, 6),
            },
          ],
          infoDetails: infoDetails,
          nominalDetails: nominalDetails,
        },
        onProcess: {
          trxDetails: [
            {
              title: I18n.t('transactionDetailsScreen.trxDate'),
              value: dateToString(data?.paymentDate, 1),
            },
            {
              title: I18n.t('transactionDetailsScreen.expiryDate'),
              value: dateToString(data?.expiredDate, 1),
            },
            {
              title: I18n.t('transactionDetailsScreen.reffNumber'),
              value: data?.reffNumber,
            },
          ],
          infoDetails: infoDetails,
          nominalDetails: nominalDetails,
        },
        completed: {
          trxDetails: [
            {
              title: I18n.t('transactionDetailsScreen.trxDate'),
              value: dateToString(data?.paymentDate, 1),
            },
            {
              title: I18n.t('transactionDetailsScreen.expiryDate'),
              value: dateToString(data?.expiredDate, 1),
            },
            {
              title: I18n.t('transactionDetailsScreen.reffNumber'),
              value: data?.trade_no,
            },
            {
              title: I18n.t('transactionDetailsScreen.serialNumber'),
              value: data?.trade_no,
            },
          ],
          infoDetails: infoDetails,
          nominalDetails: nominalDetails,
        },
      };
      break;
    case PPOB.MOBILE_TOP_UP:
      trxDetails = {
        paymentCode: 'LLL202498675672',
        awaitingPayment: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'TELKOMSEL'},
          {title: 'Nomor Pelanggan', value: '081219596800'},
          {title: 'Nominal', value: 'Rp. 5000'},
          {title: 'Batas Waktu Pembayaran', value: '21 September 2020 15.40'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        onProcess: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'TELKOMSEL'},
          {title: 'Nomor Pelanggan', value: '081219596800'},
          {title: 'Nominal', value: 'Rp. 5000'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        completed: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'TELKOMSEL'},
          {title: 'Nomor Pelanggan', value: '081219596800'},
          {title: 'Nominal', value: 'Rp. 5000'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Serial Number', value: '98364837231248'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.DATA_PACKAGE:
      trxDetails = {
        paymentCode: 'LLL202498675672',
        awaitingPayment: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'Tsel Paket Malam 1GB (00-07)'},
          {title: 'Nomor Pelanggan', value: '081219596800'},
          {title: 'Batas Waktu Pembayaran', value: '21 September 2020 15.40'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        onProcess: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'Tsel Paket Malam 1GB (00-07)'},
          {title: 'Nomor Pelanggan', value: '081219596800'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        completed: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'Tsel Paket Malam 1GB (00-07)'},
          {title: 'Nomor Pelanggan', value: '081219596800'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Serial Number', value: '98364837231248'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.PLN_PREPAID:
      trxDetails = {
        paymentCode: 'LLL202498675672',
        awaitingPayment: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'PLN Prabayar'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 100.000'},
          {title: 'Batas Waktu Pembayaran', value: '21 September 2020 15.40'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        onProcess: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'PLN Prabayar'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 100.000'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        completed: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'PLN Prabayar'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 100.000'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Token', value: '8987-8763-3299-1232-1445'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.PLN_POSTPAID:
      trxDetails = {
        paymentCode: 'LLL202498675672',
        awaitingPayment: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'PLN Pascabayar'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 150.000'},
          {title: 'Batas Waktu Pembayaran', value: '21 September 2020 15.40'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        onProcess: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'PLN Pascabayar'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 150.000'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        completed: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'PLN Pascabayar'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 150.000'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Serial Number', value: '98364837231248'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.BPJS_KESEHATAN:
      trxDetails = {
        paymentCode: 'LLL202498675672',
        awaitingPayment: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'BPJS Kesehatan'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 150.000'},
          {title: 'Batas Waktu Pembayaran', value: '21 September 2020 15.40'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        onProcess: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'BPJS Kesehatan'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 150.000'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        completed: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'BPJS Kesehatan'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 150.000'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Serial Number', value: '98364837231248'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.PDAM:
      trxDetails = {
        paymentCode: 'LLL202498675672',
        awaitingPayment: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'PDAM'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 150.000'},
          {title: 'Batas Waktu Pembayaran', value: '21 September 2020 15.40'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        onProcess: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'PDAM'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 150.000'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        completed: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'PDAM'},
          {title: 'Nomor Pelanggan', value: '1143114311'},
          {title: 'Nama', value: 'Taylor Swift'},
          {title: 'Tarif Daya', value: 'R1M / 900 VA'},
          {title: 'Nominal', value: 'Rp. 150.000'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Serial Number', value: '98364837231248'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    case PPOB.VOUCHER_GAMES:
      trxDetails = {
        paymentCode: 'LLL202498675672',
        awaitingPayment: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'Voucher games'},
          {title: 'Harga Produk', value: 'NTD 300'},
          {title: 'Pajak', value: 'NTD 3'},
          {title: 'Batas Waktu Pembayaran', value: '21 September 2020 15.40'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        onProcess: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'Voucher games'},
          {title: 'Harga Produk', value: 'NTD 300'},
          {title: 'Pajak', value: 'NTD 3'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
        completed: [
          {title: 'Tanggal Transaksi', value: '20 September 2020 15.40'},
          {title: 'Produk', value: 'Voucher games'},
          {title: 'Harga Produk', value: 'NTD 300'},
          {title: 'Pajak', value: 'NTD 3'},
          {title: 'Tanggalan Pembayaran', value: '21 September 2020 20.10'},
          {title: 'Referensi Pembayaran', value: 'XX23871JSDHN8'},
          {title: 'Serial Number', value: '98364837231248'},
          {title: 'Total Bayar', value: 'NTD 303'},
        ],
      };
      break;
    default:
      break;
  }
  return trxDetails;
};

export {trxSummaryMapper, trxDetailsMapper, trxStatusMapper, trxTypeMapper};
