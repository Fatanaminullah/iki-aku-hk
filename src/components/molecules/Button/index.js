import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {Touchable} from '../../atoms';
import {colors, fonts, hp} from '../../../utilities';
import ButtonWithIcon from './ButtonWithIcon';
import RoundedButton from './RoundedButton';

const Button = ({
  type,
  disabled,
  style,
  textStyle,
  onPress,
  icon,
  iconPosition,
  iconGap,
  rounded,
  size,
  color,
  title,
  buttonType,
  iconLeft,
  iconRight,
}) => {
  if (icon && rounded) {
    return (
      <RoundedButton
        title={title}
        icon={icon}
        iconGap={iconGap}
        onPress={onPress}
        size={size}
        color={color}
      />
    );
  } else if (icon) {
    return (
      <ButtonWithIcon
        title={title}
        icon={icon}
        iconPosition={iconPosition}
        type={type}
        style={style}
        textStyle={textStyle}
        iconGap={iconGap}
        onPress={onPress}
        disabled={disabled}
        buttonType={buttonType}
      />
    );
  } else {
    return (
      <Touchable
        style={[styles.container(type, disabled, rounded, buttonType), style]}
        onPress={onPress}
        disabled={disabled}>
        {iconLeft && iconLeft}
        <Text style={[styles.text(type, disabled, buttonType), textStyle]}>
          {title}
        </Text>
        {iconRight && iconRight}
      </Touchable>
    );
  }
};

export default Button;

const setButtonStyle = (type, buttonType) => {
  switch (type) {
    case 'primary':
      if (buttonType === 'outline') {
        return {
          borderColor: colors.blue1,
          backgroundColor: colors.white,
          color: colors.blue1,
        };
      } else {
        return {
          backgroundColor: colors.blue1,
          color: colors.white,
        };
      }
    case 'warning':
      if (buttonType === 'outline') {
        return {
          borderWidth: 1,
          borderColor: colors.orange2,
          backgroundColor: colors.white,
          color: colors.orange2,
        };
      } else {
        return {
          backgroundColor: colors.orange2,
          color: colors.white,
        };
      }
    case 'danger':
      if (buttonType === 'outline') {
        return {
          borderColor: colors.red2,
          backgroundColor: colors.white,
          color: colors.red2,
        };
      } else {
        return {
          backgroundColor: colors.red2,
          color: colors.white,
        };
      }
    case 'disabled':
      if (buttonType === 'outline') {
        return {
          borderColor: colors.grey2,
          backgroundColor: colors.white,
          color: colors.grey2,
        };
      } else {
        return {
          backgroundColor: colors.grey2,
          color: colors.white,
        };
      }
    default:
      if (buttonType === 'outline') {
        return {
          borderColor: colors.blue1,
          backgroundColor: colors.white,
          color: colors.blue1,
        };
      } else {
        return {
          backgroundColor: colors.blue1,
          color: colors.white,
        };
      }
  }
};

const styles = StyleSheet.create({
  container: (type, disabled, rounded, buttonType) => ({
    backgroundColor: disabled
      ? setButtonStyle('disabled', buttonType).backgroundColor
      : setButtonStyle(type, buttonType).backgroundColor,
    height: hp(6),
    paddingHorizontal: 62,
    borderRadius: rounded ? 25 : 10,
    borderColor: disabled
      ? setButtonStyle('disabled', buttonType).borderColor
      : setButtonStyle(type, buttonType).borderColor,
    borderWidth: buttonType === 'outline' ? 1 : 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  }),
  text: (type, disabled, buttonType) => ({
    fontFamily: fonts.primary[700],
    fontSize: fonts.size[16],
    marginHorizontal: 5,
    color: disabled
      ? setButtonStyle('disabled').color
      : setButtonStyle(type, buttonType).color,
  }),
});
