import React from 'react';
import {StyleSheet, View, TouchableOpacity} from 'react-native';
import {colors} from '../../../utilities';

const Card = ({children, onPress, style, index}) => {
  if (onPress) {
    return (
      <TouchableOpacity
        key={index}
        style={[styles.container, style]}
        onPress={onPress}>
        {children}
      </TouchableOpacity>
    );
  }
  return (
    <View key={index} style={[styles.container, style]}>
      {children}
    </View>
  );
};

export default Card;

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: colors.grey2,
    borderRadius: 10,
    backgroundColor: colors.white,
    padding: 10,
    alignItems: 'center',
  },
});
