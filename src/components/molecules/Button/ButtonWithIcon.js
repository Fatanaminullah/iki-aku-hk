import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {Gap, Touchable} from '../../atoms';
import {colors, fonts} from '../../../utilities';

const ButtonWithIcon = ({
  type,
  style,
  textStyle,
  title,
  onPress,
  icon,
  iconPosition = 'right',
  iconGap,
  disabled,
  buttonType,
}) => {
  if (title) {
    return (
      <Touchable
        style={[styles.container(type, disabled, buttonType), style]}
        onPress={onPress}
        disabled={disabled}>
        {iconPosition === 'left' && (
          <>
            {icon}
            <Gap width={iconGap || 12} />
          </>
        )}
        <Text style={[styles.text(type, disabled, buttonType), textStyle]}>
          {title}
        </Text>
        {iconPosition === 'right' && (
          <>
            <Gap width={iconGap || 12} />
            {icon}
          </>
        )}
      </Touchable>
    );
  }
  return (
    <Touchable style={[style]} onPress={onPress} disabled={disabled}>
      {icon}
    </Touchable>
  );
};

export default ButtonWithIcon;

const setButtonStyle = (type, buttonType) => {
  switch (type) {
    case 'primary':
      if (buttonType === 'outline') {
        return {
          borderWidth: 1,
          borderColor: colors.blue1,
          backgroundColor: colors.white,
          color: colors.blue1,
        };
      } else {
        return {
          backgroundColor: colors.blue1,
          color: colors.white,
        };
      }
    case 'danger':
      if (buttonType === 'outline') {
        return {
          borderWidth: 1,
          borderColor: colors.red2,
          backgroundColor: colors.white,
          color: colors.red2,
        };
      } else {
        return {
          backgroundColor: colors.red2,
          color: colors.white,
        };
      }
    case 'warning':
      if (buttonType === 'outline') {
        return {
          borderWidth: 1,
          borderColor: colors.orange2,
          backgroundColor: colors.white,
          color: colors.orange2,
        };
      } else {
        return {
          backgroundColor: colors.orange2,
          color: colors.white,
        };
      }
    case 'disabled':
      if (buttonType === 'outline') {
        return {
          borderWidth: 1,
          borderColor: colors.grey2,
          backgroundColor: colors.white,
          color: colors.grey2,
        };
      } else {
        return {
          backgroundColor: colors.grey2,
          color: colors.white,
        };
      }
    default:
      if (buttonType === 'outline') {
        return {
          borderWidth: 1,
          borderColor: colors.blue1,
          backgroundColor: colors.white,
          color: colors.blue1,
        };
      } else {
        return {
          backgroundColor: colors.blue1,
          color: colors.white,
        };
      }
  }
};

const styles = StyleSheet.create({
  container: (type, disabled, buttonType) => ({
    backgroundColor: disabled
      ? setButtonStyle('disabled', buttonType).backgroundColor
      : setButtonStyle(type, buttonType).backgroundColor,
    paddingHorizontal: 25,
    paddingVertical: 18,
    borderRadius: 15,
    flexDirection: 'row',
    alignItems: 'center',
  }),
  text: (type, disabled, buttonType) => ({
    fontFamily: fonts.primary[700],
    fontSize: fonts.size[16],
    color: disabled
      ? setButtonStyle('disabled', buttonType).color
      : setButtonStyle(type, buttonType).color,
  }),
});
