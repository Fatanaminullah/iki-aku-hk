export const tnc = [
  '1. IKI TOKO is an application used to assist Indonesian migrant workers in collecting money in cooperation with banks in Taiwan, banks in Indonesia and money service operators, which take 1-2 banking days to the recipient.',
  '2. PMI is a registered and legal Indonesian migrant worker that is recognized by the Taiwanese government and the Indonesian government.',
  '3. Error in inputting information is your own personal responsibility. This information includes, but not limited to : Amount, Sender, Beneficiary and Bank account details.',
  '4. The source of fund for sending is genuinely obtained from the salary of PMI that works legally and is recognized by the Taiwanese government.',
  '5. The information enterred is appropriate and genuinely submitted by the PMI.',
  '6. It is mandatory to use ARC from the relevant owner and signed by the migrant worker.',
  '7 The source of fund is not related to Money Laundering, Terrorism, Narcotic and Corruption.',
  '8. KYC/AML must be carried out by agents against PMI who will conduct transactions.',
];
