import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {getBrand, getDeviceType, getModel} from 'react-native-device-info';
import I18n from 'react-native-redux-i18n';
import {useDispatch} from 'react-redux';
import {IkiAkuIcon1} from '../../assets';
import {
  Button,
  Container,
  Content,
  Field,
  Form,
  Gap,
  Header,
  Input,
  Link,
  SvgWrapper,
} from '../../components';
import {
  alertActions,
  authenticationActions,
  loadingActions,
} from '../../redux/store/actions';
import {getUserInfo} from '../../redux/store/actions/profile';
import {fonts, hp, registerUserId, validation, wp} from '../../utilities';

const {showLoading, dismissLoading} = loadingActions;
const {showAlertError} = alertActions;
const {login} = authenticationActions;

const {required, arcNumber} = validation;

const SignInScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        type="danger"
        title={I18n.t('signInScreen.login')}
        rounded
        onPress={handleSubmit}
      />
    );
  };
  const onSubmit = (data, setValue) => {
    const {arcNumber, password} = data;
    if (arcNumber.toUpperCase() !== arcNumber) {
      setValue('arcNumber', arcNumber.toUpperCase());
    }
    const description = getModel();
    const name = getBrand();
    const type = getDeviceType();
    const request = {
      password,
      username: arcNumber,
      metadata: {
        device: {
          description,
          name,
          type,
        },
      },
    };
    dispatch(login(request, showLoading, dismissLoading, showAlertError)).then(
      (res) => {
        registerUserId(res?.username);
        navigation.reset({
          index: 0,
          routes: [{name: 'MainApp'}],
        });
        dispatch(dismissLoading());
      },
    );
  };
  return (
    <Container>
      <Header title={I18n.t('signInScreen.title')} />
      <Content>
        <SvgWrapper
          component={<IkiAkuIcon1 />}
          width={wp(25)}
          height={wp(25)}
        />
        <Gap height={hp(5)} />
        <Text style={styles.instructionText}>
          {I18n.t('signInScreen.instruction')}
        </Text>
        <Gap height={hp(4)} />
        <Form onSubmit={onSubmit} submitComponent={renderSubmitComponent}>
          <Field name="arcNumber" validation={{required}}>
            <Input
              rounded
              placeholder={I18n.t('signInScreen.arcNumber')}
              maxLength={10}
            />
          </Field>
          <Gap height={hp(3)} />
          <Field name="password" validation={{required}}>
            <Input
              rounded
              placeholder={I18n.t('signInScreen.password')}
              secureTextEntry
            />
          </Field>
          <Gap height={hp(3)} />
        </Form>
        <Gap height={hp(3)} />
        <Link
          title={I18n.t('signInScreen.forgotPasswordQuestion')}
          align="center"
          size={fonts.size[14]}
          onPress={() => navigation.navigate('ForgotPasswordScreen')}
        />
        <Gap height={hp(3)} />
        <Text style={styles.infoText}>
          <Text>{I18n.t('signInScreen.agreement')}</Text>{' '}
          <Link
            type="text"
            onPress={() => navigation.navigate('TermsAndConditionsScreen')}
            title={I18n.t('signInScreen.termsAndConditions')}
            size={fonts.size[12]}
            containerStyle={styles.termsAndConditionsLink}
          />{' '}
          <Text>{I18n.t('signInScreen.and')}</Text>{' '}
          <Link
            type="text"
            onPress={() => navigation.navigate('PrivacyPolicyScreen')}
            title={I18n.t('signInScreen.privacyPolicy')}
            containerStyle={styles.termsAndConditionsLink}
            size={fonts.size[12]}
          />
        </Text>
        <Gap height={hp(7)} />
        <Text style={styles.infoText}>
          {I18n.t('signInScreen.noAccountInstruction')}
        </Text>
        <Gap height={hp(2)} />
        <Button
          type="primary"
          title={I18n.t('signInScreen.registerNow')}
          rounded
          onPress={() => navigation.navigate('AgreementScreen')}
        />
      </Content>
    </Container>
  );
};

export default SignInScreen;

const styles = StyleSheet.create({
  instructionText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    textAlign: 'center',
  },
  infoText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[12],
    textAlign: 'center',
    maxWidth: wp(80),
    alignSelf: 'center',
  },
  termsAndConditionsLink: {
    justifyContent: 'flex-end',
  },
});
