import React from 'react';
import ImageZoom from 'react-native-image-pan-zoom';
import ReactNativeModal from 'react-native-modal';
import I18n from 'react-native-redux-i18n';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {BackIcon, CameraIcon3} from '../../../assets';
import {colors, fonts, hp, wp} from '../../../utilities';
import {Gap, SvgWrapper} from '../../atoms';

const PreviewImage = ({
  visible,
  setVisible,
  sourceImg,
  showButtonUpdate,
  onClickUpdate,
  imgType = 'Base64',
}) => {
  return (
    <ReactNativeModal
      onBackButtonPress={() => setVisible(false)}
      isVisible={visible}
      style={styles.container}
      animationIn="zoomIn"
      animationOut="fadeOutDown"
      backdropTransitionOutTiming={0}>
      <View style={styles.content}>
        <View style={styles.buttonBackContainer}>
          <TouchableOpacity
            style={styles.flexRow}
            onPress={() => setVisible(false)}>
            <SvgWrapper component={<BackIcon />} width={wp(2)} height={hp(3)} />
            <Gap width={15} />
            <Text
              style={{
                color: colors.white,
                fontSize: fonts.size[14],
                fontFamily: fonts.primary.bold,
              }}>
              {I18n.t('camera.back')}
            </Text>
          </TouchableOpacity>
          {showButtonUpdate && (
            <TouchableOpacity
              style={styles.buttonUpdate}
              onPress={() => onClickUpdate()}>
              <Text
                style={{
                  color: colors.white,
                  fontSize: fonts.size[14],
                  fontFamily: fonts.primary.bold,
                }}>
                {I18n.t('camera.updatePhoto')}
              </Text>
              <SvgWrapper
                component={<CameraIcon3 />}
                width={wp(8)}
                height={hp(4)}
              />
            </TouchableOpacity>
          )}
        </View>
        <View style={styles.imageContainer}>
          <ImageZoom
            cropWidth={wp(100)}
            cropHeight={hp(100)}
            imageWidth={wp(80)}
            imageHeight={hp(80)}>
            <Image
              resizeMode="contain"
              source={{
                uri:
                  imgType === 'Base64'
                    ? `data:image/png;base64,${sourceImg}`
                    : sourceImg,
              }}
              style={{width: wp(80), height: hp(80)}}
            />
          </ImageZoom>
        </View>
      </View>
    </ReactNativeModal>
  );
};

export default PreviewImage;

const styles = StyleSheet.create({
  container: {
    margin: 0,
  },
  content: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.7)',
  },
  flexRow: {
    flexDirection: 'row',
  },
  buttonBackContainer: {
    height: hp(10),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  buttonUpdate: {
    flexDirection: 'row',
    backgroundColor: colors.black,
    borderRadius: 25,
    borderWidth: 0.5,
    borderColor: colors.white,
    paddingHorizontal: 10,
    paddingVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
