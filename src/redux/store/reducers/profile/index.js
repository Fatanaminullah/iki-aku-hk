import React from 'react';
import I18n from 'react-native-redux-i18n';
import {profileActionTypes} from '../../action-types';
import {
  ArcFront,
  ArcBack,
  SelfieArc,
  DummyProfileIcon,
} from '../../../../assets';

const {
  SET_LIST_CONTACT,
  SET_LOCALE,
  SET_USER_DETAILS,
  SET_LIST_OCCUPATION,
} = profileActionTypes;

const initState = {
  contacts: [],
  language: I18n.defaultLocale,
  userDetails: {},
  listOccupation: [],
};

export default (state = initState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SET_LIST_CONTACT:
      return {
        ...state,
        contacts: payload,
      };
    case SET_LOCALE:
      return {
        ...state,
        language: payload,
      };
    case SET_USER_DETAILS:
      return {
        ...state,
        userDetails: payload,
      };
    case SET_LIST_OCCUPATION:
      return {
        ...state,
        listOccupation: payload,
      };

    default:
      return state;
  }
};
