import React from 'react';
import {StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {LeftArrowIcon} from '../../../assets';
import {fonts, colors, hp, wp} from '../../../utilities';
import {Gap, SvgWrapper} from '../../atoms';

const Header = ({
  title = 'Header',
  onPress,
  separator = true,
  showButtonBack = true,
}) => {
  const navigation = useNavigation();
  return (
    <>
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.backButton}
          onPress={
            showButtonBack
              ? onPress
                ? onPress
                : () => navigation.goBack()
              : null
          }>
          {showButtonBack && (
            <SvgWrapper
              component={<LeftArrowIcon />}
              width={wp(5)}
              height={wp(5)}
            />
          )}
          <Gap width={16} />
          <Text style={styles.title}>{title}</Text>
        </TouchableOpacity>
      </View>
      {separator && <View style={styles.shadow} />}
    </>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 15,
    height: hp(10),
    justifyContent: 'center',
    backgroundColor: colors.white,
    marginTop: hp(2),
  },
  shadow: {
    height: 1,
    borderBottomColor: colors.black,
    shadowColor: colors.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 0.5,
    elevation: 3,
  },
  backButton: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  title: {
    fontFamily: fonts.primary.bold,
    color: colors.black,
    fontSize: fonts.size[20],
  },
});
