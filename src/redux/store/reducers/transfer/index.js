import I18n from 'react-native-redux-i18n';
import {transferActionTypes} from '../../action-types';
import {
  coupon100Percent,
  coupon20Percent,
  coupon50Percent,
} from './mockCouponImg';

const {SET_COUPON, SET_TRANSFER_DATA} = transferActionTypes;

const initState = {
  rate: 512,
  maxTrx: 20000,
  adminFee: 150,
  sourceOfFunds: [
    {label: I18n.t('transferScreen.salary'), value: 'salary'},
    {label: I18n.t('transferScreen.gifts'), value: 'gifts'},
    {label: I18n.t('transferScreen.businessIncome'), value: 'businessIncome'},
    {label: I18n.t('transferScreen.dividend'), value: 'dividend'},
    {label: I18n.t('transferScreen.interest'), value: 'interest'},
    {label: I18n.t('transferScreen.pocketMoney'), value: 'pocketMoney'},
    {label: I18n.t('transferScreen.bonus'), value: 'bonus'},
    {label: I18n.t('transferScreen.others'), value: 'others'},
  ],
  purpose: [
    {label: I18n.t('transferScreen.shoppingMoney'), value: 'shoppingMoney'},
    {label: I18n.t('transferScreen.buySnack'), value: 'buySnack'},
  ],
  listCoupons: [
    {
      id: 'coupon1',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 20%',
      amount: 20,
      min: 100,
      max: 200,
      expired: '10-12-2020 23:59:59',
      image: coupon20Percent,
    },
    {
      id: 'coupon2',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 20%',
      amount: 20,
      min: 100,
      max: 200,
      expired: '10-13-2020 23:59:59',
      image: coupon20Percent,
    },
    {
      id: 'coupon3',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 50%',
      amount: 50,
      min: 100,
      max: 200,
      expired: '10-14-2020 23:59:59',
      image: coupon50Percent,
    },
    {
      id: 'coupon4',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 100%',
      amount: 100,
      min: 100,
      max: 200,
      expired: '10-15-2020 23:59:59',
      image: coupon100Percent,
    },
  ],
  selectedCoupon: {},
  transferData: {},
};

export default (state = initState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SET_COUPON:
      return {...state, selectedCoupon: payload};
    case SET_TRANSFER_DATA:
      return {...state, transferData: payload};
    default:
      return state;
  }
};
