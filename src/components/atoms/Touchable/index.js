import React from 'react'
import { TouchableOpacity } from 'react-native'

const Touchable = ({children, disabled, style, onPress, delayLongPress, delayPressIn, onLongPress}) => {
    return (
        <TouchableOpacity
            disabled={disabled}
            activeOpacity={0.5}
            delayPressIn={delayPressIn || 0}
            delayLongPress={delayLongPress || 700}
            style={style}
            onPress={onPress}
            onLongPress={() => onLongPress !== undefined ? onLongPress : null}
        >
            {children}
        </TouchableOpacity>
    )
}

export default Touchable;
