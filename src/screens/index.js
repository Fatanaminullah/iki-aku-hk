import SplashScreen from './SplashScreen';
import SignInScreen from './SignInScreen';
import ForgotPasswordScreen from './ForgotPasswordScreen';
import ForgotPasswordOtpScreen from './ForgotPasswordOtpScreen';
import CreateNewPasswordScreen from './CreateNewPasswordScreen';
import ForgotPinScreen from './ForgotPinScreen';
import ForgotPinOtpScreen from './ForgotPinOtpScreen';
import CreateNewPinScreen from './CreateNewPinScreen';
import WalkthroughScreen from './WalkthroughScreen';
import AgreementScreen from './AgreementScreen';
import SignUpScreen from './SignUpScreen';
import SignUpOtpScreen from './SignUpOtpScreen';
import HomeScreen from './HomeScreen';
import BeneficiaryScreen from './BeneficiaryScreen';
import BeneficiaryFormScreen from './BeneficiaryFormScreen';
import DetailBeneficiaryScreen from './DetailBeneficiaryScreen';
import TransferScreen from './TransferScreen';
import HelpScreen from './HelpScreen';
import OtherScreen from './OtherScreen';
import ListContactScreen from './ListContactScreen';
import PpobScreen from './PpobScreen';
import MobileTopUpScreen from './MobileTopUpScreen';
import DataPackageScreen from './DataPackageScreen';
import PlnScreen from './PlnScreen';
import PlnPrepaidScreen from './PlnPrepaidScreen';
import PlnPostpaidScreen from './PlnPostpaidScreen';
import BpjsScreen from './BpjsScreen';
import BpjsKesehatanScreen from './BpjsKesehatanScreen';
import PdamScreen from './PdamScreen';
import TransactionHistoryScreen from './TransactionHistoryScreen';
import TransactionSummaryScreen from './TransactionSummaryScreen';
import TransactionDetailsScreen from './TransactionDetailsScreen';
import TransferConfirmationScreen from './TransferConfirmationScreen';
import TransferConfirmationDataScreen from './TransferConfirmationDataScreen';
import CouponScreen from './CouponScreen';
import DetailCouponScreen from './DetailCouponScreen';
import EditProfileScreen from './EditProfileScreen';
import UserDetailsScreen from './UserDetailsScreen';
import EditProfileScreenV2 from './EditProfileScreenV2';
import AboutUsScreen from './AboutsUsScreen';
import TermsAndConditionsScreen from './TermsAndConditionsScreen';
import PrivacyPolicyScreen from './PrivacyPolicyScreen';
import ReferralScreen from './ReferralScreen';
import RewardScreen from './RewardScreen';
import DetailRewardScreen from './DetailRewardScreen';
import NotificationScreen from './NotificationScreen';
import ChangePasswordScreen from './ChangePasswordScreen';
import ChangePinScreen from './ChangePinScreen';
import UpdateProfileOtpScreen from './UpdateProfileOtpScreen';
import VoucherGamesScreen from './VoucherGamesScreen';
import VoucherGamesProductScreen from './VoucherGamesProductScreen';

export {
  SplashScreen,
  WalkthroughScreen,
  SignInScreen,
  HomeScreen,
  AgreementScreen,
  SignUpScreen,
  ForgotPasswordScreen,
  ForgotPasswordOtpScreen,
  CreateNewPasswordScreen,
  ForgotPinScreen,
  ForgotPinOtpScreen,
  CreateNewPinScreen,
  BeneficiaryScreen,
  BeneficiaryFormScreen,
  DetailBeneficiaryScreen,
  TransferScreen,
  HelpScreen,
  OtherScreen,
  ListContactScreen,
  PpobScreen,
  MobileTopUpScreen,
  DataPackageScreen,
  PlnScreen,
  PlnPrepaidScreen,
  PlnPostpaidScreen,
  BpjsScreen,
  BpjsKesehatanScreen,
  PdamScreen,
  TransactionHistoryScreen,
  TransactionSummaryScreen,
  TransactionDetailsScreen,
  TransferConfirmationScreen,
  CouponScreen,
  DetailCouponScreen,
  EditProfileScreen,
  UserDetailsScreen,
  EditProfileScreenV2,
  AboutUsScreen,
  TermsAndConditionsScreen,
  PrivacyPolicyScreen,
  ReferralScreen,
  RewardScreen,
  DetailRewardScreen,
  NotificationScreen,
  TransferConfirmationDataScreen,
  ChangePasswordScreen,
  UpdateProfileOtpScreen,
  ChangePinScreen,
  SignUpOtpScreen,
  VoucherGamesScreen,
  VoucherGamesProductScreen,
};
