import React, {useEffect, useRef, useState} from 'react';
import {
  Image,
  RefreshControl,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import Dialog from 'react-native-popup-dialog';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {
  CameraIcon,
  EditIcon,
  FlagHkgRound,
  NoProfilePictIcon,
  PencilIcon,
} from '../../assets';
import {
  Button,
  CameraWidget,
  Container,
  Content,
  DatePicker,
  Field,
  Form,
  Gap,
  Header,
  HorizontalLine,
  Input,
  InputPicker,
  PreviewImage,
  RadioGroup,
  SvgWrapper,
} from '../../components';
import {
  showAlertError,
  showAlertSuccess,
} from '../../redux/store/actions/alert';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {
  getListOccupation,
  getUserInfo,
  updateProfile,
} from '../../redux/store/actions/profile';
import {colors, hp, validation, wp} from '../../utilities';
import mapperDetail from './mapper';
import styles from './style';

const {required, email} = validation;

const EditProfileScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {userDetails, listOccupation} = useSelector((state) => state.profile);
  const detail = mapperDetail(userDetails);
  const [isEditMode, setIsEditMode] = useState(false);
  const [isDialogVisible, setIsDialogVisible] = useState(false);
  const [dataForm, setDataForm] = useState(detail);
  const [dialogForm, setDialogForm] = useState({});
  const [visiblePreview, setVisiblePreview] = useState(false);
  const [previewImage, setPreviewImage] = useState('');
  const [visibleCamera, setVisibleCamera] = useState(false);
  const [params, setParams] = useState('');
  const cameraRef = useRef();
  useEffect(() => {
    fetchData();
  }, []);

  const onEditPress = () => {
    setIsEditMode(true);
  };
  const onEditModeActionPress = (actionType) => {
    if (actionType === 'cancel') {
      return setIsEditMode(false);
    }
    setIsEditMode(false);
  };
  const fetchData = () => {
    dispatch(getUserInfo(showLoading, null))
      .then((res) => {
        const details = mapperDetail(res);
        setDataForm(details);
        dispatch(getListOccupation(null, dismissLoading));
      })
      .catch(() => {
        dispatch(dismissLoading());
      });
  };

  const onCapture = (e, param) => {
    setVisibleCamera(false);
    setParams('');
    const result = dataForm.map((item) => {
      if (item.fieldName === 'arcPhoto') {
        return {
          ...item,
          value: item.value.map((obj) => {
            if (obj.fieldName === param) {
              return {
                ...obj,
                img: `data:image/png;base64,${e}`,
              };
            } else {
              return obj;
            }
          }),
        };
      } else if (item.fieldName === param) {
        return {
          ...item,
          value: `data:image/png;base64,${e}`,
        };
      } else {
        return item;
      }
    });
    setDataForm(result);
  };
  const onClickUpdate = () => {
    setVisibleCamera(true);
    setVisiblePreview(false);
  };
  const onSubmit = () => {
    const result = {
      id: userDetails.id,
    };
    dataForm.map((item) => {
      Object.assign(result, {[item.fieldName]: item.value});
    });
    let documents = {};
    result.arcPhoto.map((item) => {
      const arcValue =
        userDetails.arcPhoto[
          Object.keys(userDetails.arcPhoto).filter(
            (x) => x === item.fieldName,
          )[0]
        ];
      if (item.img !== arcValue) {
        documents = {
          ...documents,
          [item.fieldName]: item.img.replace('data:image/png;base64,', ''),
        };
      }
    });
    const recentProfilePict =
      userDetails.arcPhoto[
        Object.keys(userDetails.arcPhoto).filter(
          (x) => x === 'photo_profile',
        )[0]
      ];
    if (recentProfilePict !== result.profilePicture) {
      documents = {
        ...documents,
        photo_profile: result.profilePicture.replace(
          'data:image/png;base64,',
          '',
        ),
      };
    }
    const request = {
      id: result.id,
      arcExpiry: result.arcExpiredDate,
      phoneNo: result.phoneNumber.split('+')[1],
      fullName: result.fullName,
      fullAddress: result.addressInTaiwan,
      birthDate: result.birthDate,
      gender: result.gender,
      arcNumber: result.arcNumber,
      email: result.email,
      occupation: result.occupation,
      attachments: documents,
    };
    dispatch(
      updateProfile(
        request,
        showLoading,
        null,
        showAlertSuccess,
        showAlertError,
      ),
    )
      .then((res) => {
        if (res?.is_update_phoneNum) {
          dispatch(dismissLoading());
          navigation.navigate('UpdateProfileOtpScreen', {request: res});
          setIsEditMode(false);
        } else {
          dispatch(getUserInfo(null, dismissLoading)).then((results) => {
            const details = mapperDetail(results);
            setDataForm(details);
            setIsEditMode(false);
            dispatch(showAlertSuccess('', I18n.t('editProfileScreen.success')));
          });
        }
      })
      .catch(() => {
        dispatch(dismissLoading());
      });
  };

  const onPencilPress = (fieldType, fieldName, fieldValue) => {
    let form;
    setIsDialogVisible(!isDialogVisible);
    const onSubmitField = (data) => {
      const result = dataForm.map((item) => {
        if (item.fieldName === Object.keys(data)[0]) {
          if (Object.keys(data)[0] === 'occupation') {
            return {
              ...item,
              value: Object.values(data)[0]?.value,
            };
          } else {
            return {
              ...item,
              value:
                item.fieldName === 'phoneNumber'
                  ? `+852${Object.values(data)[0]}`
                  : Object.values(data)[0],
            };
          }
        } else {
          return item;
        }
      });
      setDataForm(result);
      setIsDialogVisible(false);
    };
    const defaultValues = {
      [fieldName]:
        fieldName === 'phoneNumber' ? fieldValue.split('+852')[1] : fieldValue,
    };
    const onCancel = () => {
      setIsDialogVisible(false);
    };
    const submitComponent = ({handleSubmit}) => {
      return (
        <Button
          type="primary"
          title={I18n.t('editProfileScreen.ok')}
          onPress={handleSubmit}
        />
      );
    };
    if (fieldType === 'input-text') {
      form = (
        <View>
          <Form
            onSubmit={onSubmitField}
            submitComponent={submitComponent}
            defaultValues={defaultValues}>
            <Field
              name={fieldName}
              validation={
                fieldName === 'email' ? {required, pattern: email} : {required}
              }>
              <Input label={I18n.t(`editProfileScreen.${fieldName}`)} />
            </Field>
            <Gap height={hp(10)} />
          </Form>
          <Gap height={hp(2)} />
          <Button
            type="danger"
            title={I18n.t('editProfileScreen.cancel')}
            onPress={onCancel}
          />
        </View>
      );
    } else if (fieldType === 'input-phone') {
      form = (
        <View>
          <Form
            onSubmit={onSubmitField}
            submitComponent={submitComponent}
            defaultValues={defaultValues}>
            <Field name={fieldName} validation={{required}}>
              <Input
                type="phoneNumber"
                label={I18n.t(`editProfileScreen.${fieldName}`)}
                placeholder={I18n.t('signUpScreen.placeholderPhoneNumber')}
                containerStyle={styles.inputContainer}
                leftIcon={
                  <View style={styles.rowContainer}>
                    <SvgWrapper
                      component={<FlagHkgRound />}
                      width={wp(10)}
                      height={wp(10)}
                    />
                    <Text>+852</Text>
                  </View>
                }
                keyboardType="numeric"
              />
            </Field>
            <Gap height={hp(10)} />
          </Form>
          <Gap height={hp(2)} />
          <Button
            type="danger"
            title={I18n.t('editProfileScreen.cancel')}
            onPress={onCancel}
          />
        </View>
      );
    } else if (fieldType === 'date-picker') {
      form = (
        <View>
          <Form
            onSubmit={onSubmitField}
            submitComponent={submitComponent}
            defaultValues={defaultValues}>
            <Field name={fieldName} validation={{required}}>
              <DatePicker
                format="YYYY-MM-DD"
                label={I18n.t(`editProfileScreen.${fieldName}`)}
                placeholder={I18n.t(`editProfileScreen.${fieldName}`)}
              />
            </Field>
            <Gap height={hp(10)} />
          </Form>
          <Gap height={hp(2)} />
          <Button
            type="danger"
            title={I18n.t('editProfileScreen.cancel')}
            onPress={onCancel}
          />
        </View>
      );
    } else if (fieldType === 'radio-group') {
      form = (
        <View>
          <Form
            onSubmit={onSubmitField}
            submitComponent={submitComponent}
            defaultValues={defaultValues}>
            <Field name={fieldName} validation={{required}}>
              <RadioGroup
                label={I18n.t(`editProfileScreen.${fieldName}`)}
                listItem={[
                  {label: I18n.t('signUpScreen.man'), value: 'MALE'},
                  {label: I18n.t('signUpScreen.woman'), value: 'FEMALE'},
                ]}
                direction="row"
              />
            </Field>
            <Gap height={hp(10)} />
          </Form>
          <Gap height={hp(2)} />
          <Button
            type="danger"
            title={I18n.t('editProfileScreen.cancel')}
            onPress={onCancel}
          />
        </View>
      );
    } else if (fieldType === 'input-picker') {
      form = (
        <View>
          <Form
            onSubmit={onSubmitField}
            submitComponent={submitComponent}
            defaultValues={defaultValues}>
            <Field name={fieldName} validation={{required}}>
              <InputPicker
                searchPlaceholder={I18n.t('signUpScreen.inputOccupationSearch')}
                listEmptyText={I18n.t('signUpScreen.occupationNotFound')}
                title={I18n.t('signUpScreen.placeholderOccupation')}
                label={I18n.t('signUpScreen.inputOccupation')}
                placeholder={I18n.t('signUpScreen.placeholderOccupation')}
                dataSource={listOccupation}
              />
            </Field>
            <Gap height={hp(10)} />
          </Form>
          <Gap height={hp(2)} />
          <Button
            type="danger"
            title={I18n.t('editProfileScreen.cancel')}
            onPress={onCancel}
          />
        </View>
      );
    }
    setDialogForm(form);
  };

  const renderProfileItem = (item) => {
    const fieldType = [
      'text',
      'input-text',
      'date-picker',
      'radio-group',
      'input-phone',
      'input-password',
      'input-pin',
      'input-picker',
    ];
    if (fieldType.includes(item.type)) {
      return (
        <View style={[styles.rowContainer, styles.separator()]}>
          <Text style={styles.rowItemText}>{item.title}</Text>
          <View style={styles.rowContainer}>
            {item.isPrivate ? (
              <TextInput
                value={item.value}
                editable={false}
                style={styles.obfuscatedContainer}
                secureTextEntry={true}
              />
            ) : (
              <Text style={styles.rowItemText}>{item.value}</Text>
            )}
            {item.isEditable && isEditMode && (
              <Button
                icon={
                  <SvgWrapper
                    component={<PencilIcon />}
                    style={styles.iconPencilStyle}
                  />
                }
                onPress={() =>
                  onPencilPress(item.type, item.fieldName, item.value)
                }
              />
            )}
          </View>
        </View>
      );
    }
    if (item.type === 'image-picker') {
      return (
        <View>
          <Text style={styles.profilePictureText}>{item.title}</Text>
          <Gap height={hp(2)} />
          <View style={{alignSelf: 'center'}}>
            {!item?.value ? (
              <SvgWrapper
                style={[styles.profileEmpty]}
                component={<NoProfilePictIcon />}
              />
            ) : (
              <Image source={{uri: item.value}} style={styles.profileImg} />
            )}
            {isEditMode && (
              <Button
                style={styles.cameraIcon}
                icon={
                  <SvgWrapper
                    component={<CameraIcon />}
                    width={wp(8)}
                    height={hp(4)}
                  />
                }
                onPress={() => {
                  setVisibleCamera(true);
                  setParams('profilePicture');
                }}
              />
            )}
          </View>
          <Gap height={hp(3)} />
          <HorizontalLine height={0.5} />
        </View>
      );
    }
    if (item.type === 'image-picker-group') {
      return (
        <View style={[styles.columnContainer, styles.separator()]}>
          <Text style={styles.arcPhotoText}>
            {I18n.t('editProfileScreen.arcPhoto')}
          </Text>
          <Gap height={hp(2)} />
          <View style={styles.arcPhotoContainer}>
            {item?.value.map((obj, index) => {
              return (
                <View key={index} style={styles.arcPhotoItemContainer}>
                  <View>
                    {obj.img ? (
                      <TouchableOpacity
                        onPress={() => {
                          setVisiblePreview(true);
                          setPreviewImage(obj.img);
                          setParams(obj.fieldName);
                        }}>
                        <Image
                          source={{
                            uri: obj.img,
                          }}
                          style={{width: wp(20), height: wp(20)}}
                        />
                      </TouchableOpacity>
                    ) : (
                      <Image
                        source={obj.default}
                        style={{width: wp(20), height: wp(20)}}
                      />
                    )}
                    {item.isEditable && isEditMode && (
                      <Button
                        style={styles.cameraIcon}
                        icon={
                          <SvgWrapper
                            component={<CameraIcon />}
                            width={wp(8)}
                            height={hp(4)}
                          />
                        }
                        onPress={() => {
                          setVisibleCamera(true);
                          setParams(obj.fieldName);
                        }}
                      />
                    )}
                  </View>
                  <Gap height={hp(1)} />
                  <Text style={styles.arcBottomPhotoText}>{obj.title}</Text>
                </View>
              );
            })}
          </View>
        </View>
      );
    }
  };
  return (
    <Container>
      <PreviewImage
        imgType="JPEG"
        visible={visiblePreview}
        sourceImg={previewImage}
        setVisible={setVisiblePreview}
        showButtonUpdate={isEditMode}
        onClickUpdate={onClickUpdate}
      />
      <CameraWidget
        setVisible={setVisibleCamera}
        visibility={visibleCamera}
        cameraRef={cameraRef}
        onCapture={onCapture}
        params={params}
        defaultCameraRotation={
          params === 'arc_selfie' ? 'arc_front' : 'arc_back'
        }
        isCaptureIdCard={params !== 'arc_selfie' && params !== 'profilePicture'}
        isCaptureSelfie={params === 'arc_selfie' && params !== 'profilePicture'}
        showButtonRotate={
          params === 'arc_selfie' && params !== 'profilePicture'
        }
      />
      <Header title={I18n.t('editProfileScreen.title')} />
      <Content
        refreshControl={
          <RefreshControl onRefresh={fetchData} refreshing={false} />
        }>
        {isEditMode ? (
          <View style={[styles.rowContainer, styles.separator(colors.red1)]}>
            <Text style={styles.editModeActiveText}>
              {I18n.t('editProfileScreen.editModeActive')}
            </Text>
            <View style={styles.rowContainer}>
              <Button
                style={styles.editModeButtonContainer}
                textStyle={styles.editModeButtonText}
                type="danger"
                onPress={() => {
                  onEditModeActionPress('cancel');
                  setDataForm(detail);
                }}
                title={I18n.t('editProfileScreen.cancel')}
              />
              <Gap width={wp(2)} />
              <Button
                style={styles.editModeButtonContainer}
                textStyle={styles.editModeButtonText}
                title={I18n.t('editProfileScreen.submit')}
                onPress={() => onSubmit()}
              />
              <Gap height={hp(5)} />
            </View>
          </View>
        ) : (
          <Button
            title={I18n.t('editProfileScreen.editData')}
            icon={
              <SvgWrapper
                component={<EditIcon />}
                width={hp(2.5)}
                height={hp(2.5)}
              />
            }
            style={styles.editButtonContainer}
            textStyle={styles.editButtonText}
            onPress={onEditPress}
          />
        )}
        <Gap height={hp(3)} />
        {dataForm.map((item, index) => {
          return renderProfileItem(item, index);
        })}
        <Gap height={hp(10)} />
      </Content>
      <Dialog
        onTouchOutside={() => setIsDialogVisible(!isDialogVisible)}
        onHardwareBackPress={() => {
          setIsDialogVisible(!isDialogVisible);
          return true;
        }}
        dialogStyle={styles.dialogContainer}
        visible={isDialogVisible}>
        {dialogForm}
      </Dialog>
    </Container>
  );
};

export default EditProfileScreen;
