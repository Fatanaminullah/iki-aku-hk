import ImageEditor from '@react-native-community/image-editor';
import React, {useEffect, useRef, useState} from 'react';
import I18n from 'react-native-redux-i18n';
import {Image, Text, TouchableOpacity, View} from 'react-native';
import {RNCamera} from 'react-native-camera';
import RNFS from 'react-native-fs';
import ImageResizer from 'react-native-image-resizer';
import ReactNativeModal from 'react-native-modal';
import {
  CameraIcon2,
  FlashOffIcon,
  ChangeCameraIcon,
  FlashOnIcon,
} from '../../../assets';
import {colors, fonts, wp, hp} from '../../../utilities';
import {Gap, SvgWrapper} from '../../atoms';
import {Button} from '../../molecules';
import styles from './style';
import SimpleToast from 'react-native-simple-toast';
import {useDispatch} from 'react-redux';
import {showAlertError} from '../../../redux/store/actions/alert';

const CameraWidget = ({
  visibility,
  cameraRef,
  setVisible,
  onCapture,
  isCaptureIdCard,
  isCaptureSelfie,
  showButtonRotate = true,
  showButtonFlash = true,
  params,
  defaultCameraRotation = 'back',
  setValue,
  name,
}) => {
  const pictureRef = useRef();
  const dispatch = useDispatch();
  const [showPicture, setShowPicture] = useState();
  const [picture, setPicture] = useState();
  const [cameraRotation, setCameraRotation] = useState('');
  const [flashMode, setFlashMode] = useState('');
  useEffect(() => {
    if (defaultCameraRotation === 'front') {
      setCameraRotation(RNCamera.Constants.Type.front);
    } else {
      setCameraRotation(RNCamera.Constants.Type.back);
    }
    setFlashMode(RNCamera.Constants.FlashMode.off);
  }, [visibility]);
  const capture = async () => {
    const data = await cameraRef.current.takePictureAsync();
    if (isCaptureIdCard) {
      pictureRef.current.measure((fx, fy, width, height, px, py) => {
        cropPicture(data.uri, px, py, width, height);
      });
    } else {
      ImageResizer.createResizedImage(
        data.uri,
        768,
        1024,
        'JPEG',
        100,
        0,
        null,
      ).then((response) => {
        RNFS.readFile(response.uri, 'base64').then((result) => {
          setPicture(result);
          setShowPicture(true);
        });
      });
    }
  };
  const cropPicture = (data, x, y, width, height) => {
    ImageResizer.createResizedImage(data, 1060, 768, 'JPEG', 100, 0)
      .then((response) => {
        const cropData = {
          offset: {x: 125, y: 250},
          size: {width: width + 20, height: height + 20},
          displaySize: {width: 700, height: 430},
          resizeMode: 'contain',
        };
        ImageEditor.cropImage(response.uri, cropData)
          .then((url) => {
            RNFS.readFile(url, 'base64').then((result) => {
              setPicture(result);
              setShowPicture(true);
            });
          })
          .catch((error) => {
            console.log('crop image', error);
          });
      })
      .catch((err) => {
        console.log('err resize', err);
      });
  };
  const onRotateCamera = (rotation) => {
    if (rotation === RNCamera.Constants.Type.back) {
      setCameraRotation(RNCamera.Constants.Type.front);
    } else {
      setCameraRotation(RNCamera.Constants.Type.back);
    }
  };
  const onChangeFlashMode = (flash) => {
    if (flash === RNCamera.Constants.FlashMode.off) {
      setFlashMode(RNCamera.Constants.FlashMode.on);
      SimpleToast.showWithGravity(
        I18n.t('camera.flashOn'),
        SimpleToast.SHORT,
        SimpleToast.BOTTOM,
      );
    } else {
      setFlashMode(RNCamera.Constants.FlashMode.off);
      SimpleToast.showWithGravity(
        I18n.t('camera.flashOff'),
        SimpleToast.SHORT,
        SimpleToast.BOTTOM,
      );
    }
  };
  return (
    <ReactNativeModal
      onBackButtonPress={() => setVisible(false)}
      isVisible={visibility}
      style={styles.container}
      animationIn="zoomIn"
      animationOut="zoomOut"
      backdropTransitionOutTiming={0}>
      <ReactNativeModal
        isVisible={showPicture}
        animationIn="zoomIn"
        animationOut="zoomOut"
        backdropTransitionOutTiming={0}>
        <View style={styles.showPictureContainer}>
          <Text
            style={{fontFamily: fonts.primary.bold, fontSize: fonts.size[18]}}>
            {I18n.t('camera.warningText')}
          </Text>
          <Gap height={15} />
          <Image
            resizeMode="contain"
            source={{uri: `data:image/png;base64,${picture}`}}
            style={styles.showPicture}
          />
          <Gap height={5} />
          <View style={{width: wp(75)}}>
            <Button
              type="primary"
              rounded={true}
              title={I18n.t('camera.repeat')}
              onPress={() => {
                setPicture('');
                setShowPicture(false);
              }}
            />
            <Gap height={5} />
            <Button
              type="danger"
              rounded={true}
              title={I18n.t('camera.usePhoto')}
              onPress={() => {
                setShowPicture(false);
                onCapture(picture, params);
              }}
            />
          </View>
        </View>
      </ReactNativeModal>
      <View style={styles.cameraContainer}>
        <RNCamera
          ref={cameraRef}
          style={styles.camera}
          type={cameraRotation}
          captureAudio={false}
          onStatusChange={({cameraStatus}) => {
            if (cameraStatus === 'NOT_AUTHORIZED') {
              dispatch(showAlertError('', I18n.t('camera.warningPermission')));
              setVisible(false);
            }
          }}
          flashMode={flashMode}>
          {({camera, status}) => {
            return (
              <View style={styles.cameraContent}>
                <View style={styles.upperButtonContainer}>
                  <View style={styles.upperButtonContent}>
                    {showButtonRotate && (
                      <TouchableOpacity
                        onPress={() => onRotateCamera(cameraRotation)}>
                        <SvgWrapper
                          component={<ChangeCameraIcon />}
                          width={wp(8)}
                          height={hp(5)}
                        />
                      </TouchableOpacity>
                    )}
                    {showButtonRotate && showButtonFlash && <Gap width={20} />}
                    {showButtonFlash && (
                      <TouchableOpacity
                        onPress={() => onChangeFlashMode(flashMode)}>
                        {flashMode ? (
                          <SvgWrapper
                            component={<FlashOnIcon />}
                            width={wp(8)}
                            height={hp(5)}
                          />
                        ) : (
                          <SvgWrapper
                            component={<FlashOffIcon />}
                            width={wp(8)}
                            height={hp(5)}
                          />
                        )}
                      </TouchableOpacity>
                    )}
                  </View>
                </View>
                {isCaptureIdCard ? (
                  <View style={styles.idCardBoxContainer}>
                    <View ref={pictureRef} style={styles.idCardBox}>
                      <Text style={{color: colors.white}}>
                        {I18n.t('camera.captureCardLabel')}
                      </Text>
                    </View>
                  </View>
                ) : (
                  <View style={styles.flex1} />
                )}
                {isCaptureSelfie ? (
                  <View style={styles.selfieBoxContainer}>
                    <View style={styles.selfieTextBox}>
                      <Text
                        style={{color: colors.white, fontSize: fonts.size[14]}}>
                        {I18n.t('camera.captureSelfieLabel')}
                      </Text>
                    </View>
                    <View ref={pictureRef} style={styles.selfieBox} />
                    <View style={styles.selfieCardBox} />
                  </View>
                ) : (
                  <View style={styles.flex2} />
                )}
                <View style={styles.buttonContainer}>
                  <TouchableOpacity
                    style={{marginLeft: wp(5)}}
                    onPress={() => setVisible(false)}>
                    <Text style={styles.textButtonCancel}>
                      {I18n.t('camera.cancel')}
                    </Text>
                  </TouchableOpacity>
                  <View style={styles.buttonCaptureContainer}>
                    <TouchableOpacity
                      onPress={() => capture()}
                      style={styles.buttonCapture}>
                      <SvgWrapper
                        component={<CameraIcon2 />}
                        width={wp(10)}
                        height={hp(5)}
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            );
          }}
        </RNCamera>
      </View>
    </ReactNativeModal>
  );
};

export default CameraWidget;
