import {useState} from 'react';

export const useForm = (initialValue) => {
  const [values, setValues] = useState(initialValue);
  return [
    values,
    (fieldName, formValue) => {
      if (fieldName === 'reset') {
        return setValues(initialValue);
      }
      return setValues({...values, [fieldName]: formValue});
    },
  ];
};
