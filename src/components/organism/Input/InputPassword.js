import React, {useState} from 'react';
import {StyleSheet, TextInput, View} from 'react-native';
import {Item, Label, Icon} from 'native-base';
import {colors, fonts, hp} from '../../../utilities';

const InputPassword = ({
  rounded,
  label,
  labelStyle,
  containerStyle,
  onChangeText,
  secureTextEntry,
  keyboardType,
  disable,
  placeholder,
  onChange,
  value,
  error,
  setValue,
  setError,
  clearErrors,
  maxLength,
  ref,
  editable,
  selectTextOnFocus,
}) => {
  const [showPassword, setShowPassword] = useState(false);
  return (
    <View>
      {label && <Label style={[styles.label, labelStyle]}>{label}</Label>}
      <Item
        style={[styles.inputContainer(error), containerStyle]}
        rounded={rounded}>
        <TextInput
          value={value}
          placeholder={placeholder}
          style={styles.input}
          placeholderTextColor={colors.grey2}
          keyboardType={keyboardType}
          onChangeText={(val) => {
            if (onChange && onChangeText) {
              onChange(val);
              onChangeText(val);
            }
            if (onChange) {
              onChange(val);
            }
            if (onChangeText) {
              onChangeText(val);
            }
          }}
          editable={editable}
          selectTextOnFocus={selectTextOnFocus}
          maxLength={maxLength}
          secureTextEntry={!showPassword && secureTextEntry}
          ref={ref}
        />
        <Icon
          active
          type="MaterialIcons"
          style={styles.iconStyle}
          name={showPassword ? 'visibility' : 'visibility-off'}
          onPress={() => setShowPassword(!showPassword)}
        />
      </Item>
    </View>
  );
};

export default InputPassword;

const styles = StyleSheet.create({
  inputContainer: (error) => ({
    borderWidth: error ? 1 : 0,
    borderColor: error ? colors.red1 : colors.grey2,
    paddingHorizontal: 15,
  }),
  label: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
  input: {
    flex: 1,
    paddingVertical: hp(1),
  },
  iconStyle: {
    color: colors.grey2,
  },
});
