import axios from 'axios';
import I18n from 'react-native-redux-i18n';
import CryptoJS from 'crypto-js';
import {
  API_URL,
  API_URL_ALI_OSS,
  ALI_OSS_APPS_KEY,
  ALI_OSS_SECRET_KEY,
  ALI_OSS_APPS_IDENTIFIER,
} from '../environments/env.json';
import {navigationRef, store} from '../App';
import {
  API_SERVICES,
  getData,
  removeData,
  unregisterUserId,
} from '../utilities';
import SimpleToast from 'react-native-simple-toast';
import {HIDE_LOADING} from '../redux/store/action-types/loading';

const forceDismissLoading = () => ({
  type: HIDE_LOADING,
});

const clearStore = () => ({
  type: 'USER_LOGGED_OUT',
});
const baseUrlMapper = (service = API_SERVICES.IKI_AKU) => {
  let baseUrl;
  switch (service) {
    case API_SERVICES.IKI_AKU:
      baseUrl = API_URL;
      break;
    case API_SERVICES.ALI_OSS:
      baseUrl = API_URL_ALI_OSS;
      break;
    default:
      baseUrl = API_URL;
      break;
  }
  return baseUrl;
};

const forceLogout = async (ref, message) => {
  let signOutStacks = [
    'SignInScreen',
    'WalkthroughScreen',
    'SplashScreen',
    'SignUpScreen',
  ];
  await removeData('token');
  store.dispatch(forceDismissLoading());
  store.dispatch(clearStore());
  unregisterUserId();
  if (!signOutStacks.includes(ref.current.getCurrentRoute().name)) {
    SimpleToast.show(message, SimpleToast.SHORT);
    ref.current.reset({
      index: 0,
      routes: [{name: 'WalkthroughScreen'}],
    });
  }
};
const renderHeaders = async (service = API_SERVICES.IKI_AKU) => {
  const language = I18n.currentLocale();
  const data = await getData('token');
  if (data) {
    const {token} = data;
    let headers = {
      Authorization: `Bearer ${token}`,
      'Content-Type': 'application/json',
      'Accept-Language': language,
    };
    if (service === API_SERVICES.ALI_OSS) {
      headers = {
        app_identifier: ALI_OSS_APPS_IDENTIFIER,
        timestamp: Date.now(),
        token: CryptoJS.HmacSHA256(
          Date.now() + ALI_OSS_APPS_KEY,
          ALI_OSS_SECRET_KEY,
        ).toString(),
      };
    }
    return new Promise((resolve, reject) => {
      resolve(headers);
    });
  } else {
    return {
      'Content-Type': 'application/json',
      'Accept-Language': language,
    };
  }
};

const Get = async (path, request, service = API_SERVICES.IKI_AKU) => {
  const req = [];
  if (request) {
    Object.keys(request).forEach((item, index) => {
      req.push(`${[item]}=${request[item]}`);
    });
  }
  const headers = await renderHeaders();
  const promise = new Promise((resolve, reject) => {
    axios({
      method: 'get',
      baseURL: baseUrlMapper(service),
      url: `${path}?${req.join('&') || ''}`,
      headers,
      withCredentials: true,
    }).then(
      (res) => {
        resolve(res);
      },
      (err) => {
        reject(err);
        if (err?.response?.status === 401) {
          forceLogout(navigationRef, err.response.data.message);
        }
      },
    );
  });
  return promise;
};

const Post = async (path, request, service = API_SERVICES.IKI_AKU) => {
  const headers = await renderHeaders(service);
  const promise = new Promise((resolve, reject) => {
    axios({
      method: 'post',
      baseURL: baseUrlMapper(service),
      url: `${path}`,
      data: request,
      headers,
      withCredentials: true,
    })
      .then((res) => {
        resolve(res);
      })
      .catch((err) => {
        reject(err);
        if (err?.response?.status === 401) {
          forceLogout(navigationRef, err.response.data.message);
        }
      });
  });
  return promise;
};

const Put = async (path, request, service = API_SERVICES.IKI_AKU) => {
  const headers = await renderHeaders();
  const promise = new Promise((resolve, reject) => {
    axios({
      method: 'put',
      baseURL: baseUrlMapper(service),
      url: `${path}`,
      data: request,
      headers,
      withCredentials: true,
    }).then(
      (res) => {
        resolve(res);
      },
      (err) => {
        reject(err);
        if (err?.response?.status === 401) {
          forceLogout(navigationRef, err.response.data.message);
        }
      },
    );
  });
  return promise;
};

const Delete = async (path, request, service = API_SERVICES.IKI_AKU) => {
  const headers = await renderHeaders();
  const promise = new Promise((resolve, reject) => {
    axios({
      method: 'delete',
      baseURL: baseUrlMapper(service),
      url: `${path}`,
      data: request,
      headers,
      withCredentials: true,
    }).then(
      (res) => {
        resolve(res);
      },
      (err) => {
        reject(err);
        if (err?.response?.status === 401) {
          forceLogout(navigationRef, err.response.data.message);
        }
      },
    );
  });
  return promise;
};

const Services = {
  Get,
  Post,
  Put,
  Delete,
};

export default Services;
