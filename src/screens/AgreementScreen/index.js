import React, {useState} from 'react';
import I18n from 'react-native-redux-i18n';
import {CheckBox} from 'native-base';
import {ScrollView, StyleSheet, Text, View} from 'react-native';
import {Container, Header, Button, Content} from '../../components';
import {colors, fonts, hp} from '../../utilities';
import {agreement} from './agreementText';
import {WebView} from 'react-native-webview';

const AgreementScreen = ({navigation}) => {
  const [isChecked, setIsChecked] = useState(false);
  const [disabledCheckbox, setDisabledCheckbox] = useState(true);
  const onScroll = ({layoutMeasurement, contentOffset, contentSize}) => {
    if (layoutMeasurement.height + contentOffset.y >= contentSize.height - 10) {
      setDisabledCheckbox(false);
    }
  };
  return (
    <Container>
      <Header title={I18n.t('agreementScreen.headerTitle')} />
      {/* <ScrollView
        style={styles.content}
        onScroll={(e) => onScroll(e.nativeEvent)}>
        <Text>
          Magna sint ipsum deserunt esse eu et laboris proident adipisicing.
        </Text>
      </ScrollView> */}
      <Content>
        {agreement.map((item) => (
          <WebView
            source={{
              html:
                "<p style='text-align: justify; font-size: 250%;'>" +
                item +
                '</p>',
            }}
          />
        ))}
        <View style={styles.checkboxContainer}>
          <CheckBox
            // disabled={disabledCheckbox}
            checked={isChecked}
            // color={disabledCheckbox ? colors.grey2 : colors.blue1}
            color={colors.blue1}
            onPress={() => setIsChecked(!isChecked)}
          />
          <Text style={styles.agreementText}>
            {I18n.t('agreementScreen.agreementCheck')}
          </Text>
        </View>
      </Content>
      <View style={styles.buttonContainer}>
        <Button
          type="danger"
          disabled={!isChecked}
          title={I18n.t('agreementScreen.buttonContinue')}
          style={styles.button}
          onPress={() => navigation.navigate('SignUpScreen')}
        />
      </View>
    </Container>
  );
};

export default AgreementScreen;

const styles = StyleSheet.create({
  content: {
    flex: 1,
    padding: hp(1),
  },
  checkboxContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginRight: hp(5),
    margin: hp(0.1),
  },
  agreementText: {
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.normal,
    marginLeft: hp(3),
  },
  buttonContainer: {
    padding: hp(2),
    marginTop: hp(-2),
  },
  button: {
    borderRadius: 40,
  },
});
