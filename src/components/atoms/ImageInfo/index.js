import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {LookingAroundIllustration} from '../../../assets';
import Gap from '../Gap';
import {fonts, hp} from '../../../utilities';

const ImageInfo = ({
  source = LookingAroundIllustration,
  text = 'Data tidak ditemukan',
  hpSizeImage = 15,
  hpSizeText = 1.6,
  textStyle,
}) => {
  return (
    <View style={styles.noDataContainer}>
      <Image source={source} style={styles.image(hpSizeImage)} />
      <Gap height={20} />
      <Text style={[styles.noDataText(hpSizeText), textStyle]}>{text}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  noDataContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex:1,
  },
  image: (hpSizeImage) => ({
    width: hp(hpSizeImage),
    height: hp(hpSizeImage),
    resizeMode: 'cover',
  }),
  noDataText: (hpSizeText) => ({
    fontSize: hp(hpSizeText),
    fontFamily: fonts.primary[700],
    textAlign: 'center',
  }),
});

export default ImageInfo;