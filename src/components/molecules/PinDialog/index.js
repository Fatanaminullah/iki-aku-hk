/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useState} from 'react';
import I18n from 'react-native-redux-i18n';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import ReactNativeModal from 'react-native-modal';
import ImageBackground from 'react-native/Libraries/Image/ImageBackground';
import {DeletePinIcon, IkiAkuIcon4, PinBackground} from '../../../assets';
import {colors, fonts, hp, wp} from '../../../utilities';
import {Gap, SvgWrapper, Touchable} from '../../atoms';
import Header from '../Header';
import {Chase} from 'react-native-animated-spinkit';

const PinDialog = ({
  screen,
  visibility,
  setVisibility,
  pinLength = 6,
  label,
  labelForgot,
  rightButton = {
    icon: (
      <SvgWrapper
        component={<DeletePinIcon />}
        width={wp(20)}
        height={hp(10)}
      />
    ),
  },
  leftButton,
  headerTitle = 'PIN',
  onPressRightCustomButton,
  onFinished,
  background = PinBackground,
  errorPin = false,
  errorMessage = I18n.t('signUpScreen.wrongPin'),
  validation,
  closeOnFinished = true,
  onPressBackButton,
  onPressForgotButton,
}) => {
  const navigation = useNavigation();
  const [error, setError] = useState(false);
  const [loading, setLoading] = useState(false);
  const [pin, setPin] = useState([]);
  const [position, setPosition] = useState(0);
  useEffect(() => {
    const arr = [];
    for (let i = 0; i < pinLength; i++) {
      arr.push('');
    }
    setPin(arr);
  }, []);
  useEffect(() => {
    if (position === pinLength) {
      onFinishInput();
    }
  }, [position]);
  const onFinishInput = async () => {
    setLoading(true);
    setTimeout(async () => {
      const value = pin.toString().replace(/,/g, '');
      if (validation) {
        if (validation !== value) {
          setError(true);
          setTimeout(() => {
            resetPin();
          }, 1000);
        } else {
          onFinished && (await onFinished(value));
          closeOnFinished && setVisibility(false);
          resetPin();
        }
      } else {
        onFinished && (await onFinished(value));
        closeOnFinished && setVisibility(false);
        resetPin();
      }
      setLoading(false);
    }, 1000);
  };
  const resetPin = () => {
    setPin(pin.map(() => ''));
    setPosition(0);
  };
  const onPressButton = async (key) => {
    setError(false);
    if (position < pinLength) {
      pin[position] = key;
      setPin([...pin]);
      setPosition(position + 1);
    }
  };
  const onPressRightButton = (key) => {
    if (position >= 0) {
      pin[position - 1] = '';
      setPin([...pin]);
      setPosition(position - 1);
    }
  };
  const renderNumber = () => {
    const rows = [];
    const keys = [
      [1, 4, 7, leftButton],
      [2, 5, 8, 0],
      [3, 6, 9, rightButton],
    ];
    keys.forEach((item, index) => {
      const row = [];
      item.forEach((items, i) => {
        if (items?.icon) {
          row.push(
            <TouchableOpacity
              key={i}
              style={styles.keyboardItemCustom}
              onPress={
                onPressRightCustomButton
                  ? () => onPressRightCustomButton()
                  : () => onPressRightButton()
              }>
              {items.icon}
            </TouchableOpacity>,
          );
        } else if (items || items === 0) {
          row.push(
            <TouchableOpacity
              key={i}
              style={styles.keyboardItem}
              disabled={position === pinLength}
              onPress={() => onPressButton(items)}>
              <Text
                style={{
                  fontSize: fonts.size[24],
                  color: colors.white,
                  fontFamily: fonts.primary.bold,
                }}>
                {items}
              </Text>
            </TouchableOpacity>,
          );
        } else {
          row.push(
            <View
              key={i}
              style={{
                width: wp(20),
                height: wp(20),
              }}
            />,
          );
        }
      });
      rows.push(
        <View key={index} style={styles.keyboardRow}>
          {row}
        </View>,
      );
    });
    return rows.map((item) => item);
  };
  const renderPin = () => {
    return pin.map((item) => {
      if (item === '') {
        return <View style={styles.pinEmpty} />;
      } else {
        return (
          <View style={styles.pinEmpty}>
            <View style={styles.pin} />
          </View>
        );
      }
    });
  };
  return (
    <ReactNativeModal
      isVisible={visibility}
      style={styles.modalContainer}
      animationIn="zoomIn"
      animationOut="zoomOut"
      animationInTiming={500}
      animationOutTiming={500}
      backdropTransitionOutTiming={0}>
      <View style={styles.container}>
        <Header
          title={headerTitle}
          onPress={() => {
            resetPin();
            if (onPressBackButton) {
              onPressBackButton();
            } else {
              setVisibility(false);
            }
          }}
        />
        <ImageBackground
          source={background}
          resizeMode="cover"
          style={[{width: wp(100), height: hp(100)}, styles.content]}>
          <SvgWrapper component={<IkiAkuIcon4 />} style={styles.iconStyle} />
          {label ? (
            <Text style={styles.label}>{label}</Text>
          ) : (
            <Gap height={hp(5)} />
          )}
          <View style={styles.pinContainer}>
            {loading ? <Chase color={colors.white} /> : renderPin()}
          </View>
          <Gap height={hp(1.5)} />
          <Text
            style={
              error || errorPin ? styles.labelError : styles.labelTransparent
            }>
            {errorMessage}
          </Text>
          <Touchable
            onPress={() => {
              navigation.navigate('ForgotPinScreen', {screen});
              if (onPressForgotButton) {
                onPressForgotButton();
              } else {
                setVisibility(false);
              }
            }}>
            {labelForgot ? (
              <Text style={styles.forgotPin}>{labelForgot}</Text>
            ) : (
              <Gap height={hp(5)} />
            )}
          </Touchable>
          <View style={styles.keyboard}>{renderNumber()}</View>
        </ImageBackground>
      </View>
    </ReactNativeModal>
  );
};

export default PinDialog;

const styles = StyleSheet.create({
  modalContainer: {
    margin: 0,
  },
  label: {
    fontFamily: fonts.primary[600],
    fontSize: fonts.size[16],
    color: colors.white,
    marginVertical: hp(2),
  },
  labelError: {
    fontFamily: fonts.primary[600],
    fontSize: fonts.size[16],
    color: colors.red1,
    marginTop: hp(3),
  },
  labelTransparent: {
    fontFamily: fonts.primary[600],
    fontSize: fonts.size[16],
    color: 'transparent',
  },
  forgotPin: {
    fontFamily: fonts.primary[600],
    fontSize: fonts.size[16],
    color: colors.white,
  },
  iconStyle: {
    width: hp(15),
    height: hp(15),
    marginBottom: hp(-4),
  },
  container: {
    flex: 1,
    marginTop: hp(-3),
  },
  content: {
    flex: 1,
    alignItems: 'center',
    padding: hp(3),
  },
  pinContainer: {
    alignItems: 'center',
    padding: wp(-7),
    flexDirection: 'row',
    marginBottom: wp(-6),
  },
  keyboard: {
    width: '100%',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: hp(1),
  },
  pinEmpty: {
    width: wp(9),
    height: wp(9),
    borderRadius: 100,
    borderWidth: 5,
    borderColor: colors.white,
    marginHorizontal: hp(1),
  },
  pin: {
    width: '100%',
    height: '100%',
    borderRadius: 100,
    borderWidth: 2,
    borderColor: 'rgba(0,0,0,0.1)',
    backgroundColor: colors.white,
  },
  keyboardRow: {
    justifyContent: 'space-between',
  },
  keyboardItem: {
    width: wp(19),
    height: wp(19),
    borderRadius: 100,
    backgroundColor: 'rgba(0,0,0,0.5)',
    marginHorizontal: hp(1),
    justifyContent: 'center',
    alignItems: 'center',
  },
  keyboardItemCustom: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
