import React from 'react';
import {StyleSheet, View, Text} from 'react-native';
import Modal from 'react-native-modal';
import {Gap, Touchable, SvgWrapper} from '../../atoms';
import {LeftArrowIcon} from '../../../assets';
import {fonts, colors, wp, hp} from '../../../utilities';

const Modals = ({
  children,
  type,
  title,
  animationIn,
  animationOut,
  transparent,
  isVisible,
  style,
  popupStyle,
  onBackButtonPress,
  onBackdropPress,
}) => {
  return (
    <View>
      {type === 'dialogWithTitle' ? (
        <Modal
          backdropTransitionOutTiming={0}
          animationIn={animationIn || 'fadeInUp'}
          animationOut={animationOut || 'fadeOutDown'}
          transparent={true || transparent}
          isVisible={isVisible}
          style={[styles.modalCenteredContainer, style]}
          onBackButtonPress={onBackButtonPress}
          onBackdropPress={onBackdropPress}>
          <View style={[styles.modalDialogWithTitleContainer, popupStyle]}>
            <View style={styles.modalTitleContainer}>
              <Text style={styles.modalTitleLabel}>{title}</Text>
            </View>
            <View style={styles.modalDialogContent}>{children}</View>
          </View>
        </Modal>
      ) : type === 'dialog' ? (
        <Modal
          backdropTransitionOutTiming={0}
          animationIn={animationIn || 'fadeInUp'}
          animationOut={animationOut || 'fadeOutDown'}
          transparent={true || transparent}
          isVisible={isVisible}
          style={[styles.modalCenteredContainer, style]}
          onBackButtonPress={onBackButtonPress}
          onBackdropPress={onBackdropPress}>
          <View style={[styles.modalDialogContainer, popupStyle]}>
            <View style={styles.modalDialogContent}>{children}</View>
          </View>
        </Modal>
      ) : type === 'page' ? (
        <Modal
          backdropTransitionOutTiming={0}
          animationIn={animationIn || 'fadeInUp'}
          animationOut={animationOut || 'fadeOutDown'}
          transparent={true || transparent}
          isVisible={isVisible}
          style={[styles.modalFullscreenContainer, style]}
          onBackButtonPress={onBackButtonPress}
          onBackdropPress={onBackdropPress}>
          <View style={styles.modalFullscreenHeader}>
            <Touchable onPress={onBackdropPress}>
              <SvgWrapper
                component={<LeftArrowIcon />}
                width={wp(5)}
                height={wp(5)}
              />
            </Touchable>
            <Gap width={16} />
            <Text style={styles.modalFullscreenTitle}>{title}</Text>
          </View>
          <View style={styles.modalFullscreenContent}>{children}</View>
        </Modal>
      ) : (
        <Modal
          backdropTransitionOutTiming={0}
          animationIn={animationIn || 'fadeInUp'}
          animationOut={animationOut || 'fadeOutDown'}
          transparent={true || transparent}
          isVisible={isVisible}
          style={[styles.modalFullscreenContainer, style]}
          onBackButtonPress={onBackButtonPress}
          onBackdropPress={onBackdropPress}>
          {children}
        </Modal>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  modalCenteredContainer: {
    alignItems: 'center',
  },
  modalFullscreenContainer: {
    flex: 1,
    alignItems: undefined,
    justifyContent: undefined,
    backgroundColor: colors.white,
  },
  modalDialogContainer: {
    width: wp(85),
    height: hp(20),
    borderRadius: 14,
    backgroundColor: 'rgba(255,255,255,0.97)',
  },
  modalDialogWithTitleContainer: {
    width: wp(85),
    height: hp(30),
    borderRadius: 14,
    backgroundColor: 'rgba(255,255,255,0.97)',
  },
  modalTitleContainer: {
    justifyContent: 'center',
    paddingVertical: hp(2),
    borderBottomWidth: 1,
    borderColor: colors.grey2,
  },
  modalTitleLabel: {
    textAlign: 'center',
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
  },
  modalDialogContent: {
    flex: 1,
    margin: 14,
  },
  modalFullscreenHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: hp(2),
  },
  modalFullscreenTitle: {
    fontFamily: fonts.primary.bold,
    color: colors.black,
    fontSize: fonts.size[20],
  },
  modalFullscreenContent: {
    flex: 1,
    paddingHorizontal: 10,
  },
});

export default Modals;
