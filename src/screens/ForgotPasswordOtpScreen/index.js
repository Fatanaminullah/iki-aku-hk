import React, {useState} from 'react';
import I18n from 'react-native-i18n';
import Countdown from 'react-countdown';
import {StyleSheet, Text} from 'react-native';
import {useDispatch} from 'react-redux';
import {IkiAkuIcon1} from '../../assets';
import {
  Container,
  Content,
  Header,
  Button,
  Gap,
  Link,
  OtpInput,
  SvgWrapper,
} from '../../components';
import {fonts, hp, wp} from '../../utilities';
import {
  showAlertError,
  showAlertSuccess,
} from '../../redux/store/actions/alert';
import {requestOtp, validateOtp} from '../../redux/store/actions/profile';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';

const ForgotPasswordOtpScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const [disabled, setDisabled] = useState(true);
  const [otp, setOtp] = useState('');
  const [timer, setTimer] = useState({
    timer: Date.now() + 59000,
    startTimer: true,
    key: null,
  });

  const onCodeChanged = (code) => {
    setOtp(code);
    if (code.length < 6 && !disabled) {
      setDisabled(true);
    }
    if (code.length === 6) {
      setDisabled(false);
    }
  };

  const onResendOtp = () => {
    const request = {
      reg_id: route?.params?.request?.cust_id,
    };
    dispatch(
      requestOtp(
        request,
        showLoading,
        dismissLoading,
        showAlertSuccess,
        showAlertError,
      ),
    ).then((res) => {
      const time = res.otp_expiry * 60000;
      setOtp('');
      setTimer({
        timer: Date.now() + time,
        startTimer: true,
        key: !timer.key,
      });
    });
  };

  const renderer = ({hours, minutes, seconds, completed, ...rest}) => {
    if (completed) {
      return (
        <Link
          title={I18n.t('forgotPasswordOtpScreen.resendOtp')}
          align="center"
          onPress={onResendOtp}
        />
      );
    } else {
      return (
        <Text style={styles.timer}>
          {hours} : {minutes} : {seconds}
        </Text>
      );
    }
  };

  const onConfirm = () => {
    const request = {
      otp,
      reg_id: route?.params?.request?.cust_id,
      is_forgot_pass: true,
      username: route?.params?.username,
    };
    dispatch(validateOtp(request, showLoading, dismissLoading, showAlertError))
      .then(() => {
        navigation.navigate('CreateNewPasswordScreen', {
          forgot_pass_id: request.reg_id,
          username: request.username,
        });
      })
      .catch(() => {
        setOtp('');
        dispatch(dismissLoading());
      });
  };
  return (
    <Container>
      <Header
        title={I18n.t('forgotPasswordOtpScreen.title')}
        separator={false}
      />
      <Content centerContent>
        <SvgWrapper component={<IkiAkuIcon1 />} style={styles.alignCenter} />
        <Gap height={hp(2)} />
        <Text style={styles.instructionText}>
          {I18n.t('forgotPasswordOtpScreen.instruction')}
        </Text>
        <Gap height={hp(4)} />
        <OtpInput onCodeChanged={onCodeChanged} code={otp} pinCount={6} />
        <Gap height={hp(4)} />
        <Countdown
          key={timer.key}
          date={timer.timer}
          intervalDelay={1000}
          renderer={renderer}
          autoStart={timer.startTimer}
        />
        <Gap height={hp(4)} />
        <Button
          type="danger"
          disabled={disabled}
          title={I18n.t('forgotPasswordOtpScreen.confirmation')}
          rounder
          onPress={onConfirm}
        />
      </Content>
    </Container>
  );
};

export default ForgotPasswordOtpScreen;

const styles = StyleSheet.create({
  alignCenter: {
    alignSelf: 'center',
    width: wp(25),
    height: wp(25),
  },
  instructionText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    textAlign: 'center',
    alignSelf: 'center',
    maxWidth: wp(80),
  },
  timer: {
    alignSelf: 'center',
    fontFamily: fonts.primary.bold,
  },
});
