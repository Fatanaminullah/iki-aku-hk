import {Label} from 'native-base';
import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {CloseIcon} from '../../../assets';
import {SvgWrapper} from '../../atoms';
import {colors, fonts, wp, hp} from '../../../utilities';
import PickerDialog from '../PickerDialog';

const InputPicker = ({
  name,
  setValue,
  value,
  label,
  placeholder,
  dataSource,
  onSelectItem,
  title,
  listEmptyText,
  searchPlaceholder,
  onChange,
  disabled,
}) => {
  const [pickerVisible, setPickerVisible] = useState(false);
  const onSelect = (item) => {
    onChange && onChange(item);
    onSelectItem && onSelectItem(item);
    setValue && setValue(name, item);
  };
  const setLabel = (val) => {
    const result = dataSource.filter(
      (item) => item.value.toString() === val.toString(),
    )[0];
    setValue(name, result);
    if (result) {
      return result.label;
    } else {
      return '';
    }
  };
  return (
    <TouchableOpacity
      onPress={() => setPickerVisible(true)}
      disabled={disabled}>
      <PickerDialog
        title={title}
        dataSource={dataSource}
        visibility={pickerVisible}
        setVisibility={setPickerVisible}
        onSelectItem={onSelect}
        listEmptyText={listEmptyText}
        searchPlaceholder={searchPlaceholder}
      />
      <Label style={styles.label}>{label}</Label>
      <View style={styles.textContainer}>
        <Text style={styles.text(value)}>
          {value ? (value?.label ? value.label : setLabel(value)) : placeholder}
        </Text>
        {value ? (
          <TouchableOpacity
            onPress={() => {
              setValue && setValue(name, '');
              onChange && onChange('');
            }}
            hitSlop={{bottom: 10, left: 10, right: 10, top: 10}}>
            <SvgWrapper
              component={<CloseIcon />}
              width={wp(3)}
              height={hp(3)}
            />
          </TouchableOpacity>
        ) : null}
      </View>
    </TouchableOpacity>
  );
};

export default InputPicker;

const styles = StyleSheet.create({
  label: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
  textContainer: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 22,
    borderBottomColor: colors.grey2,
    borderBottomWidth: 0.8,
  },
  text: (value) => ({
    color: value ? colors.black : colors.grey2,
  }),
  container: {
    paddingVertical: 10,
  },
  content: {
    alignSelf: 'flex-start',
    width: wp(70),
    paddingVertical: 5,
  },
  textLabel: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary[700],
  },
});
