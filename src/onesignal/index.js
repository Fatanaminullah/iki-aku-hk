import React, {Fragment, useEffect} from 'react';
import OneSignal from 'react-native-onesignal';
import {useDispatch} from 'react-redux';
import {ONESIGNAL_ID} from '../environments/env.json';

const OneSignalListener = ({children, navigationRef}) => {
  const dispatch = useDispatch();
  useEffect(() => {
    OneSignal.setLogLevel(6, 0);
    OneSignal.init(ONESIGNAL_ID, {
      kOSSettingsKeyAutoPrompt: false,
      kOSSettingsKeyInAppLaunchURL: false,
      kOSSettingsKeyInFocusDisplayOption: 1,
    });
    OneSignal.inFocusDisplaying(2);
    OneSignal.addEventListener('received', onReceived);
    OneSignal.addEventListener('opened', onOpened);
    OneSignal.addEventListener('ids', onIds);
    return () => {
      OneSignal.removeEventListener('received', onReceived);
      OneSignal.removeEventListener('opened', onOpened);
      OneSignal.removeEventListener('ids', onIds);
    };
  });
  const onReceived = (notification) => {
    console.log('Notification received: ', notification);
  };
  const onOpened = (openResult) => {
    console.log('open result: ', openResult);
  };
  const onIds = (device) => {
    console.log('Device info: ', device);
  };
  return <Fragment>{children}</Fragment>;
};

export default OneSignalListener;
