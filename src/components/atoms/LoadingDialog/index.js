import React from 'react';
import Dialog from 'react-native-popup-dialog';
import {StyleSheet} from 'react-native';
import {Wander} from 'react-native-animated-spinkit';
import {useSelector} from 'react-redux';
import {colors} from '../../../utilities';
import ReactNativeModal from 'react-native-modal';

const LoadingDialog = ({
  spinnerSize = 50,
  spinnerColor = colors.red1,
  containerStyle,
}) => {
  const {visible} = useSelector((state) => state.loading);

  return (
    <ReactNativeModal
      visible={visible}
      overlayOpacity={0.1}
      overlayBackgroundColor="white"
      style={[styles.dialogContainer, containerStyle]}>
      <Wander size={spinnerSize} color={spinnerColor} />
    </ReactNativeModal>
  );
};

export default LoadingDialog;

const styles = StyleSheet.create({
  dialogContainer: {
    alignSelf: 'center',
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent',
  },
});
