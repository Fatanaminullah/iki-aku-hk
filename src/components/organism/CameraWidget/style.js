import {StyleSheet} from 'react-native';
import {colors, fonts, hp, wp} from '../../../utilities';

export default StyleSheet.create({
  container: {
    margin: 0,
  },
  flex1: {
    flex: 1,
  },
  flex2: {
    flex: 1,
  },
  showPictureContainer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    backgroundColor: colors.white,
    alignSelf: 'center',
  },
  showPicture: {
    width: wp(75),
    height: hp(25),
  },
  cameraContainer: {
    flex: 1,
  },
  camera: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cameraContent: {
    flex: 1,
  },
  idCardBoxContainer: {
    flex: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  selfieBoxContainer: {
    flex: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  idCardBox: {
    backgroundColor: 'rgba(255,255,255,0.1)',
    borderColor: colors.red1,
    borderWidth: 0.5,
    width: 320,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  selfieTextBox: {
    backgroundColor: 'rgba(170,170,170,0.2)',
    borderColor: colors.black,
    borderWidth: 0.5,
    marginTop: hp(5),
    marginBottom: hp(2),
    width: wp(85),
    height: hp(4),
    justifyContent: 'center',
    alignItems: 'center',
  },
  selfieBox: {
    backgroundColor: 'rgba(255,255,255,0.1)',
    borderStyle: 'dashed',
    borderColor: colors.white,
    borderWidth: 2,
    borderRadius: 350,
    width: hp(40),
    height: hp(50),
    justifyContent: 'center',
    alignItems: 'center',
  },
  selfieCardBox: {
    borderStyle: 'dashed',
    borderRadius: 1,
    backgroundColor: 'rgba(255,255,255,0.1)',
    borderColor: colors.white,
    borderWidth: 2,
    margin: hp(2),
    marginBottom: hp(15),
    width: wp(45),
    height: hp(12),
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: hp(10),
    width: wp(100),
    backgroundColor: colors.white,
  },
  textButtonCancel: {
    fontFamily: fonts.primary[700],
    fontSize: fonts.size[14],
    color: colors.black,
  },
  buttonCaptureContainer: {
    flex: 1,
    left: -wp(7),
  },
  buttonCapture: {
    borderColor: colors.grey2,
    borderWidth: 1,
    borderRadius: 30,
    paddingVertical: 10,
    paddingHorizontal: 30,
    alignSelf: 'center',
  },
  upperButtonContainer: {
    height: hp(10),
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
  },
  upperButtonContent: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    padding: 10,
    borderRadius: 15,
    flexDirection: 'row',
  },
});
