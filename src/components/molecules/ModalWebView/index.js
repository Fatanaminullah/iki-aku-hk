import React from 'react';
import ReactNativeModal from 'react-native-modal';
import WebView from 'react-native-webview';
import {useDispatch} from 'react-redux';
import { dismissLoading, showLoading } from '../../../redux/store/actions/loading';
import {hp} from '../../../utilities';
import Header from '../Header';

const ModalWebView = ({
  visibility,
  setVisibility,
  source,
  sourceType,
  title = 'Web View',
  showButtonBack,
  onPressBackButton,
}) => {
  const dispatch = useDispatch();
  return (
    <ReactNativeModal
      isVisible={visibility}
      style={{margin: 0, marginTop: -hp(2)}}>
      <Header
        showButtonBack={showButtonBack}
        title={title}
        onPress={() => onPressBackButton}
      />
      <WebView
        originWhitelist={['*']}
        source={sourceType === 'html' ? {html: source} : {uri: source}}
        scrollEnabled
        domStorageEnabled={true}
        startInLoadingState={true}
        scalesPageToFit={true}
        onLoad={() => dispatch(dismissLoading())}
        onLoadStart={() => dispatch(showLoading())}
      />
    </ReactNativeModal>
  );
};

export default ModalWebView;
