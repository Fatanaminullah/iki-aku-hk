import React from 'react'
import { StyleSheet } from 'react-native'
import ScrollIndicator from "react-native-custom-scroll-indicator";
import { colors, hp } from '../../../utilities';

const CustomScrollbar = ({ children, viewBoxStyle, indicatorBgPadding, indicatorStyle, indicatorBoxStyle, scrollViewBoxStyle }) => {
    return (
        <ScrollIndicator
            indicatorBoxStyle={[styles.indicatorBoxStyle, indicatorBoxStyle]}
            indicatorBgPadding={
                indicatorBgPadding !== undefined ?
                    indicatorBgPadding : indicatorBgPaddingDefault
            }
            viewBoxStyle={[styles.viewBoxStyle, viewBoxStyle]}
            scrollViewBoxStyle={[styles.scrollViewBoxStyle, scrollViewBoxStyle]}
            indicatorBackgroundStyle={styles.indicatorBackgroundStyle}
            indicatorStyle={[styles.indicatorStyle, indicatorStyle]}
        >
            {children}
        </ScrollIndicator>
    )
}

const indicatorBgPaddingDefault = 5

const styles = StyleSheet.create({
    scrollViewStyle: {
        flex: 1,
    },
    indicatorBoxStyle: {
        overflow: 'visible'
    },
    viewBoxStyle: {
        alignItems: "center",
    },
    indicatorBackgroundStyle: {
        backgroundColor: colors.grey6,
        borderRadius: 5,
        width: 20,
        height: hp(1.5)
    },
    indicatorStyle: {
        backgroundColor: colors.blue2,
        height: 8,
        borderRadius: 4
    },
    scrollViewBoxStyle: {
        width: "100%",
    }
});


export default CustomScrollbar
