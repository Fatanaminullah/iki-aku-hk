const SET_LIST_BENEFICIARY = '@beneficiary/set-list-beneficiary';
const SET_LIST_RECENT_BENEFICIARY = '@beneficiary/set-list-recent-beneficiary';
const SET_DETAIL_BENEFICIARY = '@beneficiary/set-detail-beneficiary';
const SET_LIST_BANK = '@beneficiary/set-list-bank';

export {
  SET_LIST_BENEFICIARY,
  SET_DETAIL_BENEFICIARY,
  SET_LIST_BANK,
  SET_LIST_RECENT_BENEFICIARY,
};
