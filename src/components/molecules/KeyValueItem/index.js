import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {Gap, HorizontalLine} from '../../atoms';
import {fonts, hp} from '../../../utilities';

const KeyValueItem = ({
  title,
  value,
  size,
  gap,
  valueBold,
  titleBold,
  index,
  withSeparator,
}) => {
  if (title) {
    return (
      <>
        {withSeparator && <HorizontalLine height={0.5} />}
        <Gap height={hp(0.5)} />
        <View style={styles.container(gap)} key={index}>
          <Text style={styles.text(size, titleBold)}>{title}</Text>
          <Text style={styles.text(size, valueBold)}>
            {value ? value : '-'}
          </Text>
        </View>
        <Gap height={hp(0.5)} />
        {withSeparator && <HorizontalLine height={0.5} />}
      </>
    );
  }
  return (
    <View>
      <Gap height={gap ? gap : hp(0.5)} />
      <HorizontalLine type="dashed" />
      <Gap height={gap ? gap : hp(0.5)} />
    </View>
  );
};

export default KeyValueItem;

const styles = StyleSheet.create({
  container: (gap = hp(0.5)) => ({
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: gap,
  }),
  text: (size = fonts.size[14], bold) => ({
    fontSize: size,
    fontFamily: bold ? fonts.primary.bold : fonts.primary.normal,
    maxWidth: 164,
    textAlign: 'right',
  }),
});
