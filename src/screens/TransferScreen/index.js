import React, {useEffect, useState} from 'react';
import {BackHandler, StyleSheet, Text, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {
  FlagIndoRectangle3,
  FlagTaiwanRectangle,
  IkiAkuIcon5,
  UnusedCouponIcon,
  UsedCouponIcon,
} from '../../assets';
import {
  Button,
  Container,
  Content,
  Gap,
  Header,
  InputMoneyTransfer,
  InputPicker,
  ListItem,
  SvgWrapper,
} from '../../components';
import {showAlertConfirmation} from '../../redux/store/actions/alert';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {
  getRateData,
  getSourceOfFunds,
  setCoupon,
} from '../../redux/store/actions/transaction';
import {colors, fonts, thousandSeparator, wp} from '../../utilities';

const TransferScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const [rate, setRate] = useState(1);
  const [maxTrx, setMaxTrx] = useState(0);
  const [idr, setIdr] = useState(0);
  const [twd, setTwd] = useState(0);
  const [adminFee, setAdminFee] = useState(1);
  const [discount, setDiscount] = useState(0);
  const [totalPayment, setTotalPayment] = useState(0);
  const [selectedSourceOfFunds, setSelectedSourceOfFunds] = useState('');
  const [selectedPurpose, setSelectedPurpose] = useState('');
  const {selectedCoupon, sourceOfFunds, purpose, rateData} = useSelector(
    (state) => state.transaction,
  );
  const {fullName} = useSelector((state) => state.profile.userDetails);
  useEffect(() => {
    dispatch(getRateData({currency: 'HKD'}, showLoading, dismissLoading)).then(
      (res) => {
        const {kurs, admin_fee, max_amount_pertrx} = res;
        setRate(Number(kurs));
        setAdminFee(Number(admin_fee));
        setMaxTrx(Number(max_amount_pertrx));
      },
    );
    dispatch(getSourceOfFunds());
  }, []);
  useEffect(() => {
    const subscribe = navigation.addListener('focus', () => {
      if (route.params.screen === 'HomeScreen') {
        resetState();
        navigation.setParams({screen: ''});
      }
    });
    return subscribe;
  });
  const resetState = () => {
    setIdr(0);
    setTwd(0);
    setSelectedSourceOfFunds('');
    setSelectedPurpose('');
    dispatch(setCoupon({}));
  };
  useEffect(() => {
    if ((twd && idr) !== 0) {
      BackHandler.addEventListener('hardwareBackPress', handleBackPress);
    }
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
    };
  }, [twd, idr]);
  useEffect(() => {
    onChangeInputNominal(twd, 'twd');
  }, [selectedCoupon]);
  const handleBackPress = () => {
    if (navigation.isFocused()) {
      if ((twd && idr) !== 0) {
        dispatch(
          showAlertConfirmation(
            '',
            I18n.t('transferScreen.confirmExitText'),
            () => {
              navigation.goBack();
              resetState();
            },
            '',
            I18n.t('transferScreen.confirmPositive'),
            I18n.t('transferScreen.confirmNegative'),
          ),
        );
        return true;
      }
    }
  };
  const onChangeInputNominal = (value, field) => {
    let amount = Number(field === 'twd' ? value : value / rate);
    let total = 0;
    let disc = 0;
    if (field === 'twd') {
      setTwd(value);
      setIdr((value * rate).toString());
    } else {
      setIdr(value);
      setTwd((value / rate).toString());
    }
    if (amount >= selectedCoupon?.min) {
      if (selectedCoupon?.type === 'percentAdmin') {
        disc = -(adminFee * (selectedCoupon?.amount / 100));
        if (disc >= selectedCoupon?.max) {
          disc = selectedCoupon?.max;
        } else {
          disc = disc;
        }
      }
    } else {
      disc = 0;
    }
    total = amount + disc + adminFee;
    setDiscount(disc.toString());
    setTotalPayment(total.toString());
  };
  const onSubmit = () => {
    const data = {
      transferAmount: twd,
      transferredEstimate: idr,
      adminFee: adminFee,
      totalEstimate: totalPayment - discount,
      discount,
      totalPayment,
      sourceOfFunds: selectedSourceOfFunds?.label,
      purpose: selectedPurpose?.label,
      sender: fullName,
      kurs: rate,
    };
    navigation.navigate('BeneficiaryScreen', {data, screen: 'TransferScreen'});
  };
  return (
    <Container>
      <Header
        title={I18n.t('transferScreen.title')}
        onPress={() => {
          if ((idr && twd) === 0) {
            navigation.goBack();
          } else {
            handleBackPress();
          }
        }}
      />
      <Content>
        <View style={styles.logoContainer}>
          <SvgWrapper
            component={<IkiAkuIcon5 />}
            width={wp(25)}
            height={wp(25)}
          />
          <Text style={styles.caption1}>
            {I18n.t('transferScreen.estimatedRate', {
              rate: thousandSeparator(rate),
            })}
          </Text>
        </View>
        <Gap height={15} />
        <View style={styles.caption2Container}>
          <Text style={styles.caption2}>
            {I18n.t('transferScreen.maxTrx', {
              maxTrx: thousandSeparator(maxTrx),
            })}
          </Text>
        </View>
        <Gap height={15} />
        <Text style={styles.caption3}>
          {I18n.t('transferScreen.labelInput')}
        </Text>
        <Gap height={15} />
        <InputMoneyTransfer
          icon={
            <SvgWrapper
              component={<FlagTaiwanRectangle />}
              width={wp(10)}
              height={wp(10)}
            />
          }
          text="HKD"
          value={twd}
          onChange={(e) => onChangeInputNominal(e?.value, 'twd')}
        />
        <Gap height={15} />
        <InputMoneyTransfer
          icon={
            <SvgWrapper
              component={<FlagIndoRectangle3 />}
              width={wp(10)}
              height={wp(10)}
            />
          }
          text="IDR"
          value={idr}
          onChange={(e) => onChangeInputNominal(e?.value, 'idr')}
        />
        <Gap height={15} />
        <Button
          onPress={() =>
            selectedCoupon.title
              ? navigation.navigate('DetailCouponScreen', {
                  item: selectedCoupon,
                  screen: 'Transfer',
                })
              : navigation.navigate('CouponScreen')
          }
          textStyle={styles.buttonCouponText}
          iconGap={0}
          style={styles.buttonCoupon}
          title={
            selectedCoupon.title
              ? I18n.t('transferScreen.couponUsed', {
                  title: selectedCoupon?.title,
                })
              : I18n.t('transferScreen.openCouponButton')
          }
          iconLeft={
            selectedCoupon.title ? (
              <SvgWrapper
                component={<UsedCouponIcon />}
                width={wp(7)}
                height={wp(7)}
              />
            ) : (
              <SvgWrapper
                component={<UnusedCouponIcon />}
                width={wp(7)}
                height={wp(7)}
              />
            )
          }
          type={
            (twd && idr) === 0
              ? 'disabled'
              : selectedCoupon?.title
              ? 'warning'
              : 'primary'
          }
        />
        <Gap height={15} />
        <ListItem
          label={I18n.t('transferScreen.adminFee')}
          labelStyle={styles.adminFee}
          valueStyle={styles.adminFee}
          value2Style={styles.adminFee}
          value1="HKD"
          value2={thousandSeparator(adminFee)}
          columns="3"
        />
        <ListItem
          label={I18n.t('transferScreen.discount')}
          value1="HKD"
          value2={thousandSeparator(discount)}
          columns="3"
        />
        <ListItem
          label={I18n.t('transferScreen.totalPayment')}
          labelStyle={styles.totalPayment}
          valueStyle={styles.totalPayment}
          value2Style={styles.totalPayment}
          value1="HKD"
          value2={thousandSeparator(totalPayment)}
          columns="3"
        />
        <Gap height={15} />
        <InputPicker
          onChange={setSelectedSourceOfFunds}
          value={selectedSourceOfFunds}
          label={I18n.t('transferScreen.sourceOfFunds')}
          dataSource={sourceOfFunds}
          disabled={(twd && idr) === 0}
          searchPlaceholder={I18n.t('transferScreen.searchSourceOfFunds')}
          listEmptyText={I18n.t('transferScreen.sourceOfFundsNotFound')}
          title={I18n.t('transferScreen.placeholderSourceOfFunds')}
          placeholder={I18n.t('transferScreen.placeholderSourceOfFunds')}
        />
        <Gap height={15} />
        <InputPicker
          onChange={setSelectedPurpose}
          value={selectedPurpose}
          label={I18n.t('transferScreen.purpose')}
          dataSource={purpose}
          disabled={(twd && idr) === 0}
          searchPlaceholder={I18n.t('transferScreen.searchPurpose')}
          listEmptyText={I18n.t('transferScreen.purposeNotFound')}
          title={I18n.t('transferScreen.placeholderPurpose')}
          placeholder={I18n.t('transferScreen.placeholderPurpose')}
        />
        <Gap height={15} />
        <Button
          title={I18n.t('transferScreen.continue')}
          type="danger"
          disabled={(idr && twd) === 0}
          rounded
          onPress={() => onSubmit()}
        />
        <Gap height={30} />
      </Content>
    </Container>
  );
};

export default TransferScreen;

const styles = StyleSheet.create({
  caption1: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[10],
    alignSelf: 'flex-start',
  },
  caption2Container: {
    borderRadius: 40,
    backgroundColor: colors.green3,
    alignSelf: 'flex-start',
    padding: 5,
  },
  caption2: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    color: colors.white,
  },
  caption3: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    color: colors.black,
  },
  buttonCoupon: {
    justifyContent: 'flex-start',
    paddingVertical: 7,
    paddingHorizontal: 15,
    borderRadius: 5,
  },
  buttonCouponText: {
    color: colors.white,
    fontFamily: fonts.primary[700],
    fontSize: fonts.size[12],
    marginLeft: 5,
  },
  adminFee: {
    color: colors.red1,
  },
  totalPayment: {
    color: colors.green3,
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    padding: 5,
  },
});
