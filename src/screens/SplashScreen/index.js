import React, {useEffect} from 'react';
import {ImageBackground, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {SplashScreenImgBg} from '../../assets/illustrations';
import {Container} from '../../components';
import {localeActions} from '../../redux/store/actions';
import {checkToken} from '../../redux/store/actions/authentication';
import {getData, removeData, unregisterUserId} from '../../utilities';

const {changeLanguage} = localeActions;

const SplashScreen = ({navigation}) => {
  const {language} = useSelector((state) => state.locale);
  const dispatch = useDispatch();
  useEffect(() => {
    setTimeout(async () => {
      dispatch(changeLanguage(language));
      const data = await getData('token');
      if (data) {
        dispatch(checkToken({token: data?.token}))
          .then((res) => {
            navigation.reset({
              index: 0,
              routes: [{name: 'MainApp'}],
            });
          })
          .catch(async (err) => {
            await removeData('token');
            unregisterUserId();
            navigation.reset({
              index: 0,
              routes: [{name: 'WalkthroughScreen'}],
            });
          });
      } else {
        navigation.replace('WalkthroughScreen');
      }
    }, 1500);
  }, [dispatch, language, navigation]);

  return (
    <Container style={{backgroundColor: '#ECF7FF'}}>
      <ImageBackground
        source={SplashScreenImgBg}
        style={styles.bgImage}
        resizeMode="contain"
      />
    </Container>
  );
};

export default SplashScreen;
const styles = StyleSheet.create({
  bgImage: {
    flex: 1,
    resizeMode: 'cover',
    alignSelf: 'stretch',
  },
});
