import {fontNormalize, hp} from '../function';

export const fonts = {
  primary: {
    200: 'OpenSans-ExtraLight',
    300: 'OpenSans-Light',
    400: 'OpenSans-Regular',
    600: 'OpenSans-SemiBold',
    700: 'OpenSans-Bold',
    800: 'OpenSans-ExtraBold',
    900: 'OpenSans-Black',
    normal: 'OpenSans-Regular',
    bold: 'OpenSans-Bold',
  },
  size: {
    9: fontNormalize(hp(1.3)),
    10: fontNormalize(hp(1.5)),
    11: fontNormalize(hp(1.5)),
    12: fontNormalize(hp(1.6)),
    12.5: fontNormalize(hp(1.65)),
    13: fontNormalize(hp(1.7)),
    14: fontNormalize(hp(1.8)),
    14.5: fontNormalize(hp(1.85)),
    15: fontNormalize(hp(1.9)),
    16: fontNormalize(hp(2)),
    18: fontNormalize(hp(2.2)),
    20: fontNormalize(hp(2.4)),
    22: fontNormalize(hp(2.6)),
    24: fontNormalize(hp(2.8)),
    25: fontNormalize(hp(2.9)),
    28: fontNormalize(hp(3.2)),
  },
};
