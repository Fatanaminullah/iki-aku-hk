import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch} from 'react-redux';
import {showAlertError} from '../../../redux/store/actions/alert';
import {
  trxStatusMapper,
  trxTypeMapper,
} from '../../../redux/store/actions/transaction/mapper';
import {colors, fonts, hp, TRANSACTION_STATUS, wp} from '../../../utilities';
import {Touchable} from '../../atoms';

const HistoryTrxListsItem = ({
  navigation,
  trxItem,
  index,
  setDetailTransaction,
}) => {
  const dispatch = useDispatch();
  const {
    trxDate,
    statusCode,
    status,
    type,
    amount,
    beneficiary,
    bankNo,
  } = trxItem;
  return (
    <Touchable
      key={index}
      onPress={() => {
        if (
          statusCode === TRANSACTION_STATUS.FAILED ||
          statusCode === TRANSACTION_STATUS.ERROR
        ) {
          dispatch(
            showAlertError(
              '',
              I18n.t('transactionHistoryScreen.trxFailedMessage'),
            ),
          );
        } else if (statusCode === TRANSACTION_STATUS.EXPIRED_ORDER) {
          dispatch(
            showAlertError(
              '',
              I18n.t('transactionHistoryScreen.trxExpiredMessage'),
            ),
          );
        } else {
          dispatch(
            setDetailTransaction(
              trxItem,
              type === 'Remittance' ? 'TRANSFER' : 'PPOB',
            ),
          ).then(() => {
            navigation.navigate('TransactionDetailsScreen');
          });
        }
      }}>
      <View style={styles.historyTrxItemRow}>
        <View style={styles.historyTrxItemColumnLeft}>
          <View
            style={[
              styles.historyTrxStatusContainer,
              {backgroundColor: trxStatusMapper(statusCode).color},
            ]}>
            <Text style={styles.historyTrxStatusLabel}>{status}</Text>
          </View>
          <Text style={styles.historyTrxDetail}>{trxDate}</Text>
        </View>
        <View style={styles.historyTrxItemColumnRight}>
          <View
            style={[
              styles.historyTrxTypeContainer,
              {backgroundColor: trxTypeMapper(0).color},
            ]}>
            <Text style={styles.historyTrxStatusLabel}>{type}</Text>
          </View>
          <Text style={styles.historyTrxAmount}>{amount}</Text>
          <Text numberOfLines={1} style={styles.historyTrxDetail}>
            {type === 'Remittance' ? I18n.t('homeScreen.to') + ': ' : ''}{' '}
            {beneficiary}
          </Text>
          <Text style={styles.historyTrxDetail2}>{bankNo}</Text>
        </View>
      </View>
    </Touchable>
  );
};

const styles = StyleSheet.create({
  historyTrxItemRow: {
    flexDirection: 'row',
    marginBottom: hp(0.3),
    paddingVertical: hp(1.5),
    paddingHorizontal: wp(3.5),
    backgroundColor: colors.white,
    height: hp(14.5),
  },
  historyTrxItemColumnLeft: {
    flex: 1,
    justifyContent: 'space-between',
  },
  historyTrxItemColumnRight: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  historyTrxStatusContainer: {
    justifyContent: 'center',
    backgroundColor: colors.orange1,
    height: hp(3),
    width: wp(39),
    borderRadius: (hp(3) + wp(24)) / 20,
  },
  historyTrxStatusLabel: {
    textAlign: 'center',
    color: colors.white,
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.bold,
  },
  historyTrxTypeContainer: {
    justifyContent: 'center',
    backgroundColor: colors.blue1,
    height: hp(3),
    width: wp(24),
    borderRadius: (hp(3) + wp(24)) / 20,
  },
  historyTrxTypeLabel: {
    textAlign: 'center',
    color: colors.white,
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.bold,
  },
  historyTrxAmount: {
    marginTop: hp(0.5),
    color: colors.blue3,
    fontSize: fonts.size[16],
    fontFamily: fonts.primary[800],
  },
  historyTrxDetail: {
    marginTop: hp(0.05),
    fontSize: fonts.size[14],
    fontFamily: fonts.primary[500],
  },
  historyTrxDetail2: {
    marginTop: hp(0.05),
    fontSize: fonts.size[14],
    fontFamily: fonts.primary[500],
    marginLeft: hp(-5),
  },
});

export default HistoryTrxListsItem;
