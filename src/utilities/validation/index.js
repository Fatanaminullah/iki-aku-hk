import I18n from 'react-native-redux-i18n';

export const validation = {
  required: {value: true, message: I18n.t('validation.required')},
  minLength: (val) => ({
    value: val,
    message: I18n.t('validation.minLength', {length: val}),
  }),
  maxLength: (val) => ({
    value: val,
    message: I18n.t('validation.maxLength', {length: val}),
  }),
  length: (val) => ({
    value: val,
    message: I18n.t('validation.length', {length: val}),
  }),
  arcNumber: {
    value: /^[A-Za-z]{2}\d{8}$/,
    message: I18n.t('validation.arcNumber'),
  },
  confirmPassword: (confirmPassword, newPassword) => {
    return (
      confirmPassword === newPassword || I18n.t('validation.confirmPassword')
    );
  },
  email: {
    value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
    message: I18n.t('validation.email'),
  },
};
