const SET_COUPON = '@transfer/set-coupon';
const SET_TRANSFER_DATA = '@transfer/set-transfer-data';

export {SET_COUPON, SET_TRANSFER_DATA};
