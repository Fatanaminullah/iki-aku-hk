import React, {useRef} from 'react';
import Bugsnag from '@bugsnag/react-native';
import i18n from './utilities/i18n';
import {LogBox} from 'react-native';
import {PersistGate} from 'redux-persist/integration/react';
import {Provider} from 'react-redux';
import Navigator from './navigator';
import configureStore from './redux/store';
import {LoadingDialog, Alert, MyStatusBar} from './components';
import OneSignalListener from './onesignal';

export let navigationRef;
export const store = configureStore().store;
const persistor = configureStore().persistor;
LogBox.ignoreAllLogs();
// Bugsnag.start();

const App = () => {
  navigationRef = useRef();
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <MyStatusBar />
        <OneSignalListener navigationRef={navigationRef} />
        <LoadingDialog />
        <Alert />
        <Navigator navigationRef={navigationRef} />
      </PersistGate>
    </Provider>
  );
};

export default App;
