import React from 'react';
import {StyleSheet, Text, Image, View} from 'react-native';
import I18n from 'react-native-i18n';
import {MerchantPayment} from '../../../assets';
import {Gap} from '../../atoms';
import {colors, fonts, hp, thousandSeparator} from '../../../utilities';

const TransactionPaymentCode = ({containerStyle, total, code}) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <Text style={styles.smallText}>
        {I18n.t('transactionDetailsScreen.totalPayment')}:
      </Text>
      <Text style={styles.bigText}>HKD {thousandSeparator(total)}</Text>
      <Gap height={hp(2.5)} />
      <Text style={styles.smallText}>
        {I18n.t('transactionDetailsScreen.paymentCode')}:
      </Text>
      <Text style={styles.bigText}>{code}</Text>
      <Gap height={hp(2.5)} />
      <Text style={styles.smallText}>
        {I18n.t('transactionDetailsScreen.codeShow')}
      </Text>
      <View style={styles.merchantContainer}>
        <View style={styles.bottomImage}>
          <Image
            resizeMode="contain"
            source={MerchantPayment}
            style={styles.merchantStyle}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 20,
    backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: colors.grey2,
    borderRadius: 10,
  },
  smallText: {
    textAlign: 'center',
    fontFamily: fonts.primary[600],
  },
  bigText: {
    textAlign: 'center',
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[22],
    color: colors.blue3,
  },
  topImage: {
    marginTop: 17,
    marginBottom: 14,
  },
  bottomImage: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  merchantContainer: {
    alignItems: 'center',
  },
  merchantStyle: {
    flex: 1,
    alignSelf: 'center',
  },
});

export default TransactionPaymentCode;
