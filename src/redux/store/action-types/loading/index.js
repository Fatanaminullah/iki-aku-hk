const SHOW_LOADING = '@loading/show-loading';
const HIDE_LOADING = '@loading/hide-loading';

export {SHOW_LOADING, HIDE_LOADING};
