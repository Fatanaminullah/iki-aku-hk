import Clipboard from '@react-native-community/clipboard';
import moment from 'moment';
import {useEffect, useRef} from 'react';
import {Dimensions, PixelRatio, Platform} from 'react-native';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import OneSignal from 'react-native-onesignal';
import {PERMISSIONS, request} from 'react-native-permissions';
import I18n from 'react-native-redux-i18n';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import SimpleToast from 'react-native-simple-toast';

const wp = (val) => {
  return widthPercentageToDP(val);
};

const hp = (val) => {
  return heightPercentageToDP(val);
};

const ww = Math.round(Dimensions.get('window').width);
const wh = Math.round(Dimensions.get('window').height);

const fontNormalize = (size) => {
  return size / PixelRatio.getFontScale();
};

const thousandSeparator = (number, prefix) => {
  var item = number.toString().split('.');
  item[0] = item[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');
  return `${prefix || ''} ${item.join('.')}`;
};

const dateToString = (value, type) => {
  switch (type) {
    case 1:
      return moment(value).format('YYYY-MM-D');
    case 2:
      return moment(value).format('YYYY MMM D');
    case 3:
      return moment(value).format('dddd, DD MMMM YYYY');
    case 4:
      return moment(value).format('DD MMMM YYYY');
    case 5:
      return moment(value).utc(false).format('DD MMM YYYY HH:mm:ss');
    case 6:
      return moment(value, 'YYYY/MM/DD HH:mm:ss').format(
        'DD MMM YYYY HH:mm:ss',
      );
    default:
      return moment(value).format('YYYY-MM-D');
  }
};

const greetingTexts = () => {
  const now = moment();
  const currentHour = now.local().hour();
  if (currentHour >= 4 && currentHour < 12)
    return `${I18n.t('greetings.morning')}`;
  else if (currentHour >= 12 && currentHour < 16)
    return `${I18n.t('greetings.afternoon')}`;
  else if (currentHour >= 16 && currentHour < 20)
    return `${I18n.t('greetings.evening')}`;
  else if (currentHour >= 20 && currentHour < 4)
    return `${I18n.t('greetings.night')}`;
  else return `${I18n.t('greetings.welcome')}`;
};

const textLimiter = (text, limit) => {
  if (text.length > limit) {
    return text.substring(0, limit - 3) + '...';
  } else {
    return text;
  }
};

const registerUserId = (userId) => {
  OneSignal.setExternalUserId(userId, (res) => {
    console.log('Results of setting external user id: ', res);
  });
};
const unregisterUserId = () => {
  OneSignal.removeExternalUserId((res) => {
    console.log('Result of remove external user id: ', res);
  });
};
const getInitials = (name, delimeter) => {
  if (name) {
    const array = name
      .replace(/\s{2,}/g, ' ')
      .trim()
      .split(delimeter);

    switch (array.length) {
      case 1:
        return array[0].substring(0, 2).toUpperCase();
      default:
        if (name.includes('Gojek')) {
          return array[0].substring(0, 2).toUpperCase();
        } else {
          return (
            array[0].charAt(0).toUpperCase() +
            array[array.length - 1].charAt(0).toUpperCase()
          );
        }
    }
  }

  return false;
};

const phoneNumberNormalizer = (phoneNumber) => {
  let noSpecialChar = phoneNumber.replace(/[^a-zA-Z0-9 ]/g, '');
  let formattedPhoneNumber = noSpecialChar.replace('62', '0').replace(/ /g, '');
  return formattedPhoneNumber;
};

const amountValueNormalizer = (val) => {
  return Number(val.toString().replace(/rp/gi, '').replace(/\./g, '').trim());
};

const compare = (a, b, params) => {
  if (a[params] < b[params]) {
    return -1;
  }
  if (a[params] > b[params]) {
    return 1;
  }
  return 0;
};

const requestAllPermission = async () => {
  let contactStatus, storageStatus, cameraStatus, locationStatus;
  if (Platform.OS === 'android') {
    contactStatus = await request(PERMISSIONS.ANDROID.READ_CONTACTS);
    storageStatus = await request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE);
    cameraStatus = await request(PERMISSIONS.ANDROID.CAMERA);
    locationStatus = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
    if (locationStatus === 'granted') {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      });
    }
  } else if (Platform.OS === 'ios') {
    contactStatus = await request(PERMISSIONS.IOS.CONTACTS);
    storageStatus = await request(PERMISSIONS.IOS.PHOTO_LIBRARY);
    cameraStatus = await request(PERMISSIONS.IOS.CAMERA);
    locationStatus = await request(PERMISSIONS.IOS.LOCATION_ALWAYS);
  }
  return {contactStatus, storageStatus, cameraStatus, locationStatus};
};

const requestLocationPermission = async () => {
  let locationStatus;
  if (Platform.OS === 'android') {
    locationStatus = await request(PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION);
    if (locationStatus === 'granted') {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      });
    }
  } else if (Platform.OS === 'ios') {
    locationStatus = await request(PERMISSIONS.IOS.LOCATION_ALWAYS);
  }
  return {locationStatus};
};

const requestContactPermission = async (dispatch, showAlertError) => {
  let status;
  if (Platform.OS === 'android') {
    status = await request(PERMISSIONS.ANDROID.READ_CONTACTS);
  } else if (Platform.OS === 'ios') {
    status = await request(PERMISSIONS.IOS.CONTACTS);
  }
  if (status !== 'granted') {
    dispatch(showAlertError('', `${I18n.t('permission.needPermission')}`));
  }
  return {status};
};
const requestStoragePermission = async (dispatch, showAlertError) => {
  let status;
  if (Platform.OS === 'android') {
    status = await request(PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE);
  } else if (Platform.OS === 'ios') {
    status = await request(PERMISSIONS.IOS.PHOTO_LIBRARY);
  }
  return {status};
};

const requestCameraPermission = async () => {
  let storageStatus, cameraStatus;
  if (Platform.OS === 'android') {
    storageStatus = await request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
    cameraStatus = await request(PERMISSIONS.ANDROID.CAMERA);
  } else if (Platform.OS === 'ios') {
    storageStatus = await request(PERMISSIONS.IOS.PHOTO_LIBRARY);
    cameraStatus = await request(PERMISSIONS.IOS.CAMERA);
  }
  return {storageStatus, cameraStatus};
};

const copyToClipboard = async (
  text,
  toastText,
  toastDuration,
  toastGravity,
) => {
  await Clipboard.setString(`${text}`);
  SimpleToast.showWithGravity(
    toastText,
    SimpleToast[toastDuration],
    SimpleToast[toastGravity],
  );
};

const GetPreviousState = (value) => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref;
};

export {
  wp,
  hp,
  ww,
  wh,
  textLimiter,
  fontNormalize,
  getInitials,
  phoneNumberNormalizer,
  amountValueNormalizer,
  requestAllPermission,
  requestContactPermission,
  requestCameraPermission,
  requestStoragePermission,
  requestLocationPermission,
  compare,
  registerUserId,
  unregisterUserId,
  thousandSeparator,
  dateToString,
  copyToClipboard,
  GetPreviousState,
  greetingTexts,
};
