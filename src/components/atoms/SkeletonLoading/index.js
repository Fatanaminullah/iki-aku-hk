import React from 'react';
import {StyleSheet, View} from 'react-native';
import {colors, hp, wp} from '../../../utilities';
import ContentLoader, {Rect, Circle} from 'react-content-loader/native';

const SkeletonLoading = ({
  title,
  backgroundColor,
  foregroundColor,
  style,
  width,
  height,
  radiusX,
  radiusY,
  type,
}) => {
  if (type === 'rectangle') {
    return (
      <ContentLoader
        title={title || 'loader'}
        animate={true}
        speed={0.5}
        backgroundColor={backgroundColor || colors.grey2}
        foregroundColor={foregroundColor || colors.white}
        style={style || styles.contentLoader}>
        <Rect
          x="0"
          y="0"
          rx={radiusX || '0'}
          ry={radiusY || '0'}
          width={width}
          height={height}
        />
      </ContentLoader>
    );
  } else if (type === 'circle') {
    return (
      <ContentLoader
        title={title || 'loader'}
        animate={true}
        speed={0.5}
        backgroundColor={backgroundColor || colors.grey2}
        foregroundColor={foregroundColor || colors.white}
        style={{width: width, height: width}}>
        <Circle cx={width / 2} cy={width / 2} r={width / 2} />
      </ContentLoader>
    );
  } else if (type === 'trxLists') {
    return (
      <View style={styles.containerTrx}>
        <View style={styles.columnLeft}>
          <ContentLoader
            title={title || 'loader'}
            animate={true}
            speed={0.5}
            backgroundColor={backgroundColor || colors.grey2}
            foregroundColor={foregroundColor || colors.white}
            style={style || styles.loaderTrxStatus}>
            <Rect
              x="0"
              y="0"
              rx={(hp(3) + wp(24)) / 20}
              ry={(hp(3) + wp(24)) / 20}
              width={wp(24)}
              height={hp(3)}
            />
          </ContentLoader>
          <ContentLoader
            title={title || 'loader'}
            animate={true}
            speed={0.5}
            backgroundColor={backgroundColor || colors.grey2}
            foregroundColor={foregroundColor || colors.white}
            style={styles.loaderSmallText}>
            <Rect
              x="0"
              y="0"
              rx={(hp(1.2) + wp(24)) / 20}
              ry={(hp(1.2) + wp(24)) / 20}
              width={wp(24)}
              height={hp(1.2)}
            />
          </ContentLoader>
        </View>
        <View style={styles.columnRight}>
          <ContentLoader
            title={title || 'loader'}
            animate={true}
            speed={0.5}
            backgroundColor={backgroundColor || colors.grey2}
            foregroundColor={foregroundColor || colors.white}
            style={styles.loaderTrxType}>
            <Rect
              x="0"
              y="0"
              rx={(hp(3) + wp(24)) / 20}
              ry={(hp(3) + wp(24)) / 20}
              width={wp(24)}
              height={hp(3)}
            />
          </ContentLoader>
          <ContentLoader
            title={title || 'loader'}
            animate={true}
            speed={0.5}
            backgroundColor={backgroundColor || colors.grey2}
            foregroundColor={foregroundColor || colors.white}
            style={styles.loaderBigText}>
            <Rect
              x="0"
              y="0"
              rx={(hp(2) + wp(24)) / 20}
              ry={(hp(2) + wp(24)) / 20}
              width={wp(24)}
              height={hp(2)}
            />
          </ContentLoader>
          <ContentLoader
            title={title || 'loader'}
            animate={true}
            speed={0.5}
            backgroundColor={backgroundColor || colors.grey2}
            foregroundColor={foregroundColor || colors.white}
            style={styles.loaderSmallText}>
            <Rect
              x="0"
              y="0"
              rx={(hp(1.2) + wp(24)) / 20}
              ry={(hp(1.2) + wp(24)) / 20}
              width={wp(24)}
              height={hp(1.2)}
            />
          </ContentLoader>
          <ContentLoader
            title={title || 'loader'}
            animate={true}
            speed={0.5}
            backgroundColor={backgroundColor || colors.grey2}
            foregroundColor={foregroundColor || colors.white}
            style={styles.loaderSmallText}>
            <Rect
              x="0"
              y="0"
              rx={(hp(1.2) + wp(24)) / 20}
              ry={(hp(1.2) + wp(24)) / 20}
              width={wp(24)}
              height={hp(1.2)}
            />
          </ContentLoader>
        </View>
      </View>
    );
  } else {
    return (
      <ContentLoader
        title={title || 'loader'}
        animate={true}
        speed={0.5}
        backgroundColor={backgroundColor || colors.grey2}
        foregroundColor={foregroundColor || colors.white}
        style={style || styles.contentLoader}>
        <Rect
          x="0"
          y="0"
          rx={radiusX || '0'}
          ry={radiusY || '0'}
          width={width}
          height={height}
        />
      </ContentLoader>
    );
  }
};

const styles = StyleSheet.create({
  contentLoader: {
    flex: 1,
    height: hp(20),
    width: wp(100),
    marginBottom: hp(1),
    alignSelf: 'center',
  },
  containerTrx: {
    flexDirection: 'row',
    marginBottom: hp(0.3),
    paddingVertical: hp(1.5),
    paddingHorizontal: wp(3.5),
    backgroundColor: colors.white,
    height: hp(14.5),
  },
  columnLeft: {
    flex: 1,
    justifyContent: 'space-between',
  },
  columnRight: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  loaderTrxStatus: {
    justifyContent: 'center',
    height: hp(3.1),
    width: wp(24),
    borderRadius: (hp(3) + wp(24)) / 20,
  },
  loaderTrxType: {
    justifyContent: 'center',
    height: hp(3.1),
    width: wp(24),
    borderRadius: (hp(3) + wp(24)) / 20,
  },
  loaderSmallText: {
    justifyContent: 'center',
    height: hp(1.3),
    width: wp(24),
    borderRadius: (hp(3) + wp(24)) / 20,
  },
  loaderBigText: {
    justifyContent: 'center',
    height: hp(2.1),
    width: wp(24),
    borderRadius: (hp(3) + wp(24)) / 20,
  },
});

export default SkeletonLoading;
