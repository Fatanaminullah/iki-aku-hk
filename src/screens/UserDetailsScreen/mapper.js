import React from 'react';
import I18n from 'react-native-redux-i18n';
import {ArcBack, ArcFront, SelfieArc} from '../../assets';
import {SvgWrapper} from '../../components';
import {hp, wp} from '../../utilities';

const arrayDetail = [
  {
    value: 'arcExpiredDate',
    type: 'date-picker',
    isEditable: true,
  },
  {
    value: 'arcPhoto',
    type: 'image-picker',
    isEditable: true,
  },
  {
    value: 'arcNumber',
    type: 'text',
    isEditable: false,
  },
  {
    value: 'fullName',
    type: 'text',
    isEditable: false,
  },
  {
    value: 'birthDate',
    type: 'date-picker',
    isEditable: false,
  },
  {
    value: 'gender',
    type: 'radio-group',
    isEditable: true,
  },
  {
    value: 'addressInTaiwan',
    type: 'input-text',
    isEditable: true,
  },
  {
    value: 'occupation',
    type: 'input-text',
    isEditable: true,
  },
  {
    value: 'phoneNumber',
    type: 'input-phone',
    isEditable: true,
  },
];

const mapperDetail = (data) => {
  const result = arrayDetail.map((item) => {
    if (item.value === 'arcPhoto') {
      return {
        title: I18n.t('editProfileScreen.arcPhoto'),
        value: [
          {
            title: I18n.t('editProfileScreen.arcFront'),
            img: (
              <SvgWrapper
                component={<ArcFront />}
                width={wp(15)}
                height={hp(15)}
              />
            ),
          },
          {
            title: I18n.t('editProfileScreen.arcBack'),
            img: (
              <SvgWrapper
                component={<ArcBack />}
                width={wp(15)}
                height={hp(15)}
              />
            ),
          },
          {
            title: I18n.t('editProfileScreen.selfieWithArc'),
            img: (
              <SvgWrapper
                component={<SelfieArc />}
                width={wp(15)}
                height={hp(15)}
              />
            ),
          },
        ],
        type: item.type,
        isEditable: item.isEditable,
      };
    } else {
      return {
        title: I18n.t(`editProfileScreen.${item.value}`),
        value: data[item.value],
        fieldName: item.value,
        type: item.type,
        isEditable: item.isEditable,
      };
    }
  });
  return result;
};

export default mapperDetail;
