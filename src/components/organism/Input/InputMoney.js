import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {amountValueNormalizer, colors, fonts, hp} from '../../../utilities';
import {TextInputMask} from 'react-native-masked-text';
import {Item, Label} from 'native-base';

const InputWithIcon = ({
  height,
  placeholder,
  onChangeText,
  value,
  secureTextEntry,
  disable,
  error,
  textAlign,
  maxLength,
  ref,
  prefix = 'Rp',
  showPrefix = true,
  label,
  labelStyle,
  prefixStyle,
  inputStyle,
}) => {
  const onValueChange = (e) => {
    const result = {
      formattedValue: e,
      value: amountValueNormalizer(e),
    };
    onChangeText(result);
  };
  return (
    <View>
      {label && <Label style={[styles.label, labelStyle]}>{label}</Label>}
      <Item style={styles.container(error)}>
        {showPrefix && (
          <Text style={[styles.currency, prefixStyle]}>{prefix}</Text>
        )}
        <TextInputMask
          placeholder={placeholder}
          type="money"
          value={value}
          style={[styles.input, inputStyle]}
          onChangeText={onValueChange}
          options={{
            separator: '.',
            precision: 0,
            delimiter: '.',
            unit: '',
            suffixUnit: '',
          }}
          placeholderTextColor={colors.grey1}
          keyboardType="numeric"
          secureTextEntry={secureTextEntry}
          editable={!disable}
          selectTextOnFocus={!disable}
          maxLength={maxLength}
          ref={ref}
        />
      </Item>
    </View>
  );
};

export default InputWithIcon;

const styles = StyleSheet.create({
  container: (error) => ({
    borderWidth: error ? 1 : 0,
    borderColor: error ? colors.red1 : colors.grey2,
    paddingHorizontal: 15,
  }),
  label: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
  currency: {
    fontFamily: fonts.primary[700],
    fontSize: fonts.size[14],
  },
  input: {
    textAlign: 'right',
    flex: 1,
    height: hp(6),
  },
});
