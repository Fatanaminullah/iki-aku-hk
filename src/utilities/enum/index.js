import React from 'react';
import I18n from 'react-native-redux-i18n';
import {
  AxisIcon,
  IndosatIcon,
  SmartfrenIcon,
  TelkomselIcon,
  ThreeIcon,
  XlIcon,
} from '../../assets';

const trxHistorySort = [
  {label: I18n.t('transactionHistoryScreen.newest'), value: 'DESC'},
  {label: I18n.t('transactionHistoryScreen.oldest'), value: 'ASC'},
];

const TRANSACTION_STATUS = {
  WAITING_PAYMENT: 1,
  EXPIRED_ORDER: 2,
  ON_PROCESS: 3,
  COMPLETED: 4,
  FAILED: 5,
  ERROR: 6,
  CANCELLED: 7,
};

const API_SERVICES = {
  IKI_AKU: 'IKI AKU',
  ALI_OSS: 'ALI OSS',
};

const trxHistoryStatusFilter = [
  {label: I18n.t('transactionHistoryScreen.all'), value: '0'},
  {
    label: I18n.t('transactionHistoryScreen.unpaid'),
    value: TRANSACTION_STATUS.WAITING_PAYMENT,
  },
  {
    label: I18n.t('transactionHistoryScreen.expired'),
    value: TRANSACTION_STATUS.EXPIRED_ORDER,
  },
  {
    label: I18n.t('transactionHistoryScreen.onProcess'),
    value: TRANSACTION_STATUS.ON_PROCESS,
  },
  {
    label: I18n.t('transactionHistoryScreen.success'),
    value: TRANSACTION_STATUS.COMPLETED,
  },
  {
    label: I18n.t('transactionHistoryScreen.failed'),
    value: TRANSACTION_STATUS.FAILED,
  },
  {
    label: I18n.t('transactionHistoryScreen.error'),
    value: TRANSACTION_STATUS.ERROR,
  },
  {
    label: I18n.t('transactionHistoryScreen.cancelled'),
    value: TRANSACTION_STATUS.CANCELLED,
  },
];

const lineURL = 'https://line.me/R/oaMessage/@ikitaiwan/';

const LIST_PROVIDER = {
  TELKOMSEL: {
    prefix: [
      '0811',
      '0812',
      '0813',
      '0821',
      '0822',
      '0852',
      '0853',
      '0823',
      '0851',
    ],
    icon: <TelkomselIcon />,
  },

  INDOSAT: {
    prefix: ['0814', '0815', '0816', '0855', '0856', '0857', '0858'],
    icon: <IndosatIcon />,
  },

  XL: {
    prefix: ['0817', '0818', '0819', '0859', '0877', '0878'],
    icon: <XlIcon />,
  },

  AXIS: {
    prefix: ['0838', '0831', '0832', '0833'],
    icon: <AxisIcon />,
  },

  THREE: {
    prefix: ['0895', '0896', '0897', '0898', '0899'],
    icon: <ThreeIcon />,
  },

  SMARTFREN: {
    prefix: [
      '0881',
      '0882',
      '0883',
      '0884',
      '0885',
      '0886',
      '0887',
      '0888',
      '0889',
    ],
    icon: <SmartfrenIcon />,
  },
};

const LIST_PROVIDER_PREFIX = new Map();
for (let key in LIST_PROVIDER) {
  const listPrefix = LIST_PROVIDER[key].prefix;
  for (let i = 0; i < listPrefix.length; i++) {
    const providerPrefix = listPrefix[i];
    LIST_PROVIDER_PREFIX.set(providerPrefix, {
      provider: key,
      icon: LIST_PROVIDER[key].icon,
    });
  }
}

const LIST_DATA_PACKAGE_PRODUCT = [
  {
    id: '1',
    title: 'Tsel Paket Malam 1GB (00-07)',
    value: 'Tsel Data 1GB (00-07) 1 hari',
  },
  {
    id: '2',
    title: 'Tsel Paket Malam 1GB (00-07)',
    value: 'Tsel Data 1GB (00-07) 1 hari',
  },
  {
    id: '3',
    title: 'Tsel Paket Malam 1GB (00-07)',
    value: 'Tsel Data 1GB (00-07) 1 hari',
  },
];

const LIST_PLN_PREPAID_PRODUCT = [
  {id: '1', value: '20.000'},
  {id: '2', value: '50.000'},
  {id: '3', value: '100.000'},
  {id: '4', value: '200.000'},
  {id: '5', value: '500.000'},
  {id: '6', value: '1.000.000'},
];

const LIST_PDAM_PRODUCT = [
  {id: '1', value: 'Bogor'},
  {id: '2', value: 'Kabupaten Bogor'},
  {id: '3', value: 'Papua'},
  {id: '4', value: 'Bali'},
  {id: '5', value: 'Aceh'},
  {id: '6', value: 'Bengkulu'},
];

const PPOB = {
  MOBILE_TOP_UP: 'MOBILE TOP UP',
  DATA_PACKAGE: 'DATA PACKAGE',
  PLN_PREPAID: 'PLN PREPAID',
  PLN_POSTPAID: 'PLN POSTPAID',
  BPJS_KESEHATAN: 'BPJS KESEHATAN',
  PDAM: 'PDAM',
  VOUCHER_GAMES: 'VOUCHER GAMES',
};

const LIST_NON_DIRECT_VOUCHER_GAMES = [
  'AERIA GAMES',
  'FACEBOOK GAME CARD',
  'GEMSCOOL',
  'LYTO',
  'MEGAXUS',
  'PLAYSTATION NETWORK',
  'POINT BLANK CASH',
  'PUBG',
  'STEAM WALLET',
  'GARENA',
];

export {
  trxHistorySort,
  trxHistoryStatusFilter,
  lineURL,
  LIST_PROVIDER_PREFIX,
  LIST_PDAM_PRODUCT,
  LIST_PLN_PREPAID_PRODUCT,
  LIST_DATA_PACKAGE_PRODUCT,
  PPOB,
  TRANSACTION_STATUS,
  LIST_NON_DIRECT_VOUCHER_GAMES,
  API_SERVICES,
};
