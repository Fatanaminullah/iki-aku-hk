import React, {useEffect, useState} from 'react';
import I18n from 'react-native-redux-i18n';
import {Label} from 'native-base';
import {FlatList, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import ReactNativeModal from 'react-native-modal';
import {Gap, HorizontalLine, ImageInfo, SvgWrapper} from '../../atoms';
import {Button} from '../../molecules';
import Input from '../Input';
import {colors, fonts, hp, wp} from '../../../utilities';
import {ContactImage, SearchIcon2} from '../../../assets';

const PickerDialog = ({
  visibility,
  dataSource,
  setVisibility,
  onSelectItem,
  title,
  listEmptyText,
  searchPlaceholder,
}) => {
  const [searchValue, setSearchValue] = useState('');
  const [list, setList] = useState(dataSource);
  useEffect(() => {
    setList(dataSource);
  }, [dataSource]);
  const onSearch = (e) => {
    setSearchValue(e);
    if (e) {
      const arr = dataSource.filter((item) =>
        item.label.toLowerCase().includes(e.toLowerCase()),
      );
      setList(arr);
    } else {
      setList(dataSource);
    }
  };
  const onSelect = (item) => {
    onSelectItem && onSelectItem(item);
    setVisibility(false);
  };
  const renderListItem = ({item, index}) => {
    return (
      <TouchableOpacity
        key={index}
        style={styles.containerListItem}
        onPress={() => onSelect(item)}>
        <View style={[styles.contentListItem]}>
          <Text style={[styles.textValue]}>{item?.label}</Text>
        </View>
        <HorizontalLine height={0.5} />
      </TouchableOpacity>
    );
  };
  return (
    <ReactNativeModal
      isVisible={visibility}
      animationIn="zoomIn"
      animationOut="zoomOut"
      animationInTiming={500}
      animationOutTiming={500}>
      <View style={styles.modalContainer}>
        <Label style={styles.title}>{title}</Label>
        <Input
          value={searchValue}
          onChange={onSearch}
          placeholderTextColor={colors.grey1}
          placeholder={searchPlaceholder}
          leftIcon={
            <SvgWrapper
              component={<SearchIcon2 />}
              width={wp(6)}
              height={wp(6)}
            />
          }
          containerStyle={styles.inputContainer}
          inputStyle={{
            fontSize: fonts.size[16],
          }}
          rounded
        />
        <Gap height={10} />
        <FlatList
          data={list}
          keyboardShouldPersistTaps="handled"
          ListEmptyComponent={
            <ImageInfo
              text={listEmptyText}
              source={ContactImage}
              hpSizeImage={22}
              hpSizeText={1.8}
            />
          }
          renderItem={renderListItem}
          keyExtractor={(item) => item.id}
        />
        <Gap height={10} />
        <Button
          rounded
          title={I18n.t('beneficiaryFormScreen.cancel')}
          onPress={() => setVisibility(false)}
        />
      </View>
    </ReactNativeModal>
  );
};

export default PickerDialog;

const styles = StyleSheet.create({
  modalContainer: {
    borderRadius: 20,
    backgroundColor: colors.white,
    alignItems: 'center',
    padding: 10,
    height: hp(60),
  },
  inputContainer: {
    backgroundColor: colors.white,
    width: wp(80),
    borderColor: colors.grey1,
  },
  title: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    marginVertical: 5,
  },
  label: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
  textContainer: {
    paddingVertical: 10,
    paddingHorizontal: 22,
    borderBottomColor: colors.grey2,
    borderBottomWidth: 0.8,
  },
  text: (value) => ({
    color: value ? colors.black : colors.grey2,
    fontFamily: value ? fonts.primary.bold : fonts.primary.normal,
    fontSize: value ? fonts.size[16] : fonts.size[14],
  }),
  containerListItem: {
    paddingVertical: 10,
  },
  contentListItem: {
    alignSelf: 'flex-start',
    width: wp(75),
    paddingVertical: 5,
  },
});
