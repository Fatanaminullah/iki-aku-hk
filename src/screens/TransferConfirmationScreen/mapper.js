import I18n from 'react-native-redux-i18n';
import {thousandSeparator} from '../../utilities';

const sender = ['sender', 'sourceOfFunds', 'purpose'];
const recipient = ['fullName', 'bankName', 'accountNumber'];
const amountDetail = [
  'transferAmount',
  'transferredEstimate',
  'adminFee',
  'totalEstimate',
  'discount',
  'totalPayment',
];

const senderMapper = (data) => {
  const result = [];
  sender.map((item) => {
    result.push({
      label: I18n.t(`transferConfirmationScreen.${item}`),
      value: data[item] || '-',
    });
  });
  return result;
};
const recipientMapper = (data) => {
  const result = [];
  recipient.map((item) => {
    result.push({
      label: I18n.t(`transferConfirmationScreen.${item}`),
      value: data[item] || '-',
    });
  });
  return result;
};
const amountDetailMapper = (data) => {
  const result = [];
  amountDetail.map((item) => {
    result.push({
      label: I18n.t(`transferConfirmationScreen.${item}`),
      currency: item === 'transferredEstimate' ? 'IDR' : 'HKD',
      value: thousandSeparator(data[item]) || '-',
    });
  });
  return result;
};

export {senderMapper, recipientMapper, amountDetailMapper};
