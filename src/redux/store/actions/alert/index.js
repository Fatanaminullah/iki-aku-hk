import {alertActionTypes} from '../../action-types';
const {
  SHOW_ALERT_SUCCESS,
  HIDE_ALERT_SUCCESS,
  SET_TITLE_SUCCESS,
  SET_TEXT_SUCCESS,
  SHOW_ALERT_ERROR,
  HIDE_ALERT_ERROR,
  SET_TITLE_ERROR,
  SET_TEXT_ERROR,
  SHOW_ALERT_CONFIRMATION,
  HIDE_ALERT_CONFIRMATION,
  SET_TITLE_CONFIRMATION,
  SET_TEXT_CONFIRMATION,
  SET_EVENT_CONFIRM_POSITIVE,
  SET_EVENT_CONFIRM_NEGATIVE,
  SET_TEXT_BUTTON_POSITIVE_CONFIRMATION,
  SET_TEXT_BUTTON_NEGATIVE_CONFIRMATION,
  SET_COMPONENT_CONFIRMATION,
  SET_TIMER_CONFIRMATION,
} = alertActionTypes;

const setTextSuccess = (data) => ({
  type: SET_TEXT_SUCCESS,
  payload: data,
});
const setTitleSuccess = (data) => ({
  type: SET_TITLE_SUCCESS,
  payload: data,
});
const showAlertSuccess = (title, text, onClick = () => {}) => (dispatch) => {
  dispatch(setTitleSuccess(title));
  dispatch(setEventConfirmPositive(onClick));
  dispatch(setTextSuccess(text));
  dispatch({
    type: SHOW_ALERT_SUCCESS,
  });
};
const dismissAlertSuccess = () => ({
  type: HIDE_ALERT_SUCCESS,
});
const setTextError = (data) => ({
  type: SET_TEXT_ERROR,
  payload: data,
});
const setTitleError = (data) => ({
  type: SET_TITLE_ERROR,
  payload: data,
});
const showAlertError = (title, text) => (dispatch) => {
  dispatch(setTitleError(title));
  dispatch(setTextError(text));
  dispatch({
    type: SHOW_ALERT_ERROR,
  });
};
const dismissAlertError = () => ({
  type: HIDE_ALERT_ERROR,
});
const setTextConfirmation = (data) => ({
  type: SET_TEXT_CONFIRMATION,
  payload: data,
});
const setTitleConfirmation = (data) => ({
  type: SET_TITLE_CONFIRMATION,
  payload: data,
});
const setEventConfirmPositive = (data) => ({
  type: SET_EVENT_CONFIRM_POSITIVE,
  payload: data,
});
const setEventConfirmNegative = (data) => ({
  type: SET_EVENT_CONFIRM_NEGATIVE,
  payload: data,
});
const setTextButtonPositiveConfirmation = (data) => ({
  type: SET_TEXT_BUTTON_POSITIVE_CONFIRMATION,
  payload: data,
});
const setTextButtonNegativeConfirmation = (data) => ({
  type: SET_TEXT_BUTTON_NEGATIVE_CONFIRMATION,
  payload: data,
});
const setComponentConfirmation = (data) => ({
  type: SET_COMPONENT_CONFIRMATION,
  payload: data,
});
const setTimerConfirmation = (payload) => ({
  type: SET_TIMER_CONFIRMATION,
  payload,
});
const showAlertConfirmation = (
  title,
  text,
  onClickPositive = () => {},
  onClickNegative = () => {},
  textButtonPositive,
  textButtonNegative,
  component,
  timerConfirmation,
) => (dispatch) => {
  dispatch(setTitleConfirmation(title));
  dispatch(setTextConfirmation(text));
  dispatch(setEventConfirmPositive(onClickPositive));
  dispatch(setEventConfirmNegative(onClickNegative));
  dispatch(setTextButtonPositiveConfirmation(textButtonPositive));
  dispatch(setTextButtonNegativeConfirmation(textButtonNegative));
  dispatch(setTimerConfirmation(timerConfirmation));
  dispatch(setComponentConfirmation(component));
  dispatch({
    type: SHOW_ALERT_CONFIRMATION,
  });
};
const dismissAlertConfirmation = () => ({
  type: HIDE_ALERT_CONFIRMATION,
});

export {
  setTextSuccess,
  setTitleSuccess,
  showAlertSuccess,
  dismissAlertSuccess,
  setTextError,
  setTitleError,
  showAlertError,
  dismissAlertError,
  setTitleConfirmation,
  setTextConfirmation,
  showAlertConfirmation,
  dismissAlertConfirmation,
  setEventConfirmPositive,
  setEventConfirmNegative,
  setTextButtonPositiveConfirmation,
  setTextButtonNegativeConfirmation,
  setComponentConfirmation,
  setTimerConfirmation,
};
