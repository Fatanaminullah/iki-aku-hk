import React from 'react';
import {
  AeriaIcon,
  FacebookGameCardIcon,
  GarenaIcon,
  GemscoolIcon,
  LytoIcon,
  MegaxusIcon,
  PlaystationNetworkIcon,
  PointBlankIcon,
  PubgIcon,
  SteamWalletIcon,
} from '../../assets';
import {validation, hp} from '../../utilities';
import I18n from 'react-native-redux-i18n';

const {required} = validation;

const iconMapper = (groupName) => {
  let image;
  switch (groupName) {
    case 'AERIA GAMES':
      image = AeriaIcon;
      break;
    case 'FACEBOOK GAME CARD':
      image = FacebookGameCardIcon;
      break;
    case 'GEMSCOOL':
      image = GemscoolIcon;
      break;
    case 'LYTO':
      image = LytoIcon;
      break;
    case 'MEGAXUS':
      image = MegaxusIcon;
      break;
    case 'PLAYSTATION NETWORK':
      image = PlaystationNetworkIcon;
      break;
    case 'POINT BLANK CASH':
      image = PointBlankIcon;
      break;
    case 'PUBG':
      image = PubgIcon;
      break;
    case 'STEAM WALLET':
      image = SteamWalletIcon;
      break;
    case 'GARENA':
      image = GarenaIcon;
      break;
  }
  return image;
};

const voucherGamesNonDirectTopUpInfoMapper = (groupName) => {
  let data;
  switch (groupName) {
    case 'STEAM WALLET':
      data = [
        I18n.t('voucherGamesSteam.how'),
        I18n.t('voucherGamesSteam.stepOne'),
        I18n.t('voucherGamesSteam.stepTwo'),
        I18n.t('voucherGamesSteam.stepThree'),
        I18n.t('voucherGamesSteam.stepFour'),
      ];
      break;
    case 'LYTO':
      data = [
        I18n.t('voucherGamesLyto.how'),
        I18n.t('voucherGamesLyto.stepOne'),
        I18n.t('voucherGamesLyto.stepTwo'),
        I18n.t('voucherGamesLyto.stepThree'),
        I18n.t('voucherGamesLyto.stepFour'),
      ];
      break;
    case 'GARENA':
      data = [
        I18n.t('voucherGamesGarena.how'),
        I18n.t('voucherGamesGarena.stepOne'),
        I18n.t('voucherGamesGarena.stepTwo'),
        I18n.t('voucherGamesGarena.stepThree'),
        I18n.t('voucherGamesGarena.stepFour'),
        I18n.t('voucherGamesGarena.stepFive'),
      ];
      break;
    case 'AERIA GAMES':
      data = [
        I18n.t('voucherGamesAeria.how'),
        I18n.t('voucherGamesAeria.stepOne'),
        I18n.t('voucherGamesAeria.stepTwo'),
        I18n.t('voucherGamesAeria.stepThree'),
        I18n.t('voucherGamesAeria.stepFour'),
        I18n.t('voucherGamesAeria.stepFive'),
        I18n.t('voucherGamesAeria.stepSix'),
        I18n.t('voucherGamesAeria.stepSeven'),
      ];
      break;
    case 'FACEBOOK GAME CARD':
      data = [
        I18n.t('voucherGamesFacebook.stepOne'),
        I18n.t('voucherGamesFacebook.stepTwo'),
        I18n.t('voucherGamesFacebook.stepThree'),
      ];
      break;
    case 'PUBG':
      data = [
        I18n.t('voucherGamesPubg.how'),
        I18n.t('voucherGamesPubg.stepOne'),
        I18n.t('voucherGamesPubg.stepTwo'),
        I18n.t('voucherGamesPubg.stepThree'),
        I18n.t('voucherGamesPubg.stepFour'),
        I18n.t('voucherGamesPubg.stepFive'),
      ];
      break;
    case 'PLAYSTATION NETWORK':
      data = [
        I18n.t('voucherGamesPlaystation.how'),
        I18n.t('voucherGamesPlaystation.stepOne'),
        I18n.t('voucherGamesPlaystation.stepTwo'),
        I18n.t('voucherGamesPlaystation.stepThree'),
        I18n.t('voucherGamesPlaystation.stepFour'),
      ];
      break;
    case 'POINT BLANK CASH':
      data = [
        I18n.t('voucherGamesPb.how'),
        I18n.t('voucherGamesPb.stepOne'),
        I18n.t('voucherGamesPb.stepTwo'),
        I18n.t('voucherGamesPb.stepThree'),
        I18n.t('voucherGamesPb.stepFour'),
        I18n.t('voucherGamesPb.stepFive'),
      ];
      break;
    case 'GEMSCOOL':
      data = [
        I18n.t('voucherGamesGemscool.how'),
        I18n.t('voucherGamesGemscool.stepOne'),
        I18n.t('voucherGamesGemscool.stepTwo'),
        I18n.t('voucherGamesGemscool.stepThree'),
      ];
      break;
    case 'MEGAXUS':
      data = [
        I18n.t('voucherGamesMegaxus.how'),
        I18n.t('voucherGamesMegaxus.stepOne'),
        I18n.t('voucherGamesMegaxus.stepTwo'),
        I18n.t('voucherGamesMegaxus.stepThree'),
        I18n.t('voucherGamesMegaxus.stepFour'),
        I18n.t('voucherGamesMegaxus.stepFive'),
      ];
      break;
    default:
      break;
  }
  return data;
};

const voucherGamesNonDirectProductMapper = (groupName) => {
  let value;
  switch (groupName) {
    case 'STEAM WALLET':
      value = [
        {
          id: '1',
          value: '12.000 Wallet Code',
        },
        {
          id: '2',
          value: '45.000 Wallet Code',
        },
        {
          id: '3',
          value: '60.000 Wallet Code',
        },
        {
          id: '4',
          value: '90.000 Wallet Code',
        },
      ];
      break;
    case 'LYTO':
      value = [
        {
          id: '1',
          value: 'Coin Lyto 15.000',
        },
        {
          id: '2',
          value: 'Coin Lyto 30.000',
        },
        {
          id: '3',
          value: 'Coin Lyto 50.000',
        },
        {
          id: '4',
          value: 'Coin Lyto 100.000',
        },
      ];
      break;
    case 'GARENA':
      value = [
        {
          id: '1',
          value: '33 Garena Shells',
        },
        {
          id: '2',
          value: '166 Garena Shells',
        },
        {
          id: '3',
          value: '333 Garena Shells',
        },
      ];
      break;
    case 'AERIA GAMES':
      value = [
        {
          id: '1',
          value: 'Aeria 510 points',
        },
        {
          id: '2',
          value: 'Aeria 2.100 points',
        },
        {
          id: '3',
          value: 'Aeria 3.240 points',
        },
      ];
      break;
    case 'FACEBOOK GAME CARD':
      value = [
        {
          id: '1',
          value: 'Facebook 30k',
        },
        {
          id: '2',
          value: 'Facebook 50k',
        },
        {
          id: '3',
          value: 'Facebook 100k',
        },
      ];
      break;
    case 'PUBG':
      value = [
        {
          id: '1',
          value: 'PUBG Early Bird key',
        },
        {
          id: '2',
          value: 'PUBG Steam game key',
        },
      ];
      break;
    case 'PLAYSTATION NETWORK':
      value = [
        {
          id: '1',
          value: 'PlayStation Network 100.000',
        },
        {
          id: '2',
          value: 'PlayStation Network 200.000',
        },
        {
          id: '3',
          value: 'PlayStation Network 400.000',
        },
      ];
      break;
    case 'POINT BLANK CASH':
      value = [
        {
          id: '1',
          value: '1200 Point Blank Cash',
        },
        {
          id: '2',
          value: '2400 Point Blank Cash',
        },
        {
          id: '3',
          value: '6000 Point Blank Cash',
        },
      ];
      break;
    case 'GEMSCOOL':
      value = [
        {
          id: '1',
          value: 'Gemscool 1000 G-Cash',
        },
        {
          id: '2',
          value: 'Gemscool 2000 G-Cash',
        },
      ];
      break;
    case 'MEGAXUS':
      value = [
        {
          id: '1',
          value: 'Megaxus 10.000',
        },
        {
          id: '2',
          value: 'Megaxus 20.000',
        },
        {
          id: '3',
          value: 'Megaxus 30.000',
        },
        {
          id: '4',
          value: 'Megaxus 40.000',
        },
        {
          id: '5',
          value: 'Megaxus 50.000',
        },
      ];
      break;
    default:
      break;
  }
  return value;
};

export {
  iconMapper,
  voucherGamesNonDirectTopUpInfoMapper,
  voucherGamesNonDirectProductMapper,
};
