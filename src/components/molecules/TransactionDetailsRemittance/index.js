import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {colors, fonts, hp, wp} from '../../../utilities';
import {Gap, ListItem} from '../../atoms';
import TransactionPaymentCode from '../TransactionPaymentCode';

const TransactionDetailsRemittance = ({
  containerStyle,
  trxItem,
  trxDetails,
}) => {
  return (
    <View>
      <TransactionPaymentCode
        total={trxDetails?.total}
        code={trxDetails?.paymentCode}
      />
      <Gap height={hp(2)} />
      <View style={styles.detailLabelContainer}>
        <Text style={styles.detailLabelText}>
          {I18n.t('transactionDetailsScreen.detail1Label')}
        </Text>
      </View>
      <Gap height={hp(1)} />
      <View style={[styles.container, containerStyle]}>
        {trxItem?.trxDetails.map((item, index) => (
          <ListItem index={index} item={item} style={styles.container} />
        ))}
      </View>
      <Gap height={hp(2)} />
      <View style={styles.detailLabelContainer}>
        <Text style={styles.detailLabelText}>
          {I18n.t('transactionDetailsScreen.detail2Label')}
        </Text>
      </View>
      <Gap height={hp(1)} />
      <View style={[styles.container, containerStyle]}>
        <Gap height={hp(2)} />
        <Text style={styles.text1b}>
          {I18n.t('transactionDetailsScreen.from')}
        </Text>
        {trxItem?.infoDetails?.from.map((item, index) => (
          <ListItem index={index} item={item} style={styles.container} />
        ))}
        <Gap height={hp(2)} />
        <Text style={styles.text1b}>
          {I18n.t('transactionDetailsScreen.to')}
        </Text>
        {trxItem?.infoDetails?.to.map((item, index) => (
          <ListItem index={index} item={item} style={styles.container} />
        ))}
      </View>
      <Gap height={hp(2)} />
      <View style={styles.detailLabelContainer}>
        <Text style={styles.detailLabelText}>
          {I18n.t('transactionDetailsScreen.detail3Label')}
        </Text>
      </View>
      <Gap height={hp(1)} />
      <View style={[styles.container, containerStyle]}>
        {trxItem?.nominalDetails.map((item, index) => (
          <ListItem
            label={item.title}
            style={styles.container}
            labelStyle={styles.textColor(item.title)}
            valueStyle={styles.textColor(item.title)}
            value2Style={styles.textColor(item.title)}
            value1={item.currency}
            value2={item.value}
            columns="3"
          />
        ))}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  detailLabelContainer: {
    backgroundColor: colors.grey3,
    paddingVertical: hp(1.4),
    borderRadius: 5,
  },
  detailLabelText: {
    textAlign: 'center',
    color: colors.black,
    fontSize: fonts.size[16],
    fontFamily: fonts.primary.bold,
  },
  container: {
    paddingHorizontal: 5,
  },
  text1b: {
    marginLeft: wp(1),
    color: colors.black,
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[18],
  },
  textColor: (label) => ({
    color:
      label === I18n.t('transactionDetailsScreen.fee')
        ? colors.red1
        : label === I18n.t('transactionDetailsScreen.totalFinal')
        ? colors.green3
        : colors.black,
  }),
});

export default TransactionDetailsRemittance;
