import React from 'react';
import {StyleSheet, Text, Image, View} from 'react-native';
import {colors, fonts, wp, hp} from '../../../utilities';
import I18n from 'react-native-redux-i18n';
import {NoTrxIllustration} from '../../../assets';

const HistoryTrxNoTrx = () => {
  return (
    <View style={styles.noTrxContainer}>
      <View style={styles.ilustImgContainer}>
        <Image style={styles.ilustImg} source={NoTrxIllustration} />
      </View>
      <View style={styles.noTrxTextContainer}>
        <Text style={styles.noTrxTextBig}>{I18n.t('homeScreen.noTrx1')}</Text>
        <Text style={styles.noTrxTextSmall}>{I18n.t('homeScreen.noTrx2')}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  ilustImgContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 20,
  },
  ilustImg: {
    width: wp(70),
    height: hp(23),
  },
  noTrxContainer: {
    backgroundColor: colors.white,
    paddingBottom: hp(5),
  },
  noTrxTextContainer: {
    alignItems: 'center',
    paddingHorizontal: 30,
  },
  noTrxTextBig: {
    color: colors.blue3,
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    textAlign: 'center',
  },
  noTrxTextSmall: {
    color: colors.grey1,
    fontFamily: fonts.primary[600],
    fontSize: fonts.size[14],
    textAlign: 'center',
  },
});

export default HistoryTrxNoTrx;
