import SplashScreenImgBg from './splash-screen-image-background.png';
import Walkthrough1 from './walkthrough_slide_1.png';
import Walkthrough2 from './walkthrough_slide_2.png';
import Walkthrough3 from './walkthrough_slide_3.png';
import HomeScreenImgBg from './home-image-background.png';
import DummyBanner1 from './dummy-banner-1.png';
import LookingAroundIllustration from './looking-around-illustration.png';
import IdFront from './arc_front.svg';
import IdFrontPng from './arc_front_png.png';
import IdBack from './arc_back.svg';
import IdBackPng from './arc_back_png.png';
import SelfieId from './selfie_with_arc.svg';
import SelfieIdPng from './arc_selfie_png.png';
import NoTrxIllustration from './notrx-illustration.png';
import ReferralScreenImgBg from './referral-screen-image-background.png';
import PinBackground from './pin-background.png';
import ContactImage from './contact.png';
import MerchantPayment from './merchant-payment.png';
import VoucherGamesImgBg from './voucher-game-background.png';

export {
  SplashScreenImgBg,
  Walkthrough1,
  Walkthrough2,
  Walkthrough3,
  HomeScreenImgBg,
  DummyBanner1,
  IdFront,
  IdBack,
  LookingAroundIllustration,
  SelfieId,
  NoTrxIllustration,
  ReferralScreenImgBg,
  PinBackground,
  ContactImage,
  MerchantPayment,
  IdFrontPng,
  IdBackPng,
  SelfieIdPng,
  VoucherGamesImgBg,
};
