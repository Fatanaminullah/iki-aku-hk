import React from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {useSelector} from 'react-redux';
import {CouponTimeIcon} from '../../assets';
import I18n from 'react-native-redux-i18n';
import {
  Card,
  Container,
  Content,
  Gap,
  Header,
  ImageInfo,
  SvgWrapper,
  Touchable,
} from '../../components';
import {RewardPoinIcon} from '../../assets';
import {colors, fonts, hp, thousandSeparator, wp} from '../../utilities';
import moment from 'moment';
import Countdown from 'react-countdown';

const RewardScreen = ({navigation, route}) => {
  const listRewards = useSelector((state) => state.reward.listRewards);
  const renderer = ({hours, minutes, seconds, completed, ...rest}) => {
    return (
      <Text
        style={{
          fontFamily: fonts.primary.bold,
          color: colors.red1,
          fontSize: fonts.size[14],
        }}>
        {rest?.formatted?.hours} : {rest?.formatted?.minutes} :{' '}
        {rest?.formatted?.seconds}
      </Text>
    );
  };
  const renderReward = ({item, index}) => {
    return (
      <Card
        onPress={() => navigation.navigate('DetailRewardScreen', {item})}
        key={index}
        style={styles.cardContainer}>
        <Image
          source={{uri: `data:image/png;base64,${item?.image}`}}
          style={{width: wp(80), height: hp(20)}}
          resizeMode="contain"
        />
        <View style={styles.caption}>
          <View style={styles.column}>
            <SvgWrapper
              component={<CouponTimeIcon />}
              width={wp(8)}
              height={hp(4)}
            />
            <View style={styles.textContainer}>
              <Text
                style={{
                  fontFamily: fonts.primary.primary,
                  fontSize: fonts.size[14],
                }}>
                {I18n.t('rewardScreen.useBefore')}
              </Text>
              <Countdown
                key={index}
                daysInHours={true}
                date={moment(item.expired, 'MM-DD-YYYY HH:mm:ss')}
                intervalDelay={1000}
                renderer={renderer}
                autoStart={true}
              />
            </View>
          </View>
        </View>
      </Card>
    );
  };
  return (
    <Container>
      <Header title={I18n.t('rewardScreen.title')} />
      <Touchable style={styles.rewardPoin}>
        <View style={styles.rowContainer}>
          <SvgWrapper
            component={<RewardPoinIcon />}
            width={hp(7)}
            height={hp(7)}
          />
        </View>
        <Text style={styles.poin}>1</Text>
      </Touchable>
      <Content style={styles.page}>
        <FlatList
          data={listRewards}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
          ListEmptyComponent={
            <ImageInfo
              text={I18n.t('rewardScreen.title')}
              hpSizeImage={22}
              hpSizeText={1.8}
            />
          }
          renderItem={renderReward}
          keyExtractor={(item) => item.id}
        />
      </Content>
    </Container>
  );
};

export default RewardScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.grey3,
  },
  cardContainer: {
    marginVertical: 10,
    borderWidth: 0,
    borderRadius: 0,
  },
  caption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(80),
  },
  column: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: wp(35),
  },
  textContainer: {
    flexWrap: 'wrap',
    marginLeft: hp(1),
  },
  rowContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rewardPoin: {
    flexDirection: 'row',
    alignItems: 'center',
    height: hp(9),
    paddingHorizontal: hp(2),
    backgroundColor: colors.white,
  },
  poin: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[20],
    marginLeft: hp(3),
    marginTop: hp(0.5),
  },
});
