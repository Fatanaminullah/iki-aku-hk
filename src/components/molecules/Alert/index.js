import React from 'react';
import Countdown from 'react-countdown';
import {StyleSheet, Text, View} from 'react-native';
import Dialog, {DialogContent, SlideAnimation} from 'react-native-popup-dialog';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {alertActions} from '../../../redux/store/actions';
import {colors, fonts} from '../../../utilities';
import {Gap} from '../../atoms';
import Button from '../Button';

const Alert = ({containerStyle}) => {
  const {
    dismissAlertSuccess,
    dismissAlertError,
    dismissAlertConfirmation,
    setTitleConfirmation,
    setTextConfirmation,
    setTextButtonPositiveConfirmation,
    setTextButtonNegativeConfirmation,
    setTimerConfirmation,
  } = alertActions;
  const dispatch = useDispatch();
  const alert = useSelector((state) => state.alert);
  const dismissConfirmation = () => {
    dispatch(dismissAlertConfirmation());
    dispatch(setTitleConfirmation(''));
    dispatch(setTextConfirmation(''));
    dispatch(setTextButtonPositiveConfirmation(''));
    dispatch(setTextButtonNegativeConfirmation(''));
    dispatch(setTimerConfirmation(''));
  };
  const eventConfirm = {
    positive: () => {
      if (alert.eventConfirmPositive) {
        alert.eventConfirmPositive();
        dismissConfirmation();
      } else {
        dismissConfirmation();
      }
    },
    negative: () => {
      if (alert.eventConfirmNegative) {
        alert.eventConfirmNegative();
        dismissConfirmation();
      } else {
        dismissConfirmation();
      }
    },
  };
  const timerCancelRenderer = ({seconds, completed, ...rest}) => {
    if (completed) {
      return null;
    } else {
      return <Text style={{color: colors.white}}>{`(${seconds})`}</Text>;
    }
  };
  const {
    visibleAlertSuccess,
    visibleAlertError,
    visibleAlertConfirmation,
    titleSuccess,
    textSuccess,
    titleError,
    textError,
    titleConfirmation,
    textConfirmation,
  } = alert;
  return (
    <View>
      <Dialog
        onTouchOutside={() => dispatch(dismissAlertSuccess())}
        dialogAnimation={
          new SlideAnimation({
            slideFrom: 'bottom',
          })
        }
        visible={visibleAlertSuccess}
        dialogStyle={[styles.dialogContainer, containerStyle]}>
        <DialogContent style={styles.dialogContent}>
          <Text style={styles.title}>{titleSuccess}</Text>
          <Text style={styles.text}>{textSuccess}</Text>
          <Button
            title={I18n.t('alert.close')}
            type="septenary"
            onPress={() => dispatch(dismissAlertSuccess())}
            style={styles.buttonStyle}
          />
        </DialogContent>
      </Dialog>
      <Dialog
        dialogAnimation={
          new SlideAnimation({
            slideFrom: 'bottom',
          })
        }
        visible={visibleAlertError}
        dialogStyle={[styles.dialogContainer, containerStyle]}>
        <DialogContent style={styles.dialogContent}>
          <Text style={styles.title}>{titleError}</Text>
          <Text style={styles.text}>{textError}</Text>
          <Button
            title={I18n.t('alert.close')}
            type="septenary"
            onPress={() => dispatch(dismissAlertError())}
            style={styles.buttonStyle}
          />
        </DialogContent>
      </Dialog>
      <Dialog
        dialogAnimation={
          new SlideAnimation({
            slideFrom: 'bottom',
          })
        }
        visible={visibleAlertConfirmation}
        dialogStyle={[styles.dialogContainer, containerStyle]}>
        <DialogContent style={styles.dialogContent}>
          <Text style={styles.title}>{titleConfirmation}</Text>
          <Text style={styles.text}>{textConfirmation}</Text>
          <View style={styles.width100}>{alert.componentConfirmation}</View>
          <View style={styles.flexRow}>
            <Button
              title={
                alert.textButtonNegativeConfirmation || I18n.t('alert.cancel')
              }
              type="primary"
              onPress={() => eventConfirm.negative()}
              style={styles.buttonStyle}
            />
            <Gap width={10} />
            <Button
              disabled={alert.timerConfirmation}
              title={alert.textButtonPositiveConfirmation || I18n.t('alert.ok')}
              iconRight={
                <Countdown
                  onComplete={() => dispatch(setTimerConfirmation(''))}
                  date={Date.now() + Number(alert.timerConfirmation)}
                  zeroPadTime={2}
                  intervalDelay={1000}
                  renderer={timerCancelRenderer}
                  autoStart={true}
                />
              }
              type="danger"
              onPress={() => eventConfirm.positive()}
              style={styles.buttonStyle}
            />
          </View>
        </DialogContent>
      </Dialog>
    </View>
  );
};

export default Alert;

const styles = StyleSheet.create({
  dialogContainer: {
    width: '75%',
    padding: 5,
    alignSelf: 'center',
    borderRadius: 25,
  },
  buttonStyle: {
    marginTop: 15,
    marginBottom: -15,
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  dialogContent: {
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    paddingVertical: 5,
    textAlign: 'center',
    fontFamily: fonts.primary[700],
    fontSize: fonts.size[15],
  },
  text: {
    paddingVertical: 5,
    paddingHorizontal: 15,
    marginBottom: 10,
    textAlign: 'center',
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[14],
  },
  flexRow: {
    flexDirection: 'row',
  },
  width100: {
    width: '100%',
  },
});
