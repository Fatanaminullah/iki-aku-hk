import React from 'react';
import {StyleSheet, ScrollView, View, RefreshControl} from 'react-native';
import {Gap} from '../../atoms';
import {hp} from '../../../utilities';

const Content = ({
  children,
  style,
  centerContent,
  noPadding,
  onRefresh,
  refreshing,
  refreshControl,
}) => {
  if (noPadding) {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        refreshControl={refreshControl}
        contentContainerStyle={{flexGrow: 1}}>
        <View style={[styles.container(noPadding), style]}>{children}</View>
      </ScrollView>
    );
  }
  if (centerContent) {
    return (
      <View
        style={[
          styles.container(noPadding),
          style,
          styles.centerContentContainer,
        ]}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
          refreshControl={refreshControl}
          contentContainerStyle={styles.centerContentContainer}>
          {children}
        </ScrollView>
      </View>
    );
  }
  return (
    <View style={[styles.container(noPadding), style]}>
      <Gap height={hp(2)} />
      <ScrollView
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        refreshControl={refreshControl}
        contentContainerStyle={{flexGrow: 1}}>
        {children}
      </ScrollView>
      <Gap height={hp(2)} />
    </View>
  );
};

export default Content;

const styles = StyleSheet.create({
  container: (noPadding) => ({
    flex: 1,
    paddingHorizontal: noPadding ? 0 : 20,
  }),
  centerContentContainer: {
    flexGrow: 1,
    justifyContent: 'center',
  },
});
