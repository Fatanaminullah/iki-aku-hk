const SHOW_ALERT_SUCCESS = '@alert/show-alert-success';
const HIDE_ALERT_SUCCESS = '@alert/hide-alert-success';
const SET_TITLE_SUCCESS = '@alert/set-title-success';
const SET_TEXT_SUCCESS = '@alert/set-text-success';
const SHOW_ALERT_ERROR = '@alert/show-alert-error';
const HIDE_ALERT_ERROR = '@alert/hide-alert-error';
const SET_TITLE_ERROR = '@alert/set-title-error';
const SET_TEXT_ERROR = '@alert/set-text-error';
const SHOW_ALERT_CONFIRMATION = '@alert/show-alert-confirmation';
const HIDE_ALERT_CONFIRMATION = '@alert/hide-alert-confirmation';
const SET_TITLE_CONFIRMATION = '@alert/set-title-confirmation';
const SET_TEXT_CONFIRMATION = '@alert/set-text-confirmation';
const SET_EVENT_CONFIRM_POSITIVE = '@alert/set-event-confirm-positive';
const SET_EVENT_CONFIRM_NEGATIVE = '@alert/set-event-confirm-negative';
const SET_TEXT_BUTTON_POSITIVE_CONFIRMATION =
  '@alert/set-text-button-positive-confirmation';
const SET_TEXT_BUTTON_NEGATIVE_CONFIRMATION =
  '@alert/set-text-button-negative-confirmation';
const SET_COMPONENT_CONFIRMATION = '@alert/set-component-confirmation';
const SET_TIMER_CONFIRMATION = '@alert/set-timer-confirmation';

export {
  SHOW_ALERT_SUCCESS,
  HIDE_ALERT_SUCCESS,
  SET_TITLE_SUCCESS,
  SET_TEXT_SUCCESS,
  SHOW_ALERT_ERROR,
  HIDE_ALERT_ERROR,
  SET_TITLE_ERROR,
  SET_TEXT_ERROR,
  SHOW_ALERT_CONFIRMATION,
  HIDE_ALERT_CONFIRMATION,
  SET_TITLE_CONFIRMATION,
  SET_TEXT_CONFIRMATION,
  SET_EVENT_CONFIRM_POSITIVE,
  SET_EVENT_CONFIRM_NEGATIVE,
  SET_TEXT_BUTTON_POSITIVE_CONFIRMATION,
  SET_TEXT_BUTTON_NEGATIVE_CONFIRMATION,
  SET_COMPONENT_CONFIRMATION,
  SET_TIMER_CONFIRMATION,
};
