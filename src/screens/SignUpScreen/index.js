import React, {useEffect, useRef, useState} from 'react';
import I18n from 'react-native-redux-i18n';
import Moment from 'moment';
import {Label} from 'native-base';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {IdBack, IdFront, SelfieId} from '../../assets';
import {CameraIcon, FlagHkgRound, IkiAkuIcon1} from '../../assets/icons';
import {
  Button,
  CameraWidget,
  Container,
  Content,
  DatePicker,
  Field,
  Form,
  Gap,
  Header,
  Input,
  InputPicker,
  InputPin,
  PinDialog,
  PreviewImage,
  RadioGroup,
  SvgWrapper,
} from '../../components';
import {colors, hp, validation, wp, fonts, useForm} from '../../utilities';
import {useDispatch, useSelector} from 'react-redux';
import {
  getListOccupation,
  registerUser,
  uploadImage,
} from '../../redux/store/actions/profile';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {showAlertError} from '../../redux/store/actions/alert';
import {ALI_OSS_BUCKET_NAME} from '../../environments/env.json';

const {required, confirmPassword, minLength} = validation;

const SignUpScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const [form, setForm] = useForm({password: '', rePassword: ''});
  const [signature, setSignature] = useState('');
  const [idPicture, setIdPicture] = useState({});
  const [pinVisible, setPinVisible] = useState(false);
  const [params, setParams] = useState('');
  const [visiblePreview, setVisiblePreview] = useState(false);
  const [previewImage, setPreviewImage] = useState('');
  const [visibleCamera, setVisibleCamera] = useState(false);
  const [visibleCameraContract, setVisibleCameraContract] = useState(false);
  const {listOccupation} = useSelector((state) => state.profile);
  const cameraRef = useRef();
  const cameraContractRef = useRef();
  useEffect(() => {
    dispatch(getListOccupation(showLoading, dismissLoading));
  }, []);
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        type="danger"
        title={I18n.t('signUpScreen.buttonSignUp')}
        onPress={handleSubmit}
        style={styles.button}
      />
    );
  };
  const onSubmit = async (data, setValue, setError) => {
    const {
      idNumber,
      idIssuedDate,
      password,
      phoneNumber,
      name,
      occupation,
      address,
      birthDate,
      gender,
      signature,
      idDocuments,
      pin,
    } = data;
    const {id_front, id_back, id_selfie, employment_contract} = idPicture;
    if (!id_front || !id_back || !id_selfie) {
      return setError('idDocuments', {
        type: 'manual',
        message: I18n.t('validation.required'),
      });
    }
    setValue('idDocuments', idPicture);
    try {
      dispatch(showLoading());
      const requestUploadImage = {
        bucket: ALI_OSS_BUCKET_NAME,
      };
      const uploadedIdFront = await dispatch(
        uploadImage(
          {
            ...requestUploadImage,
            filedata: `data:@file/jpeg;base64,${id_front}`,
          },
          null,
          null,
          showAlertError,
        ),
      );
      const uploadedIdBack = await dispatch(
        uploadImage(
          {
            ...requestUploadImage,
            filedata: `data:@file/jpeg;base64,${id_back}`,
          },
          null,
          null,
          showAlertError,
        ),
      );
      const uploadedIdSelfie = await dispatch(
        uploadImage(
          {
            ...requestUploadImage,
            filedata: `data:@file/jpeg;base64,${id_selfie}`,
          },
          null,
          null,
          showAlertError,
        ),
      );
      const uploadedSignature = await dispatch(
        uploadImage(
          {
            ...requestUploadImage,
            filedata: `data:@file/jpeg;base64,${signature}`,
          },
          null,
          null,
          showAlertError,
        ),
      );
      let uploadedEmploymentContract = '';
      if (employment_contract) {
        uploadedEmploymentContract = await dispatch(
          uploadImage(
            {
              ...requestUploadImage,
              filedata: `data:@file/jpeg;base64,${employment_contract}`,
            },
            null,
            null,
            showAlertError,
          ),
        );
      }
      const request = {
        cardno: idNumber,
        cardnoExpiry: idIssuedDate,
        password,
        phoneNumber: `852${phoneNumber}`,
        fullname: name,
        country: 'HONGKONG',
        occupation: occupation.value,
        fulladdress: address,
        dateOfBirth: birthDate,
        gender,
        timezone: 'Asia/Hong_Kong',
        currency: 'HKD',
        attachments: {
          id_front: uploadedIdFront,
          id_back: uploadedIdBack,
          id_selfie: uploadedIdSelfie,
          digital_signature: uploadedSignature,
          employment_contract: uploadedEmploymentContract,
        },
        pin,
      };
      dispatch(
        registerUser(request, null, dismissLoading, showAlertError),
      ).then((res) => {
        navigation.navigate('SignUpOtpScreen', {
          request: res,
          password,
          idNumber,
        });
      });
    } catch (err) {
      dispatch(dismissLoading());
    }
  };
  const onCapture = (e, param, setValue) => {
    setIdPicture({
      ...idPicture,
      [param]: e,
    });
    if (params === 'employment_contract') {
      setVisibleCameraContract(false);
    } else {
      setVisibleCamera(false);
    }
    setParams('');
  };
  const onClickUpdate = () => {
    setVisibleCamera(true);
    setVisiblePreview(false);
  };
  return (
    <Container style={{backgroundColor: colors.white}}>
      <PinDialog
        visibility={pinVisible}
        setVisibility={setPinVisible}
        background={{}}
        headerTitle={I18n.t('signUpScreen.pinHeader')}
        label={I18n.t('signUpScreen.pinLabel')}
      />
      <PreviewImage
        visible={visiblePreview}
        sourceImg={previewImage}
        setVisible={setVisiblePreview}
        showButtonUpdate={true}
        onClickUpdate={onClickUpdate}
      />
      <Header title={I18n.t('signUpScreen.headerTitle')} />
      <Content>
        <View style={styles.iconContainer}>
          <SvgWrapper
            component={<IkiAkuIcon1 />}
            width={wp(20)}
            height={wp(20)}
          />
          <SvgWrapper
            component={<FlagHkgRound />}
            width={wp(15)}
            height={wp(15)}
          />
        </View>
        <View style={styles.formContainer}>
          <Form onSubmit={onSubmit} submitComponent={renderSubmitComponent}>
            <Field name="idNumber" validation={{required}}>
              <Input
                label={I18n.t('signUpScreen.inputIdNumber')}
                placeholder={I18n.t('signUpScreen.placeholderIdNumber')}
                containerStyle={styles.inputContainer}
                maxLength={10}
                keyboardType="numeric"
              />
            </Field>
            <Gap height={hp(3)} />
            <Field name="idIssuedDate" validation={{required}}>
              <DatePicker
                format="YYYY-MM-DD"
                label={I18n.t('signUpScreen.inputIdIssuedDate')}
                placeholder={I18n.t('signUpScreen.placeholderIdIssuedDate')}
                minDate={Moment(new Date())}
                showIcon={true}
              />
            </Field>
            <Gap height={hp(3)} />
            <Field
              name="password"
              validation={{required, minLength: minLength(8)}}>
              <Input
                secureTextEntry
                label={I18n.t('signUpScreen.inputPassword1')}
                placeholder={I18n.t('signUpScreen.placeholderPassword1')}
                containerStyle={styles.inputContainer}
                onChangeText={(val) => setForm('password', val)}
              />
            </Field>
            <Gap height={hp(3)} />
            <Field
              name="rePassword"
              validation={{
                required,
                validate: (val) => confirmPassword(val, form.password),
              }}>
              <Input
                secureTextEntry
                label={I18n.t('signUpScreen.inputPassword2')}
                placeholder={I18n.t('signUpScreen.placeholderPassword2')}
                containerStyle={styles.inputContainer}
                onChangeText={(val) => setForm('rePassword', val)}
              />
            </Field>
            <Gap height={hp(3)} />
            <Field name="pin" validation={{required}}>
              <InputPin
                label={I18n.t('signUpScreen.inputPin')}
                placeholder={I18n.t('signUpScreen.placeholderPin')}
                headerTitle={I18n.t('signUpScreen.pinHeader')}
                labelPin={I18n.t('signUpScreen.pinLabel')}
                labelReInputPin={I18n.t('signUpScreen.reInputPinLabel')}
              />
            </Field>
            <Gap height={hp(3)} />
            <Field name="phoneNumber" validation={{required}}>
              <Input
                label={I18n.t('signUpScreen.inputPhoneNumber')}
                placeholder={I18n.t('signUpScreen.placeholderPhoneNumber')}
                containerStyle={styles.inputContainer}
                leftIcon={
                  <View style={styles.iconPhoneNumber}>
                    <SvgWrapper
                      component={<FlagHkgRound />}
                      width={wp(7)}
                      height={wp(7)}
                    />
                    <Text>+852</Text>
                  </View>
                }
                keyboardType="numeric"
              />
            </Field>
            <Gap height={hp(3)} />
            <Field name="name" validation={{required}}>
              <Input
                label={I18n.t('signUpScreen.inputName')}
                placeholder={I18n.t('signUpScreen.placeholderName')}
                containerStyle={styles.inputContainer}
              />
            </Field>
            <Gap height={hp(3)} />
            <Field name="birthDate" validation={{required}}>
              <DatePicker
                format="YYYY-MM-DD"
                label={I18n.t('signUpScreen.inputBirthdate')}
                placeholder={I18n.t('signUpScreen.placeholderBirthdate')}
                showIcon={true}
                maxDate={Moment(new Date()).subtract(18, 'y')}
              />
            </Field>
            <Gap height={hp(3)} />
            <Field name="gender" validation={{required}}>
              <RadioGroup
                label={I18n.t('signUpScreen.inputGender')}
                listItem={[
                  {label: I18n.t('signUpScreen.man'), value: 'MALE'},
                  {label: I18n.t('signUpScreen.woman'), value: 'FEMALE'},
                ]}
                direction="row"
              />
            </Field>
            <Gap height={hp(3)} />
            <Field name="occupation">
              <InputPicker
                searchPlaceholder={I18n.t('signUpScreen.inputOccupationSearch')}
                listEmptyText={I18n.t('signUpScreen.occupationNotFound')}
                title={I18n.t('signUpScreen.placeholderOccupation')}
                label={I18n.t('signUpScreen.inputOccupation')}
                placeholder={I18n.t('signUpScreen.placeholderOccupation')}
                dataSource={listOccupation}
              />
            </Field>
            <Gap height={hp(3)} />
            <Field name="address" validation={{required}}>
              <Input
                label={I18n.t('signUpScreen.inputAddress')}
                placeholder={I18n.t('signUpScreen.placeholderAddress')}
                containerStyle={styles.inputContainer}
              />
            </Field>
            <Gap height={hp(3)} />
            <Field name="referral">
              <Input
                label={I18n.t('signUpScreen.inputReferralCode')}
                placeholder={I18n.t('signUpScreen.placeholderReferralCode')}
                containerStyle={styles.inputContainer}
              />
            </Field>
            <Gap height={hp(3)} />
            <Field
              name="signature"
              value={signature}
              setValue={setSignature}
              validation={{required}}>
              <Input
                label={I18n.t('signUpScreen.inputSignature')}
                inputSignature={true}
              />
            </Field>
            <Gap height={hp(3)} />
            <Field name="idDocuments" value={idPicture} setValue={setIdPicture}>
              <View>
                <CameraWidget
                  setVisible={setVisibleCamera}
                  visibility={visibleCamera}
                  cameraRef={cameraRef}
                  onCapture={onCapture}
                  defaultCameraRotation={
                    params === 'id_selfie' ? 'front' : 'back'
                  }
                  isCaptureIdCard={params !== 'id_selfie'}
                  isCaptureSelfie={params === 'id_selfie'}
                  showButtonRotate={params === 'id_selfie'}
                  params={params}
                />
                <Label style={styles.labelFieldId}>
                  {I18n.t('signUpScreen.inputPhoto')}
                </Label>
                <Gap height={hp(1)} />
                <View style={styles.fieldId}>
                  <View>
                    <TouchableOpacity
                      onPress={() => {
                        setVisibleCamera(true);
                        setParams('id_front');
                      }}
                      style={styles.buttonCamera}>
                      {idPicture?.id_front ? (
                        <TouchableOpacity
                          onPress={() => {
                            setVisiblePreview(true);
                            setPreviewImage(idPicture?.id_front);
                            setParams('id_front');
                          }}>
                          <Image
                            source={{
                              uri: `data:image/png;base64,${idPicture?.id_front}`,
                            }}
                            style={{width: wp(20), height: wp(20)}}
                          />
                        </TouchableOpacity>
                      ) : (
                        <SvgWrapper
                          style={styles.logo}
                          component={<IdFront />}
                        />
                      )}
                      <View style={styles.iconCamera}>
                        <Button
                          icon={
                            <SvgWrapper
                              component={<CameraIcon />}
                              width={wp(8)}
                              height={hp(4)}
                            />
                          }
                          rounded={true}
                          size={30}
                          title={null}
                          onPress={() => {
                            setVisibleCamera(true);
                            setParams('id_front');
                          }}
                        />
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.textId}>
                      {I18n.t('signUpScreen.inputFrontId')}
                    </Text>
                  </View>
                  <View>
                    <TouchableOpacity
                      style={styles.buttonCamera}
                      onPress={() => {
                        setVisibleCamera(true);
                        setParams('id_back');
                      }}>
                      {idPicture?.id_back ? (
                        <TouchableOpacity
                          onPress={() => {
                            setVisiblePreview(true);
                            setPreviewImage(idPicture?.id_back);
                            setParams('id_back');
                          }}>
                          <Image
                            source={{
                              uri: `data:image/png;base64,${idPicture?.id_back}`,
                            }}
                            style={{width: wp(20), height: wp(20)}}
                          />
                        </TouchableOpacity>
                      ) : (
                        <SvgWrapper
                          style={styles.logo}
                          component={<IdBack />}
                        />
                      )}
                      <View style={styles.iconCamera}>
                        <Button
                          icon={
                            <SvgWrapper
                              component={<CameraIcon />}
                              width={wp(8)}
                              height={hp(4)}
                            />
                          }
                          rounded={true}
                          size={30}
                          title={null}
                          onPress={() => {
                            setVisibleCamera(true);
                            setParams('id_back');
                          }}
                        />
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.textId}>
                      {I18n.t('signUpScreen.inputBackId')}
                    </Text>
                  </View>
                  <View>
                    <TouchableOpacity
                      style={styles.buttonCamera}
                      onPress={() => {
                        setVisibleCamera(true);
                        setParams('id_selfie');
                      }}>
                      {idPicture?.id_selfie ? (
                        <TouchableOpacity
                          onPress={() => {
                            setVisiblePreview(true);
                            setPreviewImage(idPicture?.id_selfie);
                            setParams('id_selfie');
                          }}>
                          <Image
                            source={{
                              uri: `data:image/png;base64,${idPicture?.id_selfie}`,
                            }}
                            style={{width: wp(20), height: wp(20)}}
                          />
                        </TouchableOpacity>
                      ) : (
                        <SvgWrapper
                          style={styles.logo}
                          component={<SelfieId />}
                        />
                      )}
                      <View style={styles.iconCamera}>
                        <Button
                          icon={
                            <SvgWrapper
                              component={<CameraIcon />}
                              width={wp(8)}
                              height={hp(4)}
                            />
                          }
                          rounded={true}
                          size={30}
                          title={null}
                          onPress={() => {
                            setVisibleCamera(true);
                            setParams('id_selfie');
                          }}
                        />
                      </View>
                    </TouchableOpacity>
                    <Text style={styles.textId}>
                      {I18n.t('signUpScreen.inputSelfie')}
                    </Text>
                  </View>
                </View>
              </View>
            </Field>
            <Gap height={hp(3)} />
            <Field
              name="employmentContract"
              value={idPicture}
              setValue={setIdPicture}>
              <View>
                <CameraWidget
                  setVisible={setVisibleCameraContract}
                  visibility={visibleCameraContract}
                  cameraRef={cameraContractRef}
                  onCapture={onCapture}
                  isCaptureIdCard
                  showButtonRotate
                  params={params}
                />
                <Label style={styles.labelFieldId}>
                  {I18n.t('signUpScreen.inputContractPhoto')}
                </Label>
                <Gap height={hp(1)} />
                <View style={styles.contractLabel}>
                  <TouchableOpacity
                    onPress={() => {
                      setVisibleCameraContract(true);
                      setParams('employment_contract');
                    }}
                    style={styles.buttonCamera}>
                    {idPicture?.employment_contract ? (
                      <TouchableOpacity
                        onPress={() => {
                          setVisiblePreview(true);
                          setPreviewImage(idPicture?.employment_contract);
                          setParams('employment_contract');
                        }}>
                        <Image
                          source={{
                            uri: `data:image/png;base64,${idPicture?.employment_contract}`,
                          }}
                          style={{width: wp(20), height: wp(20)}}
                        />
                      </TouchableOpacity>
                    ) : (
                      <SvgWrapper style={styles.logo} component={<IdFront />} />
                    )}
                    <View style={styles.iconCamera}>
                      <Button
                        icon={
                          <SvgWrapper
                            component={<CameraIcon />}
                            width={wp(8)}
                            height={hp(4)}
                          />
                        }
                        rounded={true}
                        size={30}
                        title={null}
                        onPress={() => {
                          setVisibleCameraContract(true);
                          setParams('employment_contract');
                        }}
                      />
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.textId}>
                    {I18n.t('signUpScreen.inputEmploymentContract')}
                  </Text>
                </View>
              </View>
            </Field>
            <Gap height={hp(3)} />
          </Form>
        </View>
      </Content>
    </Container>
  );
};

export default SignUpScreen;

const styles = StyleSheet.create({
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 10,
  },
  formContainer: {
    padding: 10,
  },
  inputContainer: {
    height: hp(6),
  },
  button: {
    borderRadius: 40,
  },
  inputPhoneNumber: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  buttonCamera: {
    marginVertical: 10,
    marginHorizontal: 15,
    width: wp(20),
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  iconCamera: {
    marginLeft: wp(-9),
    marginBottom: hp(-1),
  },
  iconPhoneNumber: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: hp(-1),
  },
  fieldId: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  textId: {
    textAlign: 'center',
    fontSize: fonts.size[12],
    fontFamily: fonts.primary[700],
    marginTop: hp(2),
  },
  labelFieldId: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
  logo: {
    alignSelf: 'center',
    width: hp(10),
    height: hp(10),
  },
  contractLabel: {
    alignSelf: 'flex-start',
  },
});
