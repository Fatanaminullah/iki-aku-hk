import {homeActionTypes} from '../../action-types';

const {SET_MENU_LIST, SET_PROMOTION_BANNERS} = homeActionTypes;

const promotionBanners = [
  {
    img:
      'https://res.cloudinary.com/ikiassets/image/upload/c_scale/v1594111463/iki-remit-android-assets/banner_01_2_em3plv.png',
    title: 'Free Ongkir',
    description:
      "Nikmati promo spesial GRATIS ongkir selama bulan Juli-Agustus melalui aplikasi IKI AKU. Kini,kirim uang dari Taiwan langsung ke penerima di Indonesia jadi lebih mudah,aman,dan cepat! <br> unduh: <a href='https://bit.do/ikiaku'>https://bit.do/ikiaku</a> <br> LINE: <a href='https://bit.do/lineiki'>https://bit.do/lineiki</a>",
  },
  {
    img:
      'https://res.cloudinary.com/ikiassets/image/upload/c_scale/v1596703977/iki-remit-android-assets/02_onedaytour_idc1lg.jpg',
    title: '#One Day Tour',
    description:
      "Yg lg booming dan yg kamu ga boleh ketinggalan apa nih yeee...? <br>Ya jalan2 meng-eksplore keindahan dalam negri Formosa donk! <br>Beberapa waktu ini tempat2 wisata di negri Taiwan menjadi pusat perhatian masyarakat domestik dan mancanegara loh! <br> Kamyuuu.. jangan ketinggalan donks! Ikuti tour bersama menjelajah negri pasir di utara Taiwan 🇹🇼 <br> yg pastinya bisa mengabadikan foto2 suantik muu Hanya 980nt saja, <br> <br>sudah termasuk:<br> +makan siang<br>+airputih<br>+transportasi<br>+tiket<br>+asuransi<br> <br>Daftar/minat/tanya2 lgsg aja inbox ke kita disini yah.. FB: <a href='https://bit.ly/ikifb'>https://bit.ly/ikifb</a><br>LINE: <a href='https://bit.ly/ikiline'>https://bit.ly/ikiline</a>",
  },
  {
    img:
      'https://res.cloudinary.com/ikiassets/image/upload/c_scale/v1596704039/iki-remit-android-assets/03_telkom_qlo39x.jpg',
    title: '#MERDEKA!',
    description:
      '<b><br>MERDEKA!!!<br></b><br>Promo Dirgahayu Indonesia 2020!<br>1 Agustus - 31 Agustus 2020<br>Di HUT RI yang ke-75 ini Kartu As akan bagi2 HADIAH ke kalian semua<br><br>Dapatkan HADIAH<br>7 Voucher 300nt As 2in1<br>5 Powerbank Xiaomi<br><b><br>*Syarat & Ketentuan<br></b><b>- Mengumpulkan 17 poin dari tanggal 1-31 Agustus 2020<br></b><b>- Pemenang akan diundi pada tanggal 2 September 2020</b>',
  },
  {
    img:
      'https://res.cloudinary.com/ikiassets/image/upload/c_scale/v1596452449/iki-remit-android-assets/banner_tuku_online_uvbgff.jpg',
    title: '#TukuOnlineTW',
    description:
      "<strong>#TukuOnlineTW</strong> Screenshot halaman ini, pencet link dibawah ini dah inbok ke Tuku Online! kamu akan mendapatkan FREE ongkir dengan hanya belanja minim 299NT aja!!,<br><br> TUKU ONLINE,REGO Murah, Ra Kaleng Kaleng<br><br>Jangan lupa Like dan ikuti terus FB TUKU ONLINE! <br> FB/LINE: <a href='@tukuonlinetw'>@tukuonlinetw</a><br> <br><a href='https://bit.ly/ikitukuonline'>https://bit.ly/ikitukuonline</a><br>",
  },
];

const initialState = {
  menu: [],
  promotionBanners: promotionBanners,
};

export default (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SET_MENU_LIST:
      return {...state, menu: payload};
    case SET_PROMOTION_BANNERS:
      return {...state, promotionBanners: payload};
    default:
      return state;
  }
};
