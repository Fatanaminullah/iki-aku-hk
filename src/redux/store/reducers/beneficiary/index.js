import {beneficiaryTypes} from '../../action-types';

const {
  SET_LIST_BENEFICIARY,
  SET_DETAIL_BENEFICIARY,
  SET_LIST_BANK,
  SET_LIST_RECENT_BENEFICIARY,
} = beneficiaryTypes;

const initState = {
  recentBeneficiary: [],
  beneficiary: [],
  detailBeneficiary: {},
  listBank: [],
};

export default (state = initState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SET_DETAIL_BENEFICIARY:
      return {...state, detailBeneficiary: payload};
    case SET_LIST_BENEFICIARY:
      return {...state, beneficiary: payload};
    case SET_LIST_RECENT_BENEFICIARY:
      return {...state, recentBeneficiary: payload};
    case SET_LIST_BANK:
      return {...state, listBank: payload};
    default:
      return state;
  }
};
