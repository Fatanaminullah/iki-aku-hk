import React, {useEffect} from 'react';
import {BackHandler, StyleSheet, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useSelector} from 'react-redux';
import {
  Container,
  Header,
  PpobDetail,
  RemittanceDetail,
} from '../../components';

const TransactionDetailsScreen = ({navigation, route}) => {
  const {trxDetails} = useSelector((state) => state.transaction);
  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackPress);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackPress);
    };
  }, []);
  const handleBackPress = () => {
    navigation.navigate('MainApp', {screen: 'Home'});
    return true;
  };
  return (
    <Container>
      <Header
        title={I18n.t('transactionDetailsScreen.title')}
        onPress={() => handleBackPress()}
      />
      <View style={styles.flex}>
        {trxDetails?.type === 'TRANSFER' ? (
          <RemittanceDetail trxDetails={trxDetails} />
        ) : (
          <PpobDetail trxDetails={trxDetails} />
        )}
      </View>
    </Container>
  );
};

export default TransactionDetailsScreen;

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },
});
