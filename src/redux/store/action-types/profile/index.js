const SET_LIST_CONTACT = '@profile/set-list-contact';
const SET_USER_DETAILS = '@profile/set-user-details';
const SET_LIST_OCCUPATION = '@profile/set-list-occupation';

export {SET_LIST_CONTACT, SET_USER_DETAILS, SET_LIST_OCCUPATION};
