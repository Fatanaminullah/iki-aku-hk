import I18n from 'react-native-redux-i18n';
import {ArcBackPng, ArcFrontPng, SelfieArcPng} from '../../assets';

const arrayDetail = [
  {
    value: 'profilePicture',
    type: 'image-picker',
  },
  {
    value: 'arcExpiredDate',
    type: 'date-picker',
  },
  {
    value: 'arcPhoto',
    type: 'image-picker-group',
  },
  {
    value: 'arcNumber',
    type: 'input-text',
  },
  {
    value: 'fullName',
    type: 'input-text',
  },
  {
    value: 'birthDate',
    type: 'date-picker',
  },
  {
    value: 'gender',
    type: 'radio-group',
  },
  {
    value: 'addressInTaiwan',
    type: 'input-text',
  },
  {
    value: 'occupation',
    type: 'input-picker',
  },
  {
    value: 'email',
    type: 'input-text',
  },
  {
    value: 'phoneNumber',
    type: 'input-phone',
  },
];

const unEditableField = ['arcNumber', 'fullName', 'birthDate'];

const editableMapper = (status, fieldName) => {
  switch (status) {
    case 0:
      return true;
    case 1:
      if (unEditableField.some((item) => fieldName === item)) {
        return false;
      } else {
        return true;
      }
    default:
      return true;
  }
};

const mapperDetail = (data) => {
  const result = arrayDetail.map((item) => {
    if (item.value === 'arcPhoto') {
      return {
        title: I18n.t('editProfileScreen.arcPhoto'),
        value: [
          {
            title: I18n.t('editProfileScreen.arcFront'),
            img: data?.arcPhoto?.arc_front,
            default: ArcFrontPng,
            fieldName: 'arc_front',
          },
          {
            title: I18n.t('editProfileScreen.arcBack'),
            img: data?.arcPhoto?.arc_back,
            default: ArcBackPng,
            fieldName: 'arc_back',
          },
          {
            title: I18n.t('editProfileScreen.selfieWithArc'),
            img: data?.arcPhoto?.arc_selfie,
            default: SelfieArcPng,
            fieldName: 'arc_selfie',
          },
        ],
        type: item.type,
        isEditable: editableMapper(data?.status, item.fieldName),
        fieldName: item.value,
      };
    } else if (item.value === 'profilePicture') {
      return {
        title: I18n.t(`editProfileScreen.${item.value}`),
        value: data?.arcPhoto?.photo_profile,
        fieldName: item.value,
        type: item.type,
        isEditable: editableMapper(data?.status, item.fieldName),
      };
    } else {
      return {
        title: I18n.t(`editProfileScreen.${item.value}`),
        value: data[item.value],
        fieldName: item.value,
        type: item.type,
        isEditable: editableMapper(data?.status, item.value),
      };
    }
  });
  return result;
};

export default mapperDetail;
