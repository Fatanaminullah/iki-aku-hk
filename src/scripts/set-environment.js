const fs = require('fs');
//Obtain the environment string passed to the node script
const environment = process.argv[2];
//read the content of the json file
const envFileContent = require(`../environments/env-${environment}.json`);
const googleServiceFileContent = require(`../../android/app/google-services.${environment}.json`);
//copy the json inside the env.json file
fs.writeFileSync(
  'src/environments/env.json',
  JSON.stringify(envFileContent, undefined, 2),
);
//copy the json inside the google-service.json file
fs.writeFileSync(
  'android/app/google-services.json',
  JSON.stringify(googleServiceFileContent, undefined, 2),
);