import React, {useState} from 'react';
import I18n from 'react-native-i18n';
import Countdown from 'react-countdown';
import {StyleSheet, Text} from 'react-native';
import {getBrand, getDeviceType, getModel} from 'react-native-device-info';
import {IkiAkuIcon1} from '../../assets';
import {
  Container,
  Content,
  Header,
  Button,
  Gap,
  Link,
  OtpInput,
  SvgWrapper,
} from '../../components';
import {fonts, hp, registerUserId, wp} from '../../utilities';
import {
  showAlertError,
  showAlertSuccess,
} from '../../redux/store/actions/alert';
import {requestOtp, validateOtp} from '../../redux/store/actions/profile';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {useDispatch} from 'react-redux';
import {login} from '../../redux/store/actions/authentication';

const SignUpOtpScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const [disabled, setDisabled] = useState(true);
  const [otp, setOtp] = useState('');
  const [timer, setTimer] = useState({
    timer: Date.now() + 59000,
    startTimer: true,
    key: null,
  });

  const onCodeChanged = (code) => {
    setOtp(code);
    if (code.length < 6 && !disabled) {
      setDisabled(true);
    }
    if (code.length === 6) {
      setDisabled(false);
    }
  };

  const onResendOtp = () => {
    const request = {
      reg_id: route?.params?.request?.reg_id,
    };
    dispatch(
      requestOtp(
        request,
        showLoading,
        dismissLoading,
        showAlertSuccess,
        showAlertError,
      ),
    ).then((res) => {
      const time = res.otp_expiry * 60000;
      setOtp('');
      setTimer({
        timer: Date.now() + time,
        startTimer: true,
        key: !timer.key,
      });
    });
  };

  const renderer = ({hours, minutes, seconds, completed, ...rest}) => {
    if (completed) {
      return (
        <Link
          title={I18n.t('signUpOtpScreen.resendOtp')}
          align="center"
          onPress={onResendOtp}
        />
      );
    } else {
      return (
        <Text style={styles.timer}>
          {hours} : {minutes} : {seconds}
        </Text>
      );
    }
  };

  const onConfirm = () => {
    const {request, password, idNumber} = route.params;
    const requestValidate = {
      otp,
      reg_id: request?.reg_id,
      is_register_user: true,
      password: password,
    };
    dispatch(validateOtp(requestValidate, showLoading, null, showAlertError))
      .then(() => {
        const description = getModel();
        const name = getBrand();
        const type = getDeviceType();
        const requestLogin = {
          password,
          username: idNumber,
          metadata: {
            device: {
              description,
              name,
              type,
            },
          },
        };
        dispatch(
          login(requestLogin, null, dismissLoading, showAlertError),
        ).then((res) => {
          registerUserId(res?.username);
          navigation.reset({
            index: 0,
            routes: [{name: 'MainApp'}],
          });
          dispatch(dismissLoading());
          dispatch(
            showAlertSuccess(
              I18n.t('signUpOtpScreen.successTitle'),
              I18n.t('signUpOtpScreen.successMessage'),
            ),
          );
        });
      })
      .catch(() => {
        setOtp('');
        setDisabled(true);
        dispatch(dismissLoading());
      });
  };
  return (
    <Container>
      <Header title={I18n.t('signUpOtpScreen.title')} separator={false} />
      <Content centerContent>
        <SvgWrapper component={<IkiAkuIcon1 />} style={styles.alignCenter} />
        <Gap height={hp(2)} />
        <Text style={styles.instructionText}>
          {I18n.t('signUpOtpScreen.instruction')}
        </Text>
        <Gap height={hp(4)} />
        <OtpInput onCodeChanged={onCodeChanged} code={otp} pinCount={6} />
        <Gap height={hp(4)} />
        <Countdown
          key={timer.key}
          date={timer.timer}
          intervalDelay={1000}
          renderer={renderer}
          autoStart={timer.startTimer}
        />
        <Gap height={hp(4)} />
        <Button
          type="danger"
          disabled={disabled}
          title={I18n.t('signUpOtpScreen.confirmation')}
          rounder
          onPress={onConfirm}
        />
      </Content>
    </Container>
  );
};

export default SignUpOtpScreen;

const styles = StyleSheet.create({
  alignCenter: {
    alignSelf: 'center',
    width: wp(25),
    height: wp(25),
  },
  instructionText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    textAlign: 'center',
    alignSelf: 'center',
    maxWidth: wp(80),
  },
  timer: {
    alignSelf: 'center',
    fontFamily: fonts.primary.bold,
  },
});
