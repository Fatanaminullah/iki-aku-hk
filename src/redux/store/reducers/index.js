import {reducer as i18n} from 'react-native-redux-i18n';
import {combineReducers} from 'redux';
import homeReducer from './home';
import loadingReducer from './loading';
import alertReducer from './alert';
import profileReducer from './profile';
import beneficiaryReducer from './beneficiary';
import transferReducer from './transfer';
import transactionReducer from './transaction';
import rewardReducer from './reward';
import localeReducer from './locale';
import notificationReducer from './notification';

export const appReducer = combineReducers({
  i18n,
  home: homeReducer,
  loading: loadingReducer,
  alert: alertReducer,
  profile: profileReducer,
  beneficiary: beneficiaryReducer,
  transfer: transferReducer,
  transaction: transactionReducer,
  reward: rewardReducer,
  locale: localeReducer,
  notification: notificationReducer,
});
