import React, {useEffect} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {FlagIndoRectangle2, IkiAkuIcon1} from '../../assets';
import {
  Button,
  Container,
  Content,
  DatePicker,
  Field,
  Form,
  Gap,
  Header,
  Input,
  InputPicker,
  RadioGroup,
  SvgWrapper,
} from '../../components';
import {
  showAlertError,
  showAlertSuccess,
} from '../../redux/store/actions/alert';
import {
  addBeneficiary,
  checkAccount,
  editBeneficiary,
  getListBank,
} from '../../redux/store/actions/beneficiary';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {setTransferData} from '../../redux/store/actions/transaction';
import {hp, validation, wp} from '../../utilities';

const {required, length} = validation;
const BeneficiaryFormScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {listBank} = useSelector((state) => state.beneficiary);
  useEffect(() => {
    dispatch(getListBank(showLoading, dismissLoading));
  }, [dispatch]);
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        rounded
        type="danger"
        title={I18n.t('beneficiaryFormScreen.submit')}
        onPress={handleSubmit}
      />
    );
  };
  const onSubmit = (data) => {
    const request = {
      idCard: data.identityNumber,
      fullName: data.fullName,
      dateOfBirth: data.birthdate,
      fullAddress: data.address,
      gender: data.gender,
      bankDatas: [
        {
          bankAcc: data.accountNumber,
          bankCode: data.bankId.label.split(' - ')[0],
          bankName: data.bankId.label.split(' - ')[1],
        },
      ],
      bankAcc: data.accountNumber,
      bankName: data.bankId.label.split(' - ')[1],
      bankCode: data.bankId.label.split(' - ')[0],
      fullPhone: data.phoneNumber,
    };

    if (route?.params?.isEdit) {
      dispatch(
        editBeneficiary(
          request,
          showLoading,
          dismissLoading,
          showAlertError,
          showAlertSuccess,
        ),
      )
        .then(() => {
          navigation.navigate('BeneficiaryScreen');
        })
        .catch((err) => {
          dispatch(showAlertError('', err.message));
        });
    } else {
      dispatch(checkAccount(request, showLoading, null))
        .then((res) => {
          dispatch(
            addBeneficiary(
              request,
              null,
              dismissLoading,
              showAlertError,
              showAlertSuccess,
            ),
          ).then(() => {
            if (route?.params?.isFromTransferScreen) {
              const result = {
                ...route.params.data,
                recipient: {
                  ...data,
                  receiver: data?.fullName,
                  bankName: data?.bankId?.label,
                },
              };
              dispatch(setTransferData(result)).then(() => {
                navigation.navigate('TransferConfirmationScreen');
              });
            } else {
              navigation.goBack();
            }
          });
        })
        .catch(() => {
          dispatch(showAlertError('', 'Account Not Found'));
        })
        .finally(() => {
          dispatch(dismissLoading());
        });
    }
  };
  return (
    <Container>
      <Header
        title={I18n.t(
          route?.params?.isEdit
            ? 'beneficiaryFormScreen.titleEdit'
            : 'beneficiaryFormScreen.titleAdd',
        )}
      />
      <Content>
        <SvgWrapper
          component={<IkiAkuIcon1 />}
          width={wp(25)}
          height={wp(25)}
        />
        <Form
          onSubmit={onSubmit}
          submitComponent={renderSubmitComponent}
          defaultValues={route?.params?.detailEdit}>
          <Field
            name="identityNumber"
            validation={{
              minLength: length(16),
              maxLength: length(16),
            }}>
            <Input
              label={I18n.t('beneficiaryFormScreen.identityNumber')}
              placeholder={I18n.t(
                'beneficiaryFormScreen.identityNumberPlaceholder',
              )}
              keyboardType="numeric"
              containerStyle={styles.inputContainer}
            />
          </Field>
          <Gap height={hp(3)} />
          <Field name="fullName" validation={{required}}>
            <Input
              label={I18n.t('beneficiaryFormScreen.fullName')}
              placeholder={I18n.t('beneficiaryFormScreen.fullNamePlaceholder')}
              containerStyle={styles.inputContainer}
            />
          </Field>
          <Gap height={hp(3)} />
          <Field name="birthdate" validation={{required}}>
            <DatePicker
              format="YYYY-MM-DD"
              label={I18n.t('beneficiaryFormScreen.birthdate')}
              placeholder={I18n.t('beneficiaryFormScreen.birthdatePlaceholder')}
              showIcon={true}
            />
          </Field>
          <Gap height={hp(3)} />
          <Field name="gender" validation={{required}}>
            <RadioGroup
              label={I18n.t('signUpScreen.inputGender')}
              listItem={[
                {label: I18n.t('signUpScreen.man'), value: 'MALE'},
                {label: I18n.t('signUpScreen.woman'), value: 'FEMALE'},
              ]}
              direction="row"
            />
          </Field>
          <Gap height={hp(3)} />
          <Field name="phoneNumber" validation={{required}}>
            <Input
              label={I18n.t('signUpScreen.inputPhoneNumber')}
              placeholder={I18n.t('signUpScreen.placeholderPhoneNumber')}
              containerStyle={styles.inputContainer}
              leftIcon={
                <View style={styles.iconPhoneNumber}>
                  <SvgWrapper
                    component={<FlagIndoRectangle2 />}
                    width={wp(10)}
                    height={wp(10)}
                  />
                  <Text>+62</Text>
                </View>
              }
              keyboardType="numeric"
              type="phoneNumber"
            />
          </Field>
          <Gap height={hp(3)} />
          <Field name="address">
            <Input
              label={I18n.t('beneficiaryFormScreen.address')}
              placeholder={I18n.t('beneficiaryFormScreen.addressPlaceholder')}
              containerStyle={styles.inputContainer}
            />
          </Field>
          <Gap height={hp(3)} />
          <Field name="accountNumber" validation={{required}}>
            <Input
              label={I18n.t('beneficiaryFormScreen.accountNumber')}
              placeholder={I18n.t('beneficiaryFormScreen.accountNumber')}
              containerStyle={styles.inputContainer}
              keyboardType="numeric"
            />
          </Field>
          <Gap height={hp(3)} />
          <Field name="bankId" validation={{required}}>
            <InputPicker
              searchPlaceholder={I18n.t(
                'beneficiaryFormScreen.bankPickerSearch',
              )}
              listEmptyText={I18n.t('beneficiaryFormScreen.bankNotFound')}
              title={I18n.t('beneficiaryFormScreen.bankPickerTitle')}
              label={I18n.t('beneficiaryFormScreen.bankName')}
              placeholder={I18n.t('beneficiaryFormScreen.bankNamePlaceholder')}
              dataSource={listBank}
            />
          </Field>
          <Gap height={hp(3)} />
        </Form>
      </Content>
    </Container>
  );
};

export default BeneficiaryFormScreen;

const styles = StyleSheet.create({
  inputContainer: {
    height: hp(6),
  },
  iconPhoneNumber: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});
