import {loadingActionTypes} from '../../action-types';

const {SHOW_LOADING, HIDE_LOADING} = loadingActionTypes;

const initState = {
  visible: false,
};

export default (state = initState, action) => {
  const {type} = action;
  switch (type) {
    case SHOW_LOADING:
      return {...state, visible: true};
    case HIDE_LOADING:
      return {...state, visible: false};
    default:
      return state;
  }
};
