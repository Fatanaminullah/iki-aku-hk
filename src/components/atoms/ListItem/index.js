import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {fonts, hp} from '../../../utilities';
import HorizontalLine from '../HorizontalLine';

const ListItem = ({
  item,
  style,
  labelStyle,
  valueStyle,
  value2Style,
  columns,
  label,
  value1,
  value2,
  noLine,
  containerStyle,
  index,
}) => {
  if (columns == 3) {
    return (
      <View style={[styles.container, containerStyle]} key={index}>
        <View style={[styles.content, style]}>
          <View style={styles.valueLeft}>
            <Text style={[styles.textLabel, labelStyle]}>{label}</Text>
          </View>
          <View style={styles.valueMiddle}>
            <Text style={[styles.textValue, valueStyle]}>: {value1}</Text>
          </View>
          <View style={styles.valueRight}>
            <Text style={[styles.textValue, value2Style]}>{value2}</Text>
          </View>
        </View>
        {!noLine && <HorizontalLine height={0.5} />}
      </View>
    );
  }
  return (
    <View style={[styles.container, containerStyle]} key={index}>
      <View style={[styles.content, style]}>
        <Text style={[styles.textLabel, labelStyle]}>
          {item?.label || item?.title}
        </Text>
        <Text style={[styles.textValue, valueStyle]}>{item?.value}</Text>
      </View>
      <HorizontalLine height={0.5} />
    </View>
  );
};

export default ListItem;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 10,
  },
  content: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 5,
  },
  textLabel: {
    fontSize: fonts.size[12],
    fontFamily: fonts.primary[700],
  },
  textValue: {
    fontSize: fonts.size[12],
    fontFamily: fonts.primary[700],
  },
  valueLeft: {
    width: '33%',
  },
  valueMiddle: {
    width: '33%',
    alignItems: 'center',
    marginLeft: hp(-5),
  },
  valueRight: {
    width: '60%',
    alignItems: 'flex-end',
    marginLeft: hp(-10),
  },
});
