import React from 'react';
import I18n from 'react-native-redux-i18n';
import {StyleSheet, Text, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {Header, Container, Content, Gap, Button} from '../../components';
import {colors, fonts, hp} from '../../utilities';
import {about} from './aboutText';
import {WebView} from 'react-native-webview';

const AboutUsScreen = () => {
  const navigation = useNavigation();
  return (
    <Container>
      <Header title={I18n.t('aboutUsScreen.title')} />
      <Content>
        <Text style={styles.title}>{I18n.t('aboutUsScreen.title')}</Text>
        <Gap height={hp(2)} />
        {about.map((item) => (
          <WebView
            source={{
              html:
                "<p style='text-align: justify; font-size: 250%;'>" +
                item +
                '</p>',
            }}
          />
        ))}
      </Content>
      <View style={styles.buttonContainer}>
        <Button
          type="danger"
          title={I18n.t('aboutUsScreen.ok')}
          style={styles.button}
          onPress={() => navigation.goBack()}
        />
      </View>
    </Container>
  );
};

export default AboutUsScreen;

const styles = StyleSheet.create({
  title: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[20],
    color: colors.red1,
    textAlign: 'center',
  },
  buttonContainer: {
    alignItems: 'center',
    padding: hp(2),
    marginTop: hp(-2),
  },
  button: {
    borderRadius: 50,
    height: hp(5),
    width: hp(43),
  },
});
