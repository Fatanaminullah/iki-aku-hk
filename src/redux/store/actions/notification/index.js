import {notificationTypes} from '../../action-types';

const {SET_NOTIFICATION_LIST} = notificationTypes;

const setNotificationList = (payload) => ({
  type: SET_NOTIFICATION_LIST,
  payload,
});

export {setNotificationList};
