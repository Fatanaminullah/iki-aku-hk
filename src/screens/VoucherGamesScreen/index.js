import React, {useEffect, useState} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ImageBackground,
  ScrollView,
} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {CardList, Container, Gap, Header, SvgWrapper} from '../../components';
import {VoucherGamesImgBg} from '../../assets/illustrations';
import {
  alertActions,
  loadingActions,
  transactionActions,
} from '../../redux/store/actions';
import {
  colors,
  fonts,
  getData,
  hp,
  PPOB,
  LIST_NON_DIRECT_VOUCHER_GAMES,
  storeData,
} from '../../utilities';
import {IkiAkuIcon5} from '../../assets';
import {
  iconMapper,
  voucherGamesNonDirectTopUpInfoMapper,
  voucherGamesNonDirectProductMapper,
} from './mapper';
const {
  setProductVoucherGames,
  setProductVoucherGamesByGroup,
  setVoucherGamesNonDirectTopUpInfo,
} = transactionActions;

const {showLoading, dismissLoading} = loadingActions;
const {showAlertError} = alertActions;

const VoucherGamesScreen = ({navigation, route}) => {
  const {listProductVoucherGamesByGroup} = useSelector(
    (state) => state.transaction,
  );
  const [listFilterProduct] = useState(listProductVoucherGamesByGroup);

  const onSelect = (groupName) => {
    let isDirect = LIST_NON_DIRECT_VOUCHER_GAMES.includes(groupName);
    if (isDirect) {
      return navigation.navigate('VoucherGamesProductScreen', {
        isDirect,
        groupName,
        voucherGamesNonDirectTopUpInfoFormData: voucherGamesNonDirectTopUpInfoMapper(
          groupName,
        ),
        voucherGamesNonDirectProductFormData: voucherGamesNonDirectProductMapper(
          groupName,
        ),
      });
    }
    navigation.navigate('VoucherGamesProductScreen', {
      isDirect,
      groupName,
    });
  };

  const renderCard = (item) => {
    return (
      <TouchableOpacity
        style={styles.buttonContainer}
        onPress={() => onSelect(item)}>
        <Image source={iconMapper(item)} style={styles.image} />
        <Gap height={hp(1)} />
        <Text style={styles.buttonTitle}>{item}</Text>
      </TouchableOpacity>
    );
  };
  return (
    <Container>
      <Header title={I18n.t('voucherGamesScreen.title')} />
      <ImageBackground source={VoucherGamesImgBg} style={styles.bgImage}>
        <ScrollView>
          <View style={styles.logoContainer}>
            <SvgWrapper
              component={<IkiAkuIcon5 />}
              width={hp(10)}
              height={hp(10)}
            />
          </View>
          <View style={styles.topContainer}>
            <Text style={styles.title}>
              {I18n.t('voucherGamesScreen.title')}
            </Text>
          </View>
          <CardList
            list={LIST_NON_DIRECT_VOUCHER_GAMES}
            renderCard={renderCard}
            numColumns={3}
          />
        </ScrollView>
      </ImageBackground>
    </Container>
  );
};

export default VoucherGamesScreen;

const styles = StyleSheet.create({
  bgImage: {
    flex: 1,
    resizeMode: 'cover',
    alignSelf: 'stretch',
  },
  title: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    color: colors.white,
  },
  content: {
    flex: 1,
  },
  topContainer: {
    marginTop: hp(1),
    marginLeft: hp(2),
    marginBottom: hp(-3),
  },
  buttonContainer: {
    marginBottom: hp(0.5),
    alignItems: 'center',
  },
  buttonTitle: {
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.bold,
    color: colors.white,
    marginTop: hp(-1),
    textAlign: 'center',
    maxWidth: 100,
  },
  image: {
    height: hp(14),
    width: hp(14),
  },
  logoContainer: {
    marginLeft: hp(2),
  },
});
