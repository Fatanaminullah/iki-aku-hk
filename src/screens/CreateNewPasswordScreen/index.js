import React from 'react';
import I18n from 'react-native-i18n';
import {useDispatch} from 'react-redux';
import {IkiAkuIcon1} from '../../assets';
import {
  Button,
  Container,
  Content,
  Field,
  Form,
  Gap,
  Header,
  Input,
  SvgWrapper,
} from '../../components';
import {
  showAlertError,
  showAlertSuccess,
} from '../../redux/store/actions/alert';
import {hp, useForm, validation, wp} from '../../utilities';
import {resetPassword} from '../../redux/store/actions/authentication';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';

const {required, confirmPassword, minLength} = validation;

const CreateNewPasswordScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const [form, setForm] = useForm({newPassword: '', confirmNewPassword: ''});
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        type="danger"
        title={I18n.t('createNewPasswordScreen.resetPassword')}
        onPress={handleSubmit}
        rounded
      />
    );
  };

  const onSubmit = (data, setValue, setError) => {
    const request = {
      username: route?.params?.username,
      newPassword: data.newPassword,
      resetId: route?.params?.forgot_pass_id,
    };
    dispatch(
      resetPassword(
        request,
        showLoading,
        dismissLoading,
        showAlertSuccess,
        showAlertError,
      ),
    )
      .then((res) => {
        dispatch(
          showAlertSuccess('', I18n.t('createNewPasswordScreen.success')),
        );
        navigation.navigate('SignInScreen');
      })
      .catch((err) => {
        if (err?.response?.status === 404) {
          setError('confirmNewPassword', {
            type: 'manual',
            message: err.response?.data?.message,
          });
        }
      });
  };

  return (
    <Container>
      <Header
        title={I18n.t('createNewPasswordScreen.title')}
        separator={false}
      />
      <Content>
        <SvgWrapper
          component={<IkiAkuIcon1 />}
          width={wp(25)}
          height={wp(25)}
        />
        <Gap height={hp(4)} />
        <Form onSubmit={onSubmit} submitComponent={renderSubmitComponent}>
          <Field
            name="newPassword"
            validation={{required, minLength: minLength(8)}}>
            <Input
              rounded
              label={I18n.t('createNewPasswordScreen.newPassword')}
              onChangeText={(val) => setForm('newPassword', val)}
              placeholder={I18n.t('createNewPasswordScreen.newPassword')}
              secureTextEntry
            />
          </Field>
          <Gap height={hp(3)} />
          <Field
            name="confirmNewPassword"
            validation={{
              required,
              validate: (val) => confirmPassword(val, form.newPassword),
            }}>
            <Input
              rounded
              label={I18n.t('createNewPasswordScreen.confirmNewPassword')}
              onChangeText={(val) => setForm('confirmNewPassword', val)}
              placeholder={I18n.t('createNewPasswordScreen.confirmNewPassword')}
              secureTextEntry
            />
          </Field>
          <Gap height={hp(4)} />
        </Form>
      </Content>
    </Container>
  );
};

export default CreateNewPasswordScreen;
