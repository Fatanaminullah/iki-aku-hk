const SET_TRX_SUMMARY = '@transaction/set-trx-summary';
const SET_TRX_DETAILS = '@transaction/set-trx-detail';
const SET_TRX_HISTORY_LIST = '@transaction/set-trx-history-list';
const SET_COUPON = '@transaction/set-coupon';
const SET_TRANSFER_DATA = '@transaction/set-transfer-data';
const SET_RATE_DATA = '@transaction/set-rate-data';
const SET_SOURCE_OF_FUNDS = '@transaction/set-source-of-funds';

export {
  SET_TRX_SUMMARY,
  SET_TRX_DETAILS,
  SET_TRX_HISTORY_LIST,
  SET_TRANSFER_DATA,
  SET_COUPON,
  SET_RATE_DATA,
  SET_SOURCE_OF_FUNDS,
};
