import React, {useState} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  Button,
  Container,
  Content,
  Gap,
  Header,
  ListItem,
} from '../../components';
import I18n from 'react-native-redux-i18n';
import {colors, fonts, hp, thousandSeparator, wp} from '../../utilities';
import {amountDetailMapper, recipientMapper, senderMapper} from './mapper';
import Countdown from 'react-countdown';
import {
  getRateData,
  setTransferData,
} from '../../redux/store/actions/transaction';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';

const TransferConfirmationScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {transferData, rateData} = useSelector((state) => state.transaction);
  const senderDetail = senderMapper(transferData);
  const recipientDetail = recipientMapper(transferData?.recipient);
  const amountDetail = amountDetailMapper(transferData);
  const [timer, setTimer] = useState({
    timer: Date.now() + 600000,
    startTimer: true,
    key: null,
  });

  const renderer = ({hours, minutes, seconds, completed, ...rest}) => {
    if (completed) {
      return (
        <Text style={{fontFamily: fonts.primary.primary, color: colors.white}}>
          {minutes} : {seconds}
        </Text>
      );
    } else {
      return (
        <Text style={{fontFamily: fonts.primary.primary, color: colors.white}}>
          {minutes} : {seconds}
        </Text>
      );
    }
  };
  const refreshData = (rate) => {
    const result = {
      ...transferData,
      adminFee: Number(rate.admin_fee),
      transferredEstimate:
        Number(transferData.transferAmount) * Number(rate.kurs),
      totalEstimate:
        Number(transferData.transferAmount) + Number(rate.admin_fee),
      totalPayment:
        Number(transferData.transferAmount) +
        Number(rate.admin_fee) +
        Number(transferData.discount),
    };
    dispatch(setTransferData(result));
  };
  return (
    <Container>
      <Header title={I18n.t('transferConfirmationScreen.title')} />
      <Content noPadding>
        <View style={{padding: 20}}>
          <Text style={styles.caption1}>
            {I18n.t('transferScreen.estimatedRate', {
              rate: thousandSeparator(Number(rateData?.kurs)),
            })}
          </Text>
          <Gap height={15} />
          <View style={styles.sectionContainer}>
            <Text style={styles.sectionText}>
              {I18n.t('transferConfirmationScreen.transferDetail')}
            </Text>
          </View>
          <Text style={styles.titleDetail}>
            {I18n.t('transferConfirmationScreen.from')}
          </Text>
          {senderDetail.map((item, index) => (
            <ListItem
              key={index}
              noLine
              label={item.label}
              containerStyle={styles.textDetailContainer}
              labelStyle={styles.textDetail}
              valueStyle={styles.textDetail}
              value2Style={styles.textDetail}
              value2={item.value}
              columns="3"
            />
          ))}
          <Text style={styles.titleDetail}>
            {I18n.t('transferConfirmationScreen.to')}
          </Text>
          {recipientDetail.map((item) => (
            <ListItem
              noLine
              label={item.label}
              containerStyle={styles.textDetailContainer}
              labelStyle={styles.textDetail}
              valueStyle={styles.textDetail}
              value2Style={styles.textDetail}
              value2={item.value}
              columns="3"
            />
          ))}
          <View style={styles.sectionContainer}>
            <Text style={styles.sectionText}>Detail Nominal</Text>
          </View>
          {amountDetail.map((item) => (
            <ListItem
              noLine
              label={item.label}
              containerStyle={styles.textDetailContainer}
              labelStyle={styles.textColor(item.label)}
              valueStyle={styles.textColor(item.label)}
              value2Style={styles.textColor(item.label)}
              value1={item.currency}
              value2={item.value}
              columns="3"
            />
          ))}
        </View>
        <Gap height={hp(10)} />
        <View style={{paddingHorizontal: 20}}>
          <Button
            onPress={() => {
              navigation.navigate('TransferConfirmationDataScreen');
            }}
            title={I18n.t('transferConfirmationScreen.confirmButton')}
            type="danger"
            rounded
          />
        </View>
        <Gap height={hp(5)} />
      </Content>
      <View style={styles.section2Container}>
        <Text style={{color: colors.white}}>
          {I18n.t('transferConfirmationScreen.countdownRateChanged')}
        </Text>
        <Countdown
          key={timer.key}
          daysInHours={true}
          date={timer.timer}
          intervalDelay={1000}
          renderer={renderer}
          autoStart={timer.startTimer}
          onComplete={() => {
            dispatch(
              getRateData({currency: 'HKD'}, showLoading, dismissLoading),
            ).then((res) => {
              setTimer({
                timer: Date.now() + 30000,
                startTimer: true,
                key: !timer.key,
              });
              refreshData(res);
            });
          }}
        />
      </View>
    </Container>
  );
};

export default TransferConfirmationScreen;

const styles = StyleSheet.create({
  caption1: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    alignSelf: 'flex-end',
  },
  textDetailContainer: {
    paddingVertical: 0,
  },
  textDetail: {
    // fontSize: fonts.size[12],
    fontFamily: fonts.primary.normal,
    color: colors.black,
  },
  textColor: (label) => ({
    color:
      label === I18n.t('transferConfirmationScreen.adminFee')
        ? colors.red1
        : label === I18n.t('transferConfirmationScreen.totalPayment')
        ? colors.green1
        : colors.black,
  }),
  textBiayaAdmin: {
    color: colors.red1,
  },
  textTotalBayar: {
    color: colors.green1,
  },
  sectionContainer: {
    backgroundColor: colors.grey2,
    padding: 10,
    alignItems: 'center',
    marginVertical: 10,
  },
  titleDetail: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[18],
  },
  sectionText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[18],
  },
  section2Container: {
    width: wp(100),
    backgroundColor: colors.black,
    padding: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});
