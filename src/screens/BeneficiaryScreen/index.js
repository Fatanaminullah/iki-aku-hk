import React, {useEffect, useState} from 'react';
import {
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {AddUserIcon, ContactImage, SearchIcon2, SendIcon} from '../../assets';
import {
  ContactItem,
  Container,
  Content,
  Gap,
  Header,
  ImageInfo,
  Input,
  SvgWrapper,
} from '../../components';
import {
  getListBeneficiary,
  getListRecentBeneficiary,
  setDetailBeneficiary,
} from '../../redux/store/actions/beneficiary';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {setTransferData} from '../../redux/store/actions/transaction';
import {colors, fonts, hp, wp} from '../../utilities';

const BeneficiaryScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const beneficiary = useSelector((state) => state.beneficiary.beneficiary);
  const recentBeneficiary = useSelector(
    (state) => state.beneficiary.recentBeneficiary,
  );
  const {visible} = useSelector((state) => state.loading);
  const [refreshing, setRefreshing] = useState(false);
  const [listBeneficiary, setListBeneficiary] = useState(beneficiary);
  const [searchValue, setSearchValue] = useState('');
  const isFromTransferScreen = route?.params?.screen === 'TransferScreen';
  useEffect(() => {
    const subscribe = navigation.addListener('focus', () => {
      if (navigation.isFocused()) {
        getList(true);
      }
    });
    return subscribe;
  }, [dispatch, navigation]);
  const getList = (loading) => {
    !loading && setRefreshing(true);
    dispatch(getListBeneficiary(loading && showLoading, null))
      .then((res) => {
        dispatch(
          getListRecentBeneficiary(null, loading && dismissLoading),
        ).finally(() => {
          !loading && setRefreshing(false);
          setListBeneficiary(res);
        });
      })
      .catch(() => {
        !loading && setRefreshing(false);
        dispatch(dismissLoading());
      });
  };
  const renderListItem = ({item, index}) => {
    return (
      <ContactItem
        iconColor={colors[item?.fullName[0].toLowerCase()]}
        key={`${item?.fullName}-${index}`}
        additionalIcon={
          isFromTransferScreen ? (
            <SvgWrapper component={<SendIcon />} width={wp(6)} height={wp(6)} />
          ) : null
        }
        text={item?.fullName}
        phoneNumber={item?.bankName}
        onPress={() => {
          onSelectItem(item, isFromTransferScreen);
        }}
      />
    );
  };
  const onSelectItem = (recipient, isFromTransferScreen) => {
    if (isFromTransferScreen) {
      const data = {
        ...route.params.data,
        recipient,
      };
      dispatch(setTransferData(data)).then((res) => {
        navigation.navigate('TransferConfirmationScreen');
      });
    } else {
      dispatch(setDetailBeneficiary(recipient, beneficiary)).then((res) => {
        navigation.navigate('DetailBeneficiaryScreen');
      });
    }
  };
  const onSearch = (value) => {
    setSearchValue(value);
    if (value) {
      const arr = beneficiary.filter(
        (item) =>
          item.fullName.toLowerCase().includes(value.toLowerCase()) ||
          item.bankName.toLowerCase().includes(value.toLowerCase()),
      );
      setListBeneficiary(arr);
    } else {
      setListBeneficiary(beneficiary);
    }
  };
  return (
    <Container>
      {isFromTransferScreen && (
        <Header title={I18n.t('beneficiaryScreen.title')} />
      )}
      <View style={styles.inputContainer}>
        <Input
          placeholderTextColor={colors.grey1}
          onChange={onSearch}
          value={searchValue}
          placeholder={I18n.t('beneficiaryScreen.search')}
          leftIcon={
            <SvgWrapper
              component={<SearchIcon2 />}
              width={wp(6)}
              height={wp(6)}
            />
          }
          containerStyle={{
            backgroundColor: colors.grey3,
            width: wp(80),
            borderColor: colors.grey3,
          }}
          rounded={true}
        />
        <Gap width={10} />
        <TouchableOpacity
          onPress={() => {
            // if (userDetails?.status) {
            navigation.navigate('BeneficiaryFormScreen', {
              isFromTransferScreen,
              data: route?.params?.data,
            });
            // } else {
            //   dispatch(
            //     showAlertError('', I18n.t('beneficiaryScreen.messageError')),
            //   );
            // }
          }}>
          <SvgWrapper
            component={<AddUserIcon />}
            width={wp(10)}
            height={hp(5)}
          />
        </TouchableOpacity>
      </View>
      <Content
        noPadding
        refreshControl={
          <RefreshControl
            onRefresh={() => getList(false)}
            refreshing={refreshing}
          />
        }>
        {recentBeneficiary.length && !searchValue ? (
          <View>
            <View style={styles.sectionContainer}>
              <Text style={{fontFamily: fonts.primary[600]}}>
                {I18n.t('beneficiaryScreen.recent')}
              </Text>
            </View>
            <View>
              <FlatList
                data={recentBeneficiary}
                refreshControl={<RefreshControl onRefresh={getList} />}
                keyboardShouldPersistTaps="handled"
                renderItem={renderListItem}
                keyExtractor={(item) => item.id}
              />
            </View>
          </View>
        ) : null}
        <View style={styles.sectionContainer}>
          <Text style={{fontFamily: fonts.primary[600]}}>A - Z</Text>
        </View>
        <View>
          <FlatList
            data={listBeneficiary}
            keyboardShouldPersistTaps="handled"
            refreshControl={<RefreshControl onRefresh={getList} />}
            renderItem={renderListItem}
            keyExtractor={(item) => item.id}
            ListEmptyComponent={
              !visible && (
                <View>
                  <ImageInfo
                    text={I18n.t('beneficiaryScreen.noBeneficiary')}
                    source={ContactImage}
                    hpSizeImage={22}
                    hpSizeText={1.8}
                  />
                </View>
              )
            }
          />
        </View>
      </Content>
    </Container>
  );
};

export default BeneficiaryScreen;

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: 'row',
    padding: hp(2),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: hp(2),
  },
  sectionContainer: {
    backgroundColor: colors.grey3,
    justifyContent: 'center',
    padding: hp(1),
  },
});
