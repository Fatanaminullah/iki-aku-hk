import React from 'react';
import I18n from '../../utilities/i18n';
import {useDispatch} from 'react-redux';
import {Keyboard, StyleSheet, Text, View} from 'react-native';
import {IkiAkuIcon5} from '../../assets';
import {
  CardList,
  Container,
  Content,
  Gap,
  Header,
  Input,
  SvgWrapper,
} from '../../components';
import {
  fonts,
  LIST_PLN_PREPAID_PRODUCT,
  useForm,
  hp,
  PPOB,
  wp,
  colors,
} from '../../utilities';
import {transactionActions} from '../../redux/store/actions';

const {checkBill} = transactionActions;

const PlnPrepaidScreen = ({navigation, route}) => {
  const [form, setForm] = useForm({customerId: ''});
  const dispatch = useDispatch();

  const onChangeText = (value) => {
    setForm('customerId', value);
  };

  const onSelect = (data) => {
    Keyboard.dismiss();
    dispatch(checkBill(PPOB.PLN_PREPAID)).then((res) => {
      navigation.navigate('TransactionSummaryScreen', {
        title: I18n.t('plnPrepaidScreen.title'),
        productName: PPOB.PLN_PREPAID,
      });
    });
  };
  return (
    <Container>
      <Header title={I18n.t('plnPrepaidScreen.title')} />
      <Content noPadding>
        <View style={styles.logoContainer}>
          <SvgWrapper
            component={<IkiAkuIcon5 />}
            width={wp(20)}
            height={wp(20)}
          />
        </View>
        <View style={styles.topContainer}>
          <Input
            rounded
            placeholder={I18n.t('plnPrepaidScreen.customerId')}
            value={form.phoneNumber}
            keyboardType="numeric"
            onChangeText={onChangeText}
          />
          <Gap height={hp(2)} />
          {form.customerId.length > 9 && (
            <Text style={styles.chooseNominalText}>
              {I18n.t('plnPrepaidScreen.chooseNominalInstruction')}
            </Text>
          )}
        </View>
        {form.customerId.length > 9 && (
          <CardList
            list={LIST_PLN_PREPAID_PRODUCT}
            itemWidth={`${96 / 2}%`}
            numColumns={2}
            onPress={(item) => onSelect(item)}
            containerStyle={{backgroundColor: colors.grey3}}
          />
        )}
      </Content>
    </Container>
  );
};

export default PlnPrepaidScreen;

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
  topContainer: {
    padding: 20,
  },
  chooseNominalText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
  },
  logoContainer: {
    marginLeft: 20,
  },
});
