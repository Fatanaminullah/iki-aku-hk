import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utilities';
import Input from '../Input';

const InputMoneyTransfer = ({
  icon,
  text,
  textStyle,
  containerStyle,
  value,
  defaultValue = '0',
  onChange,
}) => {
  return (
    <View style={[styles.container, containerStyle]}>
      <View style={styles.inputContainer}>
        <Input
          type="money"
          onChange={onChange}
          value={value}
          keyboardType="numeric"
          defaultValue={defaultValue}
          inputStyle={styles.input}
          showPrefix={false}
        />
      </View>
      <View style={styles.iconContainer}>
        {icon}
        <Text style={[styles.text, textStyle]}>{text}</Text>
      </View>
    </View>
  );
};

export default InputMoneyTransfer;

const styles = StyleSheet.create({
  container: {
    height: 125,
    backgroundColor: colors.grey2,
  },
  inputContainer: {
    padding: 10,
    flex: 1,
    justifyContent: 'flex-end',
  },
  input: {
    fontSize: fonts.size[28],
    fontFamily: fonts.primary.bold,
    height: '100%',
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: colors.grey5,
    paddingVertical: 5,
    paddingHorizontal: 10,
  },
  text: {
    fontSize: fonts.size[20],
    fontFamily: fonts.primary.bold,
  },
});
