import React, {useEffect} from 'react';
import I18n from 'react-native-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {IkiAkuIcon1} from '../../assets';
import {
  Button,
  Container,
  Content,
  Field,
  Form,
  Gap,
  Header,
  InputPin,
  SvgWrapper,
} from '../../components';
import {
  showAlertError,
  showAlertSuccess,
} from '../../redux/store/actions/alert';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {changePin, getUserInfo} from '../../redux/store/actions/profile';
import {hp, validation, wp} from '../../utilities';

const {required} = validation;

const ChangePinScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {arcNumber} = useSelector((state) => state.profile.userDetails);
  useEffect(() => {
    dispatch(getUserInfo());
  }, []);
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        type="danger"
        title={I18n.t('changePinScreen.submit')}
        onPress={handleSubmit}
        rounded
      />
    );
  };

  const onSubmit = (data, setValue, setError) => {
    const request = {
      username: arcNumber,
      currentPin: data.currentPin,
      pin: data.newPin,
      confirmPin: data.newPin,
    };
    dispatch(
      changePin(
        request,
        showLoading,
        dismissLoading,
        showAlertSuccess,
        showAlertError,
      ),
    )
      .then((res) => {
        navigation.navigate('MainApp', {screen: 'Others'});
      })
      .catch((err) => {
        if (err?.response?.status === 404) {
          setError('currentPin', {
            type: 'manual',
            message: err.response?.data?.message,
          });
        }
      });
  };

  return (
    <Container>
      <Header title={I18n.t('changePinScreen.title')} separator={false} />
      <Content>
        <SvgWrapper
          component={<IkiAkuIcon1 />}
          width={wp(25)}
          height={wp(25)}
        />
        <Gap height={hp(4)} />
        <Form onSubmit={onSubmit} submitComponent={renderSubmitComponent}>
          <Field name="currentPin" validation={{required}}>
            <InputPin
              isChangePin
              label={I18n.t('changePinScreen.labelCurrentPin')}
              placeholder={I18n.t('changePinScreen.placeholder')}
              headerTitle={I18n.t('changePinScreen.labelCurrentPin')}
              labelOldPin={I18n.t('changePinScreen.currentPinLabel')}
            />
          </Field>
          <Gap height={hp(3)} />
          <Field name="newPin" validation={{required}}>
            <InputPin
              label={I18n.t('changePinScreen.labelNewPin')}
              placeholder={I18n.t('changePinScreen.placeholder')}
              headerTitle={I18n.t('changePinScreen.labelNewPin')}
              labelPin={I18n.t('changePinScreen.newPinLabel')}
              labelReInputPin={I18n.t('changePinScreen.reInputPinLabel')}
            />
          </Field>
          <Gap height={hp(4)} />
        </Form>
      </Content>
    </Container>
  );
};

export default ChangePinScreen;
