import Moment from 'moment';
import CryptoJS from 'react-native-crypto-js';
import {X_API_KEY} from '../../../../environments/env.json';
import Services from '../../../../services';
import {storeData} from '../../../../utilities/localStorage';

const login = (request, showLoading, dismissLoading, showAlertError) => (
  dispatch,
) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Post('users/login', request)
      .then(async (res) => {
        if (res?.data?.values) {
          const {token, refreshToken} = res.data.values;
          await storeData('token', {token, refreshToken});
          resolve(res?.data?.values);
        } else {
          reject();
        }
      })
      .catch((err) => {
        dispatch(
          showAlertError('', err?.response?.data?.message || err.message),
        );
        dismissLoading && dispatch(dismissLoading());
        reject(err);
      });
  });
};

const checkToken = (request, showLoading, dismissLoading, showAlertError) => (
  dispatch,
) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Post('users/token-validate', request)
      .then(async (res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const forgotPassword = (
  request,
  showLoading,
  dismissLoading,
  showAlertError,
) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch(showLoading());
    Services.Post('users/password-forgot', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res?.data?.values);
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
        if (err?.response?.status !== 404) {
          dispatch(
            showAlertError('', err?.response?.data?.message || err.message),
          );
        }
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const resetPassword = (
  request,
  showLoading,
  dismissLoading,
  showAlertError,
) => (dispatch) => {
  return new Promise((resolve, reject) => {
    dispatch(showLoading());
    Services.Post('users/password-reset', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res?.data?.values);
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
        if (err?.response?.status !== 404) {
          dispatch(
            showAlertError('', err?.response?.data?.message || err.message),
          );
        }
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const logout = (showLoading, dismissLoading, showAlertError) => (dispatch) => {
  const dtime = Moment(new Date()).format('YYYY-MM-DD HH:mm:ss');
  const signature = CryptoJS.MD5(dtime + X_API_KEY).toString();
  return new Promise((resolve, reject) => {
    dispatch(showLoading());
    Services.Post('android/signout', {signature, dtime})
      .then((res) => {
        if (res?.data?.data !== undefined) {
          resolve(res?.data?.data);
        } else {
          dispatch(showAlertError('', 'Terjadi kesalah sistem'));
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dispatch(dismissLoading());
      });
  });
};

const changePassword = (
  request,
  showLoading,
  dismissLoading,
  showAlertSuccess,
  showAlertError,
) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Post('users/password-change', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
          dispatch(
            showAlertSuccess('', res.data.values?.message || res.data.message),
          );
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
        if (err?.response?.status !== 404) {
          dispatch(
            showAlertError('', err?.response?.data?.message || err.message),
          );
        }
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

export {
  login,
  forgotPassword,
  resetPassword,
  logout,
  checkToken,
  changePassword,
};
