import React, {useEffect, useState} from 'react';
import Moment from 'moment';
import {StyleSheet, Text, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Container, CardList, Header, Gap, SvgWrapper} from '../../components';
import {alertActions, loadingActions} from '../../redux/store/actions';
import {colors, fonts, PPOB, hp} from '../../utilities';
import I18n from 'react-native-redux-i18n';
import {IkiAkuIcon5} from '../../assets';
import {transactionActions} from '../../redux/store/actions';

const {checkPrice} = transactionActions;
const {showLoading, dismissLoading} = loadingActions;
const {showAlertError} = alertActions;

const VoucherGamesProductScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const {
    isDirect,
    groupName,
    userData,
    voucherGamesNonDirectTopUpInfoFormData,
    voucherGamesNonDirectProductFormData,
  } = route.params;
  const {voucherGamesNonDirectTopUpInfo} = useSelector(
    (state) => state.transaction,
  );
  const [listFilterProduct] = useState(voucherGamesNonDirectProductFormData);

  const onSelect = (data) => {
    dispatch(checkPrice(PPOB.VOUCHER_GAMES)).then((res) => {
      navigation.navigate('TransactionSummaryScreen', {
        title: I18n.t('voucherGamesScreen.title'),
        productName: PPOB.VOUCHER_GAMES,
      });
    });
  };

  return (
    <Container>
      <Header title={groupName} />
      <View style={styles.logoContainer}>
        <SvgWrapper
          component={<IkiAkuIcon5 />}
          width={hp(10)}
          height={hp(10)}
        />
      </View>
      <View style={styles.content}>
        <View style={styles.topContainer}>
          {isDirect && <Gap height={hp(2)} />}
          {voucherGamesNonDirectTopUpInfoFormData?.length > 0 && (
            <View>
              {voucherGamesNonDirectTopUpInfoFormData.map((item) => {
                return (
                  <Text
                    style={{
                      fontSize: fonts.size[10],
                      marginLeft: hp(2),
                    }}>
                    {item}
                  </Text>
                );
              })}
            </View>
          )}
          {voucherGamesNonDirectTopUpInfoFormData?.length > 0 && (
            <Gap height={hp(2)} />
          )}
        </View>
        <Gap height={hp(5)} />
        <CardList
          ListHeaderComponent={() => {
            return (
              <Text style={styles.title}>
                {I18n.t('voucherGamesScreen.choose')}
              </Text>
            );
          }}
          list={listFilterProduct}
          numColumns={2}
          onPress={(item) => onSelect(item)}
          itemWidth={`${96 / 2}%`}
          containerStyle={{
            backgroundColor: colors.grey3,
            margin: hp(2),
            borderRadius: 10,
            paddingTop: 2,
          }}
        />
      </View>
    </Container>
  );
};

export default VoucherGamesProductScreen;

const styles = StyleSheet.create({
  userDataText: {
    fontSize: fonts.size[15],
    fontFamily: fonts.primary.bold,
  },
  userDataSeparator: {
    fontSize: fonts.size[14],
    marginHorizontal: 10,
  },
  userDataItemText: {
    minWidth: 100,
    fontSize: fonts.size[14],
  },
  title: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: hp(2),
  },
  content: {
    flex: 1,
  },
  topContainer: {
    marginTop: hp(1),
    marginBottom: hp(-4),
    margin: hp(2),
    borderRadius: 10,
    backgroundColor: colors.grey3,
  },
  rowContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  logoContainer: {
    marginLeft: hp(2),
  },
});
