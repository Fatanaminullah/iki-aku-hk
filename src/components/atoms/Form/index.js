import React from 'react';
import {useForm} from 'react-hook-form';
import {Keyboard, View} from 'react-native';

const Form = ({children, submitComponent, onSubmit, defaultValues}) => {
  const {
    errors,
    control,
    handleSubmit,
    setValue,
    setError,
    clearErrors,
  } = useForm({
    mode: 'onChange',
    defaultValues: defaultValues,
  });

  const SubmitComponent = submitComponent;
  const submitForm = (data) => {
    Keyboard.dismiss();
    onSubmit(data, setValue, setError);
  };

  return (
    <View>
      {React.Children.map(children, (child) => {
        return child.props.name
          ? React.cloneElement(child, {
              control,
              errors,
              defaultValues,
              setValue,
              setError,
              clearErrors,
            })
          : child;
      })}
      <SubmitComponent handleSubmit={handleSubmit(submitForm)} />
    </View>
  );
};

export default Form;
