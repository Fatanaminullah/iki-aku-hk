import React from 'react';
import {StyleSheet} from 'react-native';
import I18n from 'react-native-i18n';
import {useDispatch} from 'react-redux';
import {IkiAkuIcon1} from '../../assets';
import {
  Button,
  Container,
  Content,
  Field,
  Form,
  Gap,
  Header,
  InputPin,
  SvgWrapper,
} from '../../components';
import {
  showAlertError,
  showAlertSuccess,
} from '../../redux/store/actions/alert';
import {dismissLoading, showLoading} from '../../redux/store/actions/loading';
import {resetPin} from '../../redux/store/actions/profile';
import {hp, validation} from '../../utilities';

const {required} = validation;

const CreateNewPinScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        type="danger"
        title={I18n.t('createNewPinScreen.buttonSubmit')}
        onPress={handleSubmit}
        style={styles.button}
      />
    );
  };
  const onSubmit = (data) => {
    const request = {
      newPin: data.newPin,
      resetId: route?.params?.forgot_pin_id,
      username: route?.params?.username,
    };
    dispatch(
      resetPin(request, showLoading, dismissLoading, showAlertError),
    ).then(() => {
      dispatch(showAlertSuccess('', I18n.t('createNewPinScreen.success')));
      if (route?.params?.screen) {
        navigation.navigate(route.params.screen);
      } else {
        navigation.navigate('MainApp', {screen: 'Others'});
      }
    });
  };
  return (
    <Container>
      <Header title={I18n.t('createNewPinScreen.title')} separator={false} />
      <SvgWrapper component={<IkiAkuIcon1 />} style={styles.iconStyle} />
      <Gap height={hp(4)} />
      <Content>
        <Form onSubmit={onSubmit} submitComponent={renderSubmitComponent}>
          <Field name="newPin" validation={{required}}>
            <InputPin
              label={I18n.t('createNewPinScreen.inputPin')}
              placeholder={I18n.t('createNewPinScreen.placeholderPin')}
              headerTitle={I18n.t('createNewPinScreen.pinHeader')}
              labelPin={I18n.t('createNewPinScreen.pinLabel')}
              labelReInputPin={I18n.t('createNewPinScreen.reInputPinLabel')}
            />
          </Field>
        </Form>
      </Content>
    </Container>
  );
};

export default CreateNewPinScreen;

const styles = StyleSheet.create({
  button: {
    marginTop: hp(10),
    borderRadius: 40,
  },
  iconStyle: {
    width: hp(15),
    height: hp(15),
    marginLeft: hp(2),
  },
});
