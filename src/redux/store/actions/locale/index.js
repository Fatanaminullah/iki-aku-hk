import Moment from 'moment';
import {setLocale} from 'react-native-redux-i18n';
import {localeTypes} from '../../action-types';
require('moment/min/locales.min');

const {SET_LOCALE} = localeTypes;

const setLanguage = (payload) => ({
  type: SET_LOCALE,
  payload,
});

const changeLanguage = (locale) => (dispatch) => {
  return new Promise((resolve, reject) => {
    Moment.locale(locale);
    dispatch(setLocale(locale));
    dispatch(setLanguage(locale));
    resolve();
  });
};

export {changeLanguage};
