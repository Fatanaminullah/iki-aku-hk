import {Label} from 'native-base';
import React, {useRef} from 'react';
import {StyleSheet, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import SignatureCapture from 'react-native-signature-capture';
import {colors, fonts, hp, wp} from '../../../utilities';
import {Gap} from '../../atoms';
import {Button} from '../../molecules';

const InputSignature = ({label, name, setValue, labelStyle, onChange}) => {
  const signatureRef = useRef();
  const onSaveSignature = (result) => {
    onChange(result);
    setValue(name, result.encoded);
  };
  const onWriteSignature = () => {
    signatureRef.current.saveImage();
  };
  const onResetSignature = () => {
    signatureRef.current.resetImage();
    onChange(null);
    setValue(name, null);
  };
  return (
    <View>
      <View style={styles.labelContainer}>
        {label && <Label style={[styles.label, labelStyle]}>{label}</Label>}
        <Button
          title={I18n.t('signUpScreen.repeat')}
          onPress={() => onResetSignature()}
          style={styles.buttonReset}
          textStyle={styles.textButtonReset}
        />
      </View>
      <Gap height={10} />
      <View style={styles.signatureContainer}>
        <SignatureCapture
          onDragEvent={onWriteSignature}
          onSaveEvent={onSaveSignature}
          ref={signatureRef}
          style={styles.signature}
          showNativeButtons={false}
        />
      </View>
    </View>
  );
};

export default InputSignature;

const styles = StyleSheet.create({
  labelContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttonReset: {
    width: wp(20),
    paddingHorizontal: 5,
    paddingVertical: 5,
  },
  textButtonReset: {
    fontSize: 12,
  },
  signatureContainer: {
    width: '100%',
    height: hp(25),
    borderColor: colors.grey2,
    borderWidth: 1,
  },
  signature: {
    flex: 1,
  },
  label: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
});
