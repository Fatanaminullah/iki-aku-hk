const {STAGING} = require('./src/environments/env.json');

const plugins = () => {
  const defaultPlugins = [];
  if (STAGING !== 'DEVELOPMENT') {
    defaultPlugins.push(['transform-remove-console']);
    return defaultPlugins;
  }
};

module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: plugins(),
};