import {Label} from 'native-base';
import React, {useRef, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {
  ArcBack,
  ArcFront,
  CameraIcon,
  DummyProfileIcon,
  SelfieArc,
} from '../../assets';
import {
  Button,
  CameraWidget,
  Container,
  Content,
  DatePicker,
  Field,
  Form,
  Gap,
  Header,
  Input,
  InputPin,
  PreviewImage,
  RadioGroup,
  SvgWrapper,
} from '../../components';
import {fonts, hp, useForm, validation, wp} from '../../utilities';

const {required, confirmPassword} = validation;

const EditProfileScreenV2 = ({navigation}) => {
  const dispatch = useDispatch();
  const {userDetails} = useSelector((state) => state.profile);
  const [params, setParams] = useState('');
  const [visiblePreview, setVisiblePreview] = useState(false);
  const [previewImage, setPreviewImage] = useState('');
  const [arcPicture, setArcPicture] = useState({});
  const [profilePicture, setProfilePicture] = useState(null);
  const [visibleCamera, setVisibleCamera] = useState(false);
  const cameraRef = useRef();
  const [form, setForm] = useForm({
    password: '',
    confirmPassword: '',
  });
  const defaultValues = userDetails;
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        type="danger"
        title={I18n.t('editProfileScreen.done')}
        onPress={handleSubmit}
        style={styles.button}
      />
    );
  };

  const onCapture = (e, param) => {
    if (param === 'profilePicture') {
      setProfilePicture(e);
    } else {
      setArcPicture({
        ...arcPicture,
        [param]: e,
      });
    }
    setParams('');
    setVisibleCamera(false);
  };
  const onClickUpdate = () => {
    setVisibleCamera(true);
    setVisiblePreview(false);
  };

  const onSubmit = (data) => {
    console.log('submit data: ', data);
  };

  return (
    <Container>
      <Header title={I18n.t('editProfileScreen.title')} />
      <CameraWidget
        setVisible={setVisibleCamera}
        visibility={visibleCamera}
        cameraRef={cameraRef}
        onCapture={onCapture}
        isCaptureIdCard={['arcFront', 'arcBack'].includes(params)}
        isCaptureSelfie={['selfieArc'].includes(params)}
        showButtonRotate={!['arcFront', 'arcBack'].includes(params)}
        params={params}
      />
      <PreviewImage
        visible={visiblePreview}
        sourceImg={previewImage}
        setVisible={setVisiblePreview}
        showButtonUpdate={true}
        onClickUpdate={onClickUpdate}
      />
      <Content>
        <Text style={styles.profilePictureText}>
          {I18n.t('editProfileScreen.profilePicture')}
        </Text>
        <Gap height={hp(2)} />
        <TouchableOpacity
          onPress={() => {
            setVisibleCamera(true);
            setParams('profilePicture');
          }}
          style={[styles.buttonCamera, {alignSelf: 'center'}]}>
          {profilePicture ? (
            <TouchableOpacity
              onPress={() => {
                setVisiblePreview(true);
                setPreviewImage(profilePicture);
                setParams('profilePicture');
              }}>
              <Image
                source={{
                  uri: `data:image/png;base64,${profilePicture}`,
                }}
                style={{width: wp(25), height: wp(25), borderRadius: wp(12.5)}}
              />
            </TouchableOpacity>
          ) : (
            <SvgWrapper
              component={<DummyProfileIcon />}
              style={styles.profileImg}
            />
          )}
          <View style={styles.iconCamera2}>
            <Button
              icon={
                <SvgWrapper
                  component={<CameraIcon />}
                  width={wp(8)}
                  height={hp(4)}
                />
              }
              rounded={true}
              size={30}
              title={null}
              onPress={
                profilePicture
                  ? () => {
                      setVisibleCamera(true);
                      setParams('arcFront');
                    }
                  : () => {}
              }
            />
          </View>
        </TouchableOpacity>
        <Gap height={hp(3)} />
        <Form
          defaultValues={defaultValues}
          onSubmit={onSubmit}
          submitComponent={renderSubmitComponent}>
          <Gap height={hp(2)} />
          <Field name="arcExpiredDate" validation={{required}}>
            <DatePicker
              format="DD-MM-YYYY"
              label={I18n.t('editProfileScreen.arcExpiredDate')}
              placeholder={I18n.t('editProfileScreen.arcExpiredDate')}
            />
          </Field>
          <Gap height={hp(2)} />
          <Field
            name="arcDocuments"
            value={arcPicture}
            setValue={setArcPicture}>
            <View>
              <Label style={styles.labelFieldArcPhoto}>
                {I18n.t('editProfileScreen.arcPhoto')}
              </Label>
              <Gap height={hp(3)} />
              <View style={styles.fieldArcPhoto}>
                <View>
                  <TouchableOpacity
                    onPress={() => {
                      setVisibleCamera(true);
                      setParams('arcFront');
                    }}
                    style={styles.buttonCamera}>
                    {arcPicture?.arcFront ? (
                      <TouchableOpacity
                        onPress={() => {
                          setVisiblePreview(true);
                          setPreviewImage(arcPicture?.arcFront);
                          setParams('arcFront');
                        }}>
                        <Image
                          source={{
                            uri: `data:image/png;base64,${arcPicture?.arcFront}`,
                          }}
                          style={{width: wp(20), height: wp(20)}}
                        />
                      </TouchableOpacity>
                    ) : (
                      <SvgWrapper
                        style={styles.logo}
                        component={<ArcFront />}
                      />
                    )}
                    <View style={styles.iconCamera}>
                      <Button
                        icon={
                          <SvgWrapper
                            component={<CameraIcon />}
                            width={hp(4)}
                            height={hp(4)}
                          />
                        }
                        rounded={true}
                        size={30}
                        title={null}
                        onPress={
                          arcPicture?.arcFront
                            ? () => {
                                setVisibleCamera(true);
                                setParams('arcFront');
                              }
                            : () => {}
                        }
                      />
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.textArc}>
                    {I18n.t('signUpScreen.inputFrontArc')}
                  </Text>
                </View>
                <View>
                  <TouchableOpacity
                    style={styles.buttonCamera}
                    onPress={() => {
                      setVisibleCamera(true);
                      setParams('arcBack');
                    }}>
                    {arcPicture?.arcBack ? (
                      <TouchableOpacity
                        onPress={() => {
                          setVisiblePreview(true);
                          setPreviewImage(arcPicture?.arcBack);
                          setParams('arcBack');
                        }}>
                        <Image
                          source={{
                            uri: `data:image/png;base64,${arcPicture?.arcBack}`,
                          }}
                          style={{width: wp(20), height: wp(20)}}
                        />
                      </TouchableOpacity>
                    ) : (
                      <SvgWrapper style={styles.logo} component={<ArcBack />} />
                    )}
                    <View style={styles.iconCamera}>
                      <Button
                        icon={
                          <SvgWrapper
                            component={<CameraIcon />}
                            width={wp(8)}
                            height={hp(4)}
                          />
                        }
                        rounded={true}
                        size={30}
                        title={null}
                        onPress={
                          arcPicture?.arcBack
                            ? () => {
                                setVisibleCamera(true);
                                setParams('arcBack');
                              }
                            : () => {}
                        }
                      />
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.textArc}>
                    {I18n.t('signUpScreen.inputBackArc')}
                  </Text>
                </View>
                <View>
                  <TouchableOpacity
                    style={styles.buttonCamera}
                    onPress={() => {
                      setVisibleCamera(true);
                      setParams('selfieArc');
                    }}>
                    {arcPicture?.selfieArc ? (
                      <TouchableOpacity
                        onPress={() => {
                          setVisiblePreview(true);
                          setPreviewImage(arcPicture?.selfieArc);
                          setParams('selfieArc');
                        }}>
                        <Image
                          source={{
                            uri: `data:image/png;base64,${arcPicture?.selfieArc}`,
                          }}
                          style={{width: wp(20), height: wp(20)}}
                        />
                      </TouchableOpacity>
                    ) : (
                      <SvgWrapper
                        style={styles.logo}
                        component={<SelfieArc />}
                      />
                    )}
                    <View style={styles.iconCamera}>
                      <Button
                        icon={
                          <SvgWrapper
                            component={<CameraIcon />}
                            width={wp(8)}
                            height={hp(4)}
                          />
                        }
                        rounded={true}
                        size={30}
                        title={null}
                        onPress={
                          arcPicture?.selfieArc
                            ? () => {
                                setVisibleCamera(true);
                                setParams('selfieArc');
                              }
                            : () => {}
                        }
                      />
                    </View>
                  </TouchableOpacity>
                  <Text style={styles.textArc}>
                    {I18n.t('signUpScreen.inputSelfie')}
                  </Text>
                </View>
              </View>
            </View>
          </Field>
          <Gap height={hp(2)} />
          <Field name="gender" validation={{required}}>
            <RadioGroup
              label={I18n.t('editProfileScreen.gender')}
              listItem={[
                {label: I18n.t('editProfileScreen.man'), value: 'man'},
                {label: I18n.t('editProfileScreen.woman'), value: 'woman'},
              ]}
              direction="row"
            />
          </Field>
          <Gap height={hp(2)} />
          <Field name="addressInTaiwan" validation={{required}}>
            <Input
              label={I18n.t('editProfileScreen.addressInTaiwan')}
              placeholder={I18n.t('editProfileScreen.addressInTaiwan')}
            />
          </Field>
          <Gap height={hp(2)} />
          <Field name="password" validation={{required}}>
            <Input
              secureTextEntry
              label={I18n.t('editProfileScreen.password')}
              placeholder={I18n.t('editProfileScreen.password')}
              onChangeText={(val) => setForm('password', val)}
            />
          </Field>
          <Gap height={hp(2)} />
          <Field
            name="confirmPassword"
            validation={{
              required,
              validate: (val) => confirmPassword(val, form.password),
            }}>
            <Input
              secureTextEntry
              label={I18n.t('editProfileScreen.confirmPassword')}
              placeholder={I18n.t('editProfileScreen.confirmPassword')}
              onChangeText={(val) => setForm('confirmPassword', val)}
            />
          </Field>
          <Gap height={hp(2)} />
          <Field
            name="pin"
            validation={{
              required,
            }}>
            <InputPin
              label={I18n.t('editProfileScreen.pin')}
              placeholder={I18n.t('editProfileScreen.pin')}
              headerTitle={I18n.t('editProfileScreen.changeTransactionPin')}
              labelPin={I18n.t('editProfileScreen.newPinLabel')}
              labelReInputPin={I18n.t('editProfileScreen.repeatNewPinLabel')}
              labelOldPin={I18n.t('editProfileScreen.oldPinLabel')}
              isChangePin
              validateOldPin={(e) => {
                return new Promise((resolve, reject) => {
                  setTimeout(() => {
                    resolve();
                  }, 1000);
                });
              }}
            />
          </Field>
          <Gap height={hp(4)} />
        </Form>
      </Content>
    </Container>
  );
};

export default EditProfileScreenV2;

const styles = StyleSheet.create({
  profilePictureText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    alignSelf: 'center',
  },
  profileImg: {
    alignSelf: 'center',
    width: hp(10),
    height: wp(20),
  },
  buttonCamera: {
    marginVertical: 10,
    marginHorizontal: 15,
    width: wp(20),
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  iconCamera: {
    marginLeft: wp(-9),
    marginBottom: hp(-1.5),
  },
  iconCamera2: {
    marginLeft: hp(-5),
    marginBottom: hp(0.1),
  },
  fieldArcPhoto: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  textArc: {
    textAlign: 'center',
    fontSize: fonts.size[12],
    fontFamily: fonts.primary[700],
    marginTop: hp(2),
  },
  labelFieldArcPhoto: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
  logo: {
    alignSelf: 'center',
    width: hp(10),
    height: hp(10),
  },
});
