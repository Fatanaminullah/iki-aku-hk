import React, {useEffect, useState} from 'react';
import {StyleSheet, Text, View, ScrollView} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useDispatch, useSelector} from 'react-redux';
import {FilterIcon, SearchIcon2} from '../../assets/icons';
import {
  Button,
  Container,
  DatePicker,
  Gap,
  Header,
  HistoryTrxWidget,
  Input,
  Modals,
  Picker,
  SvgWrapper,
} from '../../components';
import Moment from 'moment';
import {showAlertError} from '../../redux/store/actions/alert';
import {
  getTransactionHistoryList,
  setDetailTransaction,
} from '../../redux/store/actions/transaction';
import {
  colors,
  fonts,
  hp,
  trxHistorySort,
  trxHistoryStatusFilter,
  wp,
} from '../../utilities';

const TransactionHistoryScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {trxHistoryList} = useSelector((state) => state.transaction);
  const [loading, setLoading] = useState(false);
  const [filteredList, setFilteredList] = useState(trxHistoryList);
  const [modalShow, setModalShow] = useState(false);
  const [searchInput, setSearchInput] = useState('');
  const [dateFrom, setDateFrom] = useState(
    Moment(new Date()).subtract(30, 'days'),
  );
  const [dateTo, setDateTo] = useState(Moment(new Date()).format('YYYY-MM-DD'));
  const [sortBy, setSortBy] = useState(trxHistorySort[0]?.value);
  const [status, setStatus] = useState(trxHistoryStatusFilter[0]?.value);
  useEffect(() => {
    getHistoryTransaction();
    const unsubscribe = navigation.addListener('blur', () => {
      resetFilter();
    });
    return () => {
      unsubscribe();
    };
  }, []);
  const onRefresh = () => {
    getHistoryTransaction();
  };
  const onSearch = (e) => {
    setSearchInput(e);
    if (!e) {
      return setFilteredList(trxHistoryList);
    }
    const value = e.toLowerCase();
    const filteredData = trxHistoryList.filter((item) => {
      return (
        item.status.toLowerCase().includes(value) ||
        item.type.toLowerCase().includes(value) ||
        item.trxDate.toLowerCase().includes(value) ||
        item.amount.toLowerCase().includes(value) ||
        item.beneficiary.toLowerCase().includes(value) ||
        item.bankNo.toLowerCase().includes(value)
      );
    });
    return setFilteredList(filteredData);
  };

  const onSubmitFilter = () => {
    const request = {
      // filter: `${Moment(dateFrom).format('YYYY-MM-DD')}|${Moment(dateTo).format(
      //   'YYYY-MM-DD',
      // )}`,
      // status: status,
      // sort: sortBy,
      filter: {
        startDate: Moment(dateFrom).format('YYYY-MM-DD'),
        endDate: Moment(dateTo).format('YYYY-MM-DD'),
      },
    };
    getHistoryTransaction(request);
    setModalShow(false);
  };

  const resetFilter = () => {
    setDateFrom(Moment(new Date()).subtract(30, 'days'));
    setDateTo(Moment(new Date()).format('YYYY-MM-DD'));
    setSortBy(trxHistorySort[0]?.value);
    setStatus(trxHistoryStatusFilter[0]?.value);
    getHistoryTransaction();
  };

  const getHistoryTransaction = (request) => {
    let req = {};
    if (!request) {
      req = {
        filter: {
          startDate: '',
          endDate: '',
        },
      };
    } else {
      req = {...request};
    }
    setLoading(true);
    dispatch(getTransactionHistoryList(req, null, null, showAlertError))
      .then((res) => {
        setFilteredList(res);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  return (
    <Container>
      <Header title={I18n.t('transactionHistoryScreen.title')} />
      <View style={styles.searchBarContainer}>
        <Input
          rounded={true}
          icon={
            <SvgWrapper
              component={<SearchIcon2 />}
              width={wp(6)}
              height={wp(6)}
            />
          }
          iconPosition={'left'}
          placeholder={I18n.t('transactionHistoryScreen.search')}
          containerStyle={styles.searchBarInputContainer}
          value={searchInput}
          onChangeText={(val) => onSearch(val)}
        />
      </View>
      <HistoryTrxWidget
        navigation={navigation}
        list={filteredList}
        loading={loading}
        setDetailTransaction={setDetailTransaction}
        onRefresh={onRefresh}
      />
      <Modals
        type="dialogWithTitle"
        title={I18n.t('transactionHistoryScreen.filter')}
        isVisible={modalShow}
        onBackButtonPress={() => setModalShow(false)}
        onBackdropPress={() => setModalShow(false)}
        popupStyle={styles.filterPopUp}>
        <ScrollView>
          <View style={styles.filterDateContainer}>
            <Text style={styles.filterItemLabel}>
              {I18n.t('transactionHistoryScreen.date')}
            </Text>
            <Gap height={hp(2)} />
            <View style={styles.filterDateRowContainer}>
              <View style={styles.filterDateItem}>
                <Text style={styles.filterDateLabel}>
                  {I18n.t('transactionHistoryScreen.from')}
                </Text>
                <DatePicker
                  androidMode="spinner"
                  showIcon={false}
                  style={styles.filterDateValue}
                  value={dateFrom}
                  format="YYYY-MM-DD"
                  onChange={(e) => setDateFrom(e)}
                  containerStyle={styles.filterDateComponentContainer}
                />
              </View>
              <Gap width={wp(8)} />
              <View style={styles.filterDateItem}>
                <Text style={styles.filterDateLabel}>
                  {I18n.t('transactionHistoryScreen.to')}
                </Text>
                <DatePicker
                  androidMode="spinner"
                  showIcon={false}
                  value={dateTo}
                  format="YYYY-MM-DD"
                  onChange={(e) => setDateTo(e)}
                  style={styles.filterDateValue}
                  containerStyle={styles.filterDateComponentContainer}
                />
              </View>
            </View>
          </View>
          <Gap height={hp(2)} />
          <Text style={styles.filterItemLabel}>
            {I18n.t('transactionHistoryScreen.sort')}
          </Text>
          <Picker
            dataSource={trxHistorySort}
            value={sortBy}
            onChange={(e) => setSortBy(e)}
          />
          <Gap height={hp(2)} />
          <Text style={styles.filterItemLabel}>
            {I18n.t('transactionHistoryScreen.status')}
          </Text>
          <Picker
            dataSource={trxHistoryStatusFilter}
            value={status}
            onChange={(e) => setStatus(e)}
          />
          <Gap height={hp(10)} />
        </ScrollView>
        <View style={styles.filterActionContainer}>
          <Button
            title={I18n.t('transactionHistoryScreen.cancel')}
            rounded
            type="primary"
            onPress={() => setModalShow(false)}
            style={styles.filterActionButton}
          />
          <Gap width={wp(5)} />
          <Button
            title={I18n.t('transactionHistoryScreen.ok')}
            rounded
            onPress={() => onSubmitFilter()}
            type="danger"
            style={styles.filterActionButton}
          />
        </View>
      </Modals>
      <View style={styles.filterButtonContainer}>
        <Button
          title={I18n.t('transactionHistoryScreen.filter')}
          type="primary"
          icon={
            <SvgWrapper
              component={<FilterIcon />}
              width={hp(3)}
              height={hp(3)}
            />
          }
          iconPosition="left"
          iconGap={10}
          style={styles.filterButton}
          onPress={() => setModalShow(true)}
        />
      </View>
    </Container>
  );
};

const styles = StyleSheet.create({
  searchBarContainer: {
    padding: 20,
    backgroundColor: colors.grey3,
  },
  searchBarInputContainer: {
    backgroundColor: colors.white,
  },
  filterButtonContainer: {
    backgroundColor: colors.grey3,
    alignItems: 'center',
    paddingVertical: hp(1.5),
  },
  filterButton: {
    justifyContent: 'center',
    borderRadius: 50,
    height: hp(5),
    width: hp(43),
  },
  filterPopUp: {
    height: hp(55),
  },
  filterItemLabel: {
    fontFamily: fonts.primary[600],
  },
  filterDateContainer: {
    justifyContent: 'space-between',
  },
  filterDateComponentContainer: {
    paddingHorizontal: 0,
    maxWidth: wp(22),
    height: hp(3.5),
  },
  filterDateRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 10,
  },
  filterDateItem: {
    flex: 1,
    flexDirection: 'row',
  },
  filterDateLabel: {
    marginRight: 10,
    fontFamily: fonts.primary[600],
  },
  filterDateValue: {
    flex: 0,
    borderWidth: 0,
  },
  filterActionContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between',
  },
  filterActionButton: {
    flex: 1,
    paddingHorizontal: 0,
  },
});

export default TransactionHistoryScreen;
