import React from 'react';
import I18n from 'react-native-redux-i18n';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {
  FlagIndoRectanglePng,
  FlagUkRectangle,
  IkiAkuIcon1,
} from '../../assets/icons';
import {
  Walkthrough1,
  Walkthrough2,
  Walkthrough3,
} from '../../assets/illustrations';
import {Container, SliderBox, Button, SvgWrapper} from '../../components';
import {localeActions} from '../../redux/store/actions';
import {colors, wp, hp} from '../../utilities';

const {changeLanguage} = localeActions;

const WalkthroughScreen = ({navigation}) => {
  const dispatch = useDispatch();
  const {language} = useSelector((state) => state.locale);
  return (
    <Container>
      <View style={styles.headerContainer}>
        <SvgWrapper
          component={<IkiAkuIcon1 />}
          width={wp(25)}
          height={wp(25)}
        />
        <View style={styles.alignItemsCenter}>
          <Text>{`${I18n.t('walkthroughScreen.languageLabel')}`}</Text>
          <View style={styles.buttonLanguageContainer}>
            <TouchableOpacity onPress={() => dispatch(changeLanguage('id'))}>
              <Image
                style={styles.imgIndo(language)}
                source={FlagIndoRectanglePng}
              />
            </TouchableOpacity>
            <TouchableOpacity onPress={() => dispatch(changeLanguage('en'))}>
              <Image style={styles.imgUk(language)} source={FlagUkRectangle} />
            </TouchableOpacity>
          </View>
        </View>
      </View>
      <View style={styles.imgSlide}>
        <SliderBox
          component={[
            {
              image: Walkthrough1,
              title: `${I18n.t('walkthroughScreen.slide1Title')}`,
              subtitle: `${I18n.t('walkthroughScreen.slide1Subtitle')}`,
            },
            {
              image: Walkthrough2,
              title: `${I18n.t('walkthroughScreen.slide2Title')}`,
              subtitle: `${I18n.t('walkthroughScreen.slide2Subtitle')}`,
            },
            {
              image: Walkthrough3,
              title: `${I18n.t('walkthroughScreen.slide3Title')}`,
              subtitle: `${I18n.t('walkthroughScreen.slide3Subtitle')}`,
            },
          ]}
          withIndicator={true}
        />
      </View>
      <View style={styles.buttonContainer}>
        <Button
          onPress={() => navigation.navigate('SignInScreen')}
          style={styles.button}
          title={`${I18n.t('walkthroughScreen.signIn')}`}
          type="danger"
        />
        <Button
          style={styles.button}
          onPress={() => navigation.navigate('AgreementScreen')}
          title={`${I18n.t('walkthroughScreen.signUp')}`}
          type="primary"
        />
      </View>
    </Container>
  );
};

export default WalkthroughScreen;

const styles = StyleSheet.create({
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: hp(3),
    marginTop: hp(2),
  },
  buttonLanguageContainer: {
    flexDirection: 'row',
    padding: hp(1),
    marginTop: hp(0.5),
  },
  imgIndo: (language) => ({
    width: wp(10),
    height: hp(4),
    marginRight: hp(1),
    borderWidth: language === 'id' ? 2 : 1,
    borderColor: language === 'id' ? colors.blue1 : colors.grey4,
  }),
  imgUk: (language) => ({
    width: wp(10),
    height: hp(4),
    borderWidth: language === 'en' ? 2 : 1,
    borderColor: language === 'en' ? colors.blue1 : colors.grey4,
  }),
  imgSlide: {
    width: wp(90),
    flex: 1,
    alignSelf: 'center',
  },
  alignItemsCenter: {
    alignItems: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: hp(3),
  },
  button: {
    maxWidth: wp(30),
    paddingVertical: hp(2),
    paddingHorizontal: hp(2),
  },
});
