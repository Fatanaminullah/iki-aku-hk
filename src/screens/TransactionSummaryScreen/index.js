import React, {useState} from 'react';
import I18n from 'react-native-redux-i18n';
import {useSelector, useDispatch} from 'react-redux';
import {StyleSheet, Text} from 'react-native';
import {
  Container,
  Content,
  Header,
  Gap,
  KeyValueItem,
  Button,
  PinDialog,
} from '../../components';
import {fonts, hp} from '../../utilities';
import {transactionActions} from '../../redux/store/actions';

const {checkout} = transactionActions;

const TransactionSummaryScreen = ({navigation, route}) => {
  const [visiblePin, setVisiblePin] = useState(false);
  const {title, productName} = route.params;
  const {trxSummary} = useSelector((state) => state.transaction);
  const dispatch = useDispatch();

  const onFinishedInputPin = (value) => {
    onCheckout();
  };

  const onCheckout = () => {
    dispatch(checkout(productName)).then((res) => {
      navigation.navigate('TransactionDetailsScreen');
    });
  };

  return (
    <Container>
      <PinDialog
        visibility={visiblePin}
        setVisibility={setVisiblePin}
        onFinished={onFinishedInputPin}
        labelForgot={I18n.t('forgotPinScreen.forgotPin')}
      />
      <Header title={title} />
      <Content style={styles.contentContainer}>
        <Gap height={hp(2)} />
        <Text style={styles.infoText}>
          {I18n.t('transactionSummaryScreen.transactionDetails')}
        </Text>
        <Gap height={hp(2)} />
        {trxSummary.transactionDetails.map((item, index) => (
          <KeyValueItem index={index} title={item.title} value={item.value} />
        ))}
        <Gap height={hp(2)} />
        <Text style={styles.infoText}>
          {I18n.t('transactionSummaryScreen.paymentDetails')}
        </Text>
        <Gap height={hp(2)} />
        {trxSummary.paymentDetails.map((item, index) => (
          <KeyValueItem index={index} title={item.title} value={item.value} />
        ))}
        <Gap height={hp(5)} />
        <Button
          title={I18n.t('transactionSummaryScreen.pay')}
          onPress={() => setVisiblePin(true)}
        />
      </Content>
    </Container>
  );
};

export default TransactionSummaryScreen;

const styles = StyleSheet.create({
  contentContainer: {
    paddingHorizontal: 30,
  },
  infoText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
  },
});
