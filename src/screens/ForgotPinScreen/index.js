/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import I18n from 'react-native-i18n';
import {StyleSheet, Text} from 'react-native';
import {useDispatch} from 'react-redux';
import {
  Container,
  Content,
  Header,
  Form,
  Field,
  Input,
  Button,
  Gap,
} from '../../components';
import {fonts, hp, wp, validation} from '../../utilities';
import {
  loadingActions,
  alertActions,
  authenticationActions,
  profileActions,
} from '../../redux/store/actions';

const {showLoading, dismissLoading} = loadingActions;
const {showAlertError} = alertActions;
const {forgotPin} = profileActions;

const {required, arcNumber} = validation;

const ForgotPinScreen = ({navigation, route}) => {
  const dispatch = useDispatch();
  const renderSubmitComponent = ({handleSubmit}) => {
    return (
      <Button
        type="danger"
        title={I18n.t('forgotPinScreen.resetPin')}
        rounded
        onPress={handleSubmit}
      />
    );
  };
  const onSubmit = (data, setValue) => {
    if (data.arcNumber.toUpperCase() !== data.arcNumber) {
      setValue('arcNumber', data.arcNumber.toUpperCase());
    }
    const request = {
      username: data.arcNumber.toUpperCase(),
    };
    dispatch(
      forgotPin(request, showLoading, dismissLoading, showAlertError),
    ).then((res) => {
      navigation.navigate('ForgotPinOtpScreen', {
        screen: route?.params?.screen,
        request: res,
        username: request.username,
      });
    });
  };
  return (
    <Container>
      <Header title={I18n.t('forgotPinScreen.title')} />
      <Content>
        <Text style={styles.infoText}>{I18n.t('forgotPinScreen.info')}</Text>
        <Gap height={hp(4)} />
        <Form onSubmit={onSubmit} submitComponent={renderSubmitComponent}>
          <Field name="arcNumber" validation={{required, pattern: arcNumber}}>
            <Input
              rounded
              placeholder={I18n.t('forgotPinScreen.arcNumber')}
              maxLength={10}
            />
          </Field>
          <Gap height={hp(8)} />
        </Form>
      </Content>
    </Container>
  );
};

export default ForgotPinScreen;

const styles = StyleSheet.create({
  infoText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    textAlign: 'center',
    alignSelf: 'center',
    maxWidth: wp(80),
  },
});
