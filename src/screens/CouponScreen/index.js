import React from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {useSelector} from 'react-redux';
import {CouponMoneyIcon, CouponTimeIcon} from '../../assets';
import I18n from 'react-native-redux-i18n';
import {
  Card,
  Container,
  Content,
  Gap,
  Header,
  ImageInfo,
  SvgWrapper,
} from '../../components';
import {colors, fonts, hp, thousandSeparator, wp} from '../../utilities';
import moment from 'moment';
import Countdown from 'react-countdown';

const CouponScreen = ({navigation, route}) => {
  const listCoupons = useSelector((state) => state.transaction.listCoupons);
  const renderer = ({hours, minutes, seconds, completed, ...rest}) => {
    return (
      <Text
        style={{
          fontFamily: fonts.primary.bold,
          color: colors.red1,
          fontSize: fonts.size[14],
        }}>
        {rest?.formatted?.hours} : {rest?.formatted?.minutes} :{' '}
        {rest?.formatted?.seconds}
      </Text>
    );
  };
  const renderCoupon = ({item, index}) => {
    return (
      <Card
        onPress={() => navigation.navigate('DetailCouponScreen', {item})}
        key={index}
        style={styles.cardContainer}>
        <Image
          source={{uri: `data:image/png;base64,${item?.image}`}}
          style={{width: wp(80), height: hp(20)}}
          resizeMode="contain"
        />
        <View style={styles.caption}>
          <View style={styles.column}>
            <SvgWrapper
              component={<CouponTimeIcon />}
              width={wp(8)}
              height={hp(4)}
            />
            <View style={styles.textContainer}>
              <Text style={styles.textStyle}>
                {I18n.t('couponScreen.useBefore')}
              </Text>
              <Countdown
                key={index}
                daysInHours={true}
                date={moment(item.expired, 'MM-DD-YYYY HH:mm:ss')}
                intervalDelay={1000}
                renderer={renderer}
                autoStart={true}
              />
            </View>
          </View>
          <Gap width={wp(5)} />
          <View style={styles.column}>
            <SvgWrapper
              component={<CouponMoneyIcon />}
              width={wp(8)}
              height={hp(4)}
            />
            <View style={styles.textContainer}>
              <Text style={styles.textStyle}>
                {I18n.t('couponScreen.minimumTransaction')}
              </Text>
              <Text
                style={{
                  fontFamily: fonts.primary.bold,
                  color: colors.red1,
                  fontSize: fonts.size[14],
                }}>
                {I18n.t('couponScreen.amount', {
                  amount: thousandSeparator(item.min),
                })}
              </Text>
            </View>
          </View>
        </View>
      </Card>
    );
  };
  return (
    <Container>
      <Header title={I18n.t('couponScreen.title')} />
      <Content style={styles.page}>
        <FlatList
          data={listCoupons}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="handled"
          ListEmptyComponent={
            <ImageInfo
              text={I18n.t('couponScreen.title')}
              hpSizeImage={22}
              hpSizeText={1.8}
            />
          }
          renderItem={renderCoupon}
          keyExtractor={(item) => item.id}
        />
      </Content>
    </Container>
  );
};

export default CouponScreen;

const styles = StyleSheet.create({
  page: {
    backgroundColor: colors.grey3,
  },
  cardContainer: {
    marginVertical: 10,
    borderWidth: 0,
    borderRadius: 0,
  },
  caption: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp(80),
  },
  column: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: wp(35),
  },
  textContainer: {
    flexWrap: 'wrap',
    marginLeft: hp(1.5),
  },
  textStyle: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.primary,
  },
});
