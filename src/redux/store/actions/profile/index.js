import Contacts from 'react-native-contacts';
import {profileActionTypes} from '../../action-types';
import {
  API_SERVICES,
  compare,
  phoneNumberNormalizer,
  requestContactPermission,
} from '../../../../utilities';
import I18n from 'react-native-redux-i18n';
import Services from '../../../../services';
import SimpleToast from 'react-native-simple-toast';

const {
  SET_LIST_CONTACT,
  SET_USER_DETAILS,
  SET_LIST_OCCUPATION,
} = profileActionTypes;

const setListContact = (payload) => ({
  type: SET_LIST_CONTACT,
  payload,
});

const setUserDetails = (payload) => ({
  type: SET_USER_DETAILS,
  payload,
});

const setListOccupation = (payload) => ({
  type: SET_LIST_OCCUPATION,
  payload,
});

const getListContact = (showAlertError) => (dispatch) => {
  return new Promise((resolve, reject) => {
    requestContactPermission(dispatch, showAlertError).then((res) => {
      if (res.status === 'granted') {
        Contacts.getAll((err, contacts) => {
          if (err) {
            reject(err);
          } else {
            const tempContacts = contacts
              .filter((item) => {
                return item.phoneNumbers.length;
              })
              .sort((a, b) => compare(a, b, 'givenName'));
            const finalContacts = [];
            tempContacts.forEach((item) => {
              if (item.phoneNumbers.length === 1) {
                finalContacts.push({
                  displayName: `${item.givenName} ${item.middleName} ${item.familyName}`,
                  phoneNumber: item.phoneNumbers[0].number,
                  label: item.phoneNumbers[0].number,
                });
              } else {
                item.phoneNumbers.forEach((items) => {
                  finalContacts.push({
                    displayName: `${item.givenName} ${item.middleName} ${item.familyName}`,
                    phoneNumber: items.number,
                    label: items.number,
                  });
                });
              }
            });
            const mostFinalContacts = finalContacts.reduce((acc, current) => {
              const a = acc.find(
                (item) =>
                  phoneNumberNormalizer(item.phoneNumber) ===
                  phoneNumberNormalizer(current.phoneNumber),
              );
              if (!a) {
                return acc.concat([current]);
              } else {
                return acc;
              }
            }, []);
            dispatch(setListContact(mostFinalContacts));
            resolve(mostFinalContacts);
          }
        });
      }
    });
  });
};

const getUserInfo = (showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Get('users/info')
      .then((res) => {
        if (res?.data?.values) {
          const data = res.data.values;
          const result = {
            id: data?.id,
            arcNumber: data?.cardno,
            fullName: data?.nama,
            birthDate: data?.tgllahir,
            addressInTaiwan: data?.alamat,
            phoneNumber: `+${data?.telp}`,
            arcExpiredDate: data?.date_of_expiry,
            gender: data?.gender,
            occupation: data?.pekerjaan,
            email: data?.email,
            arcPhoto: data?.arcPhoto,
            status: data?.status,
          };
          dispatch(setUserDetails(result));
          resolve(result);
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const getListOccupation = (showLoading, dismissLoading) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Get('master/occupancy')
      .then((res) => {
        if (res?.data?.values) {
          const result = res.data.values?.rows.map((item) => ({
            value: item.nama,
            label: item.nama,
          }));
          dispatch(setListOccupation(result));
          resolve(result);
        }
      })
      .catch((err) => {
        reject(err);
        dispatch(setListOccupation([]));
        SimpleToast.show(err?.response?.data?.message || err.message);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const updateProfile = (
  request,
  showLoading,
  dismissLoading,
  showAlertSuccess,
  showAlertError,
) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Put(`users/update/${request.id}`, request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
        } else {
          reject();
        }
      })
      .catch((err) => {
        dispatch(
          showAlertError('', err?.response?.data?.message || err?.message),
        );
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const validateOtp = (request, showLoading, dismissLoading, showAlertError) => (
  dispatch,
) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Post('users/otp', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
        dispatch(
          showAlertError('', err?.response?.data?.message || err.message),
        );
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const registerUser = (request, showLoading, dismissLoading, showAlertError) => (
  dispatch,
) => {
  showLoading && dispatch(showLoading());
  return new Promise((resolve, reject) => {
    Services.Post('users/registration', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
        } else {
          reject();
        }
      })
      .catch((err) => {
        dismissLoading && dispatch(dismissLoading());
        SimpleToast.show(
          err?.response?.data?.message || err.message,
          SimpleToast.SHORT,
        );
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const requestOtp = (
  request,
  showLoading,
  dismissLoading,
  showAlertSuccess,
  showAlertError,
) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Post('users/otp/request', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
          showAlertSuccess &&
            dispatch(
              showAlertSuccess(
                '',
                res.data.values?.message || res.data.message,
              ),
            );
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
        dispatch(
          showAlertError('', err?.response?.data?.message || err.message),
        );
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const changePin = (
  request,
  showLoading,
  dismissLoading,
  showAlertSuccess,
  showAlertError,
) => (dispatch) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Put(`users/${request.username}/pin-change`, request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values);
          dispatch(
            showAlertSuccess('', res.data.values?.message || res.data.message),
          );
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
        if (err?.response?.status !== 404) {
          dispatch(
            showAlertError('', err?.response?.data?.message || err.message),
          );
        }
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const forgotPin = (request, showLoading, dismissLoading, showAlertError) => (
  dispatch,
) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading());
    Services.Post('users/pin-forgot', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res?.data?.values);
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
        dispatch(
          showAlertError('', err?.response?.data?.message || err.message),
        );
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const resetPin = (request, showLoading, dismissLoading, showAlertError) => (
  dispatch,
) => {
  return new Promise((resolve, reject) => {
    dispatch(showLoading());
    Services.Post('users/pin-reset', request)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res?.data?.values);
        } else {
          reject();
        }
      })
      .catch((err) => {
        reject(err);
        if (err?.response?.status !== 404) {
          dispatch(
            showAlertError('', err?.response?.data?.message || err.message),
          );
        }
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

const uploadImage = (request, showLoading, dismissLoading, showAlertError) => (
  dispatch,
) => {
  return new Promise((resolve, reject) => {
    showLoading && dispatch(showLoading);
    Services.Post('/v1/files', request, API_SERVICES.ALI_OSS)
      .then((res) => {
        if (res?.data?.values) {
          resolve(res.data.values.fileurl);
        } else {
          reject(res);
        }
      })
      .catch((err) => {
        dispatch(
          showAlertError('', err?.response?.data?.message || err.message),
        );
        reject(err);
      })
      .finally(() => {
        dismissLoading && dispatch(dismissLoading());
      });
  });
};

export {
  getListContact,
  setListContact,
  setUserDetails,
  getUserInfo,
  getListOccupation,
  updateProfile,
  validateOtp,
  requestOtp,
  changePin,
  forgotPin,
  resetPin,
  registerUser,
  uploadImage,
};
