import {Label} from 'native-base';
import React, {useState} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {colors, fonts, hp} from '../../../utilities';
import PinDialog from '../PinDialog';
import I18n from 'react-native-redux-i18n';

const InputPin = ({
  onFinished,
  setValue,
  name,
  value,
  label,
  labelForgot,
  placeholder,
  headerTitle,
  labelOldPin,
  labelPin,
  labelReInputPin,
  labelForgotPin,
  isChangePin,
  validateOldPin,
  renderField,
  clearErrors,
}) => {
  const [pinVisible, setPinVisible] = useState(false);
  const [pinVisible2, setPinVisible2] = useState(false);
  const [oldPinVisible, setOldPinVisible] = useState(false);
  const [error, setError] = useState(false);
  const [pin, setPin] = useState('');

  const onFinishOldPin = (e) => {
    onFinished && onFinished(e);
    setValue && setValue(name, e);
    clearErrors && clearErrors(name);
  };
  const onFinishFirst = (e) => {
    setPin(e);
    setPinVisible2(true);
  };
  const onFinishSecond = (e) => {
    onFinished && onFinished(e);
    setValue && setValue(name, pin);
    clearErrors && clearErrors(name);
  };
  return (
    <TouchableOpacity
      onPress={() => {
        if (isChangePin) {
          setOldPinVisible(true);
        } else {
          setPinVisible(true);
        }
      }}>
      <PinDialog
        visibility={oldPinVisible}
        setVisibility={setOldPinVisible}
        background={{}}
        onFinished={onFinishOldPin}
        headerTitle={headerTitle}
        label={labelOldPin}
        labelForgot={I18n.t('forgotPinScreen.forgotPin')}
        errorPin={error}
        onPressBackButton={async () => {
          await setOldPinVisible(false);
        }}
      />
      <PinDialog
        visibility={pinVisible}
        setVisibility={setPinVisible}
        background={{}}
        onFinished={onFinishFirst}
        headerTitle={headerTitle}
        label={labelPin}
        onPressBackButton={async () => {
          await setPinVisible(false);
        }}
      />
      <PinDialog
        visibility={pinVisible2}
        setVisibility={setPinVisible2}
        onFinished={onFinishSecond}
        background={{}}
        validation={pin}
        headerTitle={headerTitle}
        label={labelReInputPin}
        onPressBackButton={async () => {
          await setPinVisible2(false);
          setPinVisible(true);
        }}
      />
      {renderField ? (
        renderField
      ) : (
        <View>
          <Label style={styles.label}>{label}</Label>
          <View style={styles.textContainer}>
            <Text style={styles.text(value)}>
              {value ? '......' : placeholder}
            </Text>
          </View>
          <Label style={styles.label2}>{labelForgot}</Label>
        </View>
      )}
    </TouchableOpacity>
  );
};

export default InputPin;

const styles = StyleSheet.create({
  label: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
  label2: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 1,
  },
  textContainer: {
    paddingVertical: 10,
    paddingHorizontal: 22,
    borderBottomColor: colors.grey2,
    borderBottomWidth: 0.8,
  },
  text: (value) => ({
    color: value ? colors.black : colors.grey2,
    fontFamily: value ? fonts.primary.bold : fonts.primary.nomal,
    fontSize: value ? fonts.size[16] : fonts.size[14],
    paddingVertical: value ? hp(1) : hp(1),
    marginLeft: hp(0.1),
  }),
});
