import React from 'react';
import {StatusBar} from 'react-native';

const MyStatusBar = ({backgroundColor}) => {
  return (
    <StatusBar
      backgroundColor={'transparent'}
      translucent
      barStyle="dark-content"
    />
  );
};

export default MyStatusBar;
