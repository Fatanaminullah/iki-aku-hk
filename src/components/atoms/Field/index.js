import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {Controller} from 'react-hook-form';
import {colors, fonts} from '../../../utilities';

const Field = ({
  children,
  name,
  errors,
  control,
  validation,
  setValue,
  setError,
  clearErrors,
}) => {
  return (
    <>
      <Controller
        control={control}
        render={({onChange, onBlur, value}) =>
          React.cloneElement(children, {
            onChange,
            onBlur,
            value,
            error: errors[name],
            setValue,
            setError,
            clearErrors,
            name,
          })
        }
        name={name}
        rules={validation}
        defaultValue=""
      />
      {errors[name] && (
        <Text style={styles.errorMessage}>{errors[name].message}</Text>
      )}
    </>
  );
};

export default Field;

const styles = StyleSheet.create({
  errorMessage: {
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[12],
    marginTop: 2,
    marginLeft: 10,
    color: colors.red1,
  },
});
