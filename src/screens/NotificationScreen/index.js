import React from 'react';
import {FlatList, StyleSheet, Text, View} from 'react-native';
import I18n from 'react-native-redux-i18n';
import {useSelector} from 'react-redux';
import {ClockIcon, DeleteIcon} from '../../assets';
import {
  Container,
  Gap,
  Header,
  HorizontalLine,
  ImageInfo,
  Touchable,
  SvgWrapper,
} from '../../components';
import {colors, fonts, hp, wp} from '../../utilities';

const NotificationScreen = () => {
  const {notificationList} = useSelector((state) => state.notification);
  const renderItem = ({item}) => {
    return (
      <>
        <Touchable style={styles.notificationItemContainer(item.isRead)}>
          <Text style={styles.title}>{item.title}</Text>
          <Gap height={hp(1)} />
          <Text style={styles.body}>{item.body}</Text>
          <Gap height={hp(2)} />
          <View style={styles.createTimeContainer}>
            <SvgWrapper
              component={<ClockIcon />}
              width={wp(4)}
              height={hp(2)}
            />
            <Gap width={wp(3)} />
            <Text style={styles.createTime}>{item.createTime}</Text>
          </View>
        </Touchable>
        <HorizontalLine height={0.5} />
      </>
    );
  };
  return (
    <Container>
      <Header title={I18n.t('notificationScreen.title')} />
      <View style={styles.topContainer}>
        <Touchable style={styles.deleteButtonContainer}>
          <Text style={styles.deleteButtonText}>
            {I18n.t('notificationScreen.deleteAll')}
          </Text>
          <Gap width={wp(3)} />
          <SvgWrapper component={<DeleteIcon />} width={wp(4)} height={hp(2)} />
        </Touchable>
      </View>
      <HorizontalLine height={0.5} />
      <FlatList
        data={notificationList}
        contentContainerStyle={styles.contentContainerStyle(notificationList)}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
        renderItem={renderItem}
        ListEmptyComponent={
          <View style={styles.noDataContainer}>
            <ImageInfo />
          </View>
        }
        keyExtractor={(item) => item.id}
      />
    </Container>
  );
};

export default NotificationScreen;

const styles = StyleSheet.create({
  topContainer: {
    padding: 20,
  },
  deleteButtonContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  deleteButtonText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    color: colors.red2,
  },
  noDataContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentContainerStyle: (list) => ({
    flex: list.length > 0 ? null : 1,
  }),
  notificationItemContainer: (isRead) => ({
    paddingHorizontal: 20,
    paddingVertical: 10,
    backgroundColor: isRead ? colors.white : colors.grey3,
  }),
  title: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
  },
  body: {
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[14],
  },
  createTimeContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  createTime: {
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[14],
    color: colors.grey1,
  },
});
