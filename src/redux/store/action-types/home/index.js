const SET_MENU_LIST = '@home/set-menu-list';
const SET_PROMOTION_BANNERS = '@home/set-promotion-banners';

export {SET_MENU_LIST, SET_PROMOTION_BANNERS};