import {Label, Radio} from 'native-base';
import React, {Fragment} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {colors, fonts} from '../../../utilities';
import Gap from '../Gap';

const RadioGroup = ({
  listItem,
  valueItem,
  onChange,
  onChangeValue,
  direction,
  label,
  name,
  value,
  setValue,
  labelStyle,
}) => {
  return (
    <Fragment>
      <Label style={[styles.label, labelStyle]}>{label}</Label>
      <Gap height={15} />
      <View
        style={
          direction === 'row' ? styles.directionRow : styles.directionColumn
        }>
        {listItem.map((item, index) => (
          <View key={index} style={styles.directionRow}>
            <View>
              <Radio
                color={colors.black}
                selectedColor={colors.black}
                selected={item.value === value}
                onPress={() =>
                  onChangeValue
                    ? onChangeValue(item.value)
                    : onChange(item.value)
                }
              />
            </View>
            <Gap
              width={direction === 'row' ? 5 : 0}
              height={direction === 'column' || !direction ? 10 : 0}
            />
            <View>
              <Text>{item.label}</Text>
            </View>
            <Gap
              width={direction === 'row' ? 10 : 0}
              height={direction === 'column' || !direction ? 10 : 0}
            />
          </View>
        ))}
      </View>
    </Fragment>
  );
};

export default RadioGroup;

const styles = StyleSheet.create({
  directionRow: {
    flexDirection: 'row',
  },
  directionColumn: {
    flexDirection: 'column',
  },
  label: {
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.bold,
    marginBottom: 5,
  },
});
