import {StyleSheet} from 'react-native';
import {colors, fonts, hp, wp} from '../../utilities';

export default StyleSheet.create({
  editButtonContainer: {
    width: wp(40),
    height: hp(6),
    borderRadius: 25,
    backgroundColor: colors.blue2,
    justifyContent: 'center',
    alignSelf: 'flex-end',
  },
  editButtonText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[14],
    paddingHorizontal: 0,
  },
  editModeActiveText: {
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[12],
    color: colors.red1,
  },
  editModeButtonContainer: {
    width: wp(18),
    height: hp(4),
    borderRadius: 25,
    paddingHorizontal: 0,
  },
  editModeButtonText: {
    fontSize: fonts.size[12],
  },
  profilePictureText: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[16],
    alignSelf: 'center',
  },
  profileImg: {
    alignSelf: 'center',
    width: hp(12),
    height: hp(12),
    borderRadius: 100,
  },
  cameraIcon: {
    position: 'absolute',
    bottom: hp(-1),
    right: wp(-2),
  },
  columnContainer: {
    justifyContent: 'center',
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  separator: (color) => ({
    borderBottomColor: color ? color : colors.grey2,
    borderBottomWidth: 1,
    paddingVertical: 8,
  }),
  rowItemText: {
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[12],
    paddingVertical: hp(0.5),
  },
  iconPencilStyle: {
    width: hp(2),
    height: hp(2),
    marginLeft: hp(1),
  },
  obfuscatedContainer: {
    padding: 0,
    textAlign: 'right',
    color: colors.black,
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.normal,
  },
  arcPhotoText: {
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.normal,
  },
  arcBottomPhotoText: {
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.normal,
    marginTop: hp(2),
  },
  arcPhotoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  arcPhotoItemContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  dialogContainer: {
    height: hp(40),
    width: hp(42),
    justifyContent: 'center',
    padding: hp(3),
  },
  profileEmpty: {
    alignSelf: 'center',
    width: hp(12),
    height: hp(12),
    borderColor: colors.grey2,
    padding: 5,
    borderWidth: 1,
    borderRadius: 100,
  },
});
