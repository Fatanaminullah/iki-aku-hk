import React from 'react';
import {
  FlatList,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {SearchIcon} from '../../../assets';
import {Gap} from '../../atoms';
import {ImageInfo} from '../../molecules';
import Input from '../Input';
import {colors, fonts} from '../../../utilities';
import I18n from '../../../utilities/i18n';

const CardList = ({
  list,
  isSearch,
  itemWidth,
  itemAlign,
  onPress,
  renderCard,
  containerStyle,
  onChangeSearch,
  inputSearch,
  onPressSearchIcon,
  onRefresh,
  refreshing,
  ListEmptyComponent = <ImageInfo />,
  numColumns = 1,
  ListHeaderComponent,
}) => {
  const Item = (item, index) => {
    return (
      <TouchableOpacity
        key={index}
        style={styles.itemContainer(itemWidth)}
        onPress={() => onPress(item)}>
        {item.title && (
          <Text style={styles.itemTitle(itemAlign)}>{item.title}</Text>
        )}
        <Text style={styles.itemText(itemAlign)}>{item.value}</Text>
      </TouchableOpacity>
    );
  };
  return (
    <View style={[styles.container, containerStyle]}>
      {isSearch && (
        <Input
          value={inputSearch}
          onChangeText={onChangeSearch}
          placeholder={I18n.t('pdamScreen.search')}
          onPressIcon={onPressSearchIcon}
          icon={<SearchIcon />}
          rounded
          containerStyle={{backgroundColor: colors.white}}
        />
      )}
      <Gap height={20} />
      {numColumns > 1 ? (
        <FlatList
          data={list}
          ListHeaderComponent={ListHeaderComponent}
          keyboardShouldPersistTaps="handled"
          refreshControl={
            onRefresh && (
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            )
          }
          numColumns={numColumns}
          contentContainerStyle={list.length === 0 && styles.listEmptyStyle}
          columnWrapperStyle={styles.innerContainer}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={ListEmptyComponent}
          renderItem={({item, index}) => {
            return renderCard ? renderCard(item, index) : Item(item, index);
          }}
          keyExtractor={(item) => item.id}
        />
      ) : (
        <FlatList
          data={list}
          refreshControl={
            onRefresh && (
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            )
          }
          contentContainerStyle={list.length === 0 && styles.listEmptyStyle}
          showsVerticalScrollIndicator={false}
          ListEmptyComponent={<ImageInfo />}
          renderItem={({item, index}) => {
            return renderCard ? renderCard(item, index) : Item(item, index);
          }}
          keyExtractor={(item) => item.id}
        />
      )}
    </View>
  );
};

export default CardList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    padding: 20,
  },
  listEmptyStyle: {
    flex: 1,
  },
  innerContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  itemContainer: (itemWidth) => ({
    padding: 20,
    width: itemWidth ? itemWidth : '100%',
    justifyContent: 'center',
    backgroundColor: colors.white,
    marginBottom: 15,
    borderRadius: 10,
  }),
  itemTitle: (itemAlign) => ({
    fontSize: fonts.size[14],
    fontFamily: fonts.primary.normal,
    marginBottom: 3,
    textAlign: itemAlign ? itemAlign : 'center',
  }),
  itemText: (itemAlign) => ({
    fontSize: fonts.size[14],
    fontFamily: fonts.primary[700],
    textAlign: itemAlign ? itemAlign : 'center',
  }),
});
