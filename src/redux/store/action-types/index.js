import * as homeActionTypes from './home';
import * as formActionTypes from './form';
import * as loadingActionTypes from './loading';
import * as alertActionTypes from './alert';
import * as profileActionTypes from './profile';
import * as transferActionTypes from './transfer';
import * as transactionTypes from './transaction';
import * as rewardTypes from './reward';
import * as localeTypes from './locale';
import * as notificationTypes from './notification';
import * as beneficiaryTypes from './beneficiary';

export {
  homeActionTypes,
  formActionTypes,
  loadingActionTypes,
  alertActionTypes,
  profileActionTypes,
  transferActionTypes,
  transactionTypes,
  rewardTypes,
  localeTypes,
  notificationTypes,
  beneficiaryTypes,
};
