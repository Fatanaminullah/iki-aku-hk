import React, {useEffect, useState} from 'react';
import {InteractionManager, RefreshControl} from 'react-native';
import SimpleToast from 'react-native-simple-toast';
import {useDispatch, useSelector} from 'react-redux';
import {
  Content,
  Gap,
  HistoryTrxWidget,
  HomeInfo,
  HomeSliderMenu,
  ImageSlider,
} from '../../components';
import {
  alertActions,
  profileActions,
  transactionActions,
} from '../../redux/store/actions';
import {colors, hp} from '../../utilities';

const {getListContact, getUserInfo} = profileActions;
const {getTransactionHistoryList, setDetailTransaction} = transactionActions;
const {showAlertError} = alertActions;

const HomeScreen = ({navigation}) => {
  const [loadingProfile, setLoadingProfile] = useState(false);
  const [loadingHistory, setLoadingHistory] = useState(false);
  const {userDetails} = useSelector((state) => state.profile);
  const {notificationList} = useSelector((state) => state.notification);
  const {promotionBanners} = useSelector((state) => state.home);
  const {trxHistoryList} = useSelector((state) => state.transaction);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getListContact(showAlertError));
    getProfile();
    getHistoryTransaction();
  }, [dispatch]);
  const getProfile = () => {
    setLoadingProfile(true);
    dispatch(getUserInfo())
      .then(() => {
        setLoadingProfile(false);
      })
      .catch((err) => {
        SimpleToast.show(
          err?.response?.data?.message || err?.message,
          SimpleToast.SHORT,
        );
      })
      .finally(() => {
        setLoadingProfile(false);
      });
  };
  const getHistoryTransaction = () => {
    setLoadingHistory(true);
    dispatch(
      getTransactionHistoryList(
        {
          filter: {
            startDate: '',
            endDate: '',
          },
        },
        null,
        null,
        null,
      ),
    ).finally(() => {
      setLoadingHistory(false);
    });
  };
  const onRefresh = () => {
    getProfile();
    getHistoryTransaction();
  };
  return (
    <Content
      noPadding
      style={{backgroundColor: colors.white}}
      refreshControl={
        <RefreshControl onRefresh={onRefresh} refreshing={false} />
      }>
      <HomeInfo
        loadingProfile={loadingProfile}
        navigation={navigation}
        userDetails={userDetails}
        notificationList={notificationList}
      />
      <ImageSlider data={promotionBanners} />
      <Gap height={hp(3.5)} />
      <HomeSliderMenu navigation={navigation} />
      <HistoryTrxWidget
        loading={loadingHistory}
        navigation={navigation}
        list={trxHistoryList}
        showRecent={true}
        setDetailTransaction={setDetailTransaction}
      />
    </Content>
  );
};

export default HomeScreen;
