import {rewardTypes} from '../../action-types';
import Moment from 'moment';
import {rewardVoucher50Percent} from './mockRewardImg';

const {SET_REWARD} = rewardTypes;

const date = Moment(new Date()).add(1, 'h').format('YYYY-MM-DD HH:mm:ss');

const initialState = {
  listRewards: [
    {
      id: 'reward1',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 20%',
      amount: 20,
      min: 100,
      max: 200,
      expired: '10-24-2020 23:59:59',
      image: rewardVoucher50Percent,
    },
    {
      id: 'reward2',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 20%',
      amount: 20,
      min: 100,
      max: 200,
      expired: '10-23-2020 23:59:59',
      image: rewardVoucher50Percent,
    },
    {
      id: 'reward3',
      type: 'percentAdmin',
      title: 'Potongan Biaya Admin 50%',
      amount: 50,
      min: 100,
      max: 200,
      expired: '10-22-2020 23:59:59',
      image: rewardVoucher50Percent,
    },
  ],
};

export default (state = initialState, action) => {
  const {type, payload} = action;
  switch (type) {
    case SET_REWARD:
      return {...state};
    default:
      return state;
  }
};
