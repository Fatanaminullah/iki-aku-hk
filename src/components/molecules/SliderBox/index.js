import ViewPager from '@react-native-community/viewpager';
import React, {useEffect, useRef, useState} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {fonts, colors, GetPreviousState} from '../../../utilities';

const SliderBox = ({
  renderComponent,
  component,
  withIndicator,
  pageContainerStyle,
  indicatorContainerStyle,
  indicatorActiveColor = colors.red2,
  indicatorColor = colors.grey2,
  indicatorActiveType,
}) => {
  const [activePage, setActivePage] = useState(0);
  const pagerRef = useRef();
  let walkInterval = '';
  useEffect(() => {
    walkTimer();
    return () => {
      clearInterval(walkInterval);
    };
  }, []);
  const prevCount = GetPreviousState(activePage);
  const walkTimer = () => {
    walkInterval = setInterval(() => {
      setActivePage(prevCount.current + 1);
      pagerRef.current.setPage(prevCount.current + 1);
      if (prevCount.current + 1 === component.length) {
        resetWalk();
      }
    }, 4000);
  };
  const resetWalk = () => {
    clearInterval(walkInterval);
    setActivePage(0);
    pagerRef.current.setPage(0);
    walkTimer();
  };
  const pauseWalk = () => {
    clearInterval(walkInterval);
  };
  return (
    <View
      style={styles.container}
      onTouchStart={() => {
        pauseWalk();
      }}
      onTouchCancel={() => {
        walkTimer();
      }}>
      <ViewPager
        ref={pagerRef}
        style={[styles.pagerContainer, pageContainerStyle]}
        initialPage={0}
        showPageIndicator={true}
        onPageSelected={(e) => setActivePage(e.nativeEvent.position)}>
        {renderComponent
          ? renderComponent(component)
          : component.map((item, i) => {
              return (
                <View key={`${i + 1}`} style={styles.componentPager}>
                  <Image
                    style={styles.imagePager}
                    resizeMode="contain"
                    source={item.image}
                  />
                  <View style={styles.textPagerContainer}>
                    <Text style={styles.textPagerTitle}>{item.title}</Text>
                    <Text style={styles.textPagerSubtitle}>
                      {item.subtitle}
                    </Text>
                  </View>
                </View>
              );
            })}
      </ViewPager>
      {withIndicator && (
        <View style={[styles.indicatorContainer, indicatorContainerStyle]}>
          {component.map((item, i) => (
            <View
              key={i}
              style={styles.indicator(
                activePage,
                i,
                indicatorColor,
                indicatorActiveColor,
                indicatorActiveType,
              )}
            />
          ))}
        </View>
      )}
    </View>
  );
};

export default SliderBox;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  pagerContainer: {
    marginTop: 10,
    width: '100%',
    height: '90%',
  },
  componentPager: {
    alignItems: 'center',
    paddingTop: 20,
  },
  imagePager: {
    width: '60%',
    height: '70%',
  },
  textPagerContainer: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  textPagerTitle: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[20],
    marginBottom: 10,
    textAlign: 'center',
  },
  textPagerSubtitle: {
    fontFamily: fonts.primary.normal,
    fontSize: fonts.size[15],
    textAlign: 'center',
  },
  indicatorContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 22,
  },
  indicator: (
    activePage,
    index,
    indicatorColor,
    indicatorActiveColor,
    indicatorActiveType,
  ) => ({
    width: activePage === index && indicatorActiveType === 'ruler' ? 30 : 9,
    height: 9,
    marginHorizontal: 7,
    backgroundColor:
      activePage === index ? indicatorActiveColor : indicatorColor,
    borderRadius: 30,
  }),
});
