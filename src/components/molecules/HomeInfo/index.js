import React from 'react';
import {
  Image,
  ImageBackground,
  Linking,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {
  HomeScreenImgBg,
  IkiAkuIcon5,
  LineIcon,
  NoProfilePictIcon,
  NotifIcon,
} from '../../../assets';
import {
  colors,
  dateToString,
  fonts,
  greetingTexts,
  hp,
  lineURL,
  wp,
} from '../../../utilities';
import {Gap, SkeletonLoading, SvgWrapper, Touchable} from '../../atoms';

const HomeInfo = ({
  navigation,
  userDetails,
  notificationList,
  loadingProfile,
}) => {
  return (
    <ImageBackground
      style={styles.bgImageContainerStyle}
      imageStyle={styles.bgImageStyle}
      source={HomeScreenImgBg}>
      <View style={styles.logoContainer}>
        <SvgWrapper component={<IkiAkuIcon5 />} style={styles.iconStyle} />
        <Touchable onPress={() => navigation.navigate('EditProfileScreen')}>
          {loadingProfile === false ? (
            !userDetails?.arcPhoto?.photo_profile ? (
              <SvgWrapper
                style={styles.profilePicture}
                component={<NoProfilePictIcon />}
              />
            ) : (
              <Image
                source={{uri: userDetails?.arcPhoto?.photo_profile}}
                style={styles.profilePicture}
              />
            )
          ) : (
            <SkeletonLoading type="circle" width={64} />
          )}
        </Touchable>
      </View>
      <Gap height={hp(2)} />
      <View style={styles.infoContainer}>
        <View style={styles.greetingContainer}>
          {loadingProfile ? (
            <SkeletonLoading
              type="rectangle"
              width={wp(45)}
              height={hp(1.5)}
              style={styles.skeletonName}
            />
          ) : (
            <Text numberOfLines={1} style={styles.greetingText}>
              {greetingTexts()}
              {userDetails?.fullName}
            </Text>
          )}
          <Gap height={hp(0.5)} />
          <Text style={styles.greetingText2}>
            {dateToString(new Date(), 3)}
          </Text>
        </View>
        <View style={styles.iconContainer}>
          <Touchable onPress={() => Linking.openURL(lineURL)}>
            <SvgWrapper component={<LineIcon />} width={hp(4)} height={hp(4)} />
          </Touchable>
          <Gap width={hp(1)} />
          <Touchable onPress={() => navigation.navigate('NotificationScreen')}>
            <SvgWrapper
              component={<NotifIcon />}
              width={hp(4)}
              height={hp(4)}
            />
            {notificationList.length > 0 && (
              <View style={styles.notifCounter}>
                <Text style={styles.notifCounterText}>
                  {notificationList.length}
                </Text>
              </View>
            )}
          </Touchable>
        </View>
      </View>
    </ImageBackground>
  );
};

export default HomeInfo;

const styles = StyleSheet.create({
  bgImageStyle: {
    resizeMode: 'stretch',
  },
  bgImageContainerStyle: {
    minHeight: hp(10),
    maxHeight: hp(25),
    resizeMode: 'contain',
    padding: hp(2),
    justifyContent: 'center',
  },
  logoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: hp(1),
  },
  infoContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
    marginBottom: hp(2),
  },
  greetingContainer: {
    flex: 1,
    paddingRight: hp(1),
    marginTop: hp(1),
  },
  greetingText: {
    fontFamily: fonts.primary.bold,
    color: colors.black,
    fontSize: fonts.size[13],
  },
  greetingText2: {
    fontFamily: fonts.primary[400],
    color: colors.black,
    fontSize: fonts.size[13],
  },
  iconContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  notifCounter: {
    alignSelf: 'flex-end',
    position: 'absolute',
    marginTop: hp(0.3),
    backgroundColor: colors.red1,
    width: wp(4.2),
    height: hp(2.1),
    borderRadius: (wp(4.2) + hp(2.1)) / 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  notifCounterText: {
    textAlign: 'center',
    color: colors.white,
    fontFamily: fonts.primary[600],
    fontSize: fonts.size[12],
  },
  profilePicture: {
    height: hp(9),
    width: hp(9),
    marginTop: hp(4),
    borderRadius: 100,
  },
  skeletonName: {
    flex: 1,
    width: wp(45),
    height: hp(1.5),
  },
  iconStyle: {
    width: hp(13),
    height: hp(13),
    paddingTop: hp(3),
  },
});
