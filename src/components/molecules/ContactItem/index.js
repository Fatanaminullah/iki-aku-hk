import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {SendIcon} from '../../../assets';
import {colors, fonts} from '../../../utilities';
import {HorizontalLine, InitialAvatar} from '../../atoms';

const ContactItem = ({
  item,
  onPress,
  text,
  phoneNumber,
  additionalIcon,
  iconColor,
}) => {
  return (
    <TouchableOpacity onPress={onPress} key={`contact-${text}`}>
      <View style={styles.itemListContainer}>
        <View style={styles.itemText}>
          <InitialAvatar
            page={'contact'}
            iconColor={iconColor}
            rounded
            text={text}
            size={45}
            fontSize={fonts.size[18]}
          />
          <View style={styles.itemListTextContainer}>
            <Text style={styles.itemListLabel}>{text}</Text>
            {phoneNumber ? (
              <Text style={styles.itemListContent}>{phoneNumber}</Text>
            ) : null}
          </View>
        </View>
        <View style={styles.itemIcon}>{additionalIcon && additionalIcon}</View>
      </View>
      <HorizontalLine height={1} width="100%" />
    </TouchableOpacity>
  );
};

export default ContactItem;

const styles = StyleSheet.create({
  itemListContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 10,
  },
  itemListTextContainer: {
    flexDirection: 'column',
    paddingLeft: 10,
    width: '100%',
  },
  itemText: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  itemIcon: {
    paddingHorizontal: 10,
  },
  itemListLabel: {
    fontFamily: fonts.primary.bold,
    fontSize: fonts.size[15],
    color: colors.black,
    paddingBottom: 5,
  },
  itemListContent: {
    fontSize: fonts.size[12],
    fontFamily: fonts.primary.normal,
    color: colors.black,
    paddingBottom: 5,
  },
});
