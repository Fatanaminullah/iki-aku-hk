import React from 'react';
import {Animated, StyleSheet, Text} from 'react-native';
import {colors, fonts, getInitials} from '../../../utilities';

const InitialAvatar = ({
  size,
  iconColor = colors.blue2,
  rounded,
  fontFamily = fonts.primary.bold,
  fontSize = fonts.size[12],
  color = colors.white,
  text,
}) => {
  return (
    <Animated.View style={styles.container(size, iconColor, rounded)}>
      <Text style={styles.text(fontFamily, fontSize, color)}>
        {text ? getInitials(text, ' ') : 'IKI'}
      </Text>
    </Animated.View>
  );
};

const styles = StyleSheet.create({
  container: (size, iconColor, rounded) => ({
    width: size || 55,
    height: size || 55,
    borderRadius: rounded ? 100 : 0,
    backgroundColor: iconColor,
    alignItems: 'center',
    justifyContent: 'center',
  }),
  text: (fontFamily, fontSize, color) => ({
    fontSize: fontSize,
    fontFamily: fontFamily,
    color: color,
    textShadowColor: 'rgba(0, 0, 0, 0.40)',
    textShadowOffset: {width: -1, height: 1},
    textShadowRadius: 5,
  }),
});

export default InitialAvatar;
