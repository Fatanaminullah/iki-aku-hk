import React from 'react';
import {StyleSheet, View} from 'react-native';
import {colors} from '../../../utilities';

const HorizontalLine = ({
  width = '100%',
  height = 1,
  color = colors.grey2,
  type,
  style,
}) => {
  return <View style={[styles.line(height, width, color, type), style]} />;
};

export default HorizontalLine;

const styles = StyleSheet.create({
  line: (height, width, color, type) => ({
    width: width,
    borderWidth: height,
    borderColor: color,
    borderStyle: type,
    borderRadius: 1,
  }),
});
