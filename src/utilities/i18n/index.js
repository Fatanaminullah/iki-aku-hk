import I18n from 'react-native-redux-i18n';
import translations from './translations';
import Moment from 'moment';
require('moment/min/locales.min');

I18n.fallbacks = true;
I18n.translations = translations;
Moment.locale(I18n.locale);

export default I18n;
